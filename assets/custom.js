$(document).ready(function(){
    setTimeout(function(){
        if(thumb_images.length > 0) {
            var counter = 1;
            var interval = 2000;
            jQuery.each(thumb_images, function (i, val) {
                setTimeout(function() {
                    var imgdiv = '<div class="thumb-box"><span>' + counter + '</span><img src="' + val + '" class="materialboxed img-thumbnail"></div>';
                    $(".video_slide_thumb").append(imgdiv);
                    counter++;
                },interval);
                interval = interval + 2000;
            });
        }
    }, 1000);
});