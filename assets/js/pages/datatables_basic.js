/* ------------------------------------------------------------------------------
*
*  # Basic datatables
*
*  Specific JS code additions for datatable_basic.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{ 
            orderable: false,
            width: '100px',
            targets: [ 5 ]
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    /* Admin Display Question Table */
    //var question_hidden_date = $("#question_hidden_date").val();
    var question_table = $("#question_table").DataTable({
 	   "ajax": {
 	            "url": "get_question_data.php"
 	        },
 	        "columns": [
 	            { "data": 0,className: "hidden" },
 	            { "data": 1 },
 	            { "data": 2 },
 	            { "data": 3 },
 	            { "data": 4 },
 	            { "data": 5 ,className: "hidden" }
 	        ],
 	        "ordering": false,
			"lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
 	    } );
	 setInterval( function () {
		 question_table.ajax.reload();
	 }, 10000);
	 
	 /* Admin Side Display users in dashboard */
	 var dashboard_user_datatable = $("#dashboard_user_datatable").DataTable({
 	   "ajax": {
 	            "url": "process.php",
				"method":"POST",
				"data":{user_data:true}
 	        },
 	        "columns": [
				{ "data": 0, className: "hidden" },
 	            { "data": 1 },
 	            { "data": 2 },
 	            { "data": 3 }
 	        ],
			"columnDefs": [{ 
				"targets": [ 3 ]
			}],
 	        "ordering": false,
			"bLengthChange" : false,
			"lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
 	    } );
		
		
		
	 
	 /* Admin Display Pending Question Table */
    //var pending_question_hidden_date = $("#pending_question_hidden_date").val();
    var pending_question_table = $("#pending_question_table").DataTable({
        "ajax": {
            "url": "get_question_data.php?pending_question"
        },
        "columns": [
 	            { "data": 0, className: "hidden" },
 	            { "data": 1 },
 	            { "data": 2 },
 	            { "data": 3 },
 	            { "data": 4 },
 	            { "data": 5, className: "hidden"},
 	            { "data": 6, className: "hidden" }
 	        ],
        "ordering": false,
		"lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
    });
    setInterval(function () {
    	pending_question_table.ajax.reload();
    }, 10000);
	 
	 /* Admin Display User Table */
     var user_hidden_date = $("#user_hidden_date").val();
	 var user_table = $("#user_table").DataTable({
	   "processing": true,
	   "ajax": {
	            "url": "process.php?action=get_user",
 	            "type": "POST"
	        },
	        "columns": [
	            { "data": 0,className: "hidden" },
	            { "data": 1 },
	            { "data": 2 },
	            { "data": 3 },
	            { "data": 4 },
	            { "data": 5 ,className: "hidden"}
	        ],
	        "ordering": false,
			"lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
	    } );
	 /*setInterval( function () {
		 user_table.ajax.reload();
	 }, 5000);*/
		 
    // Basic datatable
    $('.datatable-basic').DataTable();


    // Alternative pagination
    $('.datatable-pagination').DataTable({
        pagingType: "simple",
        language: {
            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
        }
    });


    // Datatable with saving state
    $('.datatable-save-state').DataTable({
        stateSave: true
    });


    // Scrollable datatable
    $('.datatable-scroll-y').DataTable({
        autoWidth: true,
        scrollY: 300
    });



    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });
    
});
