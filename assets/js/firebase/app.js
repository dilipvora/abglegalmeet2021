// Initialize Firebase

var config = {
	apiKey: api_key,
	authDomain: firebase_url,
	databaseURL: firebase_url,
};

firebase.initializeApp(config);
// Get a reference to the database service
var firebase_redik_webinar = firebase.database();
var page_load_true = true;
var interval = 1000;
if(isWebRTCVideo  == undefined) {
    var isWebRTCVideo  = false;
}

if(firbase_ajax_setting == 1) {
    //console.log("firebase");
    //Get Current Slide

    var firebase_current_slide = firebase.database().ref(event_id+'/current_slide');
    firebase_current_slide.on('value', function (snapshot) {
        setTimeout(function () {
            $('#video_slide_img').attr('src', snapshot.val());
        }, isWebRTCVideo ? 1 : interval);
    });

    // Get Announcement

    var current_announcement = firebase.database().ref(event_id+'/current_announcement');
    current_announcement.on('value', function (snapshot) {
        $('#lbldata').html(snapshot.val());
    });


    // PPT Delay Interval

    var ppt_delay_interval = firebase.database().ref(event_id+'/ppt_delay');
    ppt_delay_interval.on('value', function (snapshot) {
		if(parseInt(snapshot.val()) > 0) {
			interval = parseInt(snapshot.val());
			//console.log("interval: "+interval);
		}
    });
	
	// Feedback on off
	var feedback_on_off = firebase.database().ref(event_id+'/feedback_on_off');
    feedback_on_off.on('value', function (snapshot) {
		if(snapshot.val() == "on") {
			$('.GiveFeedback').show();
            $("#FeedbackContent").html("<div class='center'>Loading...</div>");
            $("#FeedbackContent").load("feedback_modal.php");
            $("#FeedbackModel").modal('show');

            $("#FeedbackResultContent").html("");
            $("#FeedbackResultModel").modal("hide");
		}else{
			$('.GiveFeedback').hide();
            $("#FeedbackContent").html("");
			$("#FeedbackModel").modal('hide');
		}
    });
	
	// Feedback result on off
	var feedback_result_on_off = firebase.database().ref(event_id+'/feedback_result_on_off');
    feedback_result_on_off.on('value', function (snapshot) {
        if (snapshot.val() == "on") {
            $('.FeedbackResults').show();
            $("#FeedbackResultContent").html("<div class='center'>Loading...</div>");
            $("#FeedbackResultContent").load("live_feedback_result.php");
            $("#FeedbackResultModel").modal('show');

            $("#FeedbackContent").html("");
            $("#FeedbackModel").modal('hide');
        } else {
            $('.FeedbackResults').hide();
            $("#FeedbackResultContent").html("");
            $("#FeedbackResultModel").modal("hide");
        }
    });

    var firebase_cursor_images = firebase.database().ref(event_id+'/cursor_images');
    firebase_cursor_images.on('value', function (snapshot) {
        var data = snapshot.val();
        $("#dot").attr('src',data);

    });


    // live chat
    let lastCid = "all";
    var chatData = firebase.database().ref(event_id + '/one_to_one_chatData');
    $(document).on("shown.bs.modal", "#chatBoxModal", function (e) {
        e.preventDefault();
        chatData.on('value', function (snapshot) {
            var chatRow = snapshot.val();
            var chatStr = JSON.parse(chatRow);

            var incoming_msg_id = $("#chat_frm #incoming_msg_id").val();
            var outgoing_msg_id = $("#chat_frm #outgoing_msg_id").val();
            //console.log(outgoing_msg_id + " " + incoming_msg_id, chatStr);

            if (((outgoing_msg_id === chatStr.incoming_msg_id && incoming_msg_id === chatStr.outgoing_msg_id) || (outgoing_msg_id === chatStr.outgoing_msg_id && incoming_msg_id === chatStr.incoming_msg_id)) && (page_load_true == false)) {
                //console.log("data fetch", "incoming -> "+incoming_msg_id + " outgoing -> " + outgoing_msg_id);
                /* ==== Chat Box Ajax Code ==== */
                //var cid = $("#chatBoxModal").find('input[name="lastCid"]').val();
                if (typeof chatStr.lastCid === "undefined") {
                    lastCid = "all";
                } else {
                    lastCid = chatStr.lastCid;
                }
                message_ajax_call(outgoing_msg_id, incoming_msg_id, lastCid);
                //console.log("firebase if console");
            } else {
                page_load_true = false;
                //console.log("firebase else console");
            }
        });
    });

   /* var firebase_cursor_position = firebase.database().ref(event_id+'/cursor_position');
    firebase_cursor_position.on('value', function (snapshot) {
        var data = snapshot.val();

        //setTimeout(function () {
            $("#dot").css({"left": data.x, "top": data.y});
        //}, isWebRTCVideo ? 1 : interval);
    });*/
}