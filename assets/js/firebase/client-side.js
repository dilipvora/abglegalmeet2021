var firebase_redik_webinar = new Firebase(firebase_url);
var firebase_cursor_position = firebase_redik_webinar.child('cursor_position');
var firebase_cursor_images = firebase_redik_webinar.child('cursor_images');
var firebase_current_slide = firebase_redik_webinar.child('current_slide');
var firebase_current_announcement = firebase_redik_webinar.child('current_announcement');
var firebase_feedback_on_off = firebase_redik_webinar.child('feedback_on_off');
var firebase_ppt_delay = firebase_redik_webinar.child('ppt_delay');
var firebase_feedback_result_on_off = firebase_redik_webinar.child('feedback_result_on_off');
//firebase_cursor_position.remove();

var interval = 35000;
if(isWebRTCVideo  == undefined) {
	var isWebRTCVideo  = false;
}

firebase_feedback_result_on_off.on("child_added", function(snapshot, prevChildKey) {
    firebase_feedback_result_on_off.once("value", function(snapshot) {
        var data = snapshot.val();
        if(data.text == "on") {
            $('#FeedbackResults').show();
        } else {
            $('#FeedbackResults').hide();
		}
    });
});

firebase_feedback_result_on_off.on('child_changed', function(childSnapshot) {
    firebase_feedback_result_on_off.once("value", function(snapshot) {
        var data = snapshot.val();
        if(data.text == "on") {
            $('#FeedbackResults').show();
        } else {
            $('#FeedbackResults').hide();
        }
    });
});

firebase_ppt_delay.on("child_added", function(snapshot, prevChildKey) {
    firebase_ppt_delay.once("value", function(snapshot) {
        var data = snapshot.val();
        interval = parseInt(data.text);
    });
});

firebase_ppt_delay.on('child_changed', function(childSnapshot) {
    firebase_ppt_delay.once("value", function(snapshot) {
        var data = snapshot.val();
        interval = parseInt(data.text);
    });
});

firebase_feedback_on_off.on("child_added", function(snapshot, prevChildKey) {
    firebase_feedback_on_off.once("value", function(snapshot) {
        var data = snapshot.val();
        if(data.text == "on") {
            $('#GiveFeedback').show();
        } else {
            $('#GiveFeedback').hide();
		}
    });
});

firebase_feedback_on_off.on('child_changed', function(childSnapshot) {
    firebase_feedback_on_off.once("value", function(snapshot) {
        var data = snapshot.val();
        if(data.text == "on") {
            $('#GiveFeedback').show();
        } else {
            $('#GiveFeedback').hide();
        }
    });
});

/**
 * Current Announcement Text 
 */

firebase_current_announcement.on("child_added", function(snapshot, prevChildKey) { 
	firebase_current_announcement.once("value", function(snapshot) {
	    var data = snapshot.val();
	    $('#lbldata').html(data.text);
	});
});

firebase_current_announcement.on('child_changed', function(childSnapshot) {
	firebase_current_announcement.once("value", function(snapshot) {
	    var data = snapshot.val();
	    $('#lbldata').html(data.text);
	  });
});


/**
 * Current Slide Image 
 */

firebase_current_slide.on("child_added", function(snapshot, prevChildKey) { 
	firebase_current_slide.once("value", function(snapshot) {
		setTimeout(function(){
			var data = snapshot.val();
		    $('#video_slide_img').attr( 'src' ,data.src);
		}, isWebRTCVideo ? 1 : interval);
	});
});

firebase_current_slide.on('child_changed', function(childSnapshot) {
	firebase_current_slide.once("value", function(snapshot) {
		setTimeout(function(){
			var data = snapshot.val();
		    $('#video_slide_img').attr( 'src' ,data.src);
		}, isWebRTCVideo ? 1 : interval);
	  });
});

/**
 * Cursor Image 
 */

firebase_cursor_images.on("child_added", function(snapshot, prevChildKey) { 
	firebase_cursor_images.once("value", function(snapshot) {
		setTimeout(function(){
			var data = snapshot.val();
		    $("#dot").attr('src',data.src);
		}, isWebRTCVideo ? 1 : interval);
	});
});

firebase_cursor_images.on('child_changed', function(childSnapshot) {
	firebase_cursor_images.once("value", function(snapshot) {
		setTimeout(function(){
			var data = snapshot.val();
		    $("#dot").attr('src',data.src);
		}, isWebRTCVideo ? 1 : interval);
	});
});

/**
 * Cursor Image Move 
 */
firebase_cursor_position.on("child_added", function(snapshot, prevChildKey) { 
	firebase_cursor_position.once("value", function(snapshot) {
		setTimeout(function(){
			var data = snapshot.val();
		    $("#dot").css({"left": data.x, "top": data.y});
		}, isWebRTCVideo ? 1 : interval);
	  });
});

firebase_cursor_position.on('child_changed', function(childSnapshot) {
	firebase_cursor_position.once("value", function(snapshot) {
		setTimeout(function(){
			var data = snapshot.val();
		    $("#dot").css({"left": data.x, "top": data.y});
		}, isWebRTCVideo ? 1 : interval);
	  });
});
