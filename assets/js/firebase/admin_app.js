// Initialize Firebase

var config = {
	apiKey: api_key,
	authDomain: firebase_url,
	databaseURL: firebase_url,
};

firebase.initializeApp(config);


// Get a reference to the database service
var firebase_redik_webinar = firebase.database();

if(firbase_ajax_setting == 1) {
	//Get Current Slide

    var firebase_current_slide = firebase.database().ref(event_id+'/current_slide');
    firebase_current_slide.on('value', function (snapshot) {
        $('#slideshow_image').attr('src', snapshot.val());
		
		var start = snapshot.val().search("Slide");
		var end = snapshot.val().search(".JPG");
		var SlideNo = snapshot.val().substring(start+5, end);
		
		SlideNo = parseInt(SlideNo);
		$('#slide_img_name span').html(SlideNo);
		$('#img_no').val(SlideNo-1);
		$(".video_slide_pagination .pagination li[data-no='"+SlideNo+"']").addClass('active').siblings().removeClass('active');
    });
	
    // PPT Delay Interval
    var ppt_delay_interval = firebase.database().ref(event_id+'/ppt_delay');
    ppt_delay_interval.on('value', function (snapshot) {
		if(parseInt(snapshot.val()) > 0) {
			$("#PPTDelayValue").val(parseInt(snapshot.val()));
		}
    });
	
	// Feedback on off
	var feedback_on_off = firebase.database().ref(event_id+'/feedback_on_off');
    feedback_on_off.on('value', function (snapshot) {
		if(snapshot.val() == "on") {
			$('#btnFeedbackOn').hide();
			$('#btnFeedbackOff').show();
		}else if(snapshot.val() == "off"){
			$('#btnFeedbackOff').hide();
			$('#btnFeedbackOn').show();
		}
    });
	
	// Feedback result on off
	var feedback_result_on_off = firebase.database().ref(event_id+'/feedback_result_on_off');
    feedback_result_on_off.on('value', function (snapshot) {
		if(snapshot.val() == "on") {
			$('#btnFeedbackResultOn').hide();
			$('#btnFeedbackResultOff').show();
		}else if(snapshot.val() == "off"){
			$('#btnFeedbackResultOff').hide();
            $('#btnFeedbackResultOn').show();
		}
    });

    $(document).on("click",".select_cursor_image" ,function() {

        var src = $(this).attr('src');
        previous_cursor_img = src;
        $(this).parent().addClass('active').siblings().removeClass('active');
        var dataString = "action=cursor_images&cursor_images=" + src;
        $.ajax({
            type: "POST",
            async: true,
            url: "../ajax.php",
            data: dataString,
            cache: false,
            success: function(result){

            },
            error: function( result ) {

            }
        });
        $('#cursor_image').attr('src',src);
    });


    /*var cursor_images = firebase.database().ref(event_id+'/cursor_images');
    cursor_images.on('value', function (snapshot) {
        $('#cursor_image').attr('src', snapshot.val());
    });*/

}