var firebase_redik_webinar = new Firebase(firebase_url);
var firebase_cursor_position = firebase_redik_webinar.child('cursor_position');
var firebase_cursor_images = firebase_redik_webinar.child('cursor_images');
var previous_cursor_img = '';
var cursor_hiden_img = '';
var firebase_feedback_on_off = firebase_redik_webinar.child('feedback_on_off');
var firebase_ppt_delay = firebase_redik_webinar.child('ppt_delay');
var firebase_feedback_result_on_off = firebase_redik_webinar.child('feedback_result_on_off');
var firebase_live_webcast_started = firebase_redik_webinar.child('live_webcast_started');
var firebase_current_slide = firebase_redik_webinar.child('current_slide');
//firebase_cursor_position.remove();

$(document).ready(function(){
	//
	firebase_live_webcast_started.on('child_changed', function(childSnapshot) {
		firebase_live_webcast_started.once("value", function(snapshot) {
			var data = snapshot.val();
			setTimeout(function(){
				reload_iframe("video"+data.video+"_iframe");
			}, 5000);
		});
	});
	//
	
	/**
	 * Current Slide Image 
	 */

	/*firebase_current_slide.on("child_added", function(snapshot, prevChildKey) { 
		firebase_current_slide.once("value", function(snapshot) {
			var data = snapshot.val();
			$('#slideshow_image').attr( 'src', data.src);
		});
	});

	firebase_current_slide.on('child_changed', function(childSnapshot) {
		firebase_current_slide.once("value", function(snapshot) {
			var data = snapshot.val();
			$('#slideshow_image').attr( 'src', data.src);
		 });
	});*/
	
    firebase_feedback_result_on_off.on("child_added", function(snapshot, prevChildKey) {
        firebase_feedback_result_on_off.once("value", function(snapshot) {
            var data = snapshot.val();
            if(data.text == "on") {
                $('#btnFeedbackResultOn').hide();
                $('#btnFeedbackResultOff').show();
            } else {
                $('#btnFeedbackResultOff').hide();
                $('#btnFeedbackResultOn').show();
            }
        });
    });

    firebase_feedback_result_on_off.on('child_changed', function(childSnapshot) {
        firebase_feedback_result_on_off.once("value", function(snapshot) {
            var data = snapshot.val();
            if(data.text == "on") {
                $('#btnFeedbackResultOff').hide();
                $('#btnFeedbackResultOn').show();
            } else {
                $('#btnFeedbackResultOn').hide();
                $('#btnFeedbackResultOff').show();
            }
        });
    });
    
    firebase_ppt_delay.on("child_added", function(snapshot, prevChildKey) {
        firebase_ppt_delay.once("value", function(snapshot) {
            var data = snapshot.val();
            $("#PPTDelayValue").val(data.text);
        });
    });

    firebase_ppt_delay.on('child_changed', function(childSnapshot) {
        firebase_ppt_delay.once("value", function(snapshot) {
            var data = snapshot.val();
            $("#PPTDelayValue").val(data.text);
        });
    });

    firebase_feedback_on_off.on("child_added", function(snapshot, prevChildKey) {
        firebase_feedback_on_off.once("value", function(snapshot) {
            var data = snapshot.val();
            if(data.text == "on") {
                $('#btnFeedbackOn').hide();
                $('#btnFeedbackOff').show();
            } else {
                $('#btnFeedbackOff').hide();
                $('#btnFeedbackOn').show();
            }
        });
    });

    firebase_feedback_on_off.on('child_changed', function(childSnapshot) {
        firebase_feedback_on_off.once("value", function(snapshot) {
            var data = snapshot.val();
            if(data.text == "on") {
                $('#btnFeedbackOff').hide();
                $('#btnFeedbackOn').show();
            } else {
                $('#btnFeedbackOn').hide();
                $('#btnFeedbackOff').show();
            }
        });
    });
	
	/**
	 * Select Cursor Image 
	 */
	
	$( ".cursor_image_section  .select_cursor_image" ).on( "click", function() {
		var src = $(this).attr('src');
		firebase_cursor_images.set({ src: src});
		previous_cursor_img = src;
		$(this).parent('span').addClass('active').siblings().removeClass('active');
		//$(this).addClass('active').siblings().removeClass('active');
	});
	
	firebase_cursor_images.on("child_added", function(snapshot, prevChildKey) { 
		firebase_cursor_images.once("value", function(snapshot) {
		    var data = snapshot.val();
		    $("#cursor_image").attr('src',data.src);
		});
	});

	firebase_cursor_images.on('child_changed', function(childSnapshot) {
		firebase_cursor_images.once("value", function(snapshot) {
		    var data = snapshot.val();
		    $("#cursor_image").attr('src',data.src);
		});
	});
	
	/**
	 * Cursor Image Move 
	 */
    $("#slideshow_image").mousemove(function(event){
    	
    		var parentOffset = $(this).parent().offset(); 
        	var relX = event.pageX - parentOffset.left;
        	var relY = event.pageY - parentOffset.top;
        	$("#cursor_image").css({"left": relX, "top": relY});
        	
        	/* New Code */
        	
        	var img_width = $("#slide_cont").width();
    		var img_height = $("#slide_cont").height();
    		
    		var position_x = (relX / img_width) * 100;
    		var position_y = (relY / img_height) * 100;
    		//console.log('X: '+relX+ ' Y: '+relY+' position_x: ' + position_x + ' position_y: '+position_y+' Width: '+img_width+' height: '+img_height);
    		firebase_cursor_position.set({ x: position_x+'%', y: position_y+'%' });
        	/* End New Code */
        	//firebase_cursor_position.set({ x: relX, y: relY });
    });
    
    $( "#slide_cont" ).mouseenter(function() {
		if(previous_cursor_img!=''){
    		firebase_cursor_images.set({ src: previous_cursor_img});	
    	}
    });
    
    $( "#slide_cont" ).mouseleave(function() {
    	firebase_cursor_images.set({ src: cursor_hiden_img});
    });
    
    
    
    /*firebase_cursor_position.on("child_added", function(snapshot, prevChildKey) { 
    	firebase_cursor_position.once("value", function(snapshot) {
    	    var data = snapshot.val();
    	    $("#cursor_image").css({"left": data.x, "top": data.y});
    	  });
    });

    firebase_cursor_position.on('child_changed', function(childSnapshot) {
    	firebase_cursor_position.once("value", function(snapshot) {
    	    var data = snapshot.val();
    	    $("#cursor_image").css({"left": data.x, "top": data.y});
    	  });
    });*/
});