<?php include 'config.php';
include_once 'src/firebaseLib.php';

/**
 *  Submit Feedback
 */
if(isset($_POST['feedback_id']) && $_POST['feedback_id'] > 0){
    $user_feedback_data = array();
    $user_feedback_form_error = false;
    //echo "<pre>"; print_r($_POST); exit;
    foreach($_POST['question_ids'] as $key=>$question_id) {
        if(isset($_POST['answer'][$key])) {
            $user_feedback_data[] = array(
                'user_id' => $_SESSION['uid'],
                'feedback_id' => $_POST['feedback_id'],
                'question_id' => $question_id,
                'answer' => $_POST['answer'][$key],
            );
        } else {
            $user_feedback_form_error = true;
        }
    }

    if ($user_feedback_form_error == false) {
        $user_id = $_SESSION['uid'];
        $feedback_id = $_POST['feedback_id'];
        foreach($user_feedback_data as $key=>$user_feedback_row) {
            $question_id = trim($user_feedback_row['question_id']);
            $answer = addslashes(trim($user_feedback_row['answer']));
            $res = insert_details("users_feedbacks", "event_id = '".EVENT_ID."', uid = '$user_id' ,feedback_id = '$feedback_id', question_id = '{$question_id}', answer = '{$answer}'");
        }
        echo "success";
    } else {
        echo "Please give answer for all questions.";
    }
}

/**
 *  Submit Quiz
 */
if(isset($_POST['quiz_id']) && $_POST['quiz_id'] > 0) {
    $user_feedback_data = array();
    $user_feedback_form_error = false;
    $already_submit_quiz = true;
    $quiz_id = $_POST['quiz_id'];

    if (user_is_access_quiz_page()) {
        $already_submit_quiz = false;
    }

    if ($already_submit_quiz) {
        foreach ($_POST['question_ids'] as $key => $question_id) {
            if (isset($_POST['answer'][$key])) {
                $true_answer = get_values("quiz_questions", "correct_option", "event_id = '" . EVENT_ID . "' AND quiz_id='{$quiz_id}' AND id='{$question_id}'");
                if ($true_answer == $_POST['answer'][$key]) {
                    $answer_status = 1;
                } else {
                    $answer_status = 0;
                }
                $user_feedback_data[] = array(
                    'user_id' => $_SESSION['uid'],
                    'quiz_id' => $_POST['quiz_id'],
                    'question_id' => $question_id,
                    'answer' => addslashes(trim($_POST['answer'][$key])),
                    'answer_status' => $answer_status,
                );
            } else {
                $user_feedback_form_error = true;
            }
        }

        if ($user_feedback_form_error == false) {
            $res = array();
            $user_id = $_SESSION['uid'];
            $quiz_id = $_POST['quiz_id'];
            foreach ($user_feedback_data as $key => $user_feedback_row) {
                $question_id = trim($user_feedback_row['question_id']);
                $answer = addslashes(trim($user_feedback_row['answer']));
                $answer_status = $user_feedback_row['answer_status'];
                //$res = insert_details("quiz_result", "event_id = '".EVENT_ID."', uid = '$user_id' ,quiz_id = '$quiz_id', question_id = '{$question_id}', answer = '{$answer}', answer_status='{$answer_status}'");
                $res[] = "'" . EVENT_ID . "', $user_id, $quiz_id, $question_id, '{$answer}', '{$answer_status}'";
            }
            $fields = "`event_id`, `uid`, `quiz_id`, `question_id`, `answer`, `answer_status`";
            insert_batch("quiz_result", $fields, $res);

            $date = date('Y-m-d H:i:s');
            $start_date = get_values("options", "option_value", "event_id = '" . EVENT_ID . "' AND option_name='quiz_start'");
            //$start_date = get_values("quiz_time","start_time","event_id = '".EVENT_ID."' AND uid='{$user_id}' AND quiz_id = '{$quiz_id}'");
            $date_a = new DateTime($start_date);
            $date_b = new DateTime($date);

            $interval = date_diff($date_a, $date_b);

            $total_time = $interval->format('%H:%I:%S');
            update_details("quiz_time", "end_time = '{$date}', total_time = '{$total_time}'", "uid = '{$user_id}' AND `event_id`='" . EVENT_ID . "' AND quiz_id = '{$quiz_id}'");
            echo "success";
        } else {
            echo "Please give answer for all questions.";
        }

    } else {
        echo "already_give";
    }
}

/**
 *  Submit Question
 */
if(isset($_POST['q_submit_query']) && $_POST['q_submit_query'] == 'true'){
    $message=array();
    //$uid = $_POST['user_id'];
    $uid = $_SESSION['uid'];

    if( isset($_POST['q_question']) && $_POST['q_question'] !=''){
        $question = $_POST['q_question'];
    }else{
        $message[] = 'Please enter question.';
    }

    if(isset($_POST['team']) && $_POST['team'] != ""){
        $team = $_POST['team'];
    }else{
        $team = null;
    }

    $countError=count($message);

    if($countError > 0){
        for($i=0;$i<$countError;$i++){
            echo ucwords($message[$i]).'<br/>';
        }
    }else{
        $submit_question = mysql_query("Insert into question SET
											`uid` = '".$uid."',
											`team_id` = '".$team."',
											`event_id` = '".EVENT_ID."',
											`q_question` = '".addslashes($question)."'");
        if($submit_question){
            echo 'success';
        }else{
            echo 'Your Question not submited try again.';
        }
    }
}

/**
 *  Set Current Slide
 */

if(isset($_POST['action']) && $_POST['action'] == 'set_current_slide' && $_POST['webinar_id'] !=''){
	$slide = $_POST['slide'];
	$slide_no = $_POST['slide_no'];
	
	if($slide !=''){
		$webinar_id = $_POST['webinar_id'];
		
		if($webinar_id !=''){
			mysql_query("UPDATE `webinar` SET
				`current_slide` = '{$slide}',
				`slide_no` = '{$slide_no}'
				WHERE `id`= '{$webinar_id}' AND `event_id` = '".EVENT_ID."'");
		}else{
			mysql_query("INSERT INTO `webinar` SET
				`event_id` = '".EVENT_ID."',
				`current_slide` = '{$slide}',
				'slide_no' = '{$slide_no}'");
		}
		
		//////
		mysql_query("INSERT INTO `ppt_time_all` SET
	 				`event_id` = '".EVENT_ID."',
					`src` = '{$slide}',
					`created_date` = now()");

		//////////
		if(isset($_SESSION['event_start_time'])) {
	 		mysql_query("INSERT INTO `ppt_time` SET
	 				`event_id` = '".EVENT_ID."',
					`src` = '{$slide}',
					`start` = ".(time()-$_SESSION['event_start_time']).",
					`created_date` = now()");
 		}

        $json_data = file_get_contents("current_slide.json");
        if(!empty($json_data)) {
            $json_array = json_decode($json_data);
            $json_array->slide = $slide;
            $json_array->slide_no = $slide_no;
            $json_array->time = date('Y-m-d H:i:s');
        }else{
            $json_array['slide'] = $slide;
            $json_array['slide_no'] = $slide_no;
            $json_array['time'] = date('Y-m-d H:i:s');
        }
        file_put_contents("current_slide.json",json_encode($json_array));
		//update in firebase
        $firebase = new \Firebase\FirebaseLib($firebase_url,API_KEY);
        $firebase->set(EVENT_ID.'/current_slide', SITE_URL.$slide);
		//end update in firebase
		echo 'success';
	}else{
		echo 'Current slide not set try again';
	}
}

/**
 *  Get Current Slide
 */

if(isset($_POST['action']) && $_POST['action'] == 'get_current_slide'){
	
	$res = mysql_query("SELECT current_slide FROM `webinar` WHERE `event_id` = '".EVENT_ID."'");
	echo mysql_fetch_object($res)->current_slide;
		
}

if(isset($_POST['action']) && $_POST['action'] == 'start_event'){
	if(!isset($_SESSION['event_start']) || $_SESSION['event_start'] != 1) {
		$_SESSION['event_start_time'] = time();
		$slide = $_POST['slide'];
		mysql_query("INSERT INTO `ppt_time` SET
				`event_id` = '".EVENT_ID."',
				`src` = '{$slide}',
				`start` = 0,
				`created_date` = now()");

		$_SESSION['event_start'] = '1';
		echo 'Stop Event';
	} else  if(isset($_SESSION['event_start'])) {
		unset($_SESSION['event_start']);
		unset($_SESSION['event_start_time']);
		echo 'Start Event';
	}
}

if(isset($_POST['action']) && $_POST['action'] == "cursor_images"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/cursor_images', $_POST['cursor_images']);
}

/**
 *  Submit Chat Message
 */
if(isset($_POST['chatForm']) && $_POST['chatForm'] == 'true') {
    $message = array();
    $_POST['incoming_msg_id'] = trim(mysql_real_escape_string($_POST['incoming_msg_id']));
    $uid = trim(mysql_real_escape_string($_SESSION['uid']));

    if (isset($_POST['msg']) && $_POST['msg'] != '') {
        $msg = trim(mysql_real_escape_string($_POST['msg']));
    } else {
        $message[] = 'Please enter question.';
    }

    $countError = count($message);

    if ($countError > 0) {
        for ($i = 0; $i < $countError; $i++) {
            echo ucwords($message[$i]) . '<br/>';
        }
    } else {
        $submit_question = mysql_query("INSERT INTO `one_to_one_chat` SET `event_id` = '".EVENT_ID."', `incoming_msg_id` = '{$_POST['incoming_msg_id']}', `outgoing_msg_id` = '{$uid}', `msg` = '{$msg}'") or die(mysql_error());
        $last_id = mysql_insert_id($db);
        if ($submit_question) {
            //$firebaseData = array("incoming_msg_id" => $_POST['incoming_msg_id'], "outgoing_msg_id" => $uid, "msg" => urldecode($msg));
            $firebaseData = array("incoming_msg_id" => $_POST['incoming_msg_id'], "outgoing_msg_id" => $uid, "lastCid" => $last_id);
            $firebase = new \Firebase\FirebaseLib($firebase_url, API_KEY);
            $firebase->set(EVENT_ID . '/one_to_one_chatData', json_encode($firebaseData));
            echo json_encode(array("cid" => $last_id));
        } else {
            //echo json_encode(array("status" => "fail", "msg" => ""));
            echo json_encode(array('status' => 'fail'));
        }
    }
}

if (isset($_POST['action']) && ($_POST['action'] == "get_ajax_message")) {
    $event_id = EVENT_ID;
    $uid = $_SESSION["uid"];
    $html = '';

    $incoming_msg_id = mysql_real_escape_string($_POST['incoming_msg_id']);

    $whereStr = "event_id = '{$event_id}' AND ((outgoing_msg_id = {$uid} AND incoming_msg_id = {$incoming_msg_id}) OR (outgoing_msg_id = {$incoming_msg_id} AND incoming_msg_id = {$uid}))";
    if (!empty($_POST['lastCid']) && $_POST['lastCid'] !== 'all') {
        $rs_query = "SELECT `msg`,`outgoing_msg_id`,`cid` FROM `one_to_one_chat` WHERE $whereStr AND `cid` = '{$_POST['lastCid']}'";
    } else {
        $rs_query = "SELECT `msg`,`outgoing_msg_id`,`cid` FROM `one_to_one_chat` WHERE $whereStr";
    }

    //echo "$rs_query<br/>";

    $rs = mysql_query($rs_query) or die(mysql_error());

    $last_id = null;

    if (mysql_num_rows($rs)) {
        $html = "";
        while ($row = mysql_fetch_object($rs)) {
            $last_id = $row->cid;
            //print_r($row);
            if ($row->outgoing_msg_id === $uid) {
                $html .= '<li>
                            <div class="chat outgoing last_id_'.$row->cid.'">
                                <div class="details">
                                    <label class="pre">'.$row->msg.'</label>
                                </div>
                            </div>
                        </li>';
            } else {
                $html .= '<li>
                            <div class="chat incoming last_id_'.$row->cid.'">
                                <div class="details">
                                    <label class="pre">'.$row->msg.'</label>
                                </div>
                            </div>
                        </li>';
            }
        }
    } else {
        $html .= "d-none";
    }
    echo json_encode(array("data" => $html,"last_id" => $last_id));
    //echo $html;
}