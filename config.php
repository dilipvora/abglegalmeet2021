<?php session_start();
date_default_timezone_set("asia/kolkata");

define("EVENT_ID", "abglegalmeet2021");
define("FOLDER_NAME", "abglegalmeet2021");
define("SHOW_HOME_PAGE_LOGO", 0);
define("SHOW_LOGIN", 1);
define("SHOW_REGISTER", 0);

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

error_reporting(0);

/*if (strpos($_SERVER['REQUEST_URI'], FOLDER_NAME) == false) {
    echo 'Invalid access, Check url!';
    exit();
}*/

define("COMPANY_NAME", "ABG Annual Legal Meet 2021");
define("POWERED_BY", "");
define("POWERED_BY_URL", "");

define("DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT'].'/'.FOLDER_NAME);
require DOCUMENT_ROOT."/mail_config.php";

require_once('mysql_functions.php');
require_once 'function.php';

if(($_SERVER['HTTP_HOST'] === "localhost" || $_SERVER["HTTP_HOST"] === "127.0.0.1") || ($_SERVER["HTTP_HOST"] === "192.168.1.4" || $_SERVER["HTTP_HOST"] === "192.168.1.8")) {
    define("SITE_URL", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/");
    $DBSERVER = "localhost";
    $DATABASENAME = "cipla_new";
    $USERNAME = "root";
    $PASSWORD = "";
} else {
    define("SITE_URL", "https://www.abglegalmeet2021.com/");
    $DBSERVER = "localhost";
    $DATABASENAME = "puneet_webcasts";
    $USERNAME = "redik_webcast";
    $PASSWORD = "{rEdeEk@webCasst$132#}";
}
$db = mysql_connect($DBSERVER, $USERNAME, $PASSWORD);
mysql_select_db($DATABASENAME, $db) or die("Not access database");

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$firebase_url = get_selected('setting', 'firebase_url');
$firebase_url2 = get_selected('setting', 'firebase_url2');

//$firebase_url = $firebase_url.EVENT_ID."/";
define("FIREBASE_URL", $firebase_url);
define("FIREBASE_URL2", $firebase_url2);
define('LOGOUT_TIME_INTERVAL',300000);
define('AJAX_CURRENT_SLIDE_INTERVAL',15000);
define('FIREBASE_AJAX_SETTING_INTERVAL',60000);
define('API_KEY','nHBtVAMOQ1kDvpAtg7LZKIFmiQVnKoaCHZgaqsi7');

define("PPT_CONVERT_URL", "http://51.89.155.19:88/ppt2jpg/index.php");
define("PPTPATH", "Redik-Client-Webcast/Puneet/".FOLDER_NAME."/upload/ppt/");

$page = basename($_SERVER['PHP_SELF']);