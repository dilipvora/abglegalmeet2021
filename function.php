<?php

/**
 * SuperAdmin, Admin, Presenter Access This Page
 */
function check_user_login(){
    $query = "SELECT uid FROM `new_users` WHERE `event_id` = '".EVENT_ID."' and is_login = 1";
    $result = mysql_query($query);
    $num_rows = mysql_num_rows($result);
    $time = date('Y-m-d H:i:s');
    if($num_rows){
        while($rows = mysql_fetch_array($result)){
            $uid = $rows['uid'];
            $qry = "SELECT logout_time,id FROM `users_login_detail` where uid='".$uid."' ORDER BY id DESC LIMIT 1";
            $res = mysql_query($qry);
            $count_rows = mysql_num_rows($res);
            if($count_rows>0){
                $result_data = mysql_fetch_array($res);
                $logout_minute = date('i',strtotime($result_data['logout_time']));
                $current_minute = date('i');
                $id = $result_data['id'];
                ///
                $timeFirst  = strtotime($result_data['logout_time']);
                $timeSecond = strtotime(date('Y-m-d H:i:s'));
                $differenceInSeconds = $timeSecond - $timeFirst;

                if($differenceInSeconds > 310) {
                    mysql_query("UPDATE new_users SET is_login = 0 WHERE uid = '{$uid}'");
                }
                ///
            }
        }
    }


    $login_count = mysql_query("SELECT count(uid) as total_login from `new_users` where `event_id` = '".EVENT_ID."' and is_login = 1");
    $count_result = mysql_fetch_array($login_count);
    $count_rows = mysql_num_rows($login_count);
    if($count_rows>0){
        $total_login_users = $count_result['total_login'];
    }else{
        $total_login_users = 0;
    }

    print $total_login_users;
}


/**
 * SuperAdmin, Admin, Presenter Access This Page
 */
function other_presentation_page_not_access_page(){
    if(isset($_SESSION['status']) && ($_SESSION['status'] == '0' || $_SESSION['status'] == '1' || $_SESSION['status'] == '2')){
        return true;
    }else{
        //echo '<meta http-equiv="refresh" content="0; URL=index.php">';
        header("location:index.php");
    }
}

/**
 * Presenter Not Access This Page
 */
function presenter_not_access_page(){
    if(isset($_SESSION['status']) && $_SESSION['status'] == '2'){
        //echo '<meta http-equiv="refresh" content="0; URL=presentation.php">';
        header("location:presentation.php");
    }else{
        return true;
    }
}

/**
 * Get Selected Data
 */
function get_selected($tablename, $field){
    $res = mysql_query("SELECT {$field} FROM {$tablename}");
    $num = mysql_num_rows($res);
    if($num > 0){
        $row=mysql_fetch_object($res);
        return $row->$field;
    }else{
        return false;
    }
}



function get_selected_with_where($tablename,$condition){
    $data_rs = mysql_query("SELECT * FROM `{$tablename}` WHERE {$condition}");
    if(mysql_num_rows($data_rs) > 0){
        $output=array();
        while($row=mysql_fetch_object($data_rs)){
            $output[]= $row;
        }
        return $output;
    }else{
        return false;
    }
}

function total_count($table, $key, $value){

    $total_count_rs = mysql_query("SELECT * FROM {$table} WHERE {$key} = {$value}");
    return mysql_num_rows($total_count_rs);
}

function time_ago($time)
{
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60","60","24","7","4.35","12","10");

    $now = time();

    $difference     = $now - $time;
    $tense         = "ago";

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if($difference != 1) {
        $periods[$j].= "s";
    }

    return "$difference $periods[$j] ago";
}

function get_current_time(){
    return date('Y-m-d H:i:s');
}

function get_values($table,$field,$condition){
    $qry="SELECT $field from $table where $condition";
    //echo $qry;
    $details_rs=mysql_query($qry);
    if(mysql_num_rows($details_rs)>0)
    {
        $row = mysql_fetch_object($details_rs);
        return $row->$field;
    }
    else
    {
        return false;
    }
}


function get_option($optionkey)
{
    $qry="SELECT option_value from options where option_name = '{$optionkey}'";
    $details_rs=mysql_query($qry);
    if(mysql_num_rows($details_rs)>0)
    {
        $row = mysql_fetch_object($details_rs);
        return $row->option_value;
    }
    else
    {
        return false;
    }
}

function check_poll_is_access(){
    $poll_time = '';
    $option_res = mysql_query("SELECT * FROM `options` WHERE `option_name` = 'poll_time'");
    if(mysql_num_rows($option_res)>0) {
        while ($option_row = mysql_fetch_object($option_res)) {
            $poll_time = $option_row->option_value;
            //$start_date = $option_row->start_date;

            $start_date = get_option('poll_start');


            if($poll_time !=''){
                $minutes_to_add = $poll_time;

                $time = new DateTime($start_date);
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $stamp = $time->format('Y-m-d H:i:s');
                //echo 'start_date'. $stamp;
                //echo '<br/> current date:'.date("Y-m-d H:i:s");

                /*if (!isset($_SESSION['token'])) {
                    $token = md5(uniqid(rand(), TRUE));
                    $_SESSION['token'] = $token;
                }*/
                //session_destroy();

                if ( strtotime($stamp) > time()){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }else{
        return false;
    }
}

function check_vote_is_access()
{
    $option_res = mysql_query("SELECT * FROM `options` WHERE `option_name` = 'vote_time'");
    if (mysql_num_rows($option_res) > 0) {
        while ($option_row = mysql_fetch_object($option_res)) {
            $vote_time = $option_row->option_value;
            $start_date = get_option('vote_start');

            if ($vote_time != '') {
                $minutes_to_add = $vote_time;

                $time = new DateTime($start_date);
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $stamp = $time->format('Y-m-d H:i:s');
                if (strtotime($stamp) > time()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    } else {
        return false;
    }
}

function check_feedback_is_access()
{
    $option_res = mysql_query("SELECT * FROM `feedbacks` LIMIT 1");
    if (mysql_num_rows($option_res) > 0) {
        return true;
    } else {
        return false;
    }
}

function check_quiz_is_access()
{
    $option_res = mysql_query("SELECT * FROM `quiz` LIMIT 1");
    if (mysql_num_rows($option_res) > 0) {
        return true;
    } else {
        return false;
    }
}


function user_is_access_feedback_page()
{
    $user_id = $_SESSION['uid'];
    $feedback_id = get_values("options", "option_value", "event_id = '".EVENT_ID."' AND option_name='feedback_id'");

    /*if($feedback_id === false) {
        $check_feedback_available = mysql_query("SELECT `id` FROM `feedbacks` where event_id = '".EVENT_ID."' LIMIT 1");
        if(mysql_num_rows($check_feedback_available) > 0) {
            $check_feedback_available_row = mysql_fetch_object($check_feedback_available);
            $created_date= date('Y-m-d H:i:s');
            insert_details('options', "option_name = 'feedback_id', option_value='$check_feedback_available_row->id', created_date='$created_date'");
            $feedback_id = $check_feedback_available_row->id;
        } else {
            return false;
        }
    }*/

    if($feedback_id > 0) {
        $res = mysql_query("SELECT `id` FROM `users_feedbacks` WHERE event_id = '".EVENT_ID."' AND `feedback_id` = '{$feedback_id}' AND `uid` = '{$user_id}'");
        if (mysql_num_rows($res) > 0) {
            return true;
        }
    }
    return false;
}

function user_is_access_quiz_page($qid = null)
{
    $user_id = $_SESSION['uid'];
    if (!empty($qid)) {
        $quiz_id = $qid;
    } else {
        $quiz_id = get_values("options", "option_value", "event_id = '".EVENT_ID."' AND option_name='quiz_id'");
    }

    if($quiz_id > 0) {
        $res = mysql_query("SELECT `id` FROM `quiz_result` WHERE event_id = '".EVENT_ID."' AND `quiz_id` = '{$quiz_id}' AND `uid` = '{$user_id}'");
        if (mysql_num_rows($res) > 0) {
            return true;
        }
    }
    return false;
}

function get_quiz_data($feedback_id)
{
    $details_rs = mysql_query("SELECT q.* FROM quiz q WHERE q.id = '{$feedback_id}' LIMIT 1");
    if (mysql_num_rows($details_rs) > 0) {
        $row = mysql_fetch_object($details_rs);
        return $row;
    } else {
        return false;
    }
}

function get_feedback_data($feedback_id)
{
    $details_rs = mysql_query("SELECT f.* FROM feedbacks f WHERE f.id = '{$feedback_id}' LIMIT 1");
    if (mysql_num_rows($details_rs) > 0) {
        $row = mysql_fetch_object($details_rs);
        return $row;
    } else {
        return false;
    }
}

function feedback_question_data($feedback_id)
{
    $details_rs = mysql_query("SELECT fq.* FROM feedback_questions fq WHERE fq.feedback_id = '{$feedback_id}'");
    if (mysql_num_rows($details_rs) > 0) {
        $result = array();
        while($row = mysql_fetch_object($details_rs)){
            $result[] = $row;
        }
        return $result;
    } else {
        return false;
    }
}

function quiz_question_data($feedback_id)
{
    $details_rs = mysql_query("SELECT fq.* FROM quiz_questions fq WHERE fq.quiz_id = '{$feedback_id}'");
    if (mysql_num_rows($details_rs) > 0) {
        $result = array();
        while($row = mysql_fetch_object($details_rs)){
            $result[] = $row;
        }
        return $result;
    } else {
        return false;
    }
}

function get_details($table, $key, $value){
    $details_rs = mysql_query("SELECT * FROM {$table} WHERE {$key} = {$value}");
    if(mysql_num_rows($details_rs) > 0){
        $row = mysql_fetch_object($details_rs);
        return $row;
    }else{
        return false;
    }
}

function update_details($table, $setvalue, $where){
    $update_rs = mysql_query("UPDATE `{$table}` SET {$setvalue}  where {$where} ");
    if($update_rs){
        return true;
    }else{
        return false;
    }
}

function insert_details($table, $setvalue){
    //echo "INSERT INTO `{$table}` SET {$setvalue}<br>";
    $insert_rs = mysql_query("INSERT INTO `{$table}` SET {$setvalue}");
    if($insert_rs){
        return mysql_insert_id($db);
    }else{
        return false;
    }
}

function insert_batch($table, $fields, $values)
{
    $sql = array();
    foreach ($values as $row) {
        $sql[] = '('.$row.')';
    }
    mysql_query("INSERT INTO `{$table}` ($fields) VALUES ".implode(',', $sql)) or die(mysql_error());
}

function get_user_detail(){
    $user_query = mysql_query("SELECT new_users.* from new_users INNER JOIN registration_field ON registration_field.id = new_users.field_id WHERE new_users.event_id = '".EVENT_ID."' ORDER BY registration_field.display_order ASC");
    $user_num_rows = mysql_num_rows($user_query);

    $final_array = array();
    if($user_num_rows>0){
        while($user_item = mysql_fetch_array($user_query)){
            $final_array[$user_item['uid']][] = $user_item['value'];
        }
    }

    return $final_array;
}

function sendMail($to_mail, $subject, $message)
{
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 1;
    $mail->Debugoutput = 'html';

    $smtp_port = 465;

    $mail->Host = 'smtp.gmail.com';
    $mail->Port = $smtp_port;
    $mail->SMTPSecure = ($smtp_port == 465) ? 'ssl' : 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = "xxx@xxx.xxx";
    $mail->Password = 'xxxxx';

    $mail->setFrom('xxx@xxx.xxx', 'xxx');
    $mail->IsHTML();
    $mail->addAddress($to_mail);

    $mail->Subject = $subject;

    $mail->Body = $message;

    //send the message, check for errors
    if (!$mail->send()) {
        return false;
    } else {
        return true;
    }
}