<div class="modal fade" id="agendaModal" aria-hidden="true" tabindex="-1" role="dialog" aria-labelledby="agendaModalLabel" data-keyboard="true" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-md-12 col-xl-12 p-0">
                            <nav>
                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Day 1: 09th Dec, 2021</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Day 2: 10th Dec, 2021</a>
                                </div>
                            </nav>
                            <div class="tab-content p-0" id="nav-tabContent">
                                <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <img src="<?=SITE_URL?>img/agenda-day1.jpg?v=1.1" class="img-fluid w-100" />
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <img src="<?=SITE_URL?>img/agenda-day2.jpg?v=1.1" class="img-fluid w-100" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .modal-lg {
        width: 1366px !important;
        max-width: 95vw;
        margin: 0 auto;
    }
    .close-modal {
        position: absolute;
        z-index: 999;
        right: 2px;
        top: 2px;
        color: #a40d06;
        font-size: 2rem;
        background: white !important;
        padding: 0 7px !important;
        border-radius: 5px;
    }
    nav > .nav.nav-tabs {
        border: none;
        color: #fff;
        background: #272e38;
        border-radius: 0;

        display: flex;
        flex-direction: row;
        align-content: flex-end;
        justify-content: space-evenly;
        align-items: baseline;
        flex-wrap: nowrap;
    }
    nav > div a.nav-item.nav-link,
    nav > div a.nav-item.nav-link.active {
        border: none;
        padding: .4rem .5rem;
        color: #fff;
        background: #272e38;
        border-radius: 0;
    }
    nav > div a.nav-item.nav-link.active.show {
        background: #a40d06 !important;
    }
    nav > div a.nav-item.nav-link.active:after {
        content: "";
        position: relative;
        bottom: -3rem;
        left: -16%;
        border: 15px solid transparent;
        border-top-color: #a40d06 ;
    }
    .tab-content {
        background: #fdfdfd;
        line-height: 25px;
        border: 1px solid #ddd;
        border-top: 5px solid #a40d06;
        border-bottom: 5px solid #a40d06;
        padding: 30px 25px;
    }
    nav > div a.nav-item.nav-link:hover,
    nav > div a.nav-item.nav-link:focus {
        border: none;
        background: #a40d06;
        color: #fff;
        border-radius: 0;
        transition: background 0.20s linear;
    }
</style>