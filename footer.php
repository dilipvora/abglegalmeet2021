<script src="<?= SITE_URL ?>js/sweetalert2.all.min.js"></script>
<script src="<?= SITE_URL ?>assets/chart/highcharts.js"></script>
<script src="<?= SITE_URL ?>assets/chart/highcharts-more.js"></script>
<script src="<?= SITE_URL ?>assets/chart/modules/exporting.js"></script>

<!-- firebase -->
<script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-database.js"></script>
<script type="text/javascript" src="<?= SITE_URL ?>assets/js/firebase/firebase2.2.1.js"></script>

<script type="text/javascript">
    var event_id = '<?= EVENT_ID ?>';
    var firbase_ajax_setting = '<?= 1 ?>';
    var firebase_url = '<?= FIREBASE_URL ?>';
    var firebase_url2 = '<?= FIREBASE_URL2 ?>';
    var api_key = '<?= API_KEY ?>';
    var site_url = '<?= SITE_URL ?>';
    var redirect_to = "presentation";
</script>
<script type="text/javascript" src="<?= SITE_URL ?>assets/js/firebase/app.js?v=2.5"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setInterval(function () {
            update_logout_time();
        }, <?=LOGOUT_TIME_INTERVAL?>);
        update_logout_time();

        function update_logout_time() {
            var send_data = { 'uid': <?=$_SESSION["uid"]?>, 'login_session_id': <?=$_SESSION["login_session_id"]?>};
            $.ajax({
                type: "POST",
                async: true,
                data: send_data,
                url: "<?=SITE_URL?>update_logout_time.php",
                cache: false,
                dataType: "json",
                success: function (result) {
                    if (result.status == 0) {
                        window.location = "index.php";
                    }
                }
            });
        }
    });
</script>
