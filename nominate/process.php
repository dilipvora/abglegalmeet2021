<?php include_once "../config.php";
$uid = $_SESSION["uid"];
$event_id = EVENT_ID;
$now_time = date("Y-m-d H:i:s");

if(isset($_POST['submit'])) {
    unset($_POST['submit']);

    if (!is_dir("../upload/nominate/")) {
        mkdir("../upload/nominate/", 0777, true);
    }

    $uniq_name = str_replace(".", "", microtime(true));
    $uniq_name = "$uid-$uniq_name.jpg";

    $imageBase64Code = explode(";base64,", $_POST['base64_f1']);
    $image_base64 = base64_decode($imageBase64Code[1]);
    $targetFilePath = "../upload/nominate/$uniq_name";
    file_put_contents($targetFilePath, $image_base64);

    $_POST['f2'] = mysql_real_escape_string($_POST['f2']);
    $_POST['f3'] = mysql_real_escape_string($_POST['f3']);
    $_POST['f4'] = mysql_real_escape_string($_POST['f4']);
    $_POST['f5'] = mysql_real_escape_string($_POST['f5']);
    $_POST['f6'] = mysql_real_escape_string($_POST['f6']);
    $_POST['f7'] = mysql_real_escape_string($_POST['f7']);
    $_POST['f8'] = mysql_real_escape_string($_POST['f8']);

    insert_details("extra_form", "`uid` = '{$uid}', `event_id` = '{$event_id}', `f1` = '{$uniq_name}', `f2` = '{$_POST['f2']}', `f3` = '{$_POST['f3']}', `f4` = '{$_POST['f4']}', `f5` = '{$_POST['f5']}', `f6` = '{$_POST['f6']}', `f7` = '{$_POST['f7']}', `f8` = '{$_POST['f8']}', `role` = '{$_POST['action']}'");

    header("location:".SITE_URL."nominate/form.php?action=".$_POST['action']."&role=".$_POST['f2']);
    exit();
}