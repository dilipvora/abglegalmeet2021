<?php include_once "../header2.php"; ?>
<style type="text/css">
    html, body {
        height: 100%;
    }
    body {
        <?php if (!empty($_GET['action']) && ($_GET['action'] === "above-and-beyond" || $_GET['action'] === "young-achievers-award" || $_GET['action'] === "best-legal-innovator" || $_GET['action'] === "best-in-house-team-of-the-year" || $_GET['action'] === "dealmaker-of-the-year" || $_GET['action'] === "student-of-the-year" || $_GET['action'] === "collaborator-of-the-year")) { ?>
        background-image: linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
        <?php } else { ?>
        background-image: url("<?=SITE_URL?>img/video-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
        <?php } ?>
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        background-position: center;
        background-color: #e2bc83;
    }
    .theme_button {
        font-size: medium !important;
        line-height: unset !important;
    }
    .nominate-btn-div {
        position: absolute;
        bottom: 4.5%;
        right: 48.25%;
    }
    .outer {
        display: table;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }
    .middle {
        display: table-cell;
        vertical-align: middle;
    }
    .inner {
        margin: auto;
        width: auto;
        /*whatever width you want*/
    }

    @media screen and (max-width: 767px) {
        body {
            background-image: url("<?=SITE_URL?>img/video-mobile-bg.jpg");
        }
        .outer {
            display: block;
        }
        .nominate-btn-div {
            bottom: -7.5%;
            right: 43.5%;
        }
    }
    @media only screen and (orientation: landscape) and (min-device-width: 480px) and (max-device-width: 1080px) {
        .nominate-btn-div {
            bottom: 2.5%;
            right: 46.5%
        }
    }
    ::-webkit-scrollbar-thumb {
        background-color: #939194;
        border: 2px solid transparent;
        border-radius: 5px;
        background-clip: padding-box;
    }
    ::-webkit-scrollbar {
        width: .65rem;
    }
</style>
<div id="warning-message">
    <img style="width: 100vw; margin-top: 20%;" src="<?=SITE_URL?>img/rotatescreen.gif" />
    <h4 style="text-align:center;">Please rotate your phone to landscape.</h4>
</div>
<div class="container-fluid h-100">
    <div class="row h-100">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center" style="position: relative">
                            <?php if (!empty($_GET['action']) && ($_GET['action'] === "above-and-beyond" || $_GET['action'] === "young-achievers-award" || $_GET['action'] === "best-legal-innovator" || $_GET['action'] === "best-in-house-team-of-the-year" || $_GET['action'] === "dealmaker-of-the-year" || $_GET['action'] === "student-of-the-year" || $_GET['action'] === "collaborator-of-the-year")) { ?>
                                <img src="<?=SITE_URL?>img/badge-of-honour/<?=$_GET['action']?>.jpg?v=1" class="img-fluid w-100" usemap="#image_map" />
                                <div class="nominate-btn-div">
                                    <button type="button" onclick="window.location.href='<?=SITE_URL?>nominate/index.php'" class="theme_button">Back</button>
                                </div>
                            <?php } else { ?>
                                <img src="<?=SITE_URL?>img/badge-of-honour/nominee.png" class="img-fluid" width="85%" usemap="#image_map" />
                                <map name="image_map">
                                    <area alt="Above & Beyond" title="Above & Beyond" href="<?=SITE_URL?>nominate/index.php?action=above-and-beyond" coords="320,335,587,517" shape="rect" />
                                    <area alt="Young Achievers Award" title="Young Achievers Award" href="<?=SITE_URL?>nominate/index.php?action=young-achievers-award" coords="660,340,920,513" shape="rect" />
                                    <area alt="best legal innovator" title="Best Legal Innovator" href="<?=SITE_URL?>nominate/index.php?action=best-legal-innovator" coords="997,342,1257,513" shape="rect" />
                                    <area alt="Best In-house Team Of The Year" title="Best In-house Team Of The Year" href="<?=SITE_URL?>nominate/index.php?action=best-in-house-team-of-the-year" coords="1332,342,1592,514" shape="rect" />
                                    <area alt="Dealmaker Of The Year" title="Dealmaker Of The Year" href="<?=SITE_URL?>nominate/index.php?action=dealmaker-of-the-year" coords="494,626,759,799" shape="rect" />
                                    <area alt="Student Of The Year" title="Student Of The Year" href="<?=SITE_URL?>nominate/index.php?action=student-of-the-year" coords="826,626,1091,799" shape="rect" />
                                    <area alt="Collaborator Of The Year" title="Collaborator Of The Year" href="<?=SITE_URL?>nominate/index.php?action=collaborator-of-the-year" coords="1164,626,1429,799" shape="rect" />
                                </map>
                                <div class="nominate-btn-div">
                                    <button type="button" onclick="window.location.href='<?=SITE_URL?>lobby.php#lobby'" class="theme_button">Home</button>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=SITE_URL?>js/imageMapResizer.min.js"></script>
<?php include_once "../footer.php"; ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('map').imageMapResize();
    });
</script>
</body>
</html>
