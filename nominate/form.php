<?php include_once "../header2.php";
$event_id = EVENT_ID;
$uid = $_SESSION["uid"];
if (!empty($_GET['action'])) {
    $action = $_GET['action'];
} else {
    header('location:' . SITE_URL . 'nominate/index.php');
    exit();
} ?>

<style type="text/css">
    html, body {
        height: 100%;
    }
    body {
        background-image: url("<?=SITE_URL?>img/video-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        background-position: center;
        background-color: #e2bc83;
    }

    .box {
        /* ff 3.6+ */
        background: -moz-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
        /* safari 5.1+,chrome 10+ */
        background: -webkit-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
        /* opera 11.10+ */
        background: -o-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
        /* ie 6-9 */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#C5A14F', endColorstr='#DCAA61', GradientType=0);
        /* ie 10+ */
        background: -ms-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
        /* global 94%+ browsers support */
        background: linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);

        padding: 7px;
    }
    .box-title h2 {
        background: #DCAA61;
        padding: .7rem;

        background: -webkit-linear-gradient(to right, #DCAA61 0%, #744732 25%, #F3E78B 50%, #B88E41 75%, #C5A14F 100%);
        background: -moz-linear-gradient(to right, #DCAA61 0%, #744732 25%, #F3E78B 50%, #B88E41 75%, #C5A14F 100%);
        background: linear-gradient(to right, #DCAA61 0%, #744732 25%, #F3E78B 50%, #B88E41 75%, #C5A14F 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }
    .box-title {
        background: #a30e06;
    }
    .box-body {
        background: #f7f7f7;
        color: #a30e06;
    }
    .box-body p {
        font-size: 1.5rem;
        padding: 1rem 1.5rem;
        margin: 0;
    }
    .award-img {
        max-height: 100%;
        width: auto !important;
    }
    .form-control {
        background: #f7f7f7;
        border: 2px solid #b0b0b0;
        color: #a30e06;
        font-weight: bold;
        line-height: 2rem
    }
    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 1.1rem);
    }

    .outer {
        display: table;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }
    .middle {
        display: table-cell;
        vertical-align: middle;
    }
    .inner {
        margin: auto;
        width: auto;
        /*whatever width you want*/
    }

    .theme_button {
        font-size: 1.25rem;
        line-height: 1.6;
        letter-spacing: 1.5px;
        background: #a30e06;
        color: #ffffff;
        font-weight: bolder;
        border: none;
    }
    .theme_button:hover, .theme_button:focus, .theme_button:active {
        background: #a30e06;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #680502;
    }

    @media screen and (max-width: 767px) {
        body {
            background-image: url("<?=SITE_URL?>img/video-mobile-bg.jpg");
        }
        .award-img {
            max-height: 20vh;
            width: auto !important;
        }
        .outer {
            position: unset;
        }
    }

    @media only screen and (orientation: landscape) and (min-device-width: 480px) and (max-device-width: 1080px) {
        .box {
            max-height: 65vh;
            height: auto;
            overflow: auto;
        }
        .award-img {
            max-height: 65vh;
            width: auto !important;
        }
    }
    .disabled {
        cursor: not-allowed;
    }
    ::-webkit-scrollbar-thumb {
        background-color: #939194;
        border: 2px solid transparent;
        border-radius: 5px;
        background-clip: padding-box;
    }
    ::-webkit-scrollbar {
        width: .65rem;
    }
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #a30e06 !important;
        font-weight: bold;
    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: #a30e06 !important;
        font-weight: bold;
    }
    :-ms-input-placeholder { /* IE 10+ */
        color: #a30e06 !important;
        font-weight: bold;
    }
    :-moz-placeholder { /* Firefox 18+ */
        color: #a30e06 !important;
        font-weight: bold;
    }
</style>
<div class="container-fluid h-75">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 p-0 mt-3 mb-3">
            <img src="<?= SITE_URL ?>img/nominee-banner.png" class="img-fluid w-100"/>
        </div>
    </div>
    <div class="row h-100">
        <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center">
                            <img src="<?= SITE_URL ?>img/award.png" class="award-img img-fluid"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <?php if ((!empty($_GET['role'])) && ($_GET['role'] === 'individual' || $_GET['role'] === "team")) {
                            $is_form_active = true;
                            if ($action === "dealmaker-of-the-year") {
                                //$check_q = mysql_query("SELECT `fid`,`f4` FROM `extra_form` WHERE `role` = '{$action}' AND `event_id` = '{$event_id}' AND `uid` = '{$uid}'");
                                $check_q = mysql_query("SELECT `fid`,`f4` FROM `extra_form` WHERE `role` = '{$action}' AND `f2` = '{$_GET['role']}' AND `event_id` = '{$event_id}' AND `uid` = '{$uid}'");
                                if (mysql_num_rows($check_q)) {
                                    $is_form_active = false;
                                }
                            } elseif ($_GET['role'] === "individual") {
                                $check_q = mysql_query("SELECT `fid`,`f4` FROM `extra_form` WHERE `role` = '{$action}' AND `f2` = 'individual' AND `event_id` = '{$event_id}' AND `uid` = '{$uid}'");
                                if (mysql_num_rows($check_q)) {
                                    $is_form_active = false;
                                }
                            } elseif ($_GET['role'] === "team") {
                                $check_q = mysql_query("SELECT `fid`,`f4` FROM `extra_form` WHERE `role` = '{$action}' AND `f2` = 'team' AND `event_id` = '{$event_id}' AND `uid` = '{$uid}'");
                                if (mysql_num_rows($check_q)) {
                                    $is_form_active = false;
                                }
                            }

                            if ($is_form_active) { ?>
                                <form method="post" action="process.php" enctype="multipart/form-data">
                                    <input type="hidden" name="action" value="<?=$action?>" />
                                    <h2 class="h2 font-weight-bold text-center mb-5" style="color: #a30e06"><?= ucwords(str_replace(array('-', '_'), " ", $action)) ?></h2>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto">
                                            <div class="form-group">
                                                <input type="file" name="f1" id="f1" class="form-control" required/>
                                                <textarea hidden name="base64_f1" id="base64_f1"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto">
                                            <div class="form-group">
                                                <input type="text" name="f3" class="form-control" placeholder="Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto">
                                            <div class="form-group">
                                                <input type="text" name="f4" class="form-control" placeholder="Email ID" required/>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto">
                                            <div class="form-group">
                                                <input type="text" name="f5" class="form-control" placeholder="Contact Number" required/>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto">
                                            <div class="form-group">
                                                <input type="text" name="f6" class="form-control" placeholder="Business" required/>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto">
                                            <div class="form-group">
                                                <select name="f2" class="form-control" required hidden>
                                                    <?php if ($action === "dealmaker-of-the-year") { ?>
                                                        <option value="individual" <?= (($_GET['role'] === "individual") ? "selected" : null) ?>>Individual</option>
                                                        <option value="team" <?= (($_GET['role'] === "team") ? "selected" : null) ?>>Team</option>
                                                    <?php } elseif ($_GET['role'] === "individual") { ?>
                                                        <option value="individual" selected>Individual</option>
                                                    <?php } elseif ($_GET['role'] === "team") { ?>
                                                        <option value="team" selected>Team</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php if ($_GET['role'] === "team") { ?>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto">
                                            <div class="form-group">
                                                <input type="text" name="f7" class="form-control" placeholder="Team Name" required/>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mx-auto"></div>
                                    </div>
                                    <br/><br/>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mx-auto">
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <label for="f8" class="font-weight-bold" style="font-size: 1.5rem;color: #a30e06;">Please specify why you have nominated yourself/team for this award</label>
                                                    <textarea name="f8" id="f8" rows="3" class="form-control text-center" placeholder="Maximum 1000 Words" word-limit="true" max-words="1000" min-words="10" required></textarea>
                                                    <div class="text-danger"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mx-auto">
                                            <div class="form-group">
                                                <input type="submit" name="submit" class="btn btn-block theme_button" id="formBtn" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            <?php } else { ?>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="text-center">
                                    <div class="form-group">
                                        <img src="<?=SITE_URL?>img/thank-you-for-nomination.png" class="img-fluid w-100" />
                                        <h2 class="h2 font-weight-bold text-center mb-5" style="color: #a30e06">Copy the below link for Business Recommendation</h2>
                                    </div>
                                    <?php $nominationData = mysql_fetch_object($check_q);
                                    $nid = base64_encode($nominationData->fid);
                                    $email = null;
                                    $link = SITE_URL."nominate.php?nid=$nid"; ?>

                                    <div class="input-group form-group">
                                        <input type="text" class="form-control" value="<?=$link?>" id="copyText" placeholder="Invitation link" />
                                        <div class="input-group-append">
                                            <button class="btn theme_button" type="button" onclick="copyText()"><i class="fa fa-copy"></i> Copy text</button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn theme_button mb-2" type="button" onclick="window.open('https://wa.me/?text=<?=urlencode($link)?>', '_blank')">WhatsApp Link</button>
                                        <button class="btn theme_button mb-2" type="button" onclick="window.open('mailto:<?=$email?>', '_blank')">Email</button>
                                    </div>
                                </div>
                            </div>
                            <?php }
                            } else { ?>
                            <div class="box text-center">
                                <div class="box-title">
                                    <?php $title_temp = ucwords(str_replace(array('-', '_'), " ", $action));
                                    $title_temp = str_replace(array('Of The'), "of the", $title_temp); ?>
                                    <h2 class="h2 font-weight-bold mb-0"><?=$title_temp?></h2>
                                </div>
                                <div class="box-body">
                                    <?php if ($action === "above-and-beyond") {
                                        $content = "The “Above & Beyond” award is for those individuals who have performed far beyond their job / duties of what was expected of him / her. The award recognizes the contribution(s) of the individuals going beyond delivering their expected KRAs, demonstrating hard work coupled with an attitude of selflessness and contibute to the society at large particuarly in these challenging times.";
                                        $individual = true;
                                    } elseif ($action === "young-achievers-award") {
                                        $content = "This award recognizes the contribution of a young lawyer who move the extra mile in his / her activities by making significant contributions to the organization by driving change and performing outstanding work in the domain of law, career development, etc., and being a major support to the business. (Eligibility: less than 4 years of Overall  Post-Qualification Experience as on 31st Oct. 2021)";
                                        $individual = true;
                                    } elseif ($action === "best-legal-innovator") {
                                        $content = "This award recognizes the significant contributions of the lawyers for innovation in the legal space including their work pertaining to model-setting, revolutionary, and/or creative legal labor – be it legal operations and processes,  legal-tech, tech leadership, etc.";
                                        $individual = $team = true;
                                    } elseif ($action === "best-in-house-team-of-the-year") {
                                        $content = "This award shall be presented to the in-house legal team that can effectively exhibit exceptional performance for and on behalf of the organization – be it through creative and/or novel utilization of its (own) resources, effective administration of external legal advisors, firms, et al.";
                                        $team = true;
                                    } elseif ($action === "dealmaker-of-the-year") {
                                        $content = "The award recognized those individuals and/or team members who have developed and negotiated complex deals or ventures, resolution to complex litigation, use of alternate dispute resolution or mediation to avoid litigation, resulting in an outcome exceeding expectation and thereby provided significant value addition to the business group.";
                                        $individual = $team = true;
                                    } elseif ($action === "student-of-the-year") {
                                        $content = "An award for the learner who has demonstrated a dedication to learning and being the best they can be through courses, seminars or even gone on to do something amazing after learning a new skill.";
                                        $individual = true;
                                    } elseif ($action === "collaborator-of-the-year") {
                                        $content = "In line with this year's theme, this award is for the individual who have collaborated not just with their business clients but across ABG on various projects and innovations. It is to recognise the individual who has worked together across functional silos, hierarchies, businesses and geographies. Leveraging the available diversity to garner synergy benefits and promote oneness through sharing and collaborative efforts.";
                                        $individual = true;
                                    } elseif ($action === "gc-of-the-year") {
                                        $content = "Awarded to the General Counsel who has shown highest excellence beyond the expectations of their role and demonstrated added value to their organisation. It is also to recognise the extraordinary resilience and innovation in the face of all the unprecedented challenges. They have made a lasting contribution towards their team members and their business. In short, they’ve done it all.";
                                        $individual = true;
                                    } else {
                                        $content = null;
                                        $individual = $team = false;
                                    } ?>
                                    <p class="text-justify"><?= $content ?></p>
                                    <div class="text-left">
                                        <p><strong>CLICK BELOW TO NOMINATE</strong>
                                            <br/><br/>
                                            <?php if ($individual) { ?>
                                                <a href="<?= SITE_URL ?>nominate/form.php?action=<?= $action ?>&role=individual">
                                                    <img src="<?= SITE_URL ?>img/individual-btn.png" class="img-fluid">
                                                </a>
                                            <?php }
                                            if ($team) { ?>
                                                <a href="<?= SITE_URL ?>nominate/form.php?action=<?= $action ?>&role=team">
                                                    <img src="<?= SITE_URL ?>img/team-btn.png" class="img-fluid">
                                                </a>
                                            <?php } ?>
                                        </p>
                                    </div>
                                    <p>Business recommendation / Testimonials required for all categories</p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="text-center mt-2">
                        <a href="<?= SITE_URL ?>nominate/index.php" class="btn theme_button">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center">
                            <img src="<?= SITE_URL ?>img/award.png" class="award-img img-fluid"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "../footer.php"; ?>
<script src="<?= SITE_URL ?>js/compress.js" type="text/javascript" rel="script" lang="js"></script>
<script type="text/javascript">
    let compress = new Compress();

    $(document).ready(function() {
        let names = {};
        $(document).on('change', '#f1', function (evt) {
            evt.preventDefault();
            const fileName = $(this).attr('name');
            names[fileName] = [...evt.target.files];
            let maxWidth, maxHeight;
            if (fileName === "f1") {
                maxWidth = 1280;
                maxHeight = 850;
            }

            compress.compress(names[fileName], {
                size: 2,
                quality: 0.75,
                maxWidth: maxWidth,
                maxHeight: maxHeight,
                resize: true
            }).then((images) => {
                const img = images[0];
                // returns an array of compressed images
                const preview = `${img.prefix}${img.data}`
                const output = "#base64_" + fileName;
                $(output).val(preview);
            });
        });

        // Add event trigger for change to textareas with limit
        $(document).on("input", "textarea[word-limit=true]", function () {
            // Get individual limits
            thisMin = parseInt($(this).attr("min-words"));
            thisMax = parseInt($(this).attr("max-words"));
            // Create array of words, skipping the blanks
            var removedBlanks = [];
            removedBlanks = $(this).val().split(/\s+/).filter(Boolean);
            // Get word count
            var wordCount = removedBlanks.length;
            // Remove extra words from string if over word limit
            if (wordCount > thisMax) {
                // Trim string, use slice to get the first 'n' values
                var trimmed = removedBlanks.slice(0, thisMax).join(" ");
                // Add space to ensure further typing attempts to add a new word (rather than adding to penultimate word)
                $(this).val(trimmed + " ");
            }

            // Compare word count to limits and print message as appropriate
            if (wordCount < thisMin) {
                $(this).parent().children(".text-danger").text("Enter Minimum " + thisMin + " Words.");
                $("#formBtn").attr('disabled', 'disabled').addClass('disabled');
            } else if (wordCount > thisMax) {
                $(this).parent().children(".text-danger").text("Enter Maximum " + thisMax + " Words.");
                $("#formBtn").attr('disabled', 'disabled').addClass('disabled');
            } else {
                // No issues, remove warning message
                $(this).parent().children(".text-danger").text("");
                $("#formBtn").removeAttr('disabled', 'disabled').removeClass('disabled');
            }
        });
    });

    function copyText() {
        /* Get the text field */
        var copyText = document.getElementById("copyText");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        navigator.clipboard.writeText(copyText.value);
    }
</script>
</body>
</html>