<?php include 'config.php';
if(!isset($_SESSION['uid']) && $_SESSION['event_id'] !== EVENT_ID){
    header("location:" . SITE_URL . "index.php");
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=COMPANY_NAME?></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta http-equiv="x-dns-prefetch-control" content="on"/>
    <meta property="og:type" content="website"/>
    <meta name="og_site_name" property="og:site_name" content="<?=SITE_URL?>"/>
    <meta property="og:url" content="<?=SITE_URL?>"/>
    <meta name="theme-color" content="#a32421">

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap" />
    <link type="text/css" rel="stylesheet" href="<?=SITE_URL?>fonts/font-awesome/css/all.min.css" />
    <link type="text/css" rel="stylesheet" href="<?=SITE_URL?>css/bootstrap.min.css?v=0.11" />
    <link type="text/css" rel="stylesheet" href="<?=SITE_URL?>css/sweetalert2.min.css" />
    <link type="text/css" rel="stylesheet" href="<?=SITE_URL?>css/custom.css?v=0.11" />

    <script type="text/javascript" src="<?=SITE_URL?>js/jquery.min.js?v=0.11"></script>
    <script type="text/javascript" src="<?=SITE_URL?>js/popper.min.js?v=0.11"></script>
    <script type="text/javascript" src="<?=SITE_URL?>js/bootstrap.min.js?v=0.11"></script>
    <script type="text/javascript" src="<?=SITE_URL?>js/sweetalert2.all.min.js"></script>
</head>

<body>