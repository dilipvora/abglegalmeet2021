<?php include 'config.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quiz</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap"/>
    <link rel="stylesheet" href="fonts/font-awesome/css/all.min.css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/custom.css"/>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <link href="css/jquery.countdownTimer.css" rel="stylesheet" />
    <script src="js/jquery.countdownTimer.js"></script>

    <style type="text/css">
        html, body {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        }

        .theme_button, .theme_button:hover {
            background: #e8e8e8;
            color: #4e1367;
            font-weight: 600;
            border: 2px solid #4e1367;
            border-radius: 15px;
        }

        #feedback_success_msg {
            color: green;
            text-align: center;
        }

        #feedback_error_msg {
            color: red;
            text-align: center;
        }
    </style>
    <script type='text/javascript'>
        $(document).ready(function () {
            $("#QuizForm").on("submit", function (event) {
                $(".submit_btn_add").attr("disabled", "disabled").text("submitting...");
                event.preventDefault();

                /*console.log($(this).serialize());
                return;*/
                $.ajax({
                    type: "POST",
                    async: true,
                    url: "ajax.php",
                    data: $(this).serialize(),
                    cache: false,
                    success: function (result) {
                        if (result === 'success') {
                            $(".quiz_success_html").html("<div id='feedback_success_msg'><h5>Thanks for Quiz!</h5></div>");
                            //closeQuizModal_Success();
                        } else if (result === "already_give") {
                            $(".quiz_success_html").html("<div id='feedback_success_msg'><h5>You have already give a quiz!</h5></div>");
                        } else {
                            $("#quiz_error_msg").html(result);
                        }
                    },
                    error: function (result) {
                        $('#error_msg').html('Something goes wrong.');
                        $("#QuizModel").scrollTop();
                    }
                });
                return false;
            });
        });
    </script>
</head>
<body>
<div class="container-fluid quiz_success_html">
    <div class="row">
        <?php $is_feedback_available = true;
        if (check_quiz_is_access()) {
            if (user_is_access_quiz_page()) {
                $is_feedback_available = false;
            }
        } else {
            $is_feedback_available = false; ?>
            <div class="col-md-12">
                <div id='feedback_success_msg' class="text-center">
                    <h5>No quiz available at this movement!</h5>
                </div>
            </div>
            <?php die();
        }

        if ($is_feedback_available) {
            $quiz_id = get_values("options", "option_value", "event_id = '" . EVENT_ID . "' AND option_name='quiz_id'");
            $user_id = $_SESSION['uid'];
            $date = date('Y-m-d H:i:s');
            $start_date = get_values("options", "option_value", "event_id = '" . EVENT_ID . "' AND option_name='quiz_start'");
            $sel_res = mysql_query("SELECT * FROM `quiz_time` WHERE `event_id`='" . EVENT_ID . "' AND `uid` = '{$user_id}' AND quiz_id = '{$quiz_id}'");
            if (mysql_num_rows($sel_res) > 0) {
                //$start_date = get_values("quiz_time","start_time","event_id = '".EVENT_ID."' AND uid='{$user_id}' AND quiz_id = '{$quiz_id}'");
            } else {
                insert_details("quiz_time", "`event_id`='" . EVENT_ID . "', uid = '{$user_id}', quiz_id = '{$quiz_id}', start_time = '{$start_date}'");
            }
            $quiz_timer = get_values("options", "option_value", "event_id = '" . EVENT_ID . "' AND option_name='quiz_time'");
            if (empty($quiz_timer)) {
                $quiz_timer = 900;
            }
            $start_date = strtotime($start_date);
            $date = strtotime($date);
            $different = round(($date - $start_date));
            $minute = ($quiz_timer - $different) / 60;
            $minut_explode = explode('.', $minute);

            $second = ($quiz_timer - $different);
            $final_second = gmdate('s', $second);

            $final_minute = $minut_explode[0];
            if ($second <= 0) {
                echo "<div id='feedback_success_msg'><h5>No quiz available at this time!</h5></div>";
                exit;
            }

            $quiz_data = get_quiz_data($quiz_id);

        if (!empty($quiz_data)) { ?>
            <div class="container-fluid">
                <div class="clearfix">
                    <div class="row">
                        <h5 class="col-md-6 col-lg-6 col-sm-6 col-xs-12 timer_class" style="display: none1; margin-bottom: 15px; margin-top: 10px; font-weight: bolder">
                            Remaining Time:<span id="ms_timer" style="border-width: 4px; padding: 4px; font-size: 16px;"></span>
                        </h5>
                    </div>
                    <hr/>
                </div>

                <!--<h5 class="col-md-6 col-lg-6 col-sm-6 col-xs-12" style="font-weight: bolder;"><?/*= $quiz_data->title; */?></h5>-->
                <form id="QuizForm" name="frm_submit_feedback" class="form-horizontal frm_submit_quiz" method="POST">
                    <input type="hidden" name="quiz_id" value="<?= $quiz_data->id; ?>">
                    <?php $quiz_question_data = quiz_question_data($quiz_id);
                    if (!empty($quiz_question_data)) {
                        foreach ($quiz_question_data as $key => $quiz_question_row) { ?>
                            <input type="hidden" name="question_ids[<?= $key ?>]" value="<?= $quiz_question_row->id ?>">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h5 style="font-weight: bolder"><?= $quiz_question_row->title; ?></h5>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php if ($quiz_question_row->is_objective == 1) { ?>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name='answer[<?= $key ?>]' value="1" required />
                                                    &nbsp;<?= $quiz_question_row->option1; ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name='answer[<?= $key ?>]' value="2" required />
                                                    &nbsp;<?= $quiz_question_row->option2; ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php if (!empty($quiz_question_row->option3)) { ?>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="radio-inline">
                                                        <input type="radio" name='answer[<?= $key ?>]' value="3" required />
                                                        &nbsp;<?= $quiz_question_row->option3; ?>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php }
                                        if (!empty($quiz_question_row->option4)) { ?>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="radio-inline">
                                                        <input type="radio" name='answer[<?= $key ?>]' value="4" required />
                                                        &nbsp;<?= $quiz_question_row->option4; ?>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row">
                                        <?php if (!empty($quiz_question_row->option5)) { ?>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="radio-inline">
                                                        <input type="radio" name='answer[<?= $key ?>]' value="5" required />
                                                        &nbsp;<?= $quiz_question_row->option5; ?>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php }
                                        if (!empty($quiz_question_row->option6)) { ?>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="radio-inline">
                                                        <input type="radio" name='answer[<?= $key ?>]' value="6" required />
                                                        &nbsp;<?= $quiz_question_row->option6; ?>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if (!empty($quiz_question_row->option7)) { ?>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="radio-inline">
                                                        <input type="radio" name='answer[<?= $key ?>]' value="7" required />
                                                        &nbsp;<?= $quiz_question_row->option7; ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                } else { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="answer[<?= $key ?>]" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <button type="submit" class="btn btn-primary theme_button submit_btn_add">Submit</button>
                    <?php } ?>
                </form>
            </div>

            <script type="text/javascript">
                $(function () {
                    $('#ms_timer').countdowntimer({
                        minutes: <?=$final_minute?>,
                        seconds: <?=$final_second?>,
                        size: "lg",
                        timeUp: timeisUp
                    });

                    function timeisUp() {
                        $(".quiz_success_html").html("<div id='feedback_success_msg'><h5>No quiz available at this time!</h5></div>")
                    }
                });
            </script>
        <?php } else { ?>
            <div id='feedback_success_msg'>
                <h5>No quiz available at this movement!</h5>
            </div>
        <?php }
        } else { ?>
            <div id='feedback_success_msg'>
                <h5>You have already give a quiz!</h5>
            </div>
        <?php } ?>
    </div>
</div>
</body>
</html>