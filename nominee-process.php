<?php include 'config.php';
$curr_date = date("Y-m-d H:i:s");
$access = 0;
$event_id = EVENT_ID;

if (isset($_POST['submit_nominee_form']) && $_POST['submit_nominee_form'] == 'true') {
    $result = mysql_query("SELECT `uid` FROM `users` WHERE `event_id` = '{$event_id}' AND `email` = '{$_POST['email']}' AND `alias` = '{$_POST['nid']}'") or die("Se ". mysql_error());
    $num_rows = mysql_num_rows($result);

    if ($num_rows > 0) {
        $uid = mysql_fetch_object($result)->uid;
        $query = "UPDATE `users` SET `name` = '{$_POST['name']}', `location` = '{$_POST['designation']}', `alias` = '{$_POST['nid']}', `is_login` = '1' WHERE `uid` = '{$uid}'";
        mysql_query($query) or die("Up ".mysql_error());

        $_SESSION['nominee_uid'] = $uid;
        $_SESSION['event_id'] = EVENT_ID;
        $access = 1;
    } else {
        $query = "INSERT INTO `users` SET `name` = '{$_POST['name']}', `email` = '{$_POST['email']}', `location` = '{$_POST['designation']}', `alias` = '{$_POST['nid']}', `event_id` = '{$event_id}', `created_date` = '{$curr_date}', `is_login` = '1'";
        mysql_query($query) or die("In ".mysql_error());

        $uid = mysql_insert_id($db);
        $_SESSION['nominee_uid'] = $uid;
        $_SESSION['event_id'] = EVENT_ID;
        $access = 1;
    }

    $_POST['nid'] = base64_encode($_POST['nid']);
    if ($access == 0) {
        header("location:".SITE_URL."nominee.php?nid=" . $_POST['nid']);
    } else {
        header("location:".SITE_URL."nominate.php?nid=" . $_POST['nid']);
    }
}

/**
 *  Submit Nominee Comments
 */
if(isset($_POST['q_submit_query']) && $_POST['q_submit_query'] == 'true'){
    $message=array();
    $uid = $_SESSION['nominee_uid'];

    if( isset($_POST['q_question']) && $_POST['q_question'] !=''){
        $question = mysql_real_escape_string($_POST['q_question']);
    }else{
        $message[] = 'Please enter question.';
    }

    /*if(isset($_POST['team']) && $_POST['team'] != ""){
        $team = $_POST['team'];
    }else{
        $team = null;
    }*/
    $team = $_POST['nid'];

    if(isset($_POST['publication']) && $_POST['publication'] != ""){
        $publication = $_POST['publication'];
    }else{
        $publication = null;
    }

    $countError=count($message);

    if($countError > 0){
        for($i=0;$i<$countError;$i++){
            echo ucwords($message[$i]).'<br/>';
        }
    }else{
        /*$submit_question = mysql_query("Insert into question SET
											`uid` = '".$uid."',
											`team_id` = '".$team."',
											`event_id` = '".EVENT_ID."',
											`q_question` = '".addslashes($question)."'");*/
        $submit_question = mysql_query("INSERT INTO `question` SET
                                            `event_id` = '{$event_id}',
											`uid` = '{$uid}', `team_id` = '{$team}',
											`q_question` = '{$question}'");
        if($submit_question){
            echo 'success';
        }else{
            echo 'Your Comment not submitted try again.';
        }
    }
}

// Get Nominee Comments
if (isset($_POST['action']) && ($_POST['action'] == "get_ajax_message")) {
    $html = '';
    $check_exist = mysql_query("SELECT `q_id` FROM `question` WHERE `status` != '2' AND `event_id` = '{$event_id}' AND `team_id` = '{$_POST['nid']}'");
    if (!mysql_num_rows($check_exist)) {
        $html .= "d-none";
    } else {
        $rs = mysql_query("SELECT q.q_id, q.q_question, u.name FROM question q, users u WHERE u.uid = q.uid AND q.status != '2' AND q.event_id = '{$event_id}' AND q.team_id = '{$_POST['nid']}' ORDER BY q.q_id DESC") or die(mysql_error());
        $num_rows = mysql_num_rows($rs);
        if ($num_rows) {
            while ($row = mysql_fetch_object($rs)) {
                $html .= '<li>
                            <div class="comment-main-level">
                                <div class="comment-box">
                                    <div class="comment-head">
                                        <h6 class="comment-name by-author">
                                            <a href="javascript:void(0)">' . $row->name . ':</a>
                                            <span class="comment-content">' . $row->q_question . '</span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </li>';
            }
        } else {
            $html .= "d-none";
        }
    }
    echo $html;
}

// Nominee Likes
if (isset($_POST['action']) && ($_POST['action'] === "getset_emojis")) {
    $event_id = EVENT_ID;
    /*mysql_query("DELETE FROM `users_like` WHERE `event_id` = '{$event_id}'");*/
    /*mysql_query("ALTER TABLE users_like DROP FOREIGN KEY users_like_ibfk_1");*/

    $emojis_name = $_REQUEST["name"];
    $dataAction = (!empty($_POST['nid']) ? $_POST['nid'] : NULL);
    $query_str = "`event_id` = '{$event_id}' AND `emojis` = '{$emojis_name}' AND `action` = $dataAction";

    if (isset($_POST["role"]) && ($_POST["role"] == "set_data")) {
        $date = date('Y-m-d H:i:s');
        $check_exist_result = mysql_query("SELECT `count` FROM `users_like` WHERE $query_str AND `uid` = '{$_SESSION['nominee_uid']}'");
        if (mysql_num_rows($check_exist_result) === 0) {
            $get_emojis = mysql_query("SELECT * FROM `users_like` WHERE $query_str");
            $total_count = (mysql_num_rows($get_emojis) ? mysql_fetch_object($get_emojis)->count : 0);
            $count = $total_count + 1;
            //echo "INSERT INTO `users_like` SET `event_id` = '{$event_id}', `uid` = '{$_SESSION['nominee_uid']}', `count` = '{$count}', `emojis` = '{$emojis_name}', `action` = '{$dataAction}', `created_date` = '{$date}'";
            mysql_query("INSERT INTO `users_like` SET `event_id` = '{$event_id}', `uid` = '{$_SESSION['nominee_uid']}', `count` = '{$count}', `emojis` = '{$emojis_name}', `action` = '{$dataAction}', `created_date` = '{$date}'") or die(mysql_error());
        }
    }

    //die;

    //select emojis, count(*) as count from `users_like` where emojis in ('like', 'lol', 'wow', 'cool') GROUP BY emojis
    //$get_emojis = mysql_query("SELECT `emojis`, SUM(count) as `total` FROM `users_like` WHERE `event_id` = '{$event_id}' AND `emojis` IN ('operations-excellence-like', 'products-engineering-like', 'delivery-services-like', 'solutions-like', 'gfc-like', 'go5-like', 'marketing-like', 'people-operations-like') GROUP BY emojis");
    $get_emojis = mysql_query("SELECT `emojis`, `count` FROM `users_like` WHERE $query_str");
    if (mysql_num_rows($get_emojis)) {
        $data = mysql_fetch_object($get_emojis);
        $data->total_count = mysql_num_rows($get_emojis);
        $data->is_like = 'no';
        //$alredy_liked_q = mysql_query("SELECT * FROM `users_like` WHERE $query_str AND ");
        $check_exist_result = mysql_query("SELECT `count` FROM `users_like` WHERE $query_str AND `uid` = '{$_SESSION['nominee_uid']}'");
        if (mysql_num_rows($check_exist_result) > 0) {
            $data->is_like = 'yes';
        }
        echo json_encode($data);
    } else {
        echo json_encode(array("emojis" => $emojis_name, "count" => "0"));
    }
}