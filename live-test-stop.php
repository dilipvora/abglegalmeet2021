<!DOCTYPE html>
<html lang="en">
<head>
    <title>Live</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta http-equiv="x-dns-prefetch-control" content="on"/>
    <meta property="og:type" content="website"/>
    <meta name="theme-color" content="#000000">

    <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="css/custom.css?v=1">
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-10 col-md-10 col-lg-10 col-xl-10 mx-auto">
            <div class="video-container">
                <?php if ($_GET['live'] === '1') {
                    $src = "https://webcastlive.co.in/player/play_redik.php?event_id=abglegalmeet2021";
                } else {
                    $src = "https://webcastlive.co.in/player/play_redik5cent.php?event_id=5c6d78cffa59e129f040fcec2d788532.sdp";
                } ?>
                <iframe src="<?=$src?>" marginheight="0" frameborder="0" scrolling="no"  allowfullscreen="allowfullscreen"></iframe>
            </div>
        </div>
    </div>
</div>
</body>
</html>