<?php include 'header1.php'; ?>

<style type="text/css">
    .quotes-btn {
        position: absolute;
        top: 80.5%;
        right: 1%
    }
    .theme_button {
        font-size: unset;
        line-height: unset;
    }

    ::-webkit-scrollbar-thumb {
        background-color: #939194;
        border: 2px solid transparent;
        border-radius: 5px;
        background-clip: padding-box;
    }

    ::-webkit-scrollbar {
        width: .65rem;
    }
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #ffffff !important;
        font-weight: 500;
    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: #ffffff !important;
        font-weight: 500;
    }
    :-ms-input-placeholder { /* IE 10+ */
        color: #ffffff !important;
        font-weight: 500;
    }
    :-moz-placeholder { /* Firefox 18+ */
        color: #ffffff !important;
        font-weight: 500;
    }
</style>

<div class="container-fluid h-100">
    <div class="row h-100">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mx-auto p-0">
            <?php if (file_exists('img/quotes.jpg')) { ?>
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center" style="position: relative">
                            <img src="img/quotes.jpg" class="img-fluid w-100"/>
                            <div class="quotes-btn">
                                <input type="button" class="btn btn-primary theme_button" value="Back" onclick="window.location.href = '<?=SITE_URL?>lobby.php#lobby'" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php include_once "footer.php"; ?>
</body>
</html>