var firebase_redik_webinar1 = new Firebase(firebase_url2+event_id);
var firebase_cursor_position = firebase_redik_webinar1.child('cursor_position');


//var interval = 1000;
/*var config = {
    apiKey: api_key,
    authDomain: firebase_url,
    databaseURL: firebase_url,
};

firebase.initializeApp(config);
// Get a reference to the database service
var firebase_redik_webinar = firebase.database();

var ppt_delay_interval = firebase.database().ref(event_id+'/ppt_delay');
ppt_delay_interval.on('value', function (snapshot) {
    if(parseInt(snapshot.val()) > 0) {
        interval = parseInt(snapshot.val());
        //console.log("interval: "+interval);
    }
});*/


firebase_cursor_position.on("child_added", function(snapshot, prevChildKey) {
    firebase_cursor_position.once("value", function(snapshot) {
        setTimeout(function(){
            var data = snapshot.val();
            $("#dot").css({"left": data.x, "top": data.y});
        }, interval);
    });
});

firebase_cursor_position.on('child_changed', function(childSnapshot) {
    firebase_cursor_position.once("value", function(snapshot) {
        setTimeout(function(){
            var data = snapshot.val();
            $("#dot").css({"left": data.x, "top": data.y});
        }, interval);
    });
});