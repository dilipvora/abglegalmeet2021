// Initialize Firebase

var config = {
	apiKey: api_key,
	authDomain: firebase_url,
	databaseURL: firebase_url,
};

firebase.initializeApp(config);
// Get a reference to the database service
var firebase_redik_webinar = firebase.database();

var interval = 1000;
if(isWebRTCVideo  == undefined) {
    var isWebRTCVideo  = false;
}

if(firbase_ajax_setting == 1) {
    //console.log("firebase");
    //Get Current Slide

    var firebase_current_slide = firebase.database().ref(event_id+'/current_slide');
    firebase_current_slide.on('value', function (snapshot) {
        setTimeout(function () {
            $('#video_slide_img').attr('src', snapshot.val());
        }, isWebRTCVideo ? 1 : interval);
    });

    // Get Announcement

    var current_announcement = firebase.database().ref(event_id+'/current_announcement');
    current_announcement.on('value', function (snapshot) {
        $('#lbldata').html(snapshot.val());
    });


    // PPT Delay Interval

    var ppt_delay_interval = firebase.database().ref(event_id+'/ppt_delay');
    ppt_delay_interval.on('value', function (snapshot) {
		if(parseInt(snapshot.val()) > 0) {
			interval = parseInt(snapshot.val());
			//console.log("interval: "+interval);
		}
    });

    // Feedback on off
    var feedback_on_off = firebase.database().ref(event_id+'/feedback_on_off');
    feedback_on_off.on('value', function (snapshot) {
        if(snapshot.val() == "on") {
            $('.GiveFeedback').show();
            $("#FeedbackContent").html("<div class='center'>Loading...</div>");
            $("#FeedbackContent").load("feedback_modal.php");
            $("#FeedbackModel").modal('show');

            $("#FeedbackResultContent").html("");
            $("#FeedbackResultModel").modal('hide');
        }else{
            $('.GiveFeedback').hide();
            $(".FeedbackContent").html("");
            $(".FeedbackModel").modal('hide');
        }
    });

    // Feedback result on off
    var feedback_result_on_off = firebase.database().ref(event_id+'/feedback_result_on_off');
    feedback_result_on_off.on('value', function (snapshot) {
        if (snapshot.val() == "on") {
            $('.FeedbackResults').show();
            $("#FeedbackResultContent").html("<div class='center'>Loading...</div>");
            $("#FeedbackResultContent").load("live_feedback_result.php");
            $("#FeedbackResultModel").modal('show');

            $("#FeedbackContent").html("");
            $("#FeedbackModel").modal('hide');
        } else {
            $('.FeedbackResults').hide();
            $("#FeedbackResultContent").html("");
            $("#FeedbackResultModel").modal('hide');
        }
    });


    // Quiz on off
    var quiz_on_off = firebase.database().ref(event_id + '/quiz_on_off');
    quiz_on_off.on('value', function (snapshot) {
        if (snapshot.val() == "on") {
            $('.GiveQuiz').show();
            $(".quiz_success_msg").html("");
            $(".quiz_error_msg").html("");
            $(".quiz_frame").attr("src", "quiz_modal.php");
            $("#QuizModel").modal("show");
        } else {
            $('.GiveQuiz').hide();
            $(".quiz_frame").attr("src", "");
            $("#QuizModel").modal('hide');
        }
    });

    function randRange(data) {
        var newTime = data[Math.floor(data.length * Math.random())];
        return newTime;
    }

    // EVENT Start Stop
    var event_start_stop = firebase.database().ref(event_id + '/event_start_stop');
    event_start_stop.on('value', function (snapshot) {
        var timeArray = new Array(200, 300, 2500, 150, 250, 2000, 500, 3000, 1000, 3500, 1500, 2000);
        //var timeArray = new Array(100, 500, 1000, 1500, 2000, 2500, 3000, 3500);

        if (snapshot.val() == "start" && redirect_to == "video") {
            setTimeout(function () {
                window.location.href = site_url + "presentation.php?session-is=live";
            }, randRange(timeArray));
        } else if (snapshot.val() == "stop" && redirect_to == "welcome") {
            setTimeout(function () {
                window.location.href = site_url + "presentation.php";
            }, randRange(timeArray));
        }
    });

    /*var firebase_cursor_images = firebase.database().ref(event_id+'/cursor_images');
    firebase_cursor_images.on('value', function (snapshot) {
        var data = snapshot.val();
        var img = data.replace('../', '');
        $("#dot").attr('src',img);
    });*/


   /* var firebase_cursor_position = firebase.database().ref(event_id+'/cursor_position');
    firebase_cursor_position.on('value', function (snapshot) {
        var data = snapshot.val();

        //setTimeout(function () {
            $("#dot").css({"left": data.x, "top": data.y});
        //}, isWebRTCVideo ? 1 : interval);
    });*/
}