/* 02-12-2021 10:40 AM */
CREATE TABLE `one_to_one_chat`
(
    `cid`             INT                                  NOT NULL AUTO_INCREMENT,
    `event_id`        VARCHAR(255)                         NULL DEFAULT NULL,
    `incoming_msg_id` INT                                  NULL DEFAULT NULL,
    `outgoing_msg_id` INT                                  NULL DEFAULT NULL COMMENT 'as a uid',
    `msg`             BLOB                                 NULL DEFAULT NULL,
    `status`          INT                                  NULL DEFAULT '0' COMMENT '0 = unread, 1 = read',
    `created_date`    TIMESTAMP                            NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_date`    DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`cid`)
) ENGINE = InnoDB
  CHARSET = utf8
  COLLATE utf8_bin COMMENT = 'Personal chatting like WhatsApp';
