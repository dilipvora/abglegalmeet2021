-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2021 at 07:04 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cipla_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0-admin,1-superadmin, 2-presenter',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `fullname`, `email`, `mobile`, `status`, `created_date`) VALUES
(1, 'SBIWebmin2021', 'd6e85c31aea9a3f5c583b63124aba838', 'wbcst-admin', '', 'fbcee4dfbf8d4341cb0e356f8a46901b', 0, '2021-06-29 13:04:08'),
(2, 'wbcst-spa', 'd363f0a3c3d1c785277b81303bb78a03', '', '', '', 1, '2021-06-29 13:04:08'),
(3, 'blueberry-admin', '22c0c2fbf40dfc32e3d493c0ede9ee08', 'Blueberry Admin', '', '', 0, '2021-09-22 08:47:12'),
(5, 'zerohour-admin', 'b32004bc7f84e2a0cd4a744bc2b5b9da', 'Zerohour', '', '', 0, '2021-09-22 08:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `announcement_text` text NOT NULL,
  `role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `archived_session`
--

CREATE TABLE `archived_session` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `details` text COLLATE utf8_bin,
  `vod_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8_bin DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `seat_id` int(11) DEFAULT NULL,
  `slot` varchar(255) DEFAULT NULL,
  `is_attend` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `certificate_download`
--

CREATE TABLE `certificate_download` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `image` text,
  `role` varchar(255) DEFAULT NULL,
  `download_date` timestamp NULL DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cipla_users`
--

CREATE TABLE `cipla_users` (
  `id` int(11) NOT NULL,
  `event_id` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `consent`
--

CREATE TABLE `consent` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `f1` text,
  `f2` text,
  `f3` text,
  `f4` text,
  `f5` text,
  `f6` text,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_dr`
--

CREATE TABLE `contact_dr` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_bin DEFAULT '0' COMMENT '0-pending,1-contacted,2-follow_up',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `demmy_users`
--

CREATE TABLE `demmy_users` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `f1` text COLLATE utf8_bin,
  `f2` text COLLATE utf8_bin,
  `f3` text COLLATE utf8_bin,
  `f4` text COLLATE utf8_bin,
  `f5` text COLLATE utf8_bin,
  `f6` text COLLATE utf8_bin,
  `f7` text COLLATE utf8_bin,
  `f8` text COLLATE utf8_bin,
  `f9` text COLLATE utf8_bin,
  `f10` text COLLATE utf8_bin,
  `f11` text COLLATE utf8_bin,
  `f12` text COLLATE utf8_bin,
  `f13` text COLLATE utf8_bin,
  `f14` text COLLATE utf8_bin,
  `f15` text COLLATE utf8_bin,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `extra_form`
--

CREATE TABLE `extra_form` (
  `fid` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `f1` text COLLATE utf8_bin,
  `f2` text COLLATE utf8_bin,
  `f3` text COLLATE utf8_bin,
  `f4` text COLLATE utf8_bin,
  `f5` text COLLATE utf8_bin,
  `f6` text COLLATE utf8_bin,
  `f7` text COLLATE utf8_bin,
  `f8` text COLLATE utf8_bin,
  `f9` text COLLATE utf8_bin,
  `f10` text COLLATE utf8_bin,
  `role` char(255) COLLATE utf8_bin DEFAULT NULL,
  `status` char(255) COLLATE utf8_bin DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_questions`
--

CREATE TABLE `feedback_questions` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `feedback_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `is_objective` int(1) NOT NULL DEFAULT '0',
  `option1` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option2` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option3` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option4` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `option5` varchar(255) DEFAULT NULL,
  `option6` varchar(255) DEFAULT NULL,
  `option7` varchar(255) DEFAULT NULL,
  `correct_option` enum('0','1','2','3','4','5','6','7') NOT NULL DEFAULT '0',
  `image` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_score`
--

CREATE TABLE `game_score` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `game_name` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_zone`
--

CREATE TABLE `game_zone` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `game_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `play_count` int(11) DEFAULT NULL,
  `is_play` enum('Yes','No') COLLATE utf8_bin DEFAULT 'No',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `live_chat`
--

CREATE TABLE `live_chat` (
  `c_id` int(11) NOT NULL,
  `event_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `chat` text CHARACTER SET utf8 COLLATE utf8_bin,
  `chat_blob` blob,
  `status` enum('0','1','2') DEFAULT NULL,
  `team_name` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `created_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_users`
--

CREATE TABLE `new_users` (
  `uid` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `f1` text,
  `f2` text,
  `f3` text,
  `f4` text,
  `f5` text,
  `f6` text,
  `f7` text,
  `f8` text,
  `f9` text,
  `f10` text,
  `f11` text,
  `f12` text,
  `f13` text,
  `f14` text,
  `f15` text,
  `is_login` int(11) NOT NULL DEFAULT '0',
  `login_token` varchar(255) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `is_session` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `user_role` varchar(255) DEFAULT NULL,
  `user_cat` char(255) DEFAULT NULL,
  `utm_source` varchar(255) DEFAULT NULL,
  `utm_medium` varchar(255) DEFAULT NULL,
  `utm_campaign` varchar(255) DEFAULT NULL,
  `utm_term` varchar(255) DEFAULT NULL,
  `watch_vod` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `option_id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `option_name` varchar(200) NOT NULL,
  `option_value` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page_layout`
--

CREATE TABLE `page_layout` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `section_title` text COLLATE utf8_bin,
  `section` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ppt`
--

CREATE TABLE `ppt` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ppt_time`
--

CREATE TABLE `ppt_time` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `src` varchar(255) NOT NULL,
  `start` text NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ppt_time_all`
--

CREATE TABLE `ppt_time_all` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `src` varchar(255) NOT NULL,
  `start` text,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `q_id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `uid` int(10) NOT NULL,
  `team_id` varchar(250) DEFAULT NULL,
  `q_question` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `questions_blob` blob,
  `team_name` varchar(255) DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0-Pending,1-Approve,2-Reject',
  `print_status` enum('0','1','2') DEFAULT '0',
  `displayOrNot` varchar(255) DEFAULT 'no',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approve_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `title` text CHARACTER SET latin1 NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `is_send_previous` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT 'no',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_questions`
--

CREATE TABLE `quiz_questions` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `quiz_id` int(11) NOT NULL,
  `title` text CHARACTER SET latin1 NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_objective` int(1) NOT NULL DEFAULT '0',
  `option1` varchar(255) CHARACTER SET latin1 NOT NULL,
  `option2` varchar(255) CHARACTER SET latin1 NOT NULL,
  `option3` varchar(255) CHARACTER SET latin1 NOT NULL,
  `option4` varchar(255) CHARACTER SET latin1 NOT NULL,
  `option5` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `option6` varchar(225) CHARACTER SET latin1 DEFAULT NULL,
  `option7` varchar(225) CHARACTER SET latin1 DEFAULT NULL,
  `correct_option` enum('0','1','2','3','4','5','6','7') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `correct_answer` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_result`
--

CREATE TABLE `quiz_result` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `answer_status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_time`
--

CREATE TABLE `quiz_time` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `total_time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `r_id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `status` char(255) DEFAULT NULL COMMENT '	0-Pending,1-Approve,2-Reject',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registration_field`
--

CREATE TABLE `registration_field` (
  `id` int(10) NOT NULL,
  `event_id` varchar(255) NOT NULL,
  `field_label` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `type` enum('1','2','3','4','5','6','7','8','9') DEFAULT NULL COMMENT '1-text, 2-textarea, 3-radio, 4-checkbox, 5-select, 6-file, 7-password, 8-email, 9-number',
  `options` text,
  `is_required` enum('yes','no') NOT NULL DEFAULT 'no',
  `is_unique` enum('yes','no') NOT NULL DEFAULT 'no',
  `display_question` int(10) NOT NULL DEFAULT '0',
  `display_order` int(10) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `title2` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `slot1` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `slot2` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `meeting_link` varchar(255) DEFAULT NULL,
  `meeting_link2` varchar(255) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `is_slot` varchar(255) DEFAULT NULL,
  `sheet_no` int(11) DEFAULT NULL,
  `is_booked` varchar(255) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `send_mail`
--

CREATE TABLE `send_mail` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `field_name` varchar(255) DEFAULT NULL,
  `content` text,
  `role` varchar(255) DEFAULT NULL COMMENT 'auto_mail, bulk_mail',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `sid` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `speaker` varchar(200) NOT NULL,
  `date` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `firebase_url` varchar(200) NOT NULL,
  `firebase_url2` varchar(200) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`sid`, `title`, `topic`, `speaker`, `date`, `time`, `firebase_url`, `firebase_url2`, `created_date`) VALUES
(1, '', '', '', '', '', 'https://cipla-e4f04-56c85.firebaseio.com/', 'https://cipla-e4f04.firebaseio.com/', '2016-06-16 14:03:46');

-- --------------------------------------------------------

--
-- Table structure for table `speakers`
--

CREATE TABLE `speakers` (
  `id` int(11) NOT NULL,
  `f1` varchar(255) DEFAULT NULL,
  `f2` varchar(255) DEFAULT NULL,
  `f3` varchar(255) DEFAULT NULL,
  `f4` varchar(255) DEFAULT NULL,
  `f5` varchar(255) DEFAULT NULL,
  `f6` varchar(255) DEFAULT NULL,
  `f7` varchar(255) DEFAULT NULL,
  `f8` varchar(255) DEFAULT NULL,
  `f9` varchar(255) DEFAULT NULL,
  `f10` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE `suggestions` (
  `s_id` int(11) NOT NULL,
  `event_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `team_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `suggestion` text CHARACTER SET utf8 COLLATE utf8_bin,
  `status` char(255) DEFAULT '0' COMMENT '0-Pending,1-Approve,2-Reject',
  `print_status` char(255) DEFAULT '0' COMMENT '0-Pending,1-Approve,2-Reject',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `approve_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(10) NOT NULL,
  `event_id` varchar(250) DEFAULT NULL,
  `team_name` varchar(250) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `location` varchar(20) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `is_login` int(10) NOT NULL DEFAULT '0' COMMENT '0=>logout 1=>login',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rbm` varchar(250) DEFAULT NULL,
  `tm` varchar(250) DEFAULT NULL,
  `zbm` varchar(250) DEFAULT NULL,
  `team` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_detail`
--

CREATE TABLE `users_detail` (
  `uid` int(11) NOT NULL,
  `event_id` varchar(255) NOT NULL,
  `is_login` int(10) NOT NULL DEFAULT '0' COMMENT '0=>Logout 1=>Login',
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_feedbacks`
--

CREATE TABLE `users_feedbacks` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `feedback_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `team_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_images`
--

CREATE TABLE `users_images` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `img` text COLLATE utf8_bin,
  `count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users_like`
--

CREATE TABLE `users_like` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `emojis` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_login_detail`
--

CREATE TABLE `users_login_detail` (
  `id` int(10) NOT NULL,
  `uid` int(10) NOT NULL,
  `login_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `watch_archive`
--

CREATE TABLE `watch_archive` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `event_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `vod_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `watch_date` date DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Store History of Watch Archive Session';

-- --------------------------------------------------------

--
-- Table structure for table `webinar`
--

CREATE TABLE `webinar` (
  `id` int(11) NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `video_url` varchar(250) DEFAULT NULL,
  `video_thumb` text,
  `is_home_tab` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0-Not, 1-Yes',
  `tab_details` text,
  `current_ppt` int(11) DEFAULT NULL,
  `current_slide` text,
  `slide_no` int(5) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `archived_session`
--
ALTER TABLE `archived_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `seat_id` (`seat_id`);

--
-- Indexes for table `certificate_download`
--
ALTER TABLE `certificate_download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `cipla_users`
--
ALTER TABLE `cipla_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consent`
--
ALTER TABLE `consent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `contact_dr`
--
ALTER TABLE `contact_dr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `demmy_users`
--
ALTER TABLE `demmy_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `extra_form`
--
ALTER TABLE `extra_form`
  ADD PRIMARY KEY (`fid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_questions`
--
ALTER TABLE `feedback_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_score`
--
ALTER TABLE `game_score`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_zone`
--
ALTER TABLE `game_zone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `live_chat`
--
ALTER TABLE `live_chat`
  ADD PRIMARY KEY (`c_id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `new_users`
--
ALTER TABLE `new_users`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `page_layout`
--
ALTER TABLE `page_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppt`
--
ALTER TABLE `ppt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppt_time`
--
ALTER TABLE `ppt_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppt_time_all`
--
ALTER TABLE `ppt_time_all`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz_id` (`quiz_id`);

--
-- Indexes for table `quiz_result`
--
ALTER TABLE `quiz_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz_id` (`quiz_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `quiz_time`
--
ALTER TABLE `quiz_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`r_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `registration_field`
--
ALTER TABLE `registration_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `send_mail`
--
ALTER TABLE `send_mail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `speakers`
--
ALTER TABLE `speakers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`s_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `users_detail`
--
ALTER TABLE `users_detail`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `users_feedbacks`
--
ALTER TABLE `users_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_images`
--
ALTER TABLE `users_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `users_like`
--
ALTER TABLE `users_like`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `users_login_detail`
--
ALTER TABLE `users_login_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `watch_archive`
--
ALTER TABLE `watch_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `webinar`
--
ALTER TABLE `webinar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `archived_session`
--
ALTER TABLE `archived_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `certificate_download`
--
ALTER TABLE `certificate_download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cipla_users`
--
ALTER TABLE `cipla_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `consent`
--
ALTER TABLE `consent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact_dr`
--
ALTER TABLE `contact_dr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `demmy_users`
--
ALTER TABLE `demmy_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4608;
--
-- AUTO_INCREMENT for table `extra_form`
--
ALTER TABLE `extra_form`
  MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `feedback_questions`
--
ALTER TABLE `feedback_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `game_score`
--
ALTER TABLE `game_score`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `game_zone`
--
ALTER TABLE `game_zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `live_chat`
--
ALTER TABLE `live_chat`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `new_users`
--
ALTER TABLE `new_users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2050;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `page_layout`
--
ALTER TABLE `page_layout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ppt`
--
ALTER TABLE `ppt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ppt_time`
--
ALTER TABLE `ppt_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ppt_time_all`
--
ALTER TABLE `ppt_time_all`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32833;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `quiz_result`
--
ALTER TABLE `quiz_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `quiz_time`
--
ALTER TABLE `quiz_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registration_field`
--
ALTER TABLE `registration_field`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1793;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `seats`
--
ALTER TABLE `seats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `send_mail`
--
ALTER TABLE `send_mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `speakers`
--
ALTER TABLE `speakers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_detail`
--
ALTER TABLE `users_detail`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_feedbacks`
--
ALTER TABLE `users_feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users_images`
--
ALTER TABLE `users_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `users_like`
--
ALTER TABLE `users_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users_login_detail`
--
ALTER TABLE `users_login_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=818;
--
-- AUTO_INCREMENT for table `watch_archive`
--
ALTER TABLE `watch_archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `webinar`
--
ALTER TABLE `webinar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`seat_id`) REFERENCES `seats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `booking_ibfk_3` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `certificate_download`
--
ALTER TABLE `certificate_download`
  ADD CONSTRAINT `certificate_download_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `consent`
--
ALTER TABLE `consent`
  ADD CONSTRAINT `consent_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contact_dr`
--
ALTER TABLE `contact_dr`
  ADD CONSTRAINT `contact_dr_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `extra_form`
--
ALTER TABLE `extra_form`
  ADD CONSTRAINT `extra_form_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game_zone`
--
ALTER TABLE `game_zone`
  ADD CONSTRAINT `game_zone_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `live_chat`
--
ALTER TABLE `live_chat`
  ADD CONSTRAINT `live_chat_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quiz_questions`
--
ALTER TABLE `quiz_questions`
  ADD CONSTRAINT `quiz_questions_ibfk_1` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quiz_result`
--
ALTER TABLE `quiz_result`
  ADD CONSTRAINT `quiz_result_ibfk_1` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quiz_result_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `quiz_questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quiz_result_ibfk_3` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seats`
--
ALTER TABLE `seats`
  ADD CONSTRAINT `seats_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD CONSTRAINT `suggestions_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_images`
--
ALTER TABLE `users_images`
  ADD CONSTRAINT `users_images_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_like`
--
ALTER TABLE `users_like`
  ADD CONSTRAINT `users_like_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `watch_archive`
--
ALTER TABLE `watch_archive`
  ADD CONSTRAINT `watch_archive_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `new_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
