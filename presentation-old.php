<?php include 'config.php';
if(!isset($_SESSION['uid'])){
    header("location:".SITE_URL."index.php");
}
if($_SESSION['event_id'] != EVENT_ID){
    header("location:".SITE_URL."index.php");
    session_destroy();
}

$video_iframe='';
$current_slide = '';
$results = mysql_query("SELECT * FROM webinar WHERE `event_id` = '".EVENT_ID."'");
if(mysql_num_rows($results)>0){
    while ($row = mysql_fetch_object($results)){
        $webinar_id = $row->id;
        $video_iframe = $row->video_url;
        $current_slide = $row->current_slide;
    }
}

$rs = mysql_query("SELECT * FROM `announcement` WHERE `event_id` = '".EVENT_ID."'");
$num_row = mysql_num_rows($rs);
$announce_text = '';
if($num_row){
    $announce = mysql_fetch_assoc($rs);
    $announce_text = $announce['announcement_text'];
}

$team_rs = mysql_query("SELECT * from team WHERE `event_id` = '".EVENT_ID."' ORDER BY `team_name` ASC");

$json_data = file_get_contents("firebase_ajax_setting.json");
if(!empty($json_data)) {
    $json_array = json_decode($json_data);
}else{
    $json_array = array();
}
$firebase_ajax = 1;
if(!empty($json_array)){
    $firebase_ajax = $json_array->firebase_ajax;
}

/*
$one_plus_active_inactive_check_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'one_plus_active_inactive' and `event_id` = '".EVENT_ID."'");
$one_plus_active_inactive_option_data = mysql_fetch_object($one_plus_active_inactive_check_option);
if(mysql_num_rows($one_plus_active_inactive_check_option)>0){
    $one_push_active = $one_plus_active_inactive_option_data->option_value;
}else{
    $one_push_active = 1;
}
*/

$video_ppt_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'video_ppt' and `event_id` = '".EVENT_ID."'");
$video_ppt_data = mysql_fetch_object($video_ppt_option);
if(mysql_num_rows($video_ppt_option)>0){
    $video_or_ppt = $video_ppt_data->option_value;
}else{
    $video_or_ppt = 2;
}
$cursor_image_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'cursor_image' and `event_id` = '".EVENT_ID."'");
$cursor_image_data = mysql_fetch_object($cursor_image_option);
if(mysql_num_rows($cursor_image_option)>0){
    $show_hide_cursor = $cursor_image_data->option_value;
}else{
    $show_hide_cursor = 2;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta http-equiv="x-dns-prefetch-control" content="on"/>
    <meta property="og:type" content="website"/>
    <meta name="og_site_name" property="og:site_name" content="<?=SITE_URL?>"/>
    <meta property="og:url" content="<?=SITE_URL?>"/>
    <meta name="theme-color" content="#000000">

    <title><?= COMPANY_NAME ?> Live Webcast</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap" rel="stylesheet">
    <link href="fonts/font-awesome/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="css/custom.css?v=1">

    <!-- firebase -->
    <script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-database.js"></script>
    <script type="text/javascript" src="assets/js/firebase/firebase2.2.1.js"></script>

    <script src="assets/chart/highcharts.js"></script>
    <script src="assets/chart/highcharts-more.js"></script>
    <script src="assets/chart/modules/exporting.js"></script>

    <style type="text/css">
        html, body {
            height: 100%;
        }
        body {
            background-image: url("img/video-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
            background-position: center;
            background-color: #e2bc83;

            /*max-width: 1366px;
            margin: 0 auto;
            width: auto;*/
        }
        .row-height {
            height: 75%;
        }
        .video-container {
            background-image: url('img/video-frame.png?v=1.2');
            background-repeat: no-repeat;
            background-size: contain;
            margin: 0 14%;
            padding-top: 0;
        }
        .video-container iframe, .video-container img {
            position: absolute;
            top: 10.5%;
            left: 3.75%;
            width: 92%;
            height: 66.75%;
            border-radius: 12px;
        }

        .outer {
            display: table;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
        }
        .middle {
            display: table-cell;
            vertical-align: middle;
        }
        .inner {
            margin: auto;
            width: auto;
            /*whatever width you want*/
        }

        .logoutbtn {
            font-size: medium;
            top: 0;
            right: 15px;

            color: #fff;
            background-color: #680502;
            border-color: #9c361c;
        }

        @media screen and (max-width: 767px) {
            body {
                background-image: url("img/video-mobile-bg.jpg");
            }
            .video-container {
                margin: 0 10%;
            }
            .video-container iframe, .video-container img {
                position: absolute;
                top: 7.75%;
                left: 3.5%;
                width: 83.5%;
                height: 73%;
                border-radius: 12px;
            }
            .outer {
                position: unset;
            }
        }


        ::-webkit-scrollbar-thumb {
            background-color: #939194;
            border: 2px solid transparent;
            border-radius: 5px;
            background-clip: padding-box;
        }

        ::-webkit-scrollbar {
            width: .65rem;
        }
    </style>

</head>
<body class="presentation">
<div class="container-fluid h-100">
    <div class="row">
        <div class="col-md-12 mt-2 p-0">
            <img src="img/video-banner.png" class="img-fluid w-100" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 p-0">
            <div class="notification p-0" style="background: transparent; margin: 0 90px 0 10px">
                <marquee direction="left" onmouseover="this.stop();" onmouseout="this.start();" loop="true">
                    <label id="lbldata" class="mr-0 font-weight-bold" style="letter-spacing: 1.25px; color: #680502"><?= $announce_text;?></label>
                </marquee>
                <a href="logout.php" class="btn theme_button logoutbtn">Logout</a>
            </div>
        </div>
    </div>
    <div class="row row-height mt-2">
        <div class="col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="video-container">
                            <?php /*if (file_exists('img/thank-you.jpg')) { */?><!--
                                <img src="img/thank-you.jpg" class="img-fluid" />
                            --><?php /*} else { */?>
                                <!--<iframe src="https://webcastlive.co.in/player/play_redikpf.php?event_id=<?/*= FOLDER_NAME */?>" width="1280px" height="960px" marginheight="0" frameborder="0" scrolling="no"  allowfullscreen="allowfullscreen"></iframe>-->
                                <iframe src="https://player.vimeo.com/video/653221235?h=207579214c&autoplay=1&loop=1&autopause=0" width="1280px" height="960px" marginheight="0" frameborder="0" scrolling="no"  allowfullscreen="allowfullscreen"></iframe>
                            <?php /*} */?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center">
                            <img src="img/action.png?v=1.35" class="img-fluid w-75" usemap="#image-map" />

                            <map name="image-map">
                                <area class="action" alt="Quiz" title="Quiz" href="javascript:void(0)" coords="82,378,427,434" shape="rect" />
                                <area class="action" alt="Nominate Now" title="Nominees" href="javascript:void(0)" coords="569,377,914,434" shape="rect" />
                                <area class="action" alt="Quotes" title="Quotes" href="javascript:void(0)" coords="208,539,809,1076" shape="rect" />
                            </map>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #feedback_success_msg {
        color: green;
        text-align: center;
    }

    #feedback_error_msg {
        color: red;
        text-align: center;
    }
</style>

<div class="modal" id="FeedbackModel" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Feedback</h5>
                <button type="button" class="close" onclick="closeFeedbackModal()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="feedback_success_msg"></div>
                <div id="feedback_error_msg"></div>
                <div id="FeedbackContent">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="FeedbackResultModel" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Feedback Result</h5>
                <button type="button" class="close" onclick="closeFeedbackResultModal()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="FeedbackResultContent">
                </div>
            </div>
        </div>
    </div>
</div>

<script>var event_id = '<?php echo EVENT_ID;?>'</script>
<script>var firebase_url = '<?php echo FIREBASE_URL;?>'</script>
<script>var firebase_url2= '<?php echo FIREBASE_URL2;?>'</script>
<script>var api_key = '<?php echo API_KEY; ?>'</script>
<script>var firbase_ajax_setting = <?=$firebase_ajax?></script>
<script type="text/javascript" src="assets/js/firebase/app.js?v=2"></script>
<script type="text/javascript" src="assets/js/firebase/pointer.js"></script>
<script type="text/javascript" src="js/imageMapResizer.min.js"></script>
<script type='text/javascript'>
    function openFeedbackResultModal() {
        $("#FeedbackResultContent").html("<div class='center'>Loading...</div>");
        $("#FeedbackResultContent").load("live_feedback_result.php");
        $("#FeedbackResultModel").modal('show');
    }

    function closeFeedbackResultModal() {
        $("#FeedbackResultContent").html("");
        $("#FeedbackResultModel").modal('hide');
    }

    function openFeedbackModal() {
        $("#feedback_success_msg").html("");
        $("#feedback_error_msg").html("");
        $("#FeedbackContent").html("<div class='center'>Loading...</div>");
        $("#FeedbackContent").load("feedback_modal.php");
        $("#FeedbackModel").modal('show');
    }

    function closeFeedbackModal() {
        $("#FeedbackContent").html("");
        $("#FeedbackModel").modal('hide');
    }

    $( document ).ready(function() {
        $('map').imageMapResize();

        $(document).on('click', '.action', function (evt) {
            evt.preventDefault();
            var prefix = $(this).attr('title');
            swal.fire(prefix+' is closed','','success');
        });

        setInterval(function(){ update_logout_time(); }, <?=LOGOUT_TIME_INTERVAL?>);
        update_logout_time();

        $( "#ppt-swap-icon" ).click(function() {
            if($("#video_section").hasClass("col-md-4")){
                $("#ppt-swap-icon").removeClass("swap-icon-img");
                $("#ppt-swap-icon").addClass("swap-to-ppt");

                $("#video_section").removeClass("col-md-4").addClass("col-md-8 order-md-2");
                $("#video_section .question_container").hide();
            }else{
                $("#ppt-swap-icon").addClass("swap-icon-img");
                $("#ppt-swap-icon").removeClass("swap-to-ppt");

                $("#video_section").removeClass("col-md-8 order-md-2").addClass("col-md-4");
                $("#video_section .question_container").show();
            }
            if($("#ppt_section").hasClass("col-md-8")){
                $("#ppt_section").removeClass("col-md-8").addClass("col-md-4 order-md-1");
                $("#ppt_section .question_container").show();
            }else{
                $("#ppt_section").removeClass("col-md-4 order-md-1").addClass("col-md-8");
                $("#ppt_section .question_container").hide();
            }
        });

        if(firbase_ajax_setting == 2) {
            setInterval(function () {
                get_current_slide();
            }, <?=AJAX_CURRENT_SLIDE_INTERVAL?>);
        }

        setInterval(function(){ get_firebase_ajax_setting(); }, <?=FIREBASE_AJAX_SETTING_INTERVAL?>);

        /* Submit Message Form */
        $( ".query_form" ).on( "submit", function( event ) {
            event.preventDefault();
            var self = $(this);
            self.find("#success_msg").html('');
            self.find("#error_msg").html('');
            var error = 0;
            var question = self.find('#q_question').val();

            if(question ==''){
                self.find("#error_msg").html('Please enter question.');
                self.find("#q_question").focus();
                error = 1;
                return false;
            }

            if(error == 0){
                self.find("#success_msg").html('Submitting...');
                $.ajax({
                    type: "POST",
                    async: true,
                    url: "ajax.php",
                    data: $(this).serialize(),
                    cache: false,
                    success: function(result){
                        if(result == 'success'){
                            self.find("#success_msg").html('Successfully Submitted');
                            $(self)[0].reset();
                            setTimeout(function(){
                                self.find("#success_msg").html('');
                            }, 3000);
                        }else{
                            self.find("#success_msg").html('');
                            self.find("#error_msg").html(result);
                        }
                    },
                    error: function( result ) {
                        self.find("#success_msg").html('');
                        self.find('#error_msg').html('Something goes wrong.');
                    }
                });
            }
        });

        function update_logout_time(){
            var send_data = { 'uid': <?=$_SESSION['uid']?>, 'login_session_id': <?=$_SESSION['login_session_id']?> };
            $.ajax({
                type: "POST",
                async: true,
                data:send_data,
                url: "update_logout_time.php",
                cache: false,
                dataType:"json",
                success: function(result){
                    if(result.status == 0){
                        window.location = "index.php";
                    }
                },
                error: function( result ) {

                }
            });
        }

        function get_current_slide(){
            $.ajax({
                type: "POST",
                async: true,
                url: "get_current_slide.php",
                cache: false,
                dataType:"json",
                success: function(result){
                    $("#lbldata").html(result.announcement);
                    var delay_time_out = 0;
                    if(result.delay>0){
                        delay_time_out = result.delay;
                    }
                    setTimeout(function(){

                        var exist_src = $("#video_slide_img").attr("src");
                        if(exist_src != result.src) {
                            console.log("change");
                            $("#video_slide_img").attr("src", result.src);
                        }

                    }, delay_time_out);

                },
                error: function( result ) {

                }
            });
        }

        function get_firebase_ajax_setting(){
            $.ajax({
                type: "POST",
                async: true,
                url: "get_firebase_ajax_setting.php",
                success: function(result){
                    if(firbase_ajax_setting != result){
                        firbase_ajax_setting = result;
                        window.location = "presentation.php";
                    }
                },
                error: function( result ) {

                }
            });
        }

    });
</script>
<?php include 'google_analytics.php'; ?>
</body>
</html>