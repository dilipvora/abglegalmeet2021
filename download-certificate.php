<?php include 'config.php';
if (!isset($_SESSION['uid'])) {
    header("location:" . SITE_URL . "index.php");
}
if (!isset($_GET['name'])) {
    header("location:" . SITE_URL . "presentation.php");
} else {
    if (empty($_GET['name'])) {
        header("location:" . SITE_URL . "presentation.php");
    }
}

header("Content-type: image/jpeg");

$imgPath = 'img/certificate.jpg';
$image = imagecreatefromjpeg($imgPath);
$color = imagecolorallocate($image, 36, 32, 33);
$string = ucwords($_GET['name']);

$fontSize = 28;
$x = (imagesx($image) / 2) - (strlen($string) * 8.5 + strlen($string)); // text center formula
$y = 425;
putenv('GDFONTPATH=' . realpath('.'));
$font = 'open-sans/OpenSans-Bold.ttf';
imagettftext($image, $fontSize, 0, $x, $y, $color, $font, $string);
header('Content-type: image/jpeg');
header('Content-Disposition: attachment; filename="' . $string . '.jpg"');
imagejpeg($image, NULL, 100);
imagedestroy($image);