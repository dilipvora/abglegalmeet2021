<?php
//include 'header.php';
include 'config.php';
$rs_feedback = get_selected_with_where('options',"event_id = '".EVENT_ID."' AND option_name='feedback_type'");
$feedback_id=null;
if(isset($rs_feedback[0]->option_value)){
    $feedback_type = $rs_feedback[0]->option_value;
    if($feedback_type=='feedback'){
        $rs_sel_feedback = get_selected_with_where('options',"event_id = '".EVENT_ID."' AND option_name='feedback_id'");
        $feedback_id = $rs_sel_feedback[0]->option_value;
    }else{
        echo 'No result Found.';die;
    }
}else{
    echo 'No result Found.';die;
}

if(isset($feedback_id)){
    $rs_feedback = get_selected_with_where('feedbacks',"event_id = '".EVENT_ID."' AND id = '{$feedback_id}'");
    $rs_question_feedback = get_selected_with_where('feedback_questions',"feedback_id = '{$feedback_id}' AND is_objective = 1");
    //echo "<pre>"; print_r($rs_question_feedback); exit;
    $questions_results = array();
    foreach ($rs_question_feedback as $key=>$rs_question_feedback_row) {
        $questions_results[$key]['question_id'] = $rs_question_feedback_row->id;
        $questions_results[$key]['total_voting'] = mysql_num_rows(mysql_query("SELECT `id` FROM `users_feedbacks` WHERE `question_id` = '{$questions_results[$key]['question_id']}'"));

        $questions_results[$key]['option1_votting'] = mysql_num_rows(mysql_query("SELECT `id` FROM `users_feedbacks` WHERE `question_id` = '{$questions_results[$key]['question_id']}' AND  `answer` = 1"));
        $questions_results[$key]['option2_votting'] = mysql_num_rows(mysql_query("SELECT `id` FROM `users_feedbacks` WHERE `question_id` = '{$questions_results[$key]['question_id']}' AND  `answer` = 2"));
        $questions_results[$key]['option3_votting'] = mysql_num_rows(mysql_query("SELECT `id` FROM `users_feedbacks` WHERE `question_id` = '{$questions_results[$key]['question_id']}' AND  `answer` = 3"));
        $questions_results[$key]['option4_votting'] = mysql_num_rows(mysql_query("SELECT `id` FROM `users_feedbacks` WHERE `question_id` = '{$questions_results[$key]['question_id']}' AND  `answer` = 4"));

        if($questions_results[$key]['total_voting']){
            $questions_results[$key]['option1_votting_per'] = round(($questions_results[$key]['option1_votting']*100)/$questions_results[$key]['total_voting'],2);
            $questions_results[$key]['option2_votting_per'] = round(($questions_results[$key]['option2_votting']*100)/$questions_results[$key]['total_voting'],2);
            $questions_results[$key]['option3_votting_per'] = round(($questions_results[$key]['option3_votting']*100)/$questions_results[$key]['total_voting'],2);
            $questions_results[$key]['option4_votting_per'] = round(($questions_results[$key]['option4_votting']*100)/$questions_results[$key]['total_voting'],2);
        }else{
            $questions_results[$key]['option1_votting_per'] = 0;
            $questions_results[$key]['option2_votting_per'] = 0;
            $questions_results[$key]['option3_votting_per'] = 0;
            $questions_results[$key]['option4_votting_per'] = 0;
        }
    }
    //echo "<pre>"; print_r($questions_results); exit;
}else{
    echo 'No result Found.';die;
}

?>

 <!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <?php //include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="page-header">
            <div class="page-header-content">
            <div class="page-title" style="text-align: center;">
            <h5><?=$rs_feedback[0]->title;?></h5>
        </div>
    </div>
 </div>

<!-- Content area -->
<div class="content feebbackresultchart">
    <div class="row">
        <?php
        if(!empty($questions_results)) {
            foreach ($questions_results as $key=>$questions_result) {
                ?>
                <div class="col-md-12">
                    <div class="chart_container" id="container<?= $key ?>"></div>
                </div>
                <script type="text/javascript">
                    var chart<?=$key?>;
                    $(function () {
                        chart<?=$key?> = Highcharts.chart('container<?= $key ?>', {
                            credits: {
                                enabled: false
                            },
                            title: {
                                text: '<?=$rs_question_feedback[$key]->title;?>'
                            },

                            tooltip: {
                                pointFormat: '{point.y}%',
                                shared: true
                            },
                            xAxis: {
                                categories: ['<?=$rs_question_feedback[$key]->option1;?>', '<?=$rs_question_feedback[$key]->option2;?>', '<?=$rs_question_feedback[$key]->option3;?>', '<?=$rs_question_feedback[$key]->option4;?>']
                            },
                            yAxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: 'Feedbacks'
                                },
                                labels: {
                                    formatter: function () {
                                        return this.value + "%";
                                    }
                                },
                            },
                            plotOptions: {
                                column: {
                                    dataLabels: {
                                        enabled: true,
                                        // rotation: -90,
                                        shadow: false,
                                        color: '#222',
                                        align: 'center',
                                        format: '{point.y:.1f}%', // one decimal
                                        // y: 10, // 10 pixels down from the top
                                        style: {
                                            fontSize: '16px',
                                            fontFamily: 'arial, sans-serif'
                                        }
                                    }
                                }
                            },
                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: [<?=$questions_result['option1_votting_per']?>, <?=$questions_result['option2_votting_per']?>, <?=$questions_result['option3_votting_per']?>, <?=$questions_result['option4_votting_per']?>],
                                showInLegend: false
                            }]

                        });
                    });
                </script>

                <?php
            }
        }
        ?>
    </div>
</div>
</div>
<!-- /content area -->

</div>
<!-- /Main content -->
</div>
<!-- End Page content -->

</body>
</html>
