<?php include 'header1.php';

$uid = $_SESSION['uid'];
$event_id = EVENT_ID;
$now_time = date('Y-m-d H:i:s');
$workExperienceForm = true;

if (isset($_POST['submit_btn_add'])) {
    $check_exist_query = mysql_query("SELECT `fid` FROM `extra_form` WHERE `uid` = '{$uid}' AND `event_id` = '{$event_id}' LIMIT 1") or die(mysql_error());
    if (mysql_num_rows($check_exist_query)) {
        $workExperienceForm = false;
    } else {
        insert_details("extra_form", "`uid` = '{$uid}', `event_id` = '{$event_id}', `f1` = '{$_POST['f1']}', `f2` = '{$_POST['f2']}', `f3` = '{$_POST['f3']}', `f4` = '{$_POST['f4']}', `role` = 'work experience'");
        $workExperienceForm = false;
        header("location:" . SITE_URL . "welcome.php");
        exit();
    }
} else {
    $check_exist_query = mysql_query("SELECT `fid` FROM `extra_form` WHERE `uid` = '{$uid}' AND `event_id` = '{$event_id}'") or die(mysql_error());
    if (mysql_num_rows($check_exist_query)) {
        $workExperienceForm = false;
    }
}
if ($workExperienceForm === true) { ?>

    <style type="text/css">
        body {
            background-image: url("img/bg.jpg");
            background-position: center;
            background-attachment: fixed;
            background-repeat: no-repeat;
            background-size: cover;
            background-color: #fff;
        }
        .form-control {
            text-align: center;
            background: #901412;
            font-size: 1.15rem;
            letter-spacing: 1.25px;
            color: white;
            border: none;
            border-bottom: 3px solid #c5491f;
            border-bottom-right-radius: 1rem;
            border-bottom-left-radius: 1rem;
        }
        .form-control:focus {
            background: #6A0A06;
            color: white;
        }
        .textarea-div {
            background: #a32421;
            border-radius: 1rem;
        }
        .label {
            text-align: center;
            font-weight: bold;
            font-size: 1.25rem;
            background: #a32421;
            color: white;
            margin-bottom: 0;
            padding: .75rem;
            border-top-right-radius: 1rem;
            border-top-left-radius: 1rem;
        }
        .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle,
        .theme_button {
            font-size: 1.25rem;
            line-height: 1.6;
            letter-spacing: 1.5px;
            background: #ea5725;
            color: #ffffff;
            font-weight: bolder;
            border: none;
            border-bottom: 3px solid #6e090c;
            border-radius: 8px;
        }
        .theme_button:hover, .theme_button:focus, .theme_button:active {
            background: #6e090c;
            color: #ffffff;
            font-weight: bolder;
            border: none;
            border-bottom: 3px solid #ea5725;
        }
        .desktop_img {
            display: block;
        }
        .mobile_img {
            display: none;
        }

        @media screen and (max-width: 767px) {
            .desktop_img {
                display: none;
            }
            .mobile_img {
                display: block;
            }
        }
        @media only screen and (orientation: landscape) and (min-device-width: 480px) and (max-device-width: 1080px) {
            .desktop_img {
                display: block;
            }
            .mobile_img {
                display: none;
            }
        }

        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            color: #ffffff !important;
            font-weight: 500;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            color: #ffffff !important;
            font-weight: 500;
        }
        :-ms-input-placeholder { /* IE 10+ */
            color: #ffffff !important;
            font-weight: 500;
        }
        :-moz-placeholder { /* Firefox 18+ */
            color: #ffffff !important;
            font-weight: 500;
        }
    </style>
<?php } ?>

<div class="container-fluid">
    <?php if ($workExperienceForm === true) { ?>
        <div class="row">
            <?php if (file_exists('img/homeslide.png')) { ?>
                <div class="col-12 col-md-12 p-0">
                    <img src="img/homeslide.png" class="img-fluid desktop_img" style="width: 13vw"/>
                </div>
            <?php }
            if (file_exists('img/thx-banner.png')) { ?>
                <div class="col-12 col-md-12 p-0">
                    <img src="img/thx-banner.png" class="img-fluid w-100 desktop_img"/>
                    <img src="img/thx-mobile-banner.png" class="img-fluid w-100 mobile_img mb-2 mt-1"/>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-12 col-sm-11 col-md-10 col-lg-10 col-xl-10 mx-auto">
                <form method="post" autocomplete="on">
                    <div class="row">
                        <div class="col-12 col-sm-9 col-md-6 col-lg-5 col-xl-5 mx-auto">
                            <div class="form-group textarea-div">
                                <label class="d-block label" for="f1">Within Aditya Birla Group</label>
                                <textarea class="form-control" rows="3" name="f1" id="f1" required></textarea>
                            </div>
                        </div>
                        <div class="col-12 col-sm-9 col-md-6 col-lg-5 col-xl-5 mx-auto">
                            <div class="form-group textarea-div">
                                <label class="d-block label" for="f2">Overall</label>
                                <textarea class="form-control" rows="3" name="f2" id="f2" required></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-9 col-md-9 col-lg-6 col-xl-5 mx-auto">
                            <div class="form-group textarea-div">
                                <label class="d-block label" for="f3">Area/Subject expertise in Legal Domain</label>
                                <textarea class="form-control" rows="3" placeholder="Eg. Corporate Laws, Environmental Law, Litigation,&#10;Contract Management, IPR etc.&#10;(Please Specify)" name="f3" id="f3" required></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-11 col-md-11 col-lg-7 col-xl-7 mx-auto">
                            <div class="form-group textarea-div">
                                <label class="d-block label" for="f4">INTRODUCE ABOUT YOURSELF (MAX 100 WORDS)</label>
                                <textarea class="form-control" rows="3" name="f4" id="f4" required></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group text-right">
                                <input type="submit" name="submit_btn_add" class="btn btn-primary theme_button" value="Submit"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php } else { ?>
        <div class="row">
            <?php if (file_exists('img/thank-you.jpg')) { ?>
                <div class="col-12 col-md-12 p-0 mx-auto text-center">
                    <img src="img/thank-you.jpg" class="img-fluid w-100"/>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
<?php include_once "footer.php"; ?>
</body>
</html>