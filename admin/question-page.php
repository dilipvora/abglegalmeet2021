<?php
include '../config.php';
$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
$next_count = $num_rows + 3;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Question List</title>
	
	<link rel="shortcut icon" href="../../images/favicon-32x32.png" />
	
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	
	<!-- Core JS files -->
	<script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
	<!-- /core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="../../assets/js/pages/datatables_basic.js"></script>
	<script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	
</head>

<body>
	
<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Question</span></h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				
				<div class="panel panel-flat">
					<div class="panel-body">
						<div class="row text-right">
                        <!--<?php
						$hidden_date = date('m/d/Y');
						if(isset($_POST['date']) && $_POST['date'] !='' && $_POST['date'] !='1970-01-01'){
							$hidden_date = $_POST['date'];
						}?>
                            <input type="hidden" name="question_hidden_date" id="question_hidden_date" value="<?php echo $hidden_date;?>">
							<form class="form-inline" role="form" name="view-question-form" method="post" action="" style="display: inline-block;">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar"></i></span>
										<input type="text" name="date" class="form-control datepicker-menus" value="<?php if(isset($_POST['date'])){echo $_POST['date'];}else{ echo date('m/d/Y');}?>" placeholder="Select Date">
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary" name="submit-view-question-form-btn" value="save">View Question</button>
								</div>
							</form>-->
							<a href="export-question.php" class="btn btn-primary">Download Report</a>
						</div>
					</div>
				</div>
						
				<div class="panel panel-flat">
					<!-- datatable-basic -->
					<table class="table" id="question_table1">
                        <thead>
                            <tr>
                                <th class="hidden"></th>
                                <?php
                                $filed_array = array();
                                if($num_rows>0){
                                    while ($row = mysql_fetch_array($form_field_query)){
                                        array_push($filed_array,'u.'.$row['field_name']);?>
                                        <th><?=$row['field_label']?></th><?php
                                    }
                                }
                                ?>
                                <th>Question</th>
                                <th>Team</th>
                                <th>Created Date</th>
                                <th class="hidden"></th>
                            </tr>
                        </thead>
                        <tbody id="mydata">

                        </tbody>
						</table>
						<input type="hidden" name="total_question" id="total_question" value="<?php echo $num_rows;?>">
				</div>
				
				<!-- Footer -->
				<div class="footer text-muted">
					&copy; Copyright 2016 <a href="<?php echo POWERED_BY_URL; ?>" target="_blank"><?php echo POWERED_BY; ?></a> All right reserved.
				</div>
				<!-- /footer -->
				
				<script>
				/*$(".datepicker-menus").datepicker({
				    changeMonth: true,
				    changeYear: true
				});*/
				</script>

			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>
<?php
$fields = implode(',',$filed_array);
?>
<script>
    var question_table;
    var fields = '<?=$fields?>';
    $(document).ready(function () {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [ 5 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        question_table = $("#question_table1").DataTable({
            "ajax": {
                "url": "get_question_data.php?fields="+fields
            },
            "columns": [
                { "data": 0,className: "hidden" },
                <?php $count = 1; for($i=0;$i<$next_count;$i++){ ?>
                { "data": <?=$count?> },
                <?php $count++; } ?>
                { "data": 5 ,className: "hidden" },
            ],
            "ordering": false,
            "lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
        } );
        setInterval( function () {
            question_table.ajax.reload();
        }, 10000);

    });
</script>