<?php include '../config.php';

if(!isset($_SESSION['image_user']) && $_SESSION['image_user'] ==''){
    if(!isset($_SESSION['admin_username']) && $_SESSION['admin_username'] ==''){
        header('location:index.php');
    }
}

if(isset($_POST['is_submit_form']) && $_POST['is_submit_form'] == "is_submit_form"){
    $allowTypes = array('jpg','png','jpeg','gif');
    $images_arr = array();
    foreach($_FILES['images']['name'] as $key=>$val){
        $image_name = $_FILES['images']['name'][$key];
        $tmp_name   = $_FILES['images']['tmp_name'][$key];
        $size       = $_FILES['images']['size'][$key];
        $type       = $_FILES['images']['type'][$key];
        $error      = $_FILES['images']['error'][$key];

        $fileName = basename($_FILES['images']['name'][$key]);
        $targetFilePath = '../img/'.$fileName;

        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
        if(in_array(strtolower($fileType), $allowTypes)){
            if(move_uploaded_file($_FILES['images']['tmp_name'][$key],$targetFilePath)){
                $images_arr[] = $targetFilePath;
            }
        }
    }
    $_SESSION['message'] = "Setting saved successfully.";
}

if(isset($_POST['is_submit_banner_form']) && $_POST['is_submit_banner_form'] == "is_submit_banner_form"){
    $allowTypes = array('jpg','png','jpeg','gif');
    $images_arr = array();

    $image_name = $_FILES['banner_image']['name'];
    $tmp_name   = $_FILES['banner_image']['tmp_name'];
    $size       = $_FILES['banner_image']['size'];
    $type       = $_FILES['banner_image']['type'];
    $error      = $_FILES['banner_image']['error'];

    $fileType = pathinfo($image_name,PATHINFO_EXTENSION);

    $targetFilePath = "../img/banner.".$fileType;

    if(in_array(strtolower($fileType), $allowTypes)){
        if(move_uploaded_file($tmp_name,$targetFilePath)){
            $images_arr[] = $targetFilePath;
        }
    }
    $_SESSION['message'] = "Banner upload successfully.";
}

if(isset($_POST['is_submit_homeslide_form']) && $_POST['is_submit_homeslide_form'] == "is_submit_homeslide_form"){
    $allowTypes = array('jpg','png','jpeg','gif');
    $images_arr = array();

    $image_name = $_FILES['homeslide_image']['name'];
    $tmp_name   = $_FILES['homeslide_image']['tmp_name'];
    $size       = $_FILES['homeslide_image']['size'];
    $type       = $_FILES['homeslide_image']['type'];
    $error      = $_FILES['homeslide_image']['error'];

    $fileType = pathinfo($image_name,PATHINFO_EXTENSION);

    $targetFilePath = "../img/homeslide.".$fileType;

    if(in_array(strtolower($fileType), $allowTypes)){
        if(move_uploaded_file($tmp_name,$targetFilePath)){
            $images_arr[] = $targetFilePath;

            // Homeslide image Update Notification mail sending
            $content = "<p><b>link:&nbsp;</b><a href='" . SITE_URL . "' target='_blank'>" . SITE_URL . "</a></p>
                    <p><b>Date & time:&nbsp;</b>" . date('l d F, Y h:i A') . "</p>";
            include_once "upload_image_send_mail.php";
            upload_image_send_mail("homeslide Image Updated", $content);
        }
    }
    $_SESSION['message'] = "Homeslide upload successfully.";
}

if(isset($_POST['is_submit_thx_form']) && $_POST['is_submit_thx_form'] == "is_submit_thx_form"){
    $allowTypes = array('jpg','png','jpeg','gif');
    $images_arr = array();

    $image_name = $_FILES['thx_image']['name'];
    $tmp_name   = $_FILES['thx_image']['tmp_name'];
    $size       = $_FILES['thx_image']['size'];
    $type       = $_FILES['thx_image']['type'];
    $error      = $_FILES['thx_image']['error'];

    $fileType = pathinfo($image_name,PATHINFO_EXTENSION);

    $targetFilePath = "../img/thank-you.jpg";

    if(in_array(strtolower($fileType), $allowTypes)){
        if(move_uploaded_file($tmp_name,$targetFilePath)){
            $images_arr[] = $targetFilePath;
        }
    }
    $_SESSION['message'] = "Thank you image upload successfully.";
}

$img_dir    = '../img';
$img_files = array_diff(scandir($img_dir), array('..', '.'));

$home_slide_img_dir    = '../../library/homeslider';
$home_slide_img_files = array_diff(scandir($home_slide_img_dir), array('..', '.'));

$banner_img_dir    = '../../library/banner';
$banner_img_files = array_diff(scandir($banner_img_dir), array('..', '.'));

$other_img_dir    = '../../library/other';
$other_img_files = array_diff(scandir($other_img_dir), array('..', '.'));

// Copy Home Slider
if(isset($_POST['type']) && $_POST['type'] == "copy_home_slider"){
    $img = $_POST['img'];
    $name = $_POST['name'];
    copy('../../library/homeslider/'.$img,"../img/".$name);
    $_SESSION['message'] = "Image copy successfully";
}

// Copy Banner Image
if(isset($_POST['type']) && $_POST['type'] == "copy_banner_image"){
    $img = $_POST['img'];
    copy('../../library/banner/'.$img,"../img/".$_POST['name']);
    $_SESSION['message'] = "Image copy successfully";
}

// Copy Other Image
if(isset($_POST['type']) && $_POST['type'] == "copy_other_image"){
    $img = $_POST['img'];
    copy('../../library/other/'.$img,"../img/".$_POST['name']);
    $_SESSION['message'] = "Image copy successfully";
}

// Copy Other Image
if(isset($_POST['type']) && $_POST['type'] == "delete_image"){
    $img = $_POST['img'];
    unlink("../img/".$img);
    $_SESSION['message'] = "Image delete successfully";
}

if(isset($_POST['type']) && $_POST['type'] == "home_banner_content_frm"){
    $content = $_POST['content1'];
    file_put_contents("home_slider.php",$content);
    $_SESSION['message'] = "Setting save successfully";
}
if(file_exists("home_slider.php")){
    $banner_settings = htmlentities(file_get_contents("home_slider.php"));

    //$myfile = fopen("home_slider.php", "r");
    //$banner_settings = fread($myfile,filesize("home_slider.php"));
}else{
    $banner_settings = '';
}
?>
<html>
<head>
    <title>Image Settings</title>
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/ace/ace.js"></script>
    <!-- <script type="text/javascript" src="../../summernote/summernote.min.js"></script>
     <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
     <script type="text/javascript" src="../../assets/js/core/app.js"></script>-->
    <!--   <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>-->
    <style>
        /*.ace_editor {
            height: 400px;
            position: relative;
        }*/
    </style>
</head>
<body>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <div class="content-wrapper">
            <div class="content">
                <br>
                <?php if(isset($_SESSION['message']) && $_SESSION['message'] !=''):?>
                    <div class="alert alert-success alert-bordered">
                        <span class="text-semibold"><?php echo $_SESSION['message'];?></span>
                    </div>
                    <?php unset($_SESSION['message']); ?>
                <?php endif;?>

                <div class="col-md-12">
                    <div class="row">

                        <div class="col-md-4">
                            <form action="" method="post" name="savebannerform" enctype="multipart/form-data" id="savebannerform">
                                <input type="hidden" name="is_submit_banner_form" value="is_submit_banner_form">
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>Upload Banner Image:</label>
                                            <input type="file" name="banner_image" accept="image/jpg, image/jpeg" id="uploadbanner">
                                            <p>Upload only JPG image</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right hidden">
                                    <button type="submit" class="btn btn-primary" name="savebannerformbtn" value="save">Save</button>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-4">
                            <form action="" method="post" name="savehomeslideform" enctype="multipart/form-data" id="savehomeslideform">
                                <input type="hidden" name="is_submit_homeslide_form" value="is_submit_homeslide_form">
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>Upload Homeslide Image:</label>
                                            <input type="file" name="homeslide_image" accept="image/jpg, image/jpeg" id="uploadhomeslide">
                                            <p>Upload only JPG image</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right hidden">
                                    <button type="submit" class="btn btn-primary" name="savehomeslideformbtn" value="save">Save</button>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-4">
                            <form action="" method="post" name="savethximgform" enctype="multipart/form-data" id="savethximgform">
                                <input type="hidden" name="is_submit_thx_form" value="is_submit_thx_form">
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>Upload Thank You Image:</label>
                                            <input type="file" name="thx_image" accept="image/jpg, image/jpeg" id="uploadthximg">
                                            <p>Upload only JPG image</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <?php if ($_SESSION['status'] == 1) { ?>
                            <div class="col-md-4">
                                <form action="" method="post" name="savesettingform" enctype="multipart/form-data" id="savesettingform">
                                    <input type="hidden" name="is_submit_form" value="is_submit_form">
                                    <div class="panel panel-flat">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Upload Banner and Slider Images:</label>
                                                <input type="file" name="images[]" accept="image/*" multiple id="uploadimages">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right hidden">
                                        <button type="submit" class="btn btn-primary" name="savesettingformbtn" value="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <span class="h3 panel-title"><b>List Img Folder</b></>
                            <div class="pull-right">
                                <a href="logout.php" class="btn btn-warning pull-right">Logout</a>
                            </div>
                        </div>
                        <div class="panel-body">

                            <?php if (!empty($img_files)) {
                                foreach ($img_files as $img_row) { ?>
                                    <div class="col-lg-2 col-sm-4">
                                        <div class="thumbnail">
                                            <div class="thumb">
                                                <img src="../img/<?= $img_row ?>?v=<?= rand(200, 100) ?>" width="100%">
                                            </div>

                                            <div class="text-center">
                                                <a href="javascript:void(0)" data-img="<?= $img_row ?>" class="btn btn-danger delete_img">Delete</a>
                                            </div>

                                        </div>
                                    </div>
                                <?php }
                            } ?>

                        </div>
                    </div>
                </div>

                <?php if ($_SESSION['status'] == 1) { ?>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><b>Home Slider Image Library</b></h3>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Thumb</th>
                                            <th>Image Name</th>
                                            <th>Input Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody><?php
                                        if(!empty($home_slide_img_files)) {
                                            $counter = 1;
                                            foreach ($home_slide_img_files as $home_slide_row) { ?>
                                                <tr>
                                                <td><?=$counter++?></td>
                                                <td><img src="../../library/homeslider/<?=$home_slide_row?>?v=<?=rand(200,100)?>" width="150"> </td>
                                                <td><?=$home_slide_row?></td>
                                                <td class="input_name"><input type="text" class="form-control" value="<?=$home_slide_row?>"> </td>
                                                <td><button type="button" name="set_home_slide" class="btn btn-primary home_slide_copy" data-img="<?=$home_slide_row?>">Copy</button> </td>
                                                </tr><?php
                                            }
                                        }else{ ?>
                                            <tr>
                                                <td colspan="5" align="center">No data available in table</td>
                                            </tr><?php
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><b>Banner Image Library</b></h3>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Thumb</th>
                                            <th>Image Name</th>
                                            <th>Input Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody><?php
                                        if(!empty($banner_img_files)) {
                                            $counter = 1;
                                            foreach ($banner_img_files as $banner_row) { ?>
                                                <tr>
                                                <td><?=$counter++?></td>
                                                <td><img src="../../library/banner/<?=$banner_row?>?v=<?=rand(200,100)?>" width="150"> </td>
                                                <td><?=$banner_row?></td>
                                                <td class="input_name"><input type="text" class="form-control" value="<?=$banner_row?>"> </td>
                                                <td><button type="button" name="set_banner" class="btn btn-primary banner_copy" data-img="<?=$banner_row?>">Copy</button> </td>
                                                </tr><?php
                                            }
                                        } else{ ?>
                                            <tr>
                                                <td colspan="5" align="center">No data available in table</td>
                                            </tr><?php
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><b>Other Image Library</b></h3>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Thumb</th>
                                            <th>Image Name</th>
                                            <th>Input Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody><?php
                                        if(!empty($other_img_files)) {
                                            $counter = 1;
                                            foreach ($other_img_files as $other_row) { ?>
                                                <tr>
                                                <td><?=$counter++?></td>
                                                <td><img src="../../library/other/<?=$other_row?>?v=<?=rand(200,100)?>" width="150"> </td>
                                                <td><?=$other_row?></td>
                                                <td class="input_name"><input type="text" class="form-control" value="<?=$other_row?>"> </td>
                                                <td><button type="button" name="set_other_image" class="btn btn-primary other_image_copy" data-img="<?=$other_row?>">Copy</button> </td>
                                                </tr><?php
                                            }
                                        } else{ ?>
                                            <tr>
                                                <td colspan="5" align="center">No data available in table</td>
                                            </tr><?php
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-md-6">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><b>Color Settings</b></h3>
                                </div>
                                <div class="panel-body">
                                    <form id="home_banner_content_frm" action="" method="post">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div id="html_editor"><?=$banner_settings?></div>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success legitRipple save_home_banner_content">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /Content area -->
        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("change", "#uploadbanner", function () {
            $("#savebannerform").submit();
        });

        $(document).on("change", "#uploadhomeslide", function () {
            $("#savehomeslideform").submit();
        });

        $(document).on("change", "#uploadthximg", function () {
            $("#savethximgform").submit();
        });

        /*CKEDITOR.replace('editor1',{
            toolbar: 'Full',
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        });*/

        // $('#editor1').summernote();

        var html_editor = ace.edit("html_editor");
        html_editor.setTheme("ace/theme/monokai");
        html_editor.getSession().setMode("ace/mode/html");
        html_editor.setShowPrintMargin(false);

        $(document).on("change", "#uploadimages", function () {
            $("#savesettingform").submit();
        });

        $(document).on("submit", "#home_banner_content_frm", function (e) {
            e.preventDefault();
            var code = html_editor.getValue();
            var send_data = {"type": "home_banner_content_frm", "content1": code};
            $.ajax({
                url: "upload_images.php",
                type: "POST",
                data: send_data,
                success: function (data) {
                    window.location.href = "upload_images.php";
                }
            });

        });

        $(document).on("click", ".home_slide_copy", function () {
            var value = confirm('Are you sure copy this image?');
            var img = $(this).data("img");
            var name = $(this).closest("tr").find("td.input_name input").val();

            var send_data = {"type": "copy_home_slider", "img": img, "name": name};
            if (value) {
                $.ajax({
                    url: "upload_images.php",
                    type: "POST",
                    data: send_data,
                    success: function (data) {
                        window.location.href = "upload_images.php";
                    }
                });
            }
        });

        $(document).on("click", ".banner_copy", function () {
            var value = confirm('Are you sure copy this image?');
            var img = $(this).data("img");
            var name = $(this).closest("tr").find("td.input_name input").val();
            var send_data = {"type": "copy_banner_image", "img": img, "name": name};
            if (value) {
                $.ajax({
                    url: "upload_images.php",
                    type: "POST",
                    data: send_data,
                    success: function (data) {
                        window.location.href = "upload_images.php";
                    }
                });
            }
        });

        $(document).on("click", ".other_image_copy", function () {
            var value = confirm('Are you sure copy this image?');
            var img = $(this).data("img");
            var name = $(this).closest("tr").find("td.input_name input").val();
            var send_data = {"type": "copy_other_image", "img": img, "name": name};
            if (value) {
                $.ajax({
                    url: "upload_images.php",
                    type: "POST",
                    data: send_data,
                    success: function (data) {
                        window.location.href = "upload_images.php";
                    }
                });
            }
        });

        $(document).on("click", ".delete_img", function () {
            var value = confirm('Are you sure delete this image?');
            var img = $(this).data("img");
            var send_data = {"type": "delete_image", "img": img};
            if (value) {
                $.ajax({
                    url: "upload_images.php",
                    type: "POST",
                    data: send_data,
                    success: function (data) {
                        window.location.href = "upload_images.php";
                    }
                });
            }
        });
    });
</script>

</body>
</html>