<?php include 'header.php'; 

if(isset($_POST['save_form_field_data'])){
	
	$type = $_POST['type'];
	$label = $_POST['label'];
	$name = $_POST['name'];
	$options = $_POST['options'];
	$is_required = $_POST['is_required'];
	$is_unique = $_POST['is_unique'];
	$display_order = $_POST['display_order'];
	$display_question = $_POST['display_question'];
	$event_id = EVENT_ID;
	$date = date('Y-m-d H:i:s');
	
	$sql = "INSERT INTO `registration_field` SET
							`event_id` = '{$event_id}',
							`field_label` = '{$label}',
							`field_name` = '{$name}',
							`type` = '{$type}',
							`created_date` = '{$date}',
							`display_order` = '{$display_order}',
							`is_required` = '{$is_required}',
							`is_unique` = '{$is_unique}',
							`display_question` = '{$display_question}',
							`options` = '{$options}'";
		$res = mysql_query($sql);
		
	if($res){
		$_SESSION['success_msg'] = "Field saved successfully.";
	}else{
		$_SESSION['error_msg'] = "Something goes wrong try again.";
	}	
}

?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add Form Field</span></h4>
						</div>
					</div>
				</div>
				<!-- /page header -->
				
			<!-- Content area -->
			<div class="content">

				<?php include 'messages.php';?>
				
				<div class="row">
					<div class="col-md-6">
						<form action="" method="post" name="savesettingform">
							
	                        <br/>
							<div class="panel panel-flat">
				                <div class="panel-body">
									<div class="form-group">
										<label>Type</label>
										<select name="type" class="form-control" required>
											<option value="">Select</option>
											<option value="1">Textbox</option>
											<option value="2">Textarea</option>
											<option value="3">Radio</option>
											<option value="4">Checkbox</option>
											<option value="5">Select</option>
											<option value="6">Number</option>
										<select>
									</div>
									<div class="form-group">
										<label>Label</label>
										<input type="text" name="label" value="" placeholder="Enter Label" required class="form-control">
									</div>
									<div class="form-group">
										<label>Name</label>
                                        <select name="name" class="form-control" required="">
                                            <option value="">Select</option>
                                            <option value="f1">F1</option>
                                            <option value="f2">F2</option>
                                            <option value="f3">F3</option>
                                            <option value="f4">F4</option>
                                            <option value="f5">F5</option>
                                            <option value="f6">F6</option>
                                            <option value="f7">F7</option>
                                            <option value="f8">F8</option>
                                            <option value="f9">F9</option>
                                            <option value="f10">F10</option>
                                            <option value="f11">F11</option>
                                            <option value="f12">F12</option>
                                            <option value="f13">F13</option>
                                            <option value="f14">F14</option>
                                            <option value="f15">F15</option>
                                        </select>
									</div>
									<div class="form-group">
										<label>Options</label>
										<textarea name="options" value="" placeholder="Enter Options(comma separated)" class="form-control"></textarea>
									</div>
									<div class="form-group">
										<label>Display Order</label>
										<input type="text" name="display_order" value="" placeholder="Display Order" class="form-control" required></textarea>
									</div>
									<div class="form-group">
                                        <label>Is Required </label> <br>
                                        <label class="radio-inline"><input type="radio" name="is_required" value="yes" checked="" required>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="is_required" value="no" required>No</label>
                                    </div>
									<div class="form-group">
                                        <label>Is Unique </label> <br>
                                        <label class="radio-inline"><input type="radio" name="is_unique" value="yes" checked="" required>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="is_unique" value="no" required>No</label>
                                    </div>
                                    <div class="form-group">
                                        <label>Display in Question</label> <br>
                                        <label class="radio-inline"><input type="radio" name="display_question" value="1" checked="" required>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="display_question" value="0" required>No</label>
                                    </div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" name="save_form_field_data" value="save">Save</button>
										<a href="form-fields.php" class="btn btn-danger">Cancel</a>
									</div>
				                </div>
				            </div>
							
			            </form>
					</div>
					</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /Content area -->
		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>
		