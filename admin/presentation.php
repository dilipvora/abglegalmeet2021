<?php include 'header.php';
other_presentation_page_not_access_page();

$webinar_id='';
$video_url='';
$video_ppt='';
$is_home_tab=0;
$tab_details='';
$current_slide = '';
$current_ppt = '';
$slide_no = 1;

$directory = "../upload/ppt/";
$images_array = glob($directory . "*.*");


/*if(isset($_POST['VideoUrlFormBtn'])){
	if(isset($_POST['video_url']) && $_POST['video_url'] !=''){
		$video_url = $_POST['video_url'];
		$webinar_id = $_POST['webinar_id'];

		if($webinar_id !=''){
			mysql_query("UPDATE `webinar` SET
					`video_url` = '{$video_url}'
					WHERE `id`= '{$webinar_id}' ");
		}else{
		mysql_query("INSERT INTO `webinar` SET
				`video_url` = '{$video_url}'");
		}
		$_SESSION['success_msg'] = 'You have updated video url successfully.';
	}else{
		$_SESSION['error_msg'] = 'Please enter video url.';
	}
}*/

/*if(isset($_POST['VideoTabDetailsFormBtn'])){
	
	if($_POST['is_home_tab']){
		$is_hometab = 1;
	}else{
		$is_hometab = 0;
	}
	
	$tabdetails = serialize($_POST['tab']);
	
	$webinarId = $_POST['webinar_id'];
	
	if($webinarId ==''){
		mysql_query("INSERT INTO `webinar` SET `is_home_tab` = '{$is_hometab}', `tab_details` = '{$tabdetails}'");
	}else{
		mysql_query("UPDATE `webinar` SET `is_home_tab` = '{$is_hometab}', `tab_details` = '{$tabdetails}' WHERE `id`= '{$webinarId}'");
	}
	
	$_SESSION['success_msg'] = 'You have updated tab details successfully.';
	
}*/

$results = mysql_query("SELECT * FROM webinar WHERE `event_id` = '".EVENT_ID."'");

if(mysql_num_rows($results)>0){
    $row = mysql_fetch_object($results);
    $webinar_id = $row->id;
    $video_url = $row->video_url;
    $video_ppt = $row->video_thumb;
    //$is_home_tab = $row->is_home_tab;
    //$tab_details = unserialize($row->tab_details);
    $current_ppt = $row->current_ppt;
    $current_slide = $row->current_slide;
    $slide_no = $row->slide_no;

    /* if($current_slide ==''){
        $current_slide = $images_array[0];
    } */

    $images_array = glob($directory.$current_ppt.'/'. "*.*");

    if($current_slide ==''){
        $current_slide = $images_array[0];
    }

}

$event_lable = "Start Event";
if(isset($_SESSION['event_start']) && $_SESSION['event_start'] == 1) {
    $event_lable = "Stop Event";
}

$cursor_image_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'cursor_image' and `event_id` = '".EVENT_ID."'");
$cursor_image_data = mysql_fetch_object($cursor_image_option);
if(mysql_num_rows($cursor_image_option)>0){
    $show_hide_cursor = $cursor_image_data->option_value;
}else{
    $show_hide_cursor = 2;
}

?>
<div class="page-container">
    <div class="page-content">
        <?php //include 'sidebar.php';?>
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4 class="col-md-6"><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Presentation</span></h4>
                        <br>
                    </div>
                </div>
            </div>

            <div class="content">
                <?php include 'messages.php';?>
                <div class="row">

                    <div class="col-md-8">
                        <!-- Add Video PPT -->
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <?php
                                function js_str($s)
                                {
                                    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
                                }

                                function js_array($array)
                                {
                                    $temp = array_map('js_str', $array);
                                    return '['.implode(',',$temp).']';
                                }
                                $total_slide = sizeof($images_array);
                                $images = js_array($images_array);
                                ?>

                                <div class="video_slide_section">
                                    <div id="slide_msg"></div>

                                    <div class="col-md-4 text-left">
                                        <input type="image" id="prev_image" src="../../assets/images/previous.png" >
                                    </div>

                                    <div class="col-md-4 text-center">
                                        <h4 id="slide_img_name">Slide <span><?php echo $slide_no;?></span></h4>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <input type="image" id="next_image" src="../../assets/images/next.png" >
                                    </div>

                                    <input type="hidden" id="img_no" value="<?php echo $slide_no - 1;?>">
                                    <div id="slide_cont">
                                        <img src="../<?php echo $current_slide;?>" id="slideshow_image" class="img-thumbnail">
                                        <?php if($show_hide_cursor == 1){ ?>
                                            <img src="" id="cursor_image" style="max-width: 15px">
                                        <?php } ?>
                                    </div>

                                    <div class="video_slide_pagination">
                                        <ul class="pagination">
                                            <?php if($total_slide > 0):
                                                for($i = 1; $i <= $total_slide;$i++){
                                                    $active = '';
                                                    if($slide_no == $i){$active = "active";}
                                                    echo '<li class="'.$active.'" data-no="'.$i.'"><a href="javascript:void(0)">'.$i.'</a></li>';
                                                }
                                            endif;?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Add Video PPT -->
                    </div>
                    <div class="col-md-4">

                        <!--<div class="panel panel-flat cursor_image_section">
                            <div class="panel-body">
                                <div class="div-section">


                                    <table style="display: none">
                                        <tr><td><strong>SDP URL</strong></td><td><input type="hidden" id="sdpURL" size="50" value="wss://57dd49d3b78f6.streamlock.net/webinarlive-session.json"/></td></tr>
                                        <tr><td><strong>Application Name</strong></td><td><input type="hidden" id="applicationName" size="25" value="webinarlive"/></td></tr>
                                        <tr><td><strong>Stream Name</strong></td><td><input type="hidden" id="streamName" size="25" value="<?=EVENT_ID?>"/></td></tr>
                                        <tr><td><strong>Video Bitrate</strong></td><td><input type="hidden" id="videoBitrate" size="10" value=""/>&nbsp;kbps</td></tr>
                                        <tr><td><strong>Audio Bitrate</strong></td><td><input type="hidden" id="audioBitrate" size="10" value=""/>&nbsp;kbps</td></tr>
                                        <tr><td><strong>FrameRate(*)</strong></td><td><input type="hidden" id="videoFrameRate" size="10" value=""/>&nbsp;kbps</td></tr>
                                        <tr><td><strong>Video Codec</strong></td><td>
                                                <select name="videoChoice" id="videoChoice">
                                                    <option value="42e01f" selected="selected">H264</option>
                                                    <option value="VP9">VP9</option>
                                                </select></td></tr>

                                        <tr><td><strong>Audio Codec</strong></td><td>
                                                <select type="text" id="audioChoice">
                                                    <option value="opus" selected="selected">Opus</option>
                                                </select></td></tr>
                                        <tr><td><font size="1">See readme for details on FrameRate compatibility</td></tr>
                                        <input type="hidden" id="userAgent" name="userAgent" value="" />

                                    </table>
                                </div>
                                <div class="div-section">
                                    <video id="localVideo" autoplay muted style="width:100%;"></video>
                                </div>

                                <script type="text/javascript">
                                    document.getElementById("userAgent").value = navigator.userAgent;
                                    pageReady();
                                </script>

                                <div>
                                    <span id="sdpDataTag"></span>
                                </div>
                                <hr/>
                                <input type="button" class="btn btn-success" id="buttonGo" name="buttonGo" value="Start Webinar" onclick="start()"/>
                                <p>
                                <div id="StreamDisconnect" class="alert alert-warning hide">Disconnected, Something went wrong!</div>
                                <div id="StreamConnect" class="alert alert-success hide">Connected</div>
                                </p>
                            </div>
                        </div>

                        <!-- Select Cursor Images-->
                        <div class="panel panel-flat cursor_image_section">
                            <?php if($show_hide_cursor == 1){ ?>
                                <div class="panel-body" id="cursor-container">
                                    <?php $cursor_directory = "../../assets/images/cursor-img/";
                                    $cursor_images_array = glob($cursor_directory . "*.*");
                                    foreach($cursor_images_array as $filename){
                                        ?>
                                        <span style="width:40px; height: 40px; display: table-cell;text-align: center;vertical-align: middle;">
                                            <img src="../../assets/images/cursor-img/<?=basename($filename)?>" class="select_cursor_image" style="max-width: 100%;">
                                        </span>
                                    <?php }	?>

                                    <hr/>
                                </div>
                            <?php } ?>
                            <!-- Select PPT -->
                            <div class="panel-body">
                                <form name="select-ppt-form" id="select-ppt-form" method="post" action="process.php">
                                    <input type="hidden" name="submit-select-ppt-form" value="true">
                                    <input type="hidden" name="webinar_id" value="<?php echo $webinar_id;?>">
                                    <div class="form-group">
                                        <label>Select PPT</label>
                                        <?php $rs = mysql_query("SELECT * FROM `ppt` WHERE `event_id` = '".EVENT_ID."' ORDER BY id DESC");
                                        $num_rows = mysql_num_rows($rs);?>
                                        <select name="ppt-id" id="select-ppt-id" class="form-control">
                                            <option value="">Select PPT</option>
                                            <?php if($num_rows){
                                                $srno = 1;
                                                while ($row = mysql_fetch_object($rs)){?>
                                                    <option value="<?php echo $row->id;?>" <?php if($current_ppt == $row->id){?>selected="selected"<?php }?>><?php echo $row->name;?></option>
                                                <?php }
                                            }?>
                                        </select>
                                    </div>
                                    <input type="submit" class="hidden" name="submit-select-ppt-form-btn" value="select ppt"/>
                                </form>
                                <!-- End Select PPT -->

                                <hr/>
                                <input type="button" class="btn btn-primary" id="StartEvent" name="StartEvent" value="<?php echo $event_lable; ?>" onclick="start_event();"/>
                            </div>
                        </div>
                        <!-- End Select Cursor Images-->
                    </div>
                </div>
                <?php include 'footer.php';?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var images = <?php echo $images;?>;
    var baseurl = '<?php echo SITE_URL;?>';

    var firebase_redik_webinar = new Firebase(firebase_url);
    var firebase_current_slide = firebase_redik_webinar.child('current_slide');

    $(document).ready(function(){

        $( "#prev_image" ).click(function(){
            prev();
        });
        $( "#next_image" ).click(function(){
            next();
        });

        $( ".video_slide_pagination .pagination li" ).click(function(){
            var no = $(this).attr('data-no');
            pagination(no);
            $(this).addClass('active').siblings().removeClass('active');
        });

    });

    function prev()
    {
        $('#slide_msg').html('');
        var prev_val = document.getElementById( "img_no" ).value;
        var prev_val = Number(prev_val) - 1;

        if(prev_val<= -1)
        {
            $('#slide_msg').html('<div class="alert alert-danger alert-bordered">No previous image found</div>');
            //prev_val = images.length - 1;
        } else {
            var img = images[prev_val].replace('../', '');
            ajaxcall(img,(prev_val + 1));
            $( '#slideshow_image' ).attr( 'src' , baseurl+img );
            $('#slide_img_name span').html(prev_val + 1);
            document.getElementById( "img_no" ).value = prev_val;

            var pagination = prev_val + 1;
            $(".video_slide_pagination .pagination li[data-no='"+pagination+"']").addClass('active').siblings().removeClass('active');

            firebase_current_slide.set({ src: baseurl+img});
        }
    }

    function next()
    {
        $('#slide_msg').html('');
        var next_val = document.getElementById( "img_no" ).value;
        var next_val = Number(next_val)+1;
        if(next_val >= images.length)
        {
            $('#slide_msg').html('<div class="alert alert-danger alert-bordered">No next image found</div>');
            //next_val = 0;
        }else{
            img = images[next_val].replace('../', '');
            ajaxcall(img,(next_val + 1));
            $( '#slideshow_image' ).attr( 'src' , baseurl+img);
            $('#slide_img_name span').html(next_val + 1);
            document.getElementById( "img_no" ).value = next_val;

            var pagination = next_val + 1;
            $(".video_slide_pagination .pagination li[data-no='"+pagination+"']").addClass('active').siblings().removeClass('active');
            firebase_current_slide.set({ src: baseurl+img});
        }
    }

    function pagination(no)
    {
        $('#slide_msg').html('');
        img = images[no - 1].replace('../', '');
        ajaxcall(img,(no));
        $( '#slideshow_image' ).attr( 'src' , baseurl+img);
        $('#slide_img_name span').html(no);
        document.getElementById( "img_no" ).value = no - 1;
        firebase_current_slide.set({ src: baseurl+img});
    }

    function ajaxcall(slide,slide_no){
        var dataString = "action=set_current_slide&webinar_id=<?php echo $webinar_id;?>&slide=" + slide+"&slide_no="+slide_no;
        $.ajax({
            type: "POST",
            async: true,
            url: "../ajax.php",
            data: dataString,
            cache: false,
            success: function(result){
                if(result == 'success'){
                }else{
                    $("#slide_msg").html(result);
                }
            },
            error: function( result ) {
                $('#slide_msg').html('Something goes wrong.');
            }
        });
    }
    function start_event(){

        var def_slide = "upload/ppt/<?php echo $current_ppt;?>/Slide001.JPG";
        ajaxcall(def_slide,'1')
        $( '#slideshow_image' ).attr( 'src' , '../'+def_slide);
        $('#slide_img_name span').html(1);
        document.getElementById( "img_no" ).value = 0;
        $(".video_slide_pagination .pagination li:first-child").addClass('active').siblings().removeClass('active');

        var dataString = "action=start_event&slide="+def_slide;
        $.ajax({
            type: "POST",
            async: true,
            url: "../ajax.php",
            data: dataString,
            cache: false,
            success: function(result){
                //$('#slide_msg').html(result);
                $("#StartEvent").val(result);
            },
            error: function( result ) {
                $('#slide_msg').html('Something goes wrong.');
            }
        });

    }

    /* Upload PPT */
    $( "#upload-ppt-btn" ).click(function(){
        $("#upload-ppt-form #ppt").click();
    });

    $( "#upload-ppt-form #ppt" ).change(function(){
        $("#upload-ppt-form").submit();
    });

    /* Select PPT */
    $( "#select-ppt-id" ).change(function(){
        $("#select-ppt-form").submit();
    });
</script>
</body>
</html>