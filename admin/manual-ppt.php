<?php

include 'header.php';

?>
<div class="page-container">
    <div class="page-content">
        <?php include 'sidebar.php';?>
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4 class="col-md-6"><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Upload PPT Images</span></h4>

                        <br>
                    </div>
                </div>
            </div>

            <div class="content">
                <?php include 'messages.php';?>
                <div class="row">

                    <div class="col-md-4">
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <form name="upload-ppt-form" id="upload-ppt-form" method="post" action="process.php" enctype="multipart/form-data" class="">
                                    <input type="hidden" name="submit-select-ppt-image" value="true">
                                    <div class="form-group hidden">
                                        <label>Upload PPT</label>
                                        <input type="file" name="images[]" id="ppt_images" class="form-control" accept="image/jpeg,image/jpg" value="" multiple required="required">
                                    </div>
                                    <div class="text-right hidden">
                                        <button type="submit" class="btn btn bg-indigo-700" name="submit-upload-ppt-form-btn" value="upload">Upload PPT Image</button>
                                    </div>
                                    <div class="form-group">
                                        <label>Select PPT</label>
                                        <?php $rs = mysql_query("SELECT * FROM `ppt` WHERE `event_id` = '".EVENT_ID."' ORDER BY id DESC");
                                        $num_rows = mysql_num_rows($rs);?>
                                        <select name="ppt-id" id="select-ppt-id" class="form-control">
                                            <option value="">Select PPT</option>
                                            <?php if($num_rows){
                                                $srno = 1;
                                                while ($row = mysql_fetch_object($rs)){?>
                                                    <option value="<?php echo $row->id;?>" <?php if($current_ppt == $row->id){?>selected="selected"<?php }?>><?php echo $row->name;?></option>
                                                <?php }
                                            }?>
                                        </select>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn bg-indigo-700" name="upload-ppt-btn" id="upload-ppt-btn" value="upload">Upload PPT Image</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <h3>Create PPT</h3>
                                <form name="create_ppt_folder" id="create_ppt_folder" method="post" action="process.php" enctype="multipart/form-data" class="">
                                    <input type="hidden" name="create_ppt_folder" value="true">
                                    <div class="form-group">
                                        <label>PPT Name</label>
                                        <input type="text" name="folder_name" class="form-control" placeholder="Enter PPT Name" required>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn bg-indigo-700" name="submit-create-ppt-form-btn" value="upload">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'footer.php';?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var baseurl = '<?php echo SITE_URL;?>';

    $( "#upload-ppt-btn" ).click(function(){
        var ppt_id = $("#select-ppt-id").val();
        if(ppt_id != "") {
            $("#upload-ppt-form #ppt_images").click();
        }else{
            console.log("else");
        }
    });

    $( "#upload-ppt-form #ppt_images" ).change(function(){
        $("#upload-ppt-form").submit();
        $("#load_images").show();
    });
</script>
</body>
</html>