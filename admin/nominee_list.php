<?php include 'header.php';
$form_field_query = mysql_query("SELECT * from registration_field where event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);


if(isset($_GET['delete']) && isset($_GET['fid']) && $_GET['fid'] !=''){
    $fid = $_GET['fid'];
    $sql = "delete from extra_form where fid = '{$fid}' AND `event_id` = '".EVENT_ID."'";
    mysql_query($sql);
    $_SESSION['success_msg'] = 'Nominee deleted successfully.';
    echo '<script type="text/javascript">window.location.href="nominee_list.php";</script>';
    exit();
} ?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span></h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">
					<!--<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row text-right">
								<a href="export-user.php" class="btn btn-primary">Download Report</a>
							</div>
						</div>
					</div>-->
						
					<!-- Dashboard content -->
					<div class="panel panel-flat">
						<table class="table" id="nominee_table">
							<thead>
								<tr>
                                    <?php
/*                                    $field_array = array();
									if($num_rows>0){
									    $count = 0;

										while($row = mysql_fetch_object($form_field_query)){
                                            $field_array['data'][] = $count;
                                            $count++;*/?><!--
											<th><?/*=$row->field_label*/?></th>--><?php
/*										}
									} */?>
                                   <th>Image</th>
                                   <th>Action</th>
                                   <th>Name</th>
                                   <th>Email</th>
                                   <th>Contact no.</th>
                                   <th>Business</th>
                                   <th>Team</th>
                                   <th>Description</th>
                                   <th>Role</th>
                                   <th>Delete</th>
								</tr>
							</thead>
                            <tbody id="mydata">
							</tbody>
						</table>
						
						<input type="hidden" name="total_users" id="total_users" value="<?php echo $num_rows;?>">
					</div>
					<!-- /default ordering -->
					<!-- /dashboard content -->
					<?php include 'footer.php';?>

				</div>
				<!-- /content area -->

			</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>

<script>
    var question_table;
    $(document).ready(function () {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [ 3 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        var user_table = $("#nominee_table").DataTable({
 		   "processing": true,
		   "ajax": {
	            "url": "process.php?action=get_nominee",
 	            "type": "POST"
	        },
	        "columns": [
	            <?php
/*                if(!empty($field_array['data'])){
                    foreach ($field_array['data'] as $field_item) { */?>/*
                            { "data": <?/*=$field_item*/?>}, */<?php
/*                    }
                } */?>
                { "data": "f1" },
                { "data": "f2" },
                { "data": "f3" },
                { "data": "f4" },
                { "data": "f5" },
                { "data": "f6" },
                { "data": "f7" },
                { "data": "f8" },
                { "data": "role" },
                { "data": "fid" },

	        ],
	        "ordering": false,
			"lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
		});
		$("#nominee_table_length select").select2();
	});
</script>