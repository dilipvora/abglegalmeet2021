<?php include 'header.php';
$created_date = date('Y-m-d H:i:s');
$event_id = EVENT_ID;

$get_event_id_query = mysql_query("SELECT `id`, `event_id` FROM `registration_field` WHERE `event_id` != '{$event_id}' GROUP BY `event_id` ORDER BY `id` DESC");

if(isset($_GET['delete']) && isset($_GET['id']) && $_GET['id'] !=''){
	$id = $_GET['id'];
	$sql = "delete from registration_field where id = '{$id}' AND `event_id` = '".EVENT_ID."'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'Field deleted successfully.';
	echo '<script>window.location = "form-fields.php"</script>';
}

if (isset($_POST["copy_field_btn"])) {
    $fetch_fields_query = mysql_query("SELECT * FROM `registration_field` WHERE `event_id` = '{$_POST["get_event_id"]}'");
    if (mysql_num_rows($fetch_fields_query)) {
        // Delete Data
        mysql_query("DELETE FROM `registration_field` WHERE `event_id` = '{$event_id}'");

        while ($field_row = mysql_fetch_object($fetch_fields_query)) {
            $field_label = mysql_real_escape_string($field_row->field_label);
            $field_name = mysql_real_escape_string($field_row->field_name);
            $type = mysql_real_escape_string($field_row->type);
            $options = !empty($field_row->options) ? mysql_real_escape_string(addslashes($field_row->options)) : null;
            $is_required = mysql_real_escape_string($field_row->is_required);
            $is_unique = mysql_real_escape_string($field_row->is_unique);
            $display_question = mysql_real_escape_string($field_row->display_question);
            $display_order = mysql_real_escape_string($field_row->display_order);

            // Data Insert
            mysql_query("INSERT INTO `registration_field` SET `event_id` = '{$event_id}', `field_label` = '{$field_label}', `field_name` = '{$field_name}',
                `type` = '{$type}', `options` = '{$options}', `is_required` = '{$is_required}', `is_unique` = '{$is_unique}', `display_question` = '{$display_question}',
                `display_order` = '{$display_order}', `created_date` = '{$created_date}'");
        }
    }
    echo '<script>window.location = "form-fields.php"</script>';
}

if(isset($_GET['set_default_field'])){
    $field_name_array = array("f1","f2","f3","f4");
    $field_label_array = array("Name","Email","Place","Mobile");
    $field_type_array = array("1","1","1","6");
    $field_required_array = array("yes","yes","yes","no");
    $field_unique_array = array("no","yes","no","no");
    $field_question_array = array("1","0","1","0");
    $field_display_order_array = array("1","2","3","4");

    $event_id = EVENT_ID;
    $date = date('Y-m-d H:i:s');


    foreach ($field_name_array as $key=>$item) {

        $label = $field_label_array[$key];
        $name = $item;
        $type = $field_type_array[$key];
        $display_order = $field_display_order_array[$key];
        $is_required = $field_required_array[$key];
        $is_unique = $field_unique_array[$key];
        $display_question = $field_question_array[$key];
        $options = '';
        $sql = "INSERT INTO `registration_field` SET
							`event_id` = '{$event_id}',
							`field_label` = '{$label}',
							`field_name` = '{$name}',
							`type` = '{$type}',
							`created_date` = '{$date}',
							`display_order` = '{$display_order}',
							`is_required` = '{$is_required}',
							`is_unique` = '{$is_unique}',
							`display_question` = '{$display_question}',
							`options` = '{$options}'";
        $res = mysql_query($sql);
    }
    echo '<script>window.location = "form-fields.php"</script>';
}

?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Form Fields</span></h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">
					<?php include 'messages.php';?>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="text-left">
                                        <form method="post" name="copy_fields">
                                            <div class="form-group input-group">
                                                <select name="get_event_id" id="select" class="select" required>
                                                    <option value="">Select Event</option>
                                                    <?php if (mysql_num_rows($get_event_id_query)) {
                                                        while ($row = mysql_fetch_object($get_event_id_query)) { ?>
                                                            <option value="<?=$row->event_id?>"><?=$row->event_id?></option>
                                                        <?php }
                                                    } ?>
                                                </select>
                                                <span class="input-group-btn">
                                                <button type="submit" name="copy_field_btn" class="btn btn-primary">Copy Fields</button>
                                            </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="text-right">
                                        <a href="add-form-field.php" class="btn btn-primary">Add New</a>
                                        <a href="javascript:void(0)" onClick="if(confirm('Would You Like To Create Default Field?')){self.location='?set_default_field';}" class="btn btn-primary">Set Default Fields</a>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
						
					<!-- Dashboard content -->
					<div class="panel panel-body">
                        <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th class="hidden"></th>
                                        <th>Type</th>
                                        <th>Label</th>
                                        <th>Name</th>
                                        <th>Options</th>
                                        <th>Is Required</th>
                                        <th>Is Unique</th>
                                        <th>Display In Question</th>
                                        <th>Display Order</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="mydata">
                                    <?php
                                    $rs = mysql_query("SELECT * FROM registration_field WHERE event_id = '".EVENT_ID."' ORDER BY display_order ASC");
                                    $num_rows = mysql_num_rows($rs);
                                    if($num_rows){
                                        while ($row = mysql_fetch_object($rs)){
                                            $type = '';
                                            if($row->type == "1"){
                                                $type = "Textbox";
                                            }elseif($row->type == "2"){
                                                $type = "Textarea";
                                            }elseif($row->type == "3"){
                                                $type = "Radio";
                                            }elseif($row->type == "4"){
                                                $type = "Checkbox";
                                            }elseif($row->type == "5"){
                                                $type = "Select";
                                            }elseif($row->type == "6"){
                                                $type = "Number";
                                            }

                                            if($row->display_question == 1){
                                                $display_question = "Yes";
                                            }else{
                                                $display_question = "No";
                                            }

                                            ?>
                                            <tr>
                                                <td class="hidden"></td>
                                                <td><?php echo $type; ?></td>
                                                <td><?php echo $row->field_label; ?></td>
                                                <td><?php echo $row->field_name; ?></td>
                                                <td><?php echo $row->options; ?></td>
                                                <td><?php echo $row->is_required; ?></td>
                                                <td><?php echo $row->is_unique; ?></td>
                                                <td><?php echo $display_question; ?></td>
                                                <td><?php echo $row->display_order; ?></td>
                                                <td>
                                                    <a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This Field?')){self.location='?delete&id=<?php echo $row->id;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
                                                    <a href="edit-form-field.php?id=<?=$row->id?>" class="label label-success"><i class="icon-pencil5"></i></a>
                                                </td>
                                            </tr><?php
                                        }
                                    } ?>
                                </tbody>
                            </table>
                        </div>
					</div>
					<!-- /default ordering -->
					<!-- /dashboard content -->
					<?php include 'footer.php';?>

				</div>
				<!-- /content area -->

			</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>