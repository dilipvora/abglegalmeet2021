<?php include '../config.php';
$event_id = EVENT_ID;

$users_form_field_query_1 = mysql_query("SELECT * from registration_field where type != '7' AND event_id = '{$event_id}' ORDER BY display_order ASC");
$users_num_rows_1 = mysql_num_rows($users_form_field_query_1);
$next_count_users_1 = $users_num_rows_1 - 1;

$users_form_field_query_2 = mysql_query("SELECT * from registration_field where type != '7' AND event_id = '{$event_id}' ORDER BY display_order ASC");
$users_num_rows_2 = mysql_num_rows($users_form_field_query_2);
$next_count_users_2 = $users_num_rows_2 - 1;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reports</title>

    <link rel="shortcut icon" href="../images/favicon-32x32.png"/>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
    <!-- /core JS files -->
    <script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../assets/js/pages/datatables_basic.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
</head>

<body>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content">

                <div class="container-fluid">
                    <div class="row">

                        <!-- Total Live Attendees list -->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="panel panel-body">
                                <div class="h2 no-margin pull-left">Total Live Attendees</div>
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table" id="live_attendees_table">
                                        <thead>
                                        <tr>
                                            <?php $live_attendees_field_array = array();
                                            if ($users_num_rows_1 > 0) {
                                                $count = 0;
                                                while ($row = mysql_fetch_object($users_form_field_query_1)) {
                                                    $live_attendees_field_array['data'][] = $count;
                                                    $count++; ?>
                                                    <th><?= $row->field_label ?></th>
                                                <?php }
                                            } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /Total Live Attendees list -->

                        <!-- Total Attendees list -->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="panel panel-body">
                                <div class="h2 no-margin pull-left">Total Attendees</div>
                                <div class="pull-right">
                                    <a href="export-data.php?total_attendee_report" class="btn btn-primary">Download Report</a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table" id="attendees_table">
                                        <thead>
                                        <tr>
                                            <?php $attendees_field_array = array();
                                            if ($users_num_rows_2 > 0) {
                                                $count = 0;
                                                while ($row = mysql_fetch_object($users_form_field_query_2)) {
                                                    $attendees_field_array['data'][] = $count;
                                                    $count++; ?>
                                                    <th><?= $row->field_label ?></th>
                                                <?php }
                                            } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /Total Attendees list -->

                    </div>
                </div>

            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->

<script type="text/javascript">
    $(document).ready(function () {
        var interval_time = 60000;

        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                targets: [<?=$next_count_users_1?>]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        /* =========================================== Attendees DataTable  =========================================== */
        var attendees_table = $("#attendees_table").DataTable({
            "stateSave": true,
            "processing": true,
            "ajax": {
                "url": "process.php?action=get_attendees",
                "type": "POST"
            },
            "columns": [
                <?php if (!empty($attendees_field_array['data'])) {
                foreach ($attendees_field_array['data'] as $field_item) { ?>
                {"data": <?=$field_item?>},
                <?php }
                } ?>
            ],
            "ordering": false,
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
        $("#attendees_table_length select").select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        /* =========================================== Live Attendees DataTable  =========================================== */
        var live_attendees_table = $("#live_attendees_table").DataTable({
            "stateSave": true,
            "processing": true,
            "ajax": {
                "url": "process.php?action=get_live_attendees",
                "type": "POST"
            },
            "columns": [
                <?php if (!empty($live_attendees_field_array['data'])) {
                foreach ($live_attendees_field_array['data'] as $field_item) { ?>
                {"data": <?=$field_item?>},
                <?php }
                } ?>
            ],
            "ordering": false,
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
        $("#live_attendees_table_length select").select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        setInterval(function () {
            attendees_table.ajax.reload();
        }, interval_time);

        setInterval(function () {
            live_attendees_table.ajax.reload();
        }, interval_time);

    });
</script>

</body>
</html>