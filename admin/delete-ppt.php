<?php include 'header.php';

if(isset($_GET['delete']) && isset($_GET['id']) && $_GET['id'] !=''){
	
	function delete_directory($dirname) {
		if (is_dir($dirname))
			$dir_handle = opendir($dirname);
		if (!$dir_handle)
			return false;
		while($file = readdir($dir_handle)) {
			if ($file != "." && $file != "..") {
				if (!is_dir($dirname."/".$file))
					unlink($dirname."/".$file);
				else
					delete_directory($dirname.'/'.$file);
			}
		}
		closedir($dir_handle);
		rmdir($dirname);
		return true;
	}
	
	$id = $_GET['id'];
	
	$res = mysql_query("SELECT * FROM `ppt` WHERE `id` = '{$id}' AND `event_id` = '".EVENT_ID."'");
	$num = mysql_num_rows($res);
	if($num > 0){
		$row=mysql_fetch_object($res);
		$ppt_id = $row->id;
		$ppt_filename = $row->filename;
	
		$ppt_destination = '../upload/ppt/'.$ppt_id;
		delete_directory($ppt_destination);
		
		if($ppt_filename != "") {
			$destination = '../upload/ppt-sources/';
			unlink($destination.$ppt_filename);
		}
		
		$sql = "delete from ppt where id = '{$id}' AND `event_id` = '".EVENT_ID."'";
		mysql_query($sql);
		$_SESSION['success_msg'] = 'PPT deleted successfully.';
	}
}

?>
<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Delete PPT</span></h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				
				<div class="panel panel-flat">
					<!-- datatable-basic -->
					<table class="table datatable-basic" id="ppt_table1">
							<thead>
								<tr>
									<th>SR.No.</th>
									<th>Presentation List</th>
									<th>Action</th>
									<th class="hidden"></th>
									<th class="hidden"></th>
									<th class="hidden"></th>
								</tr>
							</thead>
							<tbody id="mydata">
								<?php 
									 $rs = mysql_query("SELECT * FROM `ppt` WHERE `event_id` = '".EVENT_ID."' ORDER BY id DESC");
									$num_rows = mysql_num_rows($rs);
									if($num_rows){
										$srno = 1;
										while ($row = mysql_fetch_object($rs)){
										?>
										<tr>
											<td><?php echo $srno; ?></td>
											<td><?php echo $row->name; ?></td>
											<td>
												<a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This PPT')){self.location='?delete&id=<?php echo $row->id;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
											</td>
											<td class="hidden"><?php echo $row->id;?></td>
											<td class="hidden"><?php echo $row->id; ?></td>
											<td class="hidden"><?php echo $row->id; ?></td>
										</tr>
									<?php $srno++; }}?>
							</tbody>
						</table>
						<input type="hidden" name="total_ppt" id="total_ppt" value="<?php echo $num_rows;?>">
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->

</body>
</html>