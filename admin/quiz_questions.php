<?php include 'header.php';
if(isset($_GET['quiz_id'])){
	$quiz_id = $_GET['quiz_id'];
}else{
	$_SESSION['error_msg'] = "Quiz not found!";
	echo "<script>window.location.href = 'quiz.php'</script>";
	//header("Location:feedbacks.php");
	exit();
}

if(isset($_GET['delete']) && isset($_GET['question_id']) && $_GET['question_id'] !=''){
	$id = $_GET['question_id'];
	$sql = "delete from quiz_questions where id = '{$id}' AND event_id='".EVENT_ID."'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'Questions deleted successfully.';
} ?>

<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4>
							<i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quiz Questions List</span>
							<a href="add_quiz_question.php?quiz_id=<?=$quiz_id?>" class="btn btn-primary float-right" style="margin-right: 10px;">Add Question</a>
						</h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				<div class="panel panel-flat">
					<table class="table datatable-basic">
							<thead>
								<tr>
									<th class="hidden"></th>
									<th>Question</th>
									<th>Is Objective</th>
									<th>Option 1</th>
									<th>Option 2</th>
									<th>Option 3</th>
									<th>Option 4</th>
									<th>Option 5</th>
									<th>Option 6</th>
									<th>Option 7</th>
									<th>Correct Option</th>
									<th>Image Name</th>
									<th width="150">Action</th>
								</tr>
							</thead>
							<tbody id="mydata">
								<?php 
								$rs = mysql_query("SELECT * FROM quiz_questions WHERE event_id='".EVENT_ID."' AND quiz_id = '$quiz_id'");
								$num_rows = mysql_num_rows($rs);
								if($num_rows){
									while ($row = mysql_fetch_object($rs)){
									?>
									<tr>
										<td class="hidden"></td>
										<td><?php echo $row->title; ?></td>
										<td><?php echo $row->is_objective == 1?'Yes':'No'; ?></td>
										<td><?php echo $row->option1; ?></td>
										<td><?php echo $row->option2; ?></td>
										<td><?php echo $row->option3; ?></td>
										<td><?php echo $row->option4; ?></td>
										<td><?php echo $row->option5; ?></td>
										<td><?php echo $row->option6; ?></td>
										<td><?php echo $row->option7; ?></td>
                                        <?php if ($row->is_objective != 1) { ?>
                                            <td><?php echo $row->correct_answer ?></td>
                                        <?php } else { ?>
										    <td><?php echo $row->correct_option != 0?'Option '.$row->correct_option:''; ?></td>
                                        <?php } ?>
                                        <td><?php echo $row->image_name; ?></td>
                                        <td>
											<a href="edit_quiz_question.php?quiz_id=<?php echo $quiz_id;?>&question_id=<?php echo $row->id;?>"  class="label label-success"><i class="fa fa-pencil"></i></a>
											<a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This Feedback?')){self.location='?delete&quiz_id=<?php echo $quiz_id;?>&question_id=<?php echo $row->id;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
								<?php }}?>
							</tbody>
						</table>
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>