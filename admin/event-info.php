<?php include 'header.php';

$created_date = date("Y-m-d H:i:s");
$event_id = EVENT_ID;

$event_topic_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_topic' and `event_id` = '{$event_id}'");
$topic_option_data = mysql_fetch_object($event_topic_option);

$event_date_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_date' and `event_id` = '{$event_id}'");
$event_date_option_data = mysql_fetch_object($event_date_option);

$event_time_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_time' and `event_id` = '{$event_id}'");
$event_time_option_data = mysql_fetch_object($event_time_option);

/*echo "topic_option_data: $topic_option_data->option_value<br/>event_date_option_data: $event_date_option_data->option_value<br/>event_time_option_data: $event_time_option_data->option_value";
exit();*/

if (isset($_POST["save_event_info_btn"])) {
    $event_topic_postdata = addslashes(trim($_POST["event_topic"]));
    $event_date_postdata = addslashes(trim($_POST["event_date"]));
    $event_time_postdata = addslashes(trim($_POST["event_time"]));

    $exist_num = mysql_num_rows($event_topic_option);
    if($exist_num>0){
        $res = mysql_query("UPDATE `options` set `option_value` = '{$event_topic_postdata}' where `option_name` = 'event_topic' and `event_id` = '{$event_id}'");
    }else{
        $res = mysql_query("INSERT into `options` set `option_name` = 'event_topic',`option_value`= '{$event_topic_postdata}',`event_id`='{$event_id}', `created_date` = '{$created_date}'");
    }

    $exist_num = mysql_num_rows($event_date_option);
    if($exist_num>0){
        $res = mysql_query("UPDATE `options` set `option_value` = '{$event_date_postdata}' where `option_name` = 'event_date' and `event_id` = '{$event_id}'");
    }else{
        $res = mysql_query("INSERT into `options` set `option_name` = 'event_date',`option_value`= '{$event_date_postdata}',`event_id`='{$event_id}', `created_date` = '{$created_date}'");
    }

    $exist_num = mysql_num_rows($event_time_option);
    if($exist_num>0){
        $res = mysql_query("UPDATE `options` set `option_value` = '{$event_time_postdata}' where `option_name` = 'event_time' and `event_id` = '{$event_id}'");
    }else{
        $res = mysql_query("INSERT into `options` set `option_name` = 'event_time',`option_value`= '{$event_time_postdata}',`event_id`='{$event_id}', `created_date` = '{$created_date}'");
    }

    if($res){
        $_SESSION['success_msg'] = "Event Info saved successfully.";
    }else{
        $_SESSION['error_msg'] = "Something goes wrong try again.";
    }

    echo '<script type="text/javascript">window.location.href="event-info.php";</script>';
    exit();
}

$event_topic = (mysql_num_rows($event_topic_option) > 0) ? $topic_option_data->option_value : null;
$event_date = (mysql_num_rows($event_date_option) > 0) ? $event_date_option_data->option_value : null;
$event_time = (mysql_num_rows($event_time_option) > 0) ? $event_time_option_data->option_value : null;

?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php'; ?>
        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">
                <div class="row">
                    <div class="col-md-6">

                        <?php include 'messages.php';?>

                        <form action="" method="post" name="save_event_info">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary" name="save_event_info_btn" value="save">Save</button>
                            </div>
                            <br/>
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Topic</label>
                                                <input type="text" name="event_topic" placeholder="Event Topic" class="form-control" value="<?=$event_topic?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Event Date</label>
                                                <input type="text" name="event_date" class="form-control" placeholder="Event Date" value="<?=$event_date?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Event Time</label>
                                                <input type="text" name="event_time" class="form-control" placeholder="Event Time" value="<?=$event_time?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php include 'footer.php'; ?>
            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>