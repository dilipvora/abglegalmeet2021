<?php include '../config.php';

if(isset($_POST['action']) && $_POST['action'] == 'set_print_status'){
	//$date = date('Y-m-d');
	mysql_query("UPDATE `question` SET `print_status` = '1' WHERE `print_status` = '2' AND `event_id` = '".EVENT_ID."'");
	echo "success"; exit;
}

$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Question List</title>
	
	<link rel="shortcut icon" href="../../images/favicon-32x32.png" />
	
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	
	<!-- Core JS files -->
	<script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
	<!-- /core JS files -->

	
	<script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="../../assets/js/pages/datatables_basic.js"></script>
	<script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<style>
	@media print {
		.printbtn{
			display:none;
		}
	}
	</style>
</head>

<body>
	
<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<!-- Main content -->
		<div class="content-wrapper">
			
			<!-- Content area -->
			<div class="content">
				<div class="row text-right">
					<button type="submit" onclick="setPrintStatus();" class="btn btn-primary printbtn" name="submit-view-question-form-btn" value="save">Print</button>
				</div><br>
						
				<div class="panel panel-flat">
					<!-- datatable-basic -->
					<table class="table">
							<thead>
								<tr>
									<th>Id</th>
                                    <?php
                                    $filed_array = array();
                                    if($num_rows>0){
                                        while ($row = mysql_fetch_array($form_field_query)){
                                            array_push($filed_array,'u.'.$row['field_name']);?>
                                            <th><?=$row['field_label']?></th><?php
                                        }
                                    }
                                    ?>
									<th>Question</th>
                                    <th>Team</th>
								</tr>
							</thead>
							<tbody>
								<?php
                                    $fields = implode(',',$filed_array);
									$date = date('Y-m-d');
									$rs = mysql_query("SELECT q.*, $fields FROM question q, new_users u WHERE q.`created_date` LIKE '{$date}%' AND u.uid = q.uid AND q.event_id = '".EVENT_ID."' AND status='1' AND print_status='2' ORDER BY q_id ASC");
									$num_row = mysql_num_rows($rs);
									if($num_row){
										while($row = mysql_fetch_object($rs)){
                                            $fields_array = explode(',',$fields); ?>
                                                <tr>
                                                    <td><?=$row->q_id?></td><?php
                                                if(!empty($fields_array)){
                                                    foreach ($fields_array as $item) {
                                                        $field = str_replace('u.','',$item);?>
                                                        <td><?php echo $row->$field; ?></td><?php
                                                    }
                                                } ?>
												<td><?=$row->q_question?></td>
												<td><?=$row->team_id?></td>
											</tr><?php
										}
									}
								?>
							</tbody>
						</table>
				</div>

			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->

<script type="text/javascript">
	function setPrintStatus(){
		window.print();
		var dataString = "action=set_print_status";
		$.ajax({
			type: "POST",
			async: true,
			url: "question-print.php",
			data: dataString,
			cache: false,
			success: function(result){ 
			   
			},
			error: function( result ) {
				
			}
		});
	}
</script>

</body>
</html>