<?php include 'header.php';
$username='';
$fullname='';
$email='';
$mobile='';
?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">My Profile</span></h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">My Profile</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				
			<!-- Content area -->
			<div class="content">

				<?php include 'messages.php';?>
				
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">Profile information</h6>
			                </div>
			                <div class="panel-body">
			                	<form action="" method="post" name="UpdateProfileForm">
									<div class="form-group">
										<label>Username</label>
										<input type="text" name="username" class="form-control" value="<?php echo $username;?>" readonly="readonly">
									</div>
									<div class="form-group">
										<label>Fullname</label>
										<input type="text" name="fullname" placeholder="Enter your fullname" class="form-control" value="<?php echo $fullname;?>" required="required">
									</div>
									<div class="form-group">
										<label>Email</label>
										<input type="email" name="email" placeholder="Enter your email address" class="form-control" value="<?php echo $email;?>" required="required">
									</div>
									<div class="form-group">
										<label>Mobile</label>
										<input type="text" name="mobile" placeholder="Enter your mobile number" class="form-control" value="<?php echo $mobile;?>" required="required">
									</div>
			                        <div class="text-right">
			                        	<button type="submit" class="btn btn-primary" name="UpdateProfileFormBtn">Update</button>
			                        </div>
		                        </form>
			                </div>
			            </div>
					</div>
					
					<div class="col-md-6">			    
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">Change password</h6>
			                </div>
			                	<div class="panel-body">
									<form action="" method="post" name="ChangePassForm">
										<div class="form-group">
											<label>New password</label>
											<input type="password" name="new_password" placeholder="Enter new password" class="form-control" value="" required="required">
										</div>
										<div class="form-group">
											<label>Confirm password</label>
											<input type="password" name="con_password" placeholder="Confirm new password" class="form-control" value="" required="required">
										</div>
		
				                        <div class="text-right">
				                        	<button type="submit" class="btn btn-primary" name="ChangePassFormBtn">Update</button>
				                        </div>
			                        </form>
								</div>
							</div>
						</div>
					</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /Content area -->
		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>
		