<?php 

include '../config.php';
/*if(!isset($_SESSION['admin_username']) && $_SESSION['admin_username'] ==''){
	header('location:index.php');
}*/

if($page !='presentation.php'){
	presenter_not_access_page();
}


$json_data = file_get_contents("../firebase_ajax_setting.json");
if(!empty($json_data)) {
    $json_array = json_decode($json_data, false);
}else{
    $json_array = array();
}
$firebase_ajax = 1;
if(!empty($json_array)){
    $firebase_ajax = $json_array->firebase_ajax;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Panel | <?php echo COMPANY_NAME; ?></title>
	
	
	<!-- Global stylesheets -->
	<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<link href="../assets/css/webinar.css" rel="stylesheet" type="text/css">
	
	<!-- Core JS files -->
	<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<script type="text/javascript" src="../assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/forms/inputs/touchspin.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/forms/styling/switch.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
	
	<script type="text/javascript" src="../assets/js/pages/form_validation.js"></script>

	<!-- firebase -->
	<script type="text/javascript" src="../assets/js/firebase/firebase2.2.1.js"></script>

    <script>var event_id = '<?php echo EVENT_ID;?>'</script>
    <script>var firebase_url = '<?php echo FIREBASE_URL;?>'; </script>
    <script>var firebase_url2 = '<?php echo FIREBASE_URL2;?>'; </script>
    <script>var api_key = '<?php echo API_KEY; ?>'</script>
    <script>var firbase_ajax_setting = <?=$firebase_ajax?></script>

    <script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-app.js"></script>

    <script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-database.js"></script>
    <script type="text/javascript" src="../assets/js/firebase/admin_app.js?v=1"> </script>

	<script type="text/javascript" src="../assets/js/firebase/admin_pointer.js"> </script>

	<script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="../assets/js/pages/datatables_basic.js"></script>
	
	<script type="text/javascript" src="../assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	
	<?php if($page =='generate-vod.php'){?>
	<script type="text/javascript" src="../assets/js/plugins/editors/ace/ace.js"></script>
	<?php }?>
	<script type="text/javascript" src="../assets/js/core/app.js"></script>

    <script>var streamname = '<?=EVENT_ID?>';</script>
    <script type="text/javascript" src="../publish/jquery.cookie.js"></script>
    <script type="text/javascript" src="../publish/webrtc.js?v=20"></script>

	<?php if($page == "live_users.php"){ ?> 		
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/series-label.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>	
	<?php } ?>
    <?php if($page == "webcast.php"){ ?>
        <script type="text/javascript" src="../assets/custom.js"></script>
    <?php } ?>
</head>

<body>
<div id="load_images">
	<img id="loading" src="../assets/images/loader.gif" width="80" height="">
</div>

	<!-- Main navbar -->
	<!--
	<div class="navbar navbar-default header-highlight">
		<div class="navbar-header">
			<a class="navbar-brand" href="dashboard.php">
				<span class="text-semibold" style="color: #fff"><?php echo COMPANY_NAME; ?></span>
			</a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse1" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>


			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="../assets/images/user.png" alt="">
						<span><?php echo $_SESSION['admin_fullname'];?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	-->
	<!-- /main navbar -->