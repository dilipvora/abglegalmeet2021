<?php include '../config.php';
ini_set("memory_limit", "-1");
ini_set('max_execution_time', '100000');
//header('Content-Type: text/html; charset=utf-8');

$now_date = date("Y-m-d H:i:s");

if (isset($_POST["action"]) && $_POST["action"] == "upload_csv") {
    $form_field_query = mysql_query("SELECT `field_name`, `field_label` from registration_field where event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);
    if ($num_rows > 0) {
        $fields_output = array();
        while ($row = mysql_fetch_object($form_field_query)) {
            $fields_output[] = $row;
        }
    }

    $error = '';
    $html = '';

    if ($_FILES['file']['name'] != '') {
        $file_array = explode(".", $_FILES['file']['name']);

        $extension = end($file_array);

        if ($extension == 'csv') {
            $file_data = fopen($_FILES['file']['tmp_name'], 'r');

            $file_header = fgetcsv($file_data);
            $file_header_count = count($file_header);

            $html .= '<div class="table-responsive"><table class="table table-bordered" id="users_import_table"><thead><tr>';
            for ($count = 0; $count < $file_header_count; $count++) {
                $html .= '<th>
                            <select name="set_column_data" class="input-group form-control set_column_data" data-column_number="' . $count . '">
                                <option value="">Set Count Data</option>';
                foreach ($fields_output as $value) {
                    $html .= '<option value="' . $value->field_name . '">' . $value->field_label . '</option>';
                }
                $html .= '</select></th>';
            }
            $html .= '</thead></tr><tbody>';

            $limit = 0;
            $temp_data = array();
            while (($row = fgetcsv($file_data, 50000, ",")) !== false) {
                $limit++;
                if ($limit < 5) {
                    $html .= '<tr>';
                    for ($count = 0; $count < count($row); $count++) {
                        $html .= '<td>' . $row[$count] . '</td>';
                    }

                    $html .= '</tr>';
                }

                $temp_data[] = $row;
            }

            /*if (isset($_SESSION["temp_data"])) {
                unset($_SESSION["temp_data"]);
            }
            $_SESSION["temp_data"] = $temp_data;*/

            array_walk_recursive($temp_data, function (&$item) {
                $item = utf8_encode($item);
            });

            $html .= "<textarea class='hidden' name='file_data' id='file_data'>" . json_encode($temp_data) . "</textarea>";
            $html .= "</tbody></table></div>";
            $html .= '<div align="right" class="form-group" data-records="'.$limit.'">
                            <button type="button" name="import" id="import" class="btn btn-success" disabled>Import</button>
                            <button type="button" class="btn btn-danger" onclick="location.reload()">Refresh</button>
                        </div>';
        } else {
            $error = 'Only <b>.csv</b> file allowed';
        }
    } else {
        $error = 'Please Select CSV File';
    }

    $output = array(
        'error' => $error,
        'output' => $html
    );

    echo json_encode($output);
    exit();
}

if (isset($_POST["action"]) && $_POST["action"] == "import") {
    $file_data = json_decode($_POST["file_data"]);
    $success_status = 0;
    $now_date1 = date("Y-m-d H:i:s");

    foreach ($file_data as $row) {

        /*for ($i = 1; $i <= 15; $i++) {
            if (isset($row[$_POST["f$i"]])) {
                ${"f$i"} = mysql_real_escape_string(addslashes($row[$_POST["f$i"]]));
            } else {
                ${"f$i"} = null;
            }

            if (isset(${"f$i"}) && !empty(${"f$i"})) {
                $value_set = " `f$i` = '${"f$i"}', ";
            }
        }
        echo $value_set;
        die;*/

        //$f1 = $f2 = $f3 = $f4 = $f5 = $f6 = $f7 = $f8 = $f9 = $f10 = $f11 = $f12 = $f13 = $f14 = $f15 = null;

        $f1 = (isset($row[$_POST["f1"]]) && !empty($row[$_POST["f1"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f1"]])) : null;
        $f2 = (isset($row[$_POST["f2"]]) && !empty($row[$_POST["f2"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f2"]])) : null;
        $f3 = (isset($row[$_POST["f3"]]) && !empty($row[$_POST["f3"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f3"]])) : null;
        $f4 = (isset($row[$_POST["f4"]]) && !empty($row[$_POST["f4"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f4"]])) : null;
        $f5 = (isset($row[$_POST["f5"]]) && !empty($row[$_POST["f5"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f5"]])) : null;
        $f6 = (isset($row[$_POST["f6"]]) && !empty($row[$_POST["f6"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f6"]])) : null;
        /*$f7 = (isset($row[$_POST["f7"]]) && !empty($row[$_POST["f7"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f7"]])) : null;
        $f8 = (isset($row[$_POST["f8"]]) && !empty($row[$_POST["f8"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f8"]])) : null;
        $f9 = (isset($row[$_POST["f9"]]) && !empty($row[$_POST["f9"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f9"]])) : null;
        $f10 = (isset($row[$_POST["f10"]]) && !empty($row[$_POST["f10"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f10"]])) : null;
        $f11 = (isset($row[$_POST["f11"]]) && !empty($row[$_POST["f11"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f11"]])) : null;
        $f12 = (isset($row[$_POST["f12"]]) && !empty($row[$_POST["f12"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f12"]])) : null;
        $f13 = (isset($row[$_POST["f13"]]) && !empty($row[$_POST["f13"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f13"]])) : null;
        $f14 = (isset($row[$_POST["f14"]]) && !empty($row[$_POST["f14"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f14"]])) : null;
        $f15 = (isset($row[$_POST["f15"]]) && !empty($row[$_POST["f15"]])) ? mysql_real_escape_string(addslashes($row[$_POST["f15"]])) : null;*/

        $value_set = " `f1` = '{$f1}', `f2` = '{$f2}', `f3` = '{$f3}', `f4` = '{$f4}', `f5` = '{$f5}', `f6` = '{$f6}' ";

        //echo "$value_set<hr/>";

        $check_exist = mysql_query("SELECT `f2` FROM `new_users` WHERE `event_id` = '" . EVENT_ID . "' AND `f2` = '{$f2}' LIMIT 1");
        if (mysql_num_rows($check_exist)) {
            $sql = mysql_query("UPDATE `new_users` SET $value_set WHERE `f2` = '{$f2}' AND `event_id` = '" . EVENT_ID . "'");
            $success_status = 1;
        } else {
            $sql = mysql_query("INSERT INTO `new_users` SET `event_id` = '" . EVENT_ID . "', $value_set, `is_login` = '0', `created_date` = '{$now_date}'");
            $uid = mysql_insert_id($db);

            $login_detail = mysql_query("INSERT INTO users_login_detail SET `uid` = '{$uid}', `login_time` = '{$now_date}', `logout_time` = '{$now_date}'");
            $success_status = 1;
        }
    }

    if ($success_status) {
        echo "<div class='alert alert-success'>Data Imported Successfully</div>";
    } else {
        echo "<div class='alert alert-danger'>Data Importing Failed</div>";
    }
}