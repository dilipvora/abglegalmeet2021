<?php include '../config.php';

$event_id = EVENT_ID;
if (!empty($_GET['qid'])) {
    $quiz_id = $_GET['qid'];
} else {
    $quiz_id = get_values("options", "option_value", "event_id = '{$event_id}' AND option_name='quiz_id'");
}

$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '{$event_id}' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);

if (isset($_POST["date"])) {
    $date = $_POST["date"];
} else {
    $date = date("m/d/Y");
} ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Quiz Result</title>

    <link rel="shortcut icon" href="../../images/favicon-32x32.png"/>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <!-- /core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/select2.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/datatables_basic.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>

    <style type="text/css">
        .sorting_disabled {
            width: auto !important;
        }
    </style>
</head>

<body>
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <!-- Main content -->
        <?php $title = get_values("quiz", "title", "event_id = '" . EVENT_ID . "' AND id='{$quiz_id}'") ?>
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <div class="col-md-6 text-left">
                            <h4 class="text-black"><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quiz Result <?=($title) ? "- $title" : null?></span></h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-primary" onclick="window.open('export-quiz.php?qid=<?=$quiz_id?>', '_blank')">Download Report</button>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

            <!-- Content area -->
            <div class="content">
                <div class="panel panel-flat">

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <?php $filed_array = array();
                            if ($num_rows > 0) {
                                while ($row = mysql_fetch_array($form_field_query)) {
                                    array_push($filed_array, 'u.' . $row['field_name']); ?>
                                    <th><?= $row['field_label'] ?></th>
                                <?php }
                            } ?>
                            <th>Total True Answer</th>
                            <th>Date</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $no_col = count($filed_array) + 3;
                        $fields = implode(',', $filed_array);
                        $date = date('Y-m-d', strtotime($_REQUEST['date']));
                        $date_string = "";
                        if ($date != "" && $date != '1970-01-01') {
                            $date = date('Y-m-d', strtotime($_REQUEST['date']));
                            $date_string = "date(created_date) LIKE '{$date}%' AND ";
                        }

                        if (!empty($_GET['role']) && $_GET['role'] === "all") {
                            $HAVING = (($quiz_id === '162') ? 35 : 25);
                            $rs = mysql_query("SELECT SUM(r.answer_status) as total, $fields, `u`.`uid`, `r`.`created_at` FROM `quiz_result` `r`
                                                        INNER JOIN `new_users` `u` ON u.uid = r.uid
                                                        WHERE `r`.`event_id` = '{$event_id}' GROUP BY r.uid HAVING `total` <= $HAVING ORDER BY `total` DESC") or die("1 => " . mysql_error());
                        } else {
                            $HAVING = (($quiz_id === '162') ? 10 : 5);
                            $rs = mysql_query("SELECT SUM(r.answer_status) as total, $fields, `u`.`uid`, `r`.`created_at` FROM `quiz_result` `r`
                                                        INNER JOIN `new_users` `u` ON u.uid = r.uid
                                                        WHERE `r`.`quiz_id` = '{$quiz_id}' AND `r`.`event_id` = '{$event_id}' GROUP BY r.uid HAVING `total` <= $HAVING ORDER BY `total` DESC") or die("1 => " . mysql_error());
                        }

                        $final_result = array();
                        $i = 0;
                        if (mysql_num_rows($rs) > 0) {
                            while ($rows = mysql_fetch_object($rs)) {
                                $result = mysql_query("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(`q`.`total_time`)))  as `total_time` FROM `quiz_time` `q` WHERE `q`.`event_id` = '{$event_id}' AND `q`.`uid` = '{$rows->uid}'") or die("2 => ".mysql_error());
                                $total_time = '';
                                if (mysql_num_rows($result) > 0) {
                                    $total_time = mysql_fetch_object($result)->total_time;
                                }

                                $form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '{$event_id}' ORDER BY display_order ASC") or die("3 => ".mysql_error());
                                while ($row = mysql_fetch_object($form_field_query)) {
                                    $field_name = $row->field_name;
                                    $final_result[$rows->total][$i][$field_name] = $rows->$field_name;
                                }
                                $final_result[$rows->total][$i]['total'] = $rows->total;
                                $final_result[$rows->total][$i]['date'] = date("d-m-Y", strtotime($rows->created_at));
                                $i++;
                            }
                        }

                        //echo "<pre>"; print_r($final_result);
                        function result_sort($result_a, $result_b)
                        {
                            return strtotime($result_a["total_time"]) - strtotime($result_b["total_time"]);
                        }

                        $final_result_temp = array_slice($final_result, 0, 350, true);
                        //echo "<pre>"; print_r($final_result_temp); exit;

                        if (!empty($final_result_temp)) {
                            foreach ($final_result_temp as &$sresult) {
                                usort($sresult, "result_sort");
                            }
                        }
                        //echo "<pre>"; print_r($final_result_temp); exit;

                        $fields_array = explode(',', $fields);

                        if (!empty($final_result_temp)) {
                            $no = 1;
                            foreach ($final_result_temp as $subresult) {
                                foreach ($subresult as $fresult) { ?>
                                    <tr>
                                        <td><?=$no?></td>
                                        <?php if (!empty($fields_array)) {
                                            foreach ($fields_array as $item) {
                                                $field = str_replace('u.', '', $item); ?>
                                                <td><?= $fresult[$field]; ?></td>
                                            <?php }
                                        } ?>
                                        <td><?= $fresult['total'] ?></td>
                                        <td><?= $fresult['date'] ?></td>
                                    </tr>
                                <?php $no++; }
                            }
                        } else { ?>
                            <tr><td colspan="<?=$no_col?>" class="text-center">No matching records found</td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>