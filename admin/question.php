<?php
include 'header.php';
$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
$next_count = $num_rows + 2;
?>
<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Question</span></h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				
				<!--<div class="panel panel-flat">
					<div class="panel-body">
						<div class="row text-right">
                        <?php
						$hidden_date = date('m/d/Y');
						if(isset($_POST['date']) && $_POST['date'] !='' && $_POST['date'] !='1970-01-01'){
							$hidden_date = $_POST['date'];
						}?>
                                    <input type="hidden" name="question_hidden_date" id="question_hidden_date" value="<?php echo $hidden_date;?>">
							<form class="form-inline" role="form" name="view-question-form" method="post" action="" style="display: inline-block;">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar"></i></span>
										<input type="text" name="date" class="form-control datepicker-menus" value="<?php if(isset($_POST['date'])){echo $_POST['date'];}else{ echo date('m/d/Y');}?>" placeholder="Select Date">
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary" name="submit-view-question-form-btn" value="save">View Question</button>
								</div>
							</form>
							<a href="export-question.php?date=<?php if(isset($_POST['date'])){echo $_POST['date'];}else{ echo date('m/d/Y');}?>" class="btn btn-primary">Download Report</a>
						</div>
					</div>
				</div>-->
						
				<div class="panel panel-flat">
					<!-- datatable-basic -->
					<table class="table" id="question_table1">
							<thead>
								<tr>
                                    <th class="hidden"></th>
                                    <?php
                                    $filed_array = array();
                                    if($num_rows>0){
                                        while ($row = mysql_fetch_array($form_field_query)){
                                            array_push($filed_array,'u.'.$row['field_name']);?>
                                            <th><?=$row['field_label']?></th><?php
                                        }
                                    }
                                    ?>
                                    <th>Question</th>
                                    <th>Team</th>
                                    <th class="hidden"></th>
								</tr>
							</thead>
							<tbody id="mydata">
								
							</tbody>
						</table>
						<input type="hidden" name="total_question" id="total_question" value="<?php echo $num_rows;?>">
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>
<?php
$fields = implode(',',$filed_array);
?>
<script>
    var question_table;
    var fields = '<?=$fields?>';
    $(document).ready(function () {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [ 5 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        question_table = $("#question_table1").DataTable({
            "ajax": {
                "url": "get_question_data.php?fields="+fields
            },
            "columns": [
                { "data": 0,className: "hidden" },
                <?php $count = 1; for($i=0;$i<$next_count;$i++){ ?>
                { "data": <?=$count?> },
                <?php $count++; } ?>
                { "data": 5 ,className: "hidden" },
            ],
            "ordering": false,
            "lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
        } );
        setInterval( function () {
            question_table.ajax.reload();
        }, 10000);

    });
</script>