<?php include '../config.php';
require_once '../PHPExcel/Classes/PHPExcel.php';
$event_id = EVENT_ID;

$objPHPExcel = new PHPExcel();
$bold_font = array('font' => array('bold' => true, 'size' => 13));

/* ================================ Export Users data with login - logout date & time ================================ */

if (isset($_GET["users_list"])) {
    $file_title = $event_id . ucwords(" users list");

    $form_field_query = mysql_query("SELECT * from registration_field where event_id = '{$event_id}' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);

    $field_name_array = array();
    if ($num_rows > 0) {
        $alphabet = "A";
        while ($row = mysql_fetch_object($form_field_query)) {
            if ($row->type != "7") {
                array_push($field_name_array, $row->field_name);
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", $row->field_label)->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
                $alphabet++;
            }
        }
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Login Date")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Login Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Logout Date")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Logout Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
    }

    $user_detail = mysql_query("SELECT * from `new_users` where `event_id` = '{$event_id}'");
    $num_of_rows = mysql_num_rows($user_detail);

    if ($num_of_rows > 0) {
        $h = 2;
        while ($item = mysql_fetch_object($user_detail)) {
            $row_result = mysql_query("SELECT * FROM `users_login_detail` where uid = '{$item->uid}' ORDER BY id DESC LIMIT 1");
            $user_detail_result = mysql_fetch_object($row_result);

            $login_date = $logout_date = $login_time = $logout_time = null;
            if (mysql_num_rows($row_result)) {
                $login_date = date('d-m-Y', strtotime($user_detail_result->login_time));
                $login_time = date('H:i:s', strtotime($user_detail_result->login_time));

                $logout_date = date('d-m-Y', strtotime($user_detail_result->logout_time));
                $logout_time = date('H:i:s', strtotime($user_detail_result->logout_time));
            }
            $lines = array();
            if (!empty($field_name_array)) {
                foreach ($field_name_array as $field_row) {
                    $lines[] = $item->$field_row;
                }
                $lines[] = $login_date;
                $lines[] = $login_time;

                $lines[] = $logout_date;
                $lines[] = $logout_time;
            }
            $alphabet_new = "A";
            for ($j = 0; count($lines) > $j; $j++) {
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "$h", $lines[$j]);
                $alphabet_new++;
            }
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
    }

    /* ========================================== Export Work Experience list ========================================== */

} elseif (isset($_GET["work-experience"])) {
    $file_title = $event_id . ucwords(" work experience list");

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "Name")->getStyle("A1:A1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Designation")->getStyle("B1:B1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Email Address")->getStyle("C1:C1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Company Name")->getStyle("D1:D1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Location / City")->getStyle("E1:E1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Contact Number")->getStyle("F1:F1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Within Aditya Birla Group")->getStyle("G1:G1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Overall")->getStyle("H1:H1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Area/Subject expertise in Legal Domain")->getStyle("I1:I1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Introduce about yourself")->getStyle("J1:J1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Date Time")->getStyle("K1:K1")->applyFromArray($bold_font);

    $rs = mysql_query("SELECT ef.f1, ef.f2, ef.f3, ef.f4, u.f1 as name, u.f2 as email, u.f3 as city, u.f4 as mno, u.f5 as designation, u.f6 as company, ef.created_date
                            FROM `extra_form` as `ef` INNER JOIN `new_users` as `u` ON ef.uid = u.uid
                            WHERE ef.role = 'work experience' AND ef.event_id = '{$event_id}'") or die(mysql_error());
    $num_of_rows = mysql_num_rows($rs);
    if ($num_of_rows > 0) {
        $h = 2;
        while ($row = mysql_fetch_object($rs)) {
            $objPHPExcel->getActiveSheet()->setCellValue("A$h", $row->name)->getStyle("A$h:A$h");
            $objPHPExcel->getActiveSheet()->setCellValue("B$h", $row->designation)->getStyle("B$h:B$h");
            $objPHPExcel->getActiveSheet()->setCellValue("C$h", $row->email)->getStyle("C$h:C$h");
            $objPHPExcel->getActiveSheet()->setCellValue("D$h", $row->company)->getStyle("D$h:D$h");
            $objPHPExcel->getActiveSheet()->setCellValue("E$h", $row->city)->getStyle("E$h:E$h");
            $objPHPExcel->getActiveSheet()->setCellValue("F$h", $row->mno)->getStyle("F$h:F$h");
            $objPHPExcel->getActiveSheet()->setCellValue("G$h", $row->f1)->getStyle("G$h:G$h");
            $objPHPExcel->getActiveSheet()->setCellValue("H$h", $row->f2)->getStyle("H$h:H$h");
            $objPHPExcel->getActiveSheet()->setCellValue("I$h", $row->f3)->getStyle("I$h:I$h");
            $objPHPExcel->getActiveSheet()->setCellValue("J$h", $row->f4)->getStyle("J$h:J$h");
            $objPHPExcel->getActiveSheet()->setCellValue("K$h", date('l, d F, Y h:i A', strtotime($row->created_date)))->getStyle("K$h:K$h");
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:K2");
    }

    /* ========================================== Export quotes list ========================================== */

} elseif (isset($_GET["quotes"])) {
    $file_title = $event_id . ucwords(" quotes list");

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "Name")->getStyle("A1:A1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Designation")->getStyle("B1:B1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Email Address")->getStyle("C1:C1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Company Name")->getStyle("D1:D1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Location / City")->getStyle("E1:E1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Contact Number")->getStyle("F1:F1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Quote Name")->getStyle("G1:G1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Quote Email")->getStyle("H1:H1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Quote")->getStyle("I1:I1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Date Time")->getStyle("J1:J1")->applyFromArray($bold_font);

    $rs = mysql_query("SELECT ef.f1, ef.f2, ef.f3, u.f1 as name, u.f2 as email, u.f3 as city, u.f4 as mno, u.f5 as designation, u.f6 as company, ef.created_date
                            FROM `extra_form` as `ef` INNER JOIN `new_users` as `u` ON ef.uid = u.uid
                            WHERE ef.role = 'quotes' AND ef.event_id = '{$event_id}'") or die(mysql_error());
    $num_of_rows = mysql_num_rows($rs);
    if ($num_of_rows > 0) {
        $h = 2;
        while ($row = mysql_fetch_object($rs)) {
            $objPHPExcel->getActiveSheet()->setCellValue("A$h", $row->name)->getStyle("A$h:A$h");
            $objPHPExcel->getActiveSheet()->setCellValue("B$h", $row->designation)->getStyle("B$h:B$h");
            $objPHPExcel->getActiveSheet()->setCellValue("C$h", $row->email)->getStyle("C$h:C$h");
            $objPHPExcel->getActiveSheet()->setCellValue("D$h", $row->company)->getStyle("D$h:D$h");
            $objPHPExcel->getActiveSheet()->setCellValue("E$h", $row->city)->getStyle("E$h:E$h");
            $objPHPExcel->getActiveSheet()->setCellValue("F$h", $row->mno)->getStyle("F$h:F$h");
            $objPHPExcel->getActiveSheet()->setCellValue("G$h", $row->f1)->getStyle("G$h:G$h");
            $objPHPExcel->getActiveSheet()->setCellValue("H$h", $row->f2)->getStyle("H$h:H$h");
            $objPHPExcel->getActiveSheet()->setCellValue("I$h", $row->f3)->getStyle("I$h:I$h");
            $objPHPExcel->getActiveSheet()->setCellValue("J$h", date('l, d F, Y h:i A', strtotime($row->created_date)))->getStyle("J$h:J$h");
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:J2");
    }

    /* ========================================== nominee-list ========================================== */

} elseif (isset($_GET["nominee-list"])) {
    $file_title = $event_id . ucwords(" nominee list");

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "Name")->getStyle("A1:A1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Designation")->getStyle("B1:B1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Email Address")->getStyle("C1:C1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Company Name")->getStyle("D1:D1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Location / City")->getStyle("E1:E1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Contact Number")->getStyle("F1:F1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Nominee Method")->getStyle("G1:G1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Nominee Name")->getStyle("H1:H1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Nominee Email")->getStyle("I1:I1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Nominee Contact no.")->getStyle("J1:J1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Nominee Business")->getStyle("K1:K1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Nominee Team")->getStyle("L1:L1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Nominee Description")->getStyle("M1:M1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Nominee Role")->getStyle("N1:N1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Total Nominees")->getStyle("O1:O1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Total Likes")->getStyle("P1:P1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Total Comments")->getStyle("Q1:Q1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Date Time")->getStyle("R1:R1")->applyFromArray($bold_font);

    $rs = mysql_query("SELECT ef.fid, ef.f1, ef.f2, ef.f3, ef.f4, ef.f5, ef.f6, ef.f7, ef.f8, ef.role, u.f1 as name, u.f2 as email, u.f3 as city, u.f4 as mno, u.f5 as designation, u.f6 as company, ef.created_date
                            FROM `extra_form` as `ef` INNER JOIN `new_users` as `u` ON ef.uid = u.uid
                            WHERE ef.event_id = '{$event_id}' AND
                            `role` IN ('above-and-beyond','young-achievers-award','best-legal-innovator','best-in-house-team-of-the-year','dealmaker-of-the-year','student-of-the-year','collaborator-of-the-year','gc-of-the-year')") or die(mysql_error());
    if (mysql_num_rows($rs) > 0) {
        $h = 2;
        while ($row = mysql_fetch_object($rs)) {
            $objPHPExcel->getActiveSheet()->setCellValue("A$h", $row->name)->getStyle("A$h:A$h");
            $objPHPExcel->getActiveSheet()->setCellValue("B$h", $row->designation)->getStyle("B$h:B$h");
            $objPHPExcel->getActiveSheet()->setCellValue("C$h", $row->email)->getStyle("C$h:C$h");
            $objPHPExcel->getActiveSheet()->setCellValue("D$h", $row->company)->getStyle("D$h:D$h");
            $objPHPExcel->getActiveSheet()->setCellValue("E$h", $row->city)->getStyle("E$h:E$h");
            $objPHPExcel->getActiveSheet()->setCellValue("F$h", $row->mno)->getStyle("F$h:F$h");
            $objPHPExcel->getActiveSheet()->setCellValue("G$h", $row->f2)->getStyle("G$h:G$h");
            $objPHPExcel->getActiveSheet()->setCellValue("H$h", $row->f3)->getStyle("H$h:H$h");
            $objPHPExcel->getActiveSheet()->setCellValue("I$h", $row->f4)->getStyle("I$h:I$h");
            $objPHPExcel->getActiveSheet()->setCellValue("J$h", $row->f5)->getStyle("J$h:J$h");
            $objPHPExcel->getActiveSheet()->setCellValue("K$h", $row->f6)->getStyle("K$h:K$h");
            $objPHPExcel->getActiveSheet()->setCellValue("L$h", $row->f7)->getStyle("L$h:L$h");
            $objPHPExcel->getActiveSheet()->setCellValue("M$h", $row->f8)->getStyle("M$h:M$h");
            $objPHPExcel->getActiveSheet()->setCellValue("N$h", $row->role)->getStyle("N$h:N$h");

            $total_nominees_q = mysql_query("SELECT COUNT(*) as `total_nominees` FROM `users` WHERE `alias` = {$row->fid} AND `event_id` = '{$event_id}'") or die(mysql_error());
            $total_nominees = (mysql_num_rows($total_nominees_q) > 0) ? mysql_fetch_object($total_nominees_q)->total_nominees : 0;
            $objPHPExcel->getActiveSheet()->setCellValue("O$h", $total_nominees)->getStyle("O$h:O$h");

            $total_likes_q = mysql_query("SELECT COUNT(*) as `total_likes` FROM `users` as `u` INNER JOIN `users_like` as `ul` ON u.uid = ul.uid WHERE u.event_id = '{$event_id}' AND (u.alias = '{$row->fid}' AND ul.action = '{$row->fid}')") or die(mysql_error());
            $total_likes = (mysql_num_rows($total_likes_q) > 0) ? mysql_fetch_object($total_likes_q)->total_likes : 0;
            $objPHPExcel->getActiveSheet()->setCellValue("P$h", $total_likes)->getStyle("P$h:P$h");

            $total_comments_q = mysql_query("SELECT COUNT(*) as `total_comments` FROM `users` as `u` INNER JOIN `question` as `q` ON u.uid = q.uid WHERE u.event_id = '{$event_id}' AND (u.alias = '{$row->fid}' AND q.team_id = '{$row->fid}')") or die(mysql_error());
            $total_comments = (mysql_num_rows($total_comments_q) > 0) ? mysql_fetch_object($total_comments_q)->total_comments : 0;
            $objPHPExcel->getActiveSheet()->setCellValue("Q$h", $total_comments)->getStyle("Q$h:Q$h");

            $objPHPExcel->getActiveSheet()->setCellValue("R$h", date('l, d F, Y h:i A', strtotime($row->created_date)))->getStyle("R$h:R$h");
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:R2");
    }

    /* ========================================== Export nominee Comments list ========================================== */

} elseif (isset($_GET["nominee-comments-list"])) {
    $file_title = $event_id . ucwords(" nominee Comments list");

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "Name")->getStyle("A1:A1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Email")->getStyle("B1:B1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Designation")->getStyle("C1:C1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Comments")->getStyle("D1:D1")->applyFromArray($bold_font);
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Registered Date")->getStyle("E1:E1")->applyFromArray($bold_font);

    $rs = mysql_query("SELECT q.q_question, u.name, u.email, u.location, q.created_date FROM `users` as `u` INNER JOIN `question` as `q` ON u.uid = q.uid WHERE u.event_id = '{$event_id}' ORDER BY `team_id` ASC") or die(mysql_error());
    if (mysql_num_rows($rs) > 0) {
        $h = 2;
        while ($row = mysql_fetch_object($rs)) {
            $objPHPExcel->getActiveSheet()->setCellValue("A$h", $row->name)->getStyle("A$h:A$h");
            $objPHPExcel->getActiveSheet()->setCellValue("B$h", $row->email)->getStyle("B$h:B$h");
            $objPHPExcel->getActiveSheet()->setCellValue("C$h", $row->location)->getStyle("C$h:C$h");
            $objPHPExcel->getActiveSheet()->setCellValue("D$h", $row->q_question)->getStyle("D$h:D$h");
            $objPHPExcel->getActiveSheet()->setCellValue("E$h", date('l, d F, Y h:i A', strtotime($row->created_date)))->getStyle("E$h:E$h");
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:E2");
    }

    /* ========================================== Export users Question list ========================================== */

} elseif (isset($_GET["questions_list"])) {
    $file_title = $event_id . ucwords(" questions list");

    $form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '{$event_id}' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);

    $field_name_array = array();
    if ($num_rows > 0) {
        $alphabet = "A";
        while ($row = mysql_fetch_object($form_field_query)) {
            if ($row->type != "7") {
                array_push($field_name_array, $row->field_name);
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", $row->field_label)->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
                $alphabet++;
            }
        }
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Questions")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Team")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
    }
    $fields = implode(',', $field_name_array);

    $que_detail = mysql_query("SELECT q.*, $fields FROM question q, new_users u WHERE q.status = '1' and u.uid = q.uid AND q.event_id = '{$event_id}' ORDER BY q_id DESC");
    $num_of_rows = mysql_num_rows($que_detail);

    if ($num_of_rows > 0) {
        $h = 2;
        while ($item = mysql_fetch_object($que_detail)) {
            $field_name_array = explode(',', $fields);
            $lines = array();
            if (!empty($field_name_array)) {
                foreach ($field_name_array as $field_row) {
                    $lines[] = $item->$field_row;
                }
                $lines[] = $item->q_question;
                $lines[] = $item->team_id;
            }
            $alphabet_new = "A";
            for ($j = 0; count($lines) > $j; $j++) {
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "$h", $lines[$j]);
                $alphabet_new++;
            }
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
    }

    /* ================================= Export users login data with total view time ================================= */

} elseif (isset($_GET["login_report"])) {
    $file_title = $event_id . ucwords(" user login report");

    $form_field_query = mysql_query("SELECT * from registration_field where event_id = '{$event_id}' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);

    $field_name_array = array();
    $user_field_name_array = array();

    if ($num_rows > 0) {
        $alphabet = "A";
        while ($row = mysql_fetch_object($form_field_query)) {
            if ($row->type != "7") {
                array_push($field_name_array, $row->field_name);
                array_push($user_field_name_array, 'nu.' . $row->field_name);
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", $row->field_label)->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
                $alphabet++;
            }
        }
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Login Date")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Login Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Logout Date")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Logout Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

        $alphabet++;
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Total View Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
    }
    $user_field = implode(',', $user_field_name_array);

    $user_detail = mysql_query("SELECT $user_field, ul.login_time, ul.logout_time, TIME_TO_SEC(TIMEDIFF(ul.logout_time,ul.login_time)) as view_time from `users_login_detail` as `ul` INNER JOIN `new_users` as `nu` ON nu.uid = ul.uid where nu.`event_id` = '{$event_id}'");
    $num_of_rows = mysql_num_rows($user_detail);

    if ($num_of_rows > 0) {
        $h = 2;
        while ($item = mysql_fetch_object($user_detail)) {
            $login_date = $logout_date = $login_time = $logout_time = null;

            $login_date = date('d-m-Y', strtotime($item->login_time));
            $login_time = date('H:i:s', strtotime($item->login_time));

            $logout_date = date('d-m-Y', strtotime($item->logout_time));
            $logout_time = date('H:i:s', strtotime($item->logout_time));

            $lines = array();
            if (!empty($field_name_array)) {
                foreach ($field_name_array as $field_row) {
                    $lines[] = $item->$field_row;
                }
                $lines[] = $login_date;
                $lines[] = $login_time;

                $lines[] = $logout_date;
                $lines[] = $logout_time;

                $view_time = $item->view_time;
                $total_view_time = sprintf('%02d:%02d:%02d', ($view_time / 3600), ($view_time / 60 % 60), $view_time % 60);

                $lines[] = $total_view_time;
            }
            $alphabet_new = "A";
            for ($j = 0; count($lines) > $j; $j++) {
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "$h", $lines[$j]);
                $alphabet_new++;
            }
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
    }

    /* ============================== Export users total view data with session view time ============================== */

} elseif (isset($_GET["users_view"])) {
    $file_title = $event_id . ucwords(" users view report");

    $form_field_query = mysql_query("SELECT * from registration_field where event_id = '{$event_id}' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);

    $field_name_array = array();
    $user_field_name_array = array();

    if ($num_rows > 0) {
        $alphabet = "A";
        while ($row = mysql_fetch_object($form_field_query)) {
            if ($row->type != "7") {
                array_push($field_name_array, $row->field_name);
                array_push($user_field_name_array, 'u.' . $row->field_name);
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", $row->field_label)->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
                $alphabet++;
            }
        }
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Session view time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
    }
    $user_field = implode(',', $user_field_name_array);

    $each_users_total_time_query = mysql_query("SELECT $user_field, SUM(TIME_TO_SEC(TIMEDIFF(l.logout_time,l.login_time))) as view_time FROM users_login_detail l INNER JOIN new_users u ON l.uid = u.uid WHERE u.event_id = '{$event_id}' GROUP BY l.uid ORDER BY u.uid DESC");
    $num_of_rows = mysql_num_rows($each_users_total_time_query);

    if ($num_of_rows > 0) {
        $h = 2;
        while ($item = mysql_fetch_object($each_users_total_time_query)) {
            $lines = array();
            if (!empty($field_name_array)) {
                foreach ($field_name_array as $field_row) {
                    $lines[] = $item->$field_row;
                }
                $avg_time = $item->view_time;
                $avg_view_time = sprintf('%02d:%02d:%02d', ($avg_time / 3600), ($avg_time / 60 % 60), $avg_time % 60);

                $lines[] = $avg_view_time;
            }
            $alphabet_new = "A";
            for ($j = 0; count($lines) > $j; $j++) {
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "$h", $lines[$j]);
                $alphabet_new++;
            }
            $h++;
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
    }

    /* ======================================= Export event time attendee data ======================================= */

} elseif (isset($_GET["attendee_report"])) {
    $file_title = $event_id . ucwords(" users attendee report");

    $form_field_query = mysql_query("SELECT * from registration_field where event_id = '{$event_id}' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);

    $field_name_array = array();
    $user_field_name_array = array();

    if ($num_rows > 0) {
        $alphabet = "A";
        while ($row = mysql_fetch_object($form_field_query)) {
            if ($row->type != "7") {
                array_push($field_name_array, $row->field_name);
                array_push($user_field_name_array, 'u.' . $row->field_name);
                $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", $row->field_label)->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
                $alphabet++;
            }
        }
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Session view time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
    }
    $user_field = implode(',', $user_field_name_array);

    $event_start_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_start_date_time' and `event_id` = '{$event_id}'");
    $event_start_date_time_data = mysql_fetch_object($event_start_date_time);
    if(mysql_num_rows($event_start_date_time)>0){
        $event_start_date_time_value = date('Y-m-d H:i:s',strtotime($event_start_date_time_data->option_value));
        $cenverted_start_time = date('Y-m-d H:i:s',strtotime('-15 minutes',strtotime($event_start_date_time_value)));
    }else{
        $cenverted_start_time = '';
    }

    $event_end_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_end_date_time' and `event_id` = '{$event_id}'");
    $event_end_date_time_data = mysql_fetch_object($event_end_date_time);
    if(mysql_num_rows($event_end_date_time)>0){
        $cenverted_end_time = date('Y-m-d H:i:s',strtotime($event_end_date_time_data->option_value));
        //$cenverted_end_time = date('Y-m-d H:i:s',strtotime('+15 minutes',strtotime($event_end_date_time_value)));
    }else{
        $cenverted_end_time = '';
    }

    if(!empty($cenverted_start_time) && !empty($cenverted_end_time)) {
        $each_users_total_time_query = mysql_query("SELECT $user_field, SUM(TIME_TO_SEC(TIMEDIFF(ul.logout_time,ul.login_time))) as view_time FROM `users_login_detail` as `ul` INNER JOIN `new_users` as `u` ON ul.uid = u.uid WHERE u.event_id = '{$event_id}' AND ul.login_time >= '{$cenverted_start_time}' AND ul.logout_time <= '{$cenverted_end_time}' GROUP BY ul.uid HAVING view_time >= '10' ORDER BY u.uid DESC");
        $num_of_rows = mysql_num_rows($each_users_total_time_query);

        if ($num_of_rows > 0) {
            $all_users_time = 0;
            $h = 2;
            while ($item = mysql_fetch_object($each_users_total_time_query)) {
                $lines = array();
                if (!empty($field_name_array)) {
                    foreach ($field_name_array as $field_row) {
                        $lines[] = $item->$field_row;
                    }
                    $user_time = $item->view_time;
                    $viewing_time = sprintf('%02d:%02d:%02d', ($user_time / 3600), ($user_time / 60 % 60), $user_time % 60);

                    $lines[] = $viewing_time;

                    $all_users_time += $item->view_time;
                }

                $alphabet_new = "A";
                for ($j = 0; count($lines) > $j; $j++) {
                    $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "$h", $lines[$j]);
                    $alphabet_new++;
                }
                $h++;
            }

            $avg_view_time = round($all_users_time/$num_of_rows);
            $avg_viewing_time = sprintf('%02d:%02d:%02d', ($avg_view_time / 3600), ($avg_view_time / 60 % 60), $avg_view_time % 60);
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "1", "Total View Time")->getStyle("$alphabet_new" . "1:$alphabet_new" . "1")->applyFromArray($bold_font);
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "2", "$avg_viewing_time")->getStyle("$alphabet_new" . "2:$alphabet_new" . "2")->applyFromArray($bold_font);

        } else {
            $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
            $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
            $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
        }
    } else {
        $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
        $objPHPExcel->getActiveSheet()->setCellValue("A2", "Event start or end date & time not set.")->getStyle("A2:A2")->applyFromArray($style);
        $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
    }

    /* ================== Export total attendees data with login - logout time and total view times ================== */

} elseif (isset($_GET["total_attendee_report"])) {
    $file_title = $event_id . ucwords(" total attendee users list");

    $event_id = EVENT_ID;

    $event_start_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_start_date_time' and `event_id` = '{$event_id}'");
    $event_start_date_time_data = mysql_fetch_object($event_start_date_time);
    if(mysql_num_rows($event_start_date_time)>0){
        $event_start_date_time_value = date('Y-m-d H:i:s',strtotime($event_start_date_time_data->option_value));
        $converted_start_time = date('Y-m-d H:i:s',strtotime('-10 minutes',strtotime($event_start_date_time_value)));
    }else{
        $converted_start_time = '';
    }

    $event_end_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_end_date_time' and `event_id` = '{$event_id}'");
    $event_end_date_time_data = mysql_fetch_object($event_end_date_time);
    if(mysql_num_rows($event_end_date_time)>0){
        $event_end_date_time_value = date('Y-m-d H:i:s',strtotime($event_end_date_time_data->option_value));
    }else{
        $event_end_date_time_value = '';
    }

    if(!empty($converted_start_time) && !empty($event_end_date_time_value)) {
        $form_field_query = mysql_query("SELECT * from registration_field where event_id = '{$event_id}' ORDER BY display_order ASC");
        $num_rows = mysql_num_rows($form_field_query);

        $field_name_array = array();
        if ($num_rows > 0) {
            $alphabet = "A";
            while ($row = mysql_fetch_object($form_field_query)) {
                if ($row->type != "7") {
                    array_push($field_name_array, "u.".$row->field_name);
                    $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", $row->field_label)->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
                    $alphabet++;
                }
            }
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Login Date")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

            $alphabet++;
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Login Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

            $alphabet++;
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Logout Date")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

            $alphabet++;
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Logout Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

            $alphabet++;
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Total View Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
        }

        $field_name = implode(', ', $field_name_array);

        $user_detail = mysql_query("SELECT $field_name, u.uid, ul.login_time, ul.logout_time, TIME_TO_SEC(TIMEDIFF(ul.logout_time,ul.login_time)) as view_time FROM `users_login_detail` as `ul` INNER JOIN `new_users` as `u` ON ul.uid = u.uid WHERE u.event_id = '{$event_id}' AND ul.login_time >= '{$converted_start_time}' AND ul.logout_time <= '{$event_end_date_time_value}' GROUP BY ul.uid");
        $num_of_rows = mysql_num_rows($user_detail);

        if ($num_of_rows > 0) {
            $h = 2;
            while ($item = mysql_fetch_object($user_detail)) {
                $login_date = $logout_date = $login_time = $logout_time = null;

                $login_date = date('d-m-Y', strtotime($item->login_time));
                $login_time = date('H:i:s', strtotime($item->login_time));

                $logout_date = date('d-m-Y', strtotime($item->logout_time));
                $logout_time = date('H:i:s', strtotime($item->logout_time));

                $lines = array();
                if (!empty($field_name_array)) {
                    foreach ($field_name_array as $field_row) {
                        $field_row = str_replace("u.","", $field_row);
                        $lines[] = $item->$field_row;
                    }
                    $lines[] = $login_date;
                    $lines[] = $login_time;

                    $lines[] = $logout_date;
                    $lines[] = $logout_time;

                    $view_time = $item->view_time;
                    $total_view_time = sprintf('%02d:%02d:%02d', ($view_time / 3600), ($view_time / 60 % 60), $view_time % 60);

                    $lines[] = $total_view_time;
                }

                $alphabet_new = "A";
                for ($j = 0; count($lines) > $j; $j++) {
                    $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "$h", $lines[$j]);
                    $alphabet_new++;
                }
                $h++;
            }
        } else {
            $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
            $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
            $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
        }
    }
}


/* =================================================== Export Report =================================================== */

$filename = "$file_title.xlsx"; //save our workbook as this file name
ob_clean();
ob_end_clean();
header("Content-Type: application/vnd.ms-excel"); //mime type
header("Content-Disposition: attachment;filename=$filename"); //tell browser what's the file name
header("Cache-Control: max-age=0"); //no cache
ob_flush();
flush();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

//force user to download the Excel file without writing it to server's HD
$objWriter->save("php://output");