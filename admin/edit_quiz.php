<?php include 'header.php';
if(isset($_GET['id'])){
    $feedback_id = $_GET['id'];
}else{
    $_SESSION['error_msg'] = "Quiz not found!";
    echo "<script>window.location.href = 'feedbacks.php'</script>";
    //header("Location:feedbacks.php");
    exit();
}

if(isset($_POST['save_quiz_form_btn'])){
    $title = addslashes(trim($_POST['title']));
    $quiz_id = addslashes(trim($_POST['quiz_id']));
    if(empty($title)){
        $_SESSION['error_msg'] = "Please enter Quiz Title";
    }else{
        $sql = "UPDATE `quiz` SET `title` = '{$title}' WHERE event_id='".EVENT_ID."' AND id = '{$quiz_id}'";
        $res = mysql_query($sql);
        if($res){
            $_SESSION['success_msg'] = "Quiz saved successfully.";
            echo "<script>window.location.href = 'quiz.php'</script>";
            //header("Location:feedbacks.php");
            exit();
        }else{
            $_SESSION['error_msg'] = "Something goes wrong try again.";
        }
    }
}
$rs = mysql_query("SELECT * FROM quiz WHERE event_id='".EVENT_ID."' AND id = '{$feedback_id}' LIMIT 1");
$num_rows = mysql_num_rows($rs);
if(!$num_rows > 0){
    $_SESSION['error_msg'] = "Quiz not found!";
    echo "<script>window.location.href = 'quiz.php'</script>";
    exit();
}
$row = mysql_fetch_object($rs)
?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Update Quiz</span></h4>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">

                <?php include 'messages.php';?>

                <div class="row">
                    <div class="col-md-6">
                        <form action="" method="post" name="save_feedback_form">
                            <input type="hidden" name="quiz_id" value="<?=$row->id;?>">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Feedback Title</label>
                                        <textarea name="title" class="form-control" placeholder="Quiz Title" required><?=$row->title;?></textarea>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary" name="save_quiz_form_btn" value="save">Update</button>
                                        <a href="feedbacks.php" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /Content area -->
        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>