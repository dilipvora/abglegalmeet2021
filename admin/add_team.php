<?php
include 'header.php';
$id = '';
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $query = mysql_query("SELECT * from team where `id` = $id and `event_id` = '".EVENT_ID."'");
    $result = mysql_fetch_array($query);
}else{
    $result = array();
}
?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <div class="panel panel-flat">

                    <div id="announce_msg"></div>
                    <div class="panel-heading">
                        <h5 class="panel-title"><?=isset($_GET['id'])?'Edit Team':'Add Team'?></h5>
                    </div>


                    <div class="panel-body">
                        <form class="form-horizontal" action="process.php" method="post" id="frmaddteam">
                            <input type="hidden" name="id" value="<?=$id?>">
                            <fieldset class="content-group">
                                <div class="form-group">
                                    <div class="col-lg-10">
                                        <label>Team Name</label>
                                        <input class="form-control" name="team_name" placeholder="Enter Team Name" value="<?=isset($result['team_name'])?$result['team_name']:''?>" required>
                                    </div>
                                </div>
                                <input type="hidden" name="frmaddteam" value="1">
                            </fieldset>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary" id="btnSubmit">Save <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>
