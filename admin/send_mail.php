<?php

include '../config.php';

ob_start();

$data['event_id'] = EVENT_ID;
include_once('../mail_template.php');
$mail_content = ob_get_contents();
ob_get_clean();

date_default_timezone_set('Etc/UTC');
require '../../phpmailer/class.phpmailer.php';

$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 1;
$mail->Debugoutput = 'html';
$mail->Host = EMAIL_FROM_HOST;
$mail->Port = EMAIL_FROM_PORT;
$mail->SMTPSecure  = 'tls';
$mail->SMTPAuth = true;
$mail->isHTML(true);
$mail->Username = EMAIL_FROM;
$mail->Password = EMAIL_FROM_PASS;
$mail->setFrom(EMAIL_FROM,EMAIL_FROM_NAME);

if(!empty($send_mail_to_arr)){
	foreach($send_mail_to_arr as $to){
		$mail->addAddress($to, EMAIL_TO_NAME);
	}
}

$mail->Subject = COMPANY_NAME." Webcast Settings for ".EVENT_ID;
$mail->Body = $mail_content;
if (!$mail->send()) {
	echo json_encode(array('success'=>false,"message"=>"Mail Not Send Successfully!!"));
	exit();
}else{
	echo json_encode(array('success'=>true,"message"=>"Mail Send Successfully!!"));
	exit();
}
?>