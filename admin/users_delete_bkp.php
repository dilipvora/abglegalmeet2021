<?php include 'header.php';

if(isset($_SESSION['status']) && $_SESSION['status'] != '1'){
	echo '<meta http-equiv="refresh" content="0; URL=index.php">';
}

if(isset($_GET['delete']) && isset($_GET['uid']) && $_GET['uid'] !=''){
	$uid = $_GET['uid'];
	$sql = "delete from users where uid = '{$uid}' AND `event_id` = '".EVENT_ID."'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'User deleted successfully.';
}

if(isset($_GET['delete_all']) && $_GET['delete_all'] =='true'){
	//$sql = "truncate table users";
	$sql = "delete from users where `event_id` = '".EVENT_ID."'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'All Users deleted successfully.';
}

?>
<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4>
							<i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users Delete</span>
							<button onClick="if(confirm('Would You Like To Permanently Delete All Users?')){self.location='?delete_all=true';}" type="button" class="btn btn-warning float-right">
								Delete All <i class="fa fa-trash-o position-right"></i>
							</button>
						</h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				
				<div class="panel panel-flat">
					<table class="table datatable-basic">
							<thead>
								<tr>
									<th class="hidden"></th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Location</th>
									<th>Contact Number</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="mydata">
								<?php 
								$rs = mysql_query("SELECT * FROM `users` WHERE `event_id` = '".EVENT_ID."' ORDER BY uid DESC");
								$num_rows = mysql_num_rows($rs);
								if($num_rows){
									while ($row = mysql_fetch_object($rs)){
									?>
									<tr>
										<td class="hidden"></td>
										<td><?php echo $row->name; ?></td>
										<td><?php echo $row->email; ?></td>
										<td><?php echo $row->location; ?></td>
										<td><?php echo $row->mobile; ?></td>
										<td>
											<a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This User')){self.location='?delete&uid=<?php echo $row->uid;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
								<?php }}?>
							</tbody>
						</table>
						<input type="hidden" name="total_users" id="total_users" value="<?php echo $num_rows;?>">
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
<script type="text/javascript">

var question_ajax_call = function() {
	var total_users = $("#total_users").val();
	var dataString = "action=get_user_delete&total_users="+total_users;
	$.ajax({
		async: true,
		url: 'process.php',
		type: "POST",
		data: dataString,
		//contentType: false,
		cache: false,
		//processData:false,
		//dataType: "json",
		success: function(data){
			var count = data.split('<tr>').length - 1;
			$("#total_users").val(parseInt(total_users)+parseInt(count));
			$('#mydata').prepend(data);
		}
	});
};
var interval = 60000;
setInterval(question_ajax_call, interval);

</script>
</body>
</html>