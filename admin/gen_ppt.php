<?php 
$directory = "../upload/ppt/13";
$images_array = glob($directory . "*.*");

$img_length = sizeof($images_array);
if($img_length > 0){
	foreach($images_array as $filename){
		$old_img = basename($filename);
		
		$ext = pathinfo($old_img, PATHINFO_EXTENSION);
		$file = basename($old_img, ".".$ext);
		$file_num = substr($file, 5);
		$length = strlen($file_num);
		
		if($length == 1){
			$new_name = 'Slide00'.$file_num.'.'.$ext;
		}elseif($length == 2){
			$new_name = 'Slide0'.$file_num.'.'.$ext;
		}else{
			$new_name = 'Slide'.$file_num.'.'.$ext;
		}
		rename($directory.$old_img,$directory.$new_name);
	}
}
?>