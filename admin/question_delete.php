<?php include 'header.php';
if(isset($_SESSION['status']) && $_SESSION['status'] != '1'){
    echo '<meta http-equiv="refresh" content="0; URL=index.php">';
}

if(isset($_GET['delete']) && isset($_GET['q_id']) && $_GET['q_id'] !=''){
    $q_id = $_GET['q_id'];
    $sql = "delete from question where q_id = '{$q_id}' AND `event_id` = '".EVENT_ID."'";
    mysql_query($sql);
    $_SESSION['success_msg'] = 'Question deleted successfully.';
    echo '<script type="text/javascript">window.location.href="question_delete.php";</script>';
    exit();
}

if(isset($_GET['delete_all']) && $_GET['delete_all'] =='true'){
    //$sql = "truncate table question";
    $sql = "delete from question where `event_id` = '".EVENT_ID."'";
    mysql_query($sql);
    $_SESSION['success_msg'] = 'All Questions deleted successfully.';
    echo '<script type="text/javascript">window.location.href="question_delete.php";</script>';
    exit();
}

$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);

?>

<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Question Delete</span>
                            <button onClick="if(confirm('Would You Like To Permanently Delete All Questions?')){self.location='?delete_all=true';}" type="button" class="btn btn-warning float-right" style="margin-left: 10px">
                                Delete All <i class="fa fa-trash-o position-right"></i>
                            </button>
                            &nbsp;
                            <button type="button" class="btn btn-danger multipledelete float-right">Multiple Delete</button>
                        </h4>
                    </div>
                </div>
            </div>

            <!-- Content area -->
            <div class="content">
                <?php include 'messages.php';?>
                <div class="panel panel-flat">
                    <form name="delete_frm" id="delete_frm" action="process.php" method="post">
                        <input type="hidden" name="action" value="multiple_delete_question">
                        <input type="hidden" name="redirect" value="question_delete.php">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="hidden"></th>
                                    <th>
                                        <input type="checkbox" class="delete_all_question_chk">
                                    </th>
                                    <?php
                                    $filed_array = array();
                                    if($num_rows>0){
                                        while ($row = mysql_fetch_array($form_field_query)){
                                            array_push($filed_array,'u.'.$row['field_name']);?>
                                            <th><?=$row['field_label']?></th><?php
                                        }
                                    }
                                    ?>
                                    <th>Question</th>
                                    <th>Team</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="mydata">

                            <?php
                            $fields = implode(',',$filed_array);
                            $rs = mysql_query("SELECT q.*, $fields FROM question q, new_users u WHERE u.uid = q.uid AND q.event_id = '".EVENT_ID."' ORDER BY q_id DESC LIMIT 100");
                            $num_rows1 = mysql_num_rows($rs);
                            if($num_rows1){
                                while ($row = mysql_fetch_object($rs)){
                                    $fields_array = explode(',',$fields);
                                    ?>
                                    <tr>
                                        <td class="hidden"></td>
                                        <td><input type="checkbox" name="delete_question[]" class="delete_chk" value="<?=$row->q_id?>"> </td>
                                        <?php
                                        if(!empty($fields_array)){
                                            foreach ($fields_array as $item) {
                                                $field = str_replace('u.','',$item);?>
                                                <td><?php echo $row->$field; ?></td><?php
                                            }
                                        }
                                        ?>
                                        <td><?php echo $row->q_question; ?></td>
                                        <td><?php echo $row->team_id; ?></td>
                                        <td>
                                            <?php
                                            if($row->status == 1){ ?>
                                                <span class="label label-success">Approve</span>
                                            <?php }else if($row->status == 2){?>
                                                <span class="label label-info">Reject</span>
                                            <?php }else{?>
                                                <span class="label label-danger">Pending</span>
                                            <?php }?>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This Question?')){self.location='?delete&q_id=<?php echo $row->q_id;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                <?php }
                            }else{ ?>
                                <td colspan="<?=$num_rows + 6?>" align="center" class="no_data_row">No data available in table</td><?php
                            }?>
                            </tbody>
                        </table>
                    </form>
                    <input type="hidden" name="total_question" id="total_question" value="<?php echo $num_rows1;?>">
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
<script type="text/javascript">

    $(document).ready(function () {
        $('.delete_all_question_chk').on('click',function(){
            $(".delete_chk").prop('checked', $(this).prop('checked'));
            /*if(this.checked){
                $('.delete_chk').each(function(){
                    this.checked = true;

                });
            }else{
                $('.delete_chk').each(function(){
                    this.checked = false;
                });
            }*/
        });
        $(document).on('click','.delete_chk',function(){
            if($('.delete_chk:checked').length == $('.delete_chk').length){
                $('.delete_all_question_chk').prop('checked',true);
            }else{
                $('.delete_all_question_chk').prop('checked',false);
            }

        });

        $(document).on("click",".multipledelete",function () {
            $("#delete_frm").submit();
        });

    });

    var question_ajax_call = function() {
        var total_question = $("#total_question").val();
        var fields = '<?=$fields?>';
        var dataString = "action=get_question_delete&total_question="+total_question+"&fields="+fields;
        $.ajax({
            async: true,
            url: 'process.php',
            type: "POST",
            data: dataString,
            //contentType: false,
            cache: false,
            //processData:false,
            //dataType: "json",
            success: function(data){
                var count = data.split('<tr>').length - 1;
                $("#total_question").val(parseInt(total_question)+parseInt(count));
                $(".no_data_row").remove();
                $('#mydata').prepend(data);
            }
        });
    };
    var interval = 10000;
    setInterval(question_ajax_call, interval);

</script>
</body>
</html>