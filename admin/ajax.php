<?php include '../config.php';
include_once '../src/firebaseLib.php';

$event_id = EVENT_ID;

if (isset($_POST['get_users_count']) && $_POST['get_users_count'] == "true") {

    /* ========================================  Check User is login or not  ======================================== */
    $query = "SELECT uid FROM `new_users` WHERE `event_id` = '{$event_id}' and is_login = 1";
    $result = mysql_query($query);
    $num_rows = mysql_num_rows($result);
    $time = date('Y-m-d H:i:s');
    if ($num_rows) {
        while ($rows = mysql_fetch_array($result)) {
            $uid = $rows['uid'];
            $qry = "SELECT logout_time,id FROM `users_login_detail` where uid = '{$uid}' ORDER BY id DESC LIMIT 1";
            $res = mysql_query($qry);
            $count_rows = mysql_num_rows($res);
            if ($count_rows > 0) {
                $result_data = mysql_fetch_array($res);
                $logout_minute = date('i', strtotime($result_data['logout_time']));
                $current_minute = date('i');
                $id = $result_data['id'];
                ///
                $timeFirst = strtotime($result_data['logout_time']);
                $timeSecond = strtotime(date('Y-m-d H:i:s'));
                $differenceInSeconds = $timeSecond - $timeFirst;

                if ($differenceInSeconds > 310) {
                    mysql_query("UPDATE new_users SET is_login = '0' WHERE uid = '{$uid}'");
                }
            }
        }
    }

    /* ===========================================  No of attendees users  =========================================== */
    $event_start_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_start_date_time' and `event_id` = '{$event_id}'");
    $event_start_date_time_data = mysql_fetch_object($event_start_date_time);
    if(mysql_num_rows($event_start_date_time)>0){
        $event_start_date_time_value = date('Y-m-d H:i:s',strtotime($event_start_date_time_data->option_value));
        $converted_start_time = date('Y-m-d H:i:s',strtotime('-10 minutes',strtotime($event_start_date_time_value)));
    }else{
        $converted_start_time = '';
    }

    $event_end_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_end_date_time' and `event_id` = '{$event_id}'");
    $event_end_date_time_data = mysql_fetch_object($event_end_date_time);
    if(mysql_num_rows($event_end_date_time)>0){
        $event_end_date_time_value = date('Y-m-d H:i:s',strtotime($event_end_date_time_data->option_value));
    }else{
        $event_end_date_time_value = '';
    }

    if(!empty($converted_start_time) && !empty($event_end_date_time_value)) {
        $login_count = mysql_query("SELECT u.uid FROM `users_login_detail` as `ul` INNER JOIN `new_users` as `u` ON ul.uid = u.uid WHERE u.event_id = '{$event_id}' AND ul.login_time >= '{$converted_start_time}' AND ul.logout_time <= '{$event_end_date_time_value}' GROUP BY ul.uid");
        $count_rows = mysql_num_rows($login_count);
        if ($count_rows > 0) {
            $count_no_of_attendees_users = $count_rows;
        } else {
            $count_no_of_attendees_users = 0;
        }
    } else {
        $count_no_of_attendees_users = 0;
    }

    /* =============================================  No of live users  ============================================= */
    $login_count = mysql_query("SELECT uid from `new_users` where `event_id` = '{$event_id}' and is_login = 1");
    $count_rows = mysql_num_rows($login_count);
    if ($count_rows > 0) {
        $count_no_of_live_users = $count_rows;
    } else {
        $count_no_of_live_users = 0;
    }

    /* ==========================================  No of registered users  ========================================== */
    $no_of_users_result = mysql_query("SELECT `uid` FROM `new_users` WHERE `event_id` = '{$event_id}'");
    $count_no_of_users = mysql_num_rows($no_of_users_result);


    /* ==============================================  Total AVG time  ============================================== */
    //SELECT l.id, u.uid, sum(TIME_TO_SEC(TIMEDIFF(l.logout_time,l.login_time))) FROM users_login_detail l INNER JOIN new_users u ON l.uid = u.uid WHERE u.event_id = 'quiz' GROUP BY l.uid ORDER BY u.uid DESC
    //SELECT SUM(TIME_TO_SEC(TIMEDIFF(ul.logout_time,ul.login_time))) as view_time FROM `users_login_detail` as `ul` INNER JOIN `new_users` as `u` ON ul.uid = u.uid GROUP BY ul.uid HAVING view_time >= '58' ORDER BY u.uid DESC
    //$view_time_query = mysql_query("SELECT avg(TIME_TO_SEC(TIMEDIFF(l.logout_time,l.login_time))) AS timediff FROM users_login_detail l, new_users u WHERE u.event_id='{$event_id}' and u.uid=l.uid");
    $view_time_query = mysql_query("SELECT SUM(TIME_TO_SEC(TIMEDIFF(ul.logout_time,ul.login_time))) as total_view_time FROM `users_login_detail` as `ul` INNER JOIN `new_users` as `u` ON ul.uid = u.uid WHERE u.event_id = '{$event_id}' AND ul.login_time >= '{$converted_start_time}' AND ul.logout_time <= '{$event_end_date_time_value}' GROUP BY ul.uid HAVING total_view_time >= '10' ORDER BY u.uid DESC");
    $num_of_rows = mysql_num_rows($view_time_query);

    $total_view_time = 0;
    while ($item = mysql_fetch_object($view_time_query)) {
        $total_view_time += $item->total_view_time;
    }

    $avg_time = round($total_view_time / $num_of_rows);
    $avg_view_time = sprintf('%02d:%02d:%02d', ($avg_time / 3600), ($avg_time / 60 % 60), $avg_time % 60);

    $response_data = array(
        "no_of_attendees_users" => $count_no_of_attendees_users,
        "no_of_live_users" => $count_no_of_live_users,
        "no_of_users" => $count_no_of_users,
        "avg_view_time" => $avg_view_time
    );
    
    echo json_encode(array("data" => $response_data));
}