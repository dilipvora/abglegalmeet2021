<!-- Footer -->
<div class="footer text-muted">
	&copy; Copyright 2016 <a href="<?php echo POWERED_BY_URL; ?>" target="_blank"><?php echo POWERED_BY; ?></a> All right reserved.
</div>
<!-- /footer -->

<script>
$(".datepicker-menus").datepicker({
    changeMonth: true,
    changeYear: true
});
</script>