<?php include 'header.php';

if(isset($_SESSION['status']) && $_SESSION['status'] != '1'){
    echo '<meta http-equiv="refresh" content="0; URL=index.php">';
}

if(isset($_GET['delete']) && isset($_GET['uid']) && $_GET['uid'] !=''){
    $uid = $_GET['uid'];
    $sql = "delete from new_users where uid = '{$uid}' AND `event_id` = '".EVENT_ID."'";
    mysql_query($sql);

    $_SESSION['success_msg'] = 'User deleted successfully.';
}

if(isset($_GET['delete_all']) && $_GET['delete_all'] =='true'){
    //$sql = "truncate table users";
    $sql = "delete from new_users where `event_id` = '".EVENT_ID."'";
    mysql_query($sql);

    $_SESSION['success_msg'] = 'All Users deleted successfully.';
}

$form_field_query = mysql_query("SELECT * from registration_field where event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
//$user_detail = get_user_detail();

$users = mysql_query("SELECT * from `new_users` where `event_id` = '".EVENT_ID."'");
$num_of_rows = mysql_num_rows($users);
?>
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4>
                            <i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users Delete</span>
                            <button onClick="if(confirm('Would You Like To Permanently Delete All Users?')){self.location='?delete_all=true';}" type="button" class="btn btn-warning float-right">
                                Delete All <i class="fa fa-trash-o position-right"></i>
                            </button>
                        </h4>
                    </div>
                </div>
            </div>

            <!-- Content area -->
            <div class="content">
                <?php include 'messages.php';?>

                <div class="panel panel-flat">
                    <table class="table" id="delete_user_table">
                        <thead>
                        <tr><?php
                            $field_name_array = array();
                            if($num_rows>0){
                                while($row = mysql_fetch_object($form_field_query)){
                                    array_push($field_name_array,$row->field_name)?>
                                    <th><?=$row->field_label?></th><?php
                                } ?>
                                <th>Action</th><?php
                            } ?>
                        </tr>
                        </thead>
                        <tbody id="mydata">
                        <?php
                        if($num_of_rows>0){
                            while($item = mysql_fetch_array($users)){ ?>
                                <tr>
                                    <?php
                                    if(!empty($field_name_array)){
                                        foreach ($field_name_array as $field_row) { ?>
                                            <td><?=$item[$field_row]?></td><?php
                                        }
                                    }
                                    ?>
                                    <td>
                                        <a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This User')){self.location='?delete&uid=<?php echo $item['uid'];?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr><?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <input type="hidden" name="total_users" id="total_users" value="<?php echo $num_of_rows;?>">
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
<script type="text/javascript">

    var question_ajax_call = function() {
        var total_users = $("#total_users").val();
        var dataString = "action=get_user_delete&total_users="+total_users;
        $.ajax({
            async: true,
            url: 'process.php',
            type: "POST",
            data: dataString,
            //contentType: false,
            cache: false,
            //processData:false,
            //dataType: "json",
            success: function(data){
                var count = data.split('<tr>').length - 1;
                $("#total_users").val(parseInt(total_users)+parseInt(count));
                $('#mydata').prepend(data);
            }
        });
    };
    var interval = 60000;
    setInterval(question_ajax_call, interval);

</script>
<script>
    var question_table;
    $(document).ready(function () {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [ 3 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        var user_table = $("#delete_user_table").DataTable({
            /*/!*"processing": true,
            "ajax": {
                "url": "process.php?action=get_user",
                "type": "POST"
            },
            "columns": [

            ],*/
            "ordering": false,
            "lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
        });
        $("#delete_user_table_length select").select2();
    });
</script>


</body>
</html>