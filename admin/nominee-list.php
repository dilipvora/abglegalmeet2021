<?php include 'header1.php';
if(isset($_GET['delete']) && isset($_GET['fid']) && $_GET['fid'] !=''){
    $fid = $_GET['fid'];
    $sql = "delete from extra_form where fid = '{$fid}' AND `event_id` = '".EVENT_ID."'";
    mysql_query($sql);
    $_SESSION['success_msg'] = 'Nominee deleted successfully.';
    echo '<script type="text/javascript">window.location.href="nominee-list.php";</script>';
    exit();
} ?>

<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<!-- Main content -->
		<div class="content-wrapper">
            <!-- Content area -->
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Nominee List</span></h4>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="export-data.php?nominee-list" class="btn btn-primary">Download Full Report</a>
                                <a href="export-data.php?nominee-comments-list" class="btn btn-primary">Download Comments Report</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="row text-right">
                            <a href="export-user.php" class="btn btn-primary">Download Report</a>
                        </div>
                    </div>
                </div>-->

                <!-- Dashboard content -->
                <div class="panel panel-flat">
                    <table class="table" id="nominee_table">
                        <thead>
                            <tr>
                               <th width="15%">Image</th>
                               <th>Method</th>
                               <th>Name</th>
                               <th>Email</th>
                               <th>Contact no.</th>
                               <th>Business</th>
                               <th>Team</th>
                               <th>Description</th>
                               <th>Role</th>
                               <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody id="mydata">
                        </tbody>
                    </table>
                </div>
                <!-- /default ordering -->
                <!-- /dashboard content -->

            </div>
            <!-- /content area -->

        </div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>

<div id="show_model"></div>

<script>
    var question_table;
    $(document).ready(function () {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: true
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        var user_table = $("#nominee_table").DataTable({
 		   "processing": true,
		   "ajax": {
	            "url": "process.php?action=get_nominee",
 	            "type": "POST"
	        },
	        "columns": [
                { "data": "f1" },
                { "data": "f2" },
                { "data": "f3" },
                { "data": "f4" },
                { "data": "f5" },
                { "data": "f6" },
                { "data": "f7" },
                { "data": "f8" },
                { "data": "role" },
                { "data": "fid" }
	        ],
	        "ordering": false,
			"lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
		});
		$("#nominee_table_length select").select2();

        $(document).on("click", '#view-users', function () {
            var fid = $(this).data("id");
            $.ajax({
                method: "POST",
                url: "model/get-model-data.php?action="+$(this).attr('id'),
                data: {fid},
                dataType: "JSON",
                success: function (response) {
                    $("#show_model").html(response.data);
                    $("#show_nominee_modal").modal("show");
                }
            })
        });
        $(document).on("click", '#view-like-comments', function () {
            var fid = $(this).data("id");
            $.ajax({
                method: "POST",
                url: "model/get-model-data.php?action="+$(this).attr('id'),
                data: {fid},
                dataType: "JSON",
                success: function (response) {
                    $("#show_model").html(response.data);
                    $("#show_likeComments_modal").modal("show");
                }
            })
        });
	});
</script>