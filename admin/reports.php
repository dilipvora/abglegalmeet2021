<?php include '../config.php';
$event_id = EVENT_ID;

$users_form_field_query = mysql_query("SELECT * from registration_field where type != '7' AND event_id = '{$event_id}' ORDER BY display_order ASC");
$users_num_rows = mysql_num_rows($users_form_field_query);
$next_count_users = $users_num_rows - 1;

$question_form_field_query = mysql_query("SELECT * from registration_field where type != '7' AND display_question = 1 AND event_id = '{$event_id}' ORDER BY display_order ASC");
$question_num_rows = mysql_num_rows($question_form_field_query);
$next_count_question = $question_num_rows + 1; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reports</title>

    <link rel="shortcut icon" href="../images/favicon-32x32.png"/>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
    <!-- /core JS files -->
    <script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../assets/js/pages/datatables_basic.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>

    <style type="text/css">
        .big-font {
            font-size: 30px;
        }

        #count_no_of_attendees_users, #count_no_of_live_users, #count_no_of_users, #users_avg_view_time {
            margin-left: 10px;
            font-weight: bold;
        }

        .quick_box_success:hover {
            -webkit-box-shadow: 5px 5px 7px 3px rgba(67, 160, 71, 0.5);
            -moz-box-shadow: 5px 5px 7px 3px rgba(67, 160, 71, 0.5);
            box-shadow: 5px 5px 7px 3px rgba(67, 160, 71, 0.5);
        }

        .quick_box_indigo:hover {
            -webkit-box-shadow: 5px 5px 7px 3px rgba(57, 73, 171, 0.5);
            -moz-box-shadow: 5px 5px 7px 3px rgba(57, 73, 171, 0.5);
            box-shadow: 5px 5px 7px 3px rgba(57, 73, 171, 0.5);
        }

        .quick_box_pink:hover {
            -webkit-box-shadow: 5px 5px 7px 3px rgba(216, 27, 96, 0.5);
            -moz-box-shadow: 5px 5px 7px 3px rgba(216, 27, 96, 0.5);
            box-shadow: 5px 5px 7px 3px rgba(216, 27, 96, 0.5);
        }

        .quick_box_info:hover {
            -webkit-box-shadow: 5px 5px 7px 3px rgba(0, 172, 193, 0.5);
            -moz-box-shadow: 5px 5px 7px 3px rgba(0, 172, 193, 0.5);
            box-shadow: 5px 5px 7px 3px rgba(0, 172, 193, 0.5);
        }
        .avg_btn {
            transition: transform 0.3s;
            border: 1px solid white;
            color: white;
            margin-top: -16px;
        }
        .avg_btn:hover {
            transform: scale(1.05);
            color: black;
            background-color: rgba(255,255,255,0.5);
        }
    </style>
</head>

<body>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content">

                <div class="container-fluid">
                    <div class="row">

                        <!-- Current registered users -->
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="panel bg-success-600">
                                <div class="panel-body quick_box_success">
                                    <h1 class="no-margin">
                                        <i class="icon-users4 big-font"></i>
                                        <span id="count_no_of_users" class="big-font">0</span>
                                    </h1>
                                    <span class="h6">Total Registrations</span>
                                </div>
                            </div>
                        </div>
                        <!-- /Current registered users -->

                        <!-- Total live users -->
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <a href="attendee-reports.php" target="_blank">
                                <div class="panel bg-pink-600">
                                    <div class="panel-body quick_box_pink">
                                        <h1 class="no-margin">
                                            <i class="icon-user-check big-font"></i>
                                            <span id="count_no_of_live_users" class="big-font">0</span>
                                        </h1>
                                        <span class="h6">
                                            Total Live Attendees
                                            <i class="icon-circle-right2 pull-right mt-5"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- /Total live users -->

                        <!-- Total Attendees users -->
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <a href="attendee-reports.php" target="_blank">
                                <div class="panel bg-indigo-600">
                                    <div class="panel-body quick_box_indigo">
                                        <h1 class="no-margin">
                                            <i class="icon-users big-font"></i>
                                            <span id="count_no_of_attendees_users" class="big-font">0</span>
                                        </h1>
                                        <span class="h6">
                                            Total Attendees
                                            <i class="icon-circle-right2 pull-right mt-5"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- /Total Attendees users -->

                        <!-- AVG view time -->
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="panel bg-info-600">
                                <div class="panel-body quick_box_info">
                                    <h1 class="no-margin">
                                        <i class="icon-video-camera3 big-font"></i>
                                        <span id="users_avg_view_time" class="big-font">00:00:00</span>
                                    </h1>
                                    <span class="h6">
                                        Average View
                                        <a href="export-data.php?attendee_report" class="btn-labeled pull-right btn avg_btn"><b class="icon-download"></b>EXPORT</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- /AVG view time -->

                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">

                        <!-- Users list -->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="panel panel-body">
                                <div class="h2 no-margin pull-left">Total Registrations</div>
                                <div class="pull-right">
                                    <a href="export-data.php?users_list" class="btn btn-primary">Download Report</a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table" id="users_table">
                                        <thead>
                                        <tr>
                                            <?php $field_array = array();
                                            if ($users_num_rows > 0) {
                                                $count = 0;
                                                while ($row = mysql_fetch_object($users_form_field_query)) {
                                                    $field_array['data'][] = $count;
                                                    $count++; ?>
                                                    <th><?= $row->field_label ?></th>
                                                <?php }
                                            } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /Users list -->

                        <!-- Approved Questions list -->
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="panel panel-body">
                                <div class="h2 no-margin pull-left">Questions</div>
                                <div class="pull-right">
                                    <a href="export-data.php?questions_list" class="btn btn-primary">Download Report</a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table" id="question_table1">
                                        <thead>
                                        <tr>
                                            <th class="hidden"></th>
                                            <?php $filed_array = array();
                                            if ($question_num_rows > 0) {
                                                while ($row = mysql_fetch_array($question_form_field_query)) {
                                                    array_push($filed_array, 'u.' . $row['field_name']); ?>
                                                    <th><?= $row['field_label'] ?></th>
                                                <?php }
                                            } ?>
                                            <th>Question</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /Approved Questions list -->

                    </div>
                </div>
                <!-- /default ordering -->
                <!-- /dashboard content -->
                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; Copyright 2016 <a href="<?= POWERED_BY_URL; ?>" target="_blank"><?= POWERED_BY; ?></a> All
                    right reserved.
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->

<script type="text/javascript">
    $(document).ready(function () {
        var interval_time = 60000;

        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                targets: [<?=$next_count_users?>]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        /* =========================================== Users DataTable  =========================================== */
        var user_table = $("#users_table").DataTable({
            "stateSave": true,
            "processing": true,
            "ajax": {
                "url": "process.php?action=get_user",
                "type": "POST"
            },
            "columns": [
                <?php if (!empty($field_array['data'])) {
                foreach ($field_array['data'] as $field_item) { ?>
                {"data": <?=$field_item?>},
                <?php }
                } ?>
            ],
            "ordering": false,
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
        $("#users_table_length select").select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        /* ========================================= Question DataTable  ========================================= */
        <?php $fields = implode(',', $filed_array); ?>
        var fields = '<?=$fields?>';

        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                targets: [<?=$next_count_question?>]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        var question_table = $("#question_table1").DataTable({
            "stateSave": true,
            "processing": true,
            "ajax": {
                "url": "get_question_data.php?fields=" + fields
            },
            "columns": [
                {"data": 0, className: "hidden"},
                <?php $count = 1; for($i = 0;$i < $next_count_question;$i++){ ?>
                {"data": <?=$count?>},
                <?php $count++; } ?>
            ],
            "ordering": false,
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
        $("#question_table1_length select").select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });


        function get_users_count() {
            $.ajax({
                url: "ajax.php",
                method: "POST",
                data: {"get_users_count": "true"},
                dataType: "JSON",
                success: function (response) {
                    //console.log(response);
                    $("#count_no_of_attendees_users").html(response.data.no_of_attendees_users);
                    $("#count_no_of_live_users").html(response.data.no_of_live_users);
                    $("#count_no_of_users").html(response.data.no_of_users);
                    $("#users_avg_view_time").html(response.data.avg_view_time);
                }
            });
        }

        get_users_count();

        setInterval(function () {
            user_table.ajax.reload();
        }, interval_time);

        setInterval(function () {
            question_table.ajax.reload();
        }, interval_time);

        setInterval(function () {
            get_users_count();
        }, interval_time);

    });
</script>

</body>
</html>