<?php 

include 'header.php';
$webinar_id='';
$video_url='';
$video_ppt='';
$is_home_tab=0;
$tab_details='';
$current_ppt = '';
$current_slide = '';
$slide_no = 1;

$directory = "../upload/ppt/";
$images_array = glob($directory . "*.*");


if(isset($_POST['VideoUrlFormBtn'])){
	if(isset($_POST['video_url']) && $_POST['video_url'] !=''){
		$video_url = $_POST['video_url'];
		$webinar_id = $_POST['webinar_id'];

		if($webinar_id !=''){
			mysql_query("UPDATE `webinar` SET
					`video_url` = '{$video_url}'
					WHERE `id`= '{$webinar_id}' AND `event_id` = '".EVENT_ID."'");
		}else{
			mysql_query("INSERT INTO `webinar` SET
				`video_url` = '{$video_url}', `event_id` = '".EVENT_ID."'");
		}
		$_SESSION['success_msg'] = 'You have updated video iframe successfully.';
	}else{
		$_SESSION['error_msg'] = 'Please enter video iframe.';
	}
}


/*if(isset($_POST['VideoTabDetailsFormBtn'])){
	
	if($_POST['is_home_tab']){
		$is_hometab = 1;
	}else{
		$is_hometab = 0;
	}
	
	$tabdetails = serialize($_POST['tab']);
	
	$webinarId = $_POST['webinar_id'];
	
	if($webinarId ==''){
		mysql_query("INSERT INTO `webinar` SET `is_home_tab` = '{$is_hometab}', `tab_details` = '{$tabdetails}'");
	}else{
		mysql_query("UPDATE `webinar` SET `is_home_tab` = '{$is_hometab}', `tab_details` = '{$tabdetails}' WHERE `id`= '{$webinarId}'");
	}
	
	$_SESSION['success_msg'] = 'You have updated tab details successfully.';
	
}*/

$results = mysql_query("SELECT * FROM webinar WHERE `event_id` = '".EVENT_ID."'");

if(mysql_num_rows($results)>0){
	$row = mysql_fetch_object($results);
	$webinar_id = $row->id;
	$video_url = $row->video_url;
	$video_ppt = $row->video_thumb;
	//$is_home_tab = $row->is_home_tab;
	//$tab_details = unserialize($row->tab_details);
	$current_ppt = $row->current_ppt;
	$current_slide = $row->current_slide;
	$slide_no = $row->slide_no;
	
	/* if($current_slide ==''){
		$current_slide = $images_array[0];
	} */
	
	$images_array = glob($directory.$current_ppt.'/'. "*.*");
	if($current_slide ==''){
		$current_slide = $images_array[0];
	}
	
}

$event_lable = "Start Event";
if(isset($_SESSION['event_start']) && $_SESSION['event_start'] == 1) {
	$event_lable = "Stop Event";
}


$cursor_image_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'cursor_image' and `event_id` = '".EVENT_ID."'");
$cursor_image_data = mysql_fetch_object($cursor_image_option);
if(mysql_num_rows($cursor_image_option)>0){
    $show_hide_cursor = $cursor_image_data->option_value;
}else{
    $show_hide_cursor = 2;
}

?>
<div class="page-container">
	<div class="page-content">
		<?php include 'sidebar.php';?>
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4 class="col-md-6"><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Webcast</span></h4>
						<h4 class="col-md-6 text-right">
                            <button id="btnStartEvent" type="button" class="btn btn-success" name="QuizOn">Start Event</button>
                            <button id="btnStopEvent" type="button" class="btn btn-danger" style="display: none;" name="QuizOff">Stop Event</button>

							<!--<input type="button" class="btn btn-primary" id="StartEvent" name="StartEvent" value="<?/*= $event_lable */?>" onclick="start_event();"/>-->
						</h4>
						<br>
					</div>
				</div>
			</div>
				
			<div class="content">
				<?php include 'messages.php';?>
				<div class="row">
							    
					<div class="col-md-4">	
						<div class="panel panel-flat">
		                	<div class="panel-body">
		                		<?php if($_SESSION['status'] == 1 || $_SESSION['status'] == 0){?>
		                		<form name="upload-ppt-form" id="upload-ppt-form" method="post" action="process.php" enctype="multipart/form-data" class="hidden">
		                			<input type="hidden" name="submit-upload-ppt-form" value="true">
		                			<div class="form-group">
										<label>Upload PPT</label>
										<input type="file" name="ppt" id="ppt" class="form-control" value="" accept=".pptx" required="required">
									</div>
									<div class="text-right">
										<button type="submit" class="btn btn bg-indigo-700" name="submit-upload-ppt-form-btn" value="upload">Upload PPT</button>
									</div>
		                		</form>
		                		
		                		<div class="text-center">
									<button type="button" class="btn bg-indigo-700" name="upload-ppt-btn" id="upload-ppt-btn" value="upload">Upload PPT</button>
								</div>
		                		
		                		<hr/>
		                		<?php }?>
		                		<form name="select-ppt-form" id="select-ppt-form" method="post" action="process.php">
		                			<input type="hidden" name="submit-select-ppt-form" value="true">
		                			<input type="hidden" name="webinar_id" value="<?php echo $webinar_id;?>">
		                			<div class="form-group">
										<label>Select PPT</label>
										<?php $rs = mysql_query("SELECT * FROM `ppt` WHERE `event_id` = '".EVENT_ID."' ORDER BY id DESC");
											$num_rows = mysql_num_rows($rs);?>
										<select name="ppt-id" id="select-ppt-id" class="form-control">
											<option value="">Select PPT</option>
											<?php if($num_rows){
												$srno = 1;
												while ($row = mysql_fetch_object($rs)){?>
			                                		<option value="<?php echo $row->id;?>" <?php if($current_ppt == $row->id){?>selected="selected"<?php }?>><?php echo $row->name;?></option>
			                                <?php }
			                                 }?>
			                            </select>
									</div>
									<input type="submit" class="hidden" name="submit-select-ppt-form-btn" value="select ppt"/>
		                		</form>
		                	</div>
                            <div class="panel-heading">
                                <h6 class="panel-title">PPT Delay</h6>
                            </div>
                            <div class="panel-body">
                                <form action="" method="post" name="VideoUrlForm">
                                    <div class="form-group">
                                        <input id="PPTDelayValue" type="text" name="ppt_delay" placeholder="Enter PPT Delay" class="form-control" required="required" value="35000">
                                    </div>

                                    <div class="text-right">
                                        <button id="btnPPTDelay" type="button" class="btn bg-indigo-700" name="VideoUrlFormBtn">Update</button>
                                    </div>
                                </form>
                            </div>
		                </div>
		                
						<?php if($_SESSION['status'] == 1){?>
						<!-- Add Video URL -->
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">Video iFrame</h6>
			                </div>
		                	<div class="panel-body">
								<form action="" method="post" name="VideoUrlForm">
									<div class="form-group">
										<!-- <label>Video URL</label> -->
										<input type="hidden" name="webinar_id" value="<?php echo $webinar_id;?>">
										<textarea name="video_url" placeholder="Enter video iframe" rows="7" class="form-control" required="required"><?php echo $video_url;?></textarea>
									</div>
	
			                        <div class="text-right">
			                        	<button type="submit" class="btn bg-indigo-700" name="VideoUrlFormBtn">Update</button>
			                        </div>
		                        </form>
							</div>
						</div>
						<!-- End Add Video URL -->
						<?php }?>
						
						<!--
						<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Tab Details</h6>
								</div>

								<div class="panel-body">
									<form action="" method="post" name="VideoTabDetailsForm">	
										<input type="hidden" name="webinar_id" value="<?php echo $webinar_id;?>">
										
										<div class="form-group">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="is_home_tab" value="1" class="styled" <?php if($is_home_tab == 1){?>checked="checked"<?php }?>>
													Tab Display On Page
												</label>
											</div>
										 </div>
										 
											<div class="panel-group panel-group-control panel-group-control-right content-group-lg" id="accordion-control-right">
												<?php if(!empty($tab_details)){
													$tab_count = 0;
													$tab_checked='';
													foreach ($tab_details as $tab){
														$tab_title = 'Tab Title';
														if(isset($tab['title']) && $tab['title'] !=''){
															$tab_title = $tab['title'];
														}
														
														if(isset($tab['display']) && $tab['display'] ==1){
															$tab_checked = 'checked="checked"';
														}
													?>
												
												<div class="panel panel-white">
													<div class="panel-heading">
														<h6 class="panel-title">
															<a class="collapsed" data-toggle="collapse" data-parent="#accordion-control-right" href="#accordion-control-right-group<?php echo $tab_count;?>">
																<?php echo $tab_title;?>
															</a>
														</h6>
													</div>
													<div id="accordion-control-right-group<?php echo $tab_count;?>" class="panel-collapse collapse <?php if($tab_count == 0){ echo 'in1'; }?>">
														<div class="panel-body">
															
															<div class="form-group">
																<div class="checkbox">
																	<label>
																		<input type="checkbox" name="tab[<?php echo $tab_count;?>][display]" value="1" class="styled" <?php echo $tab_checked;?>>
																		Display On Page
																	</label>
																</div>
															</div>
															
															<div class="form-group">
																<input type="text" name="tab[<?php echo $tab_count;?>][title]" class="form-control" value="<?php echo $tab_title;?>" placeholder="Tab Title"/>
															</div>
															<?php if($tab_count !=2){?>
															<div class="form-group">
																<textarea rows="5" name="tab[<?php echo $tab_count;?>][details]" class="form-control" placeholder="Tab Details"><?php echo $tab['details'];?></textarea>
															</div>
															<?php }?>
														</div>
													</div>
												</div>
												<?php $tab_count++; }
												}else{
													$tab_count = 0;
													for($i=0;$i<4;$i++){
													?>
												<div class="panel panel-white">
													<div class="panel-heading">
														<h6 class="panel-title">
															<a class="collapsed" data-toggle="collapse" data-parent="#accordion-control-right" href="#accordion-control-right-group<?php echo $tab_count;?>">
																Title
															</a>
														</h6>
													</div>
													<div id="accordion-control-right-group<?php echo $tab_count;?>" class="panel-collapse collapse <?php if($tab_count == 0){ echo 'in1'; }?>">
														<div class="panel-body">
															
															<div class="form-group">
																<div class="checkbox">
																	<label>
																		<input type="checkbox" name="tab[<?php echo $tab_count;?>][display]" value="1" class="styled" checked="checked">
																		Display On Page
																	</label>
																</div>
															</div>
															
															<div class="form-group">
																<input type="text" name="tab[<?php echo $tab_count;?>][title]" class="form-control" value="Title" placeholder="Tab Title"/>
															</div>
															<?php if($tab_count !=2){?>
															<div class="form-group">
																<textarea rows="5" name="tab[<?php echo $tab_count;?>][details]" class="form-control" placeholder="Tab Details"></textarea>
															</div>
															<?php }?>
														</div>
													</div>
												</div>
												<?php $tab_count++;} 
												}?>
											</div>
										<div class="text-right">
				                        	<button type="submit" class="btn bg-indigo-700" name="VideoTabDetailsFormBtn">Update</button>
				                        </div>
									</form>
								</div>
							</div> -->
							
					</div>
					    
					<div class="col-md-8">
						<!-- Add Video PPT -->
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">Video Slide</h6>
			                </div>
		                	<div class="panel-body">
								<?php 
								    function js_str($s)
								    {
								    	return '"' . addcslashes($s, "\0..\37\"\\") . '"';
								    }
								     
								    function js_array($array)
								    {
								    	$temp = array_map('js_str', $array);
								    	return '['.implode(',',$temp).']';
								    }
								    $total_slide = sizeof($images_array);
								    $images = js_array($images_array);
							    ?>
							    
							    <div class="video_slide_section">
							    	<div id="slide_msg"></div>
					    
							    	<div class="col-md-4 text-left">
							    		<input type="image" id="prev_image" src="../../assets/images/previous.png" >
							    	</div>
							        
							        <div class="col-md-4 text-center">
							    		<h4 id="slide_img_name">Slide <span><?php echo $slide_no;?></span></h4>
							    	</div>
							    	<div class="col-md-4 text-right">
							    		<input type="image" id="next_image" src="../../assets/images/next.png" >
							    	</div>
							        
							        <input type="hidden" id="img_no" value="<?php echo $slide_no - 1;?>">
							        <div id="slide_cont">
							            <img src="../<?php echo $current_slide;?>" id="slideshow_image" class="img-thumbnail">
							            <img src="" id="cursor_image" style="max-width: 15px">
							        </div>
							        
							        <div class="video_slide_pagination">
							        	<ul class="pagination">
							        		<?php if($total_slide > 0):
							        		for($i = 1; $i <= $total_slide;$i++){
							        			$active = '';
							        			if($slide_no == $i){$active = "active";}
							        			echo '<li class="'.$active.'" data-no="'.$i.'"><a href="javascript:void(0)">'.$i.'</a></li>';
							        		}
							        		endif;?>
										</ul>
							        </div>
						        </div>
							</div>
						</div>
						<!-- End Add Video PPT -->

                        <?php
                        if($show_hide_cursor == 1){
                        ?>
						<!-- Select Cursor Images-->
						<div class="panel panel-flat cursor_image_section">
		                	<div class="panel-body" id="cursor-container">
		                	<?php $cursor_directory = "../../assets/images/cursor-img/";
							$cursor_images_array = glob($cursor_directory . "*.*");
							foreach($cursor_images_array as $filename){ 
								?>
								<span style="width:40px; height: 40px; display: table-cell;text-align: center;vertical-align: middle;">
									<img src="../../assets/images/cursor-img/<?=basename($filename)?>" class="select_cursor_image" style="max-width: 100%;">
								</span>
						<?php }	?>
		                	</div>
		                </div>
                        <?php } ?>
		                <!-- End Select Cursor Images-->
						
						<!-- Video Slide Thumbs -->
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">Video Slide Thumbs</h6>
			                </div>
		                	<div class="panel-body">
							    <div class="video_slide_thumb">
									<?php /*
									$img_no = 1;
									foreach($images_array as $image_thumb){
										echo '<div class="thumb-box"><span>'.$img_no.'</span><img src="'.$image_thumb.'" class="materialboxed img-thumbnail"></div>';
										$img_no++;
									}*/?>
						        </div>
							</div>
						</div>
						<!-- End Video Slide Thumbs -->
						
					</div>
				</div>
				<?php include 'footer.php';?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var images = <?php echo $images;?>;
var baseurl = '<?php echo SITE_URL;?>';
var thumb_images = <?=json_encode($images_array)?>;
/*var firebase_redik_webinar = new Firebase(firebase_url);
var firebase_current_slide = firebase_redik_webinar.child('current_slide');
var firebase_ppt_delay = firebase_redik_webinar.child('ppt_delay');*/

$(document).ready(function(){

    $("#btnStartEvent").click(function(){
        var send_data = { btn_start_event:'true' };
        $.ajax({
            type: "POST",
            async: true,
            url: "process.php",
            data: send_data,
            cache: false,
            success: function(result){
                $('#btnStartEvent').hide();
                $('#btnStopEvent').show();
            },
            error: function( result ) {

            }
        });
    });

    $("#btnStopEvent").click(function(){
        var send_data = { btn_stop_event:'true' };
        $.ajax({
            type: "POST",
            async: true,
            url: "process.php",
            data: send_data,
            cache: false,
            success: function(result){
                $('#btnStopEvent').hide();
                $('#btnStartEvent').show();
            },
            error: function( result ) {

            }
        });
    });
	
	$("#btnPPTDelay").click(function(){
        var send_data = { ppt_delay_time:$("#PPTDelayValue").val(),submit_ppt_delay_time:'true' };
        $.ajax({
            type: "POST",
            async: true,
            url: "process.php",
            data: send_data,
            cache: false,
            success: function(result){

            },
            error: function( result ) {

            }
        });
    });
	
    $( "#prev_image" ).click(function(){
      	prev();
    });
    $( "#next_image" ).click(function(){
       	next();
    });

    $( ".video_slide_pagination .pagination li" ).click(function(){
    	var no = $(this).attr('data-no');
    	pagination(no);
    	$(this).addClass('active').siblings().removeClass('active');
     });
    
});

function prev()
{	
	$('#slide_msg').html('');
       var prev_val = document.getElementById( "img_no" ).value;
       var prev_val = Number(prev_val) - 1;
       
       if(prev_val<= -1)
       { 
			$('#slide_msg').html('<div class="alert alert-danger alert-bordered">No previous image found</div>');
          //prev_val = images.length - 1;
       } else {
           	var img = images[prev_val].replace('../', '');
    	   	ajaxcall(img,(prev_val + 1));
	       	$( '#slideshow_image' ).attr( 'src' , baseurl+img );
	       	$('#slide_img_name span').html(prev_val + 1);
	       	document.getElementById( "img_no" ).value = prev_val;

	       	var pagination = prev_val + 1;
		   	$(".video_slide_pagination .pagination li[data-no='"+pagination+"']").addClass('active').siblings().removeClass('active');

			//firebase_current_slide.set({ src: baseurl+img});
       }
}

function next()
{
	$('#slide_msg').html('');
        var next_val = document.getElementById( "img_no" ).value;
        var next_val = Number(next_val)+1;
        if(next_val >= images.length)
        { 
        	$('#slide_msg').html('<div class="alert alert-danger alert-bordered">No next image found</div>');
          //next_val = 0;
        }else{
        	img = images[next_val].replace('../', '');
      	    ajaxcall(img,(next_val + 1));
	        $( '#slideshow_image' ).attr( 'src' , baseurl+img);
	        $('#slide_img_name span').html(next_val + 1);
	        document.getElementById( "img_no" ).value = next_val;

	        var pagination = next_val + 1;
	        $(".video_slide_pagination .pagination li[data-no='"+pagination+"']").addClass('active').siblings().removeClass('active');
	        //firebase_current_slide.set({ src: baseurl+img});
        }
}

function pagination(no)
{
	$('#slide_msg').html('');
	img = images[no - 1].replace('../', '');
    ajaxcall(img,(no));
	$( '#slideshow_image' ).attr( 'src' , baseurl+img);
	$('#slide_img_name span').html(no);
	document.getElementById( "img_no" ).value = no - 1;
	//firebase_current_slide.set({ src: baseurl+img});
}

function ajaxcall(slide,slide_no){
	var dataString = "action=set_current_slide&webinar_id=<?php echo $webinar_id;?>&slide=" + slide+"&slide_no="+slide_no;
	$.ajax({
		type: "POST",
		async: true,
		url: "../ajax.php",
		data: dataString,
		cache: false,
		success: function(result){ 
		   if(result == 'success'){
		   }else{
		   	   $("#slide_msg").html(result);
		  }
		},
		error: function( result ) {
			$('#slide_msg').html('Something goes wrong.');
		}
    });
}
function start_event(){
	
	var def_slide = "upload/ppt/<?php echo $current_ppt;?>/Slide001.JPG";
	ajaxcall(def_slide,'1')
    $( '#slideshow_image' ).attr( 'src' , '../'+def_slide);
    $('#slide_img_name span').html(1);
    document.getElementById( "img_no" ).value = 0;
    $(".video_slide_pagination .pagination li:first-child").addClass('active').siblings().removeClass('active');
    
	var dataString = "action=start_event&slide="+def_slide;
	$.ajax({
		type: "POST",
		async: true,
		url: "../ajax.php",
		data: dataString,
		cache: false,
		success: function(result){ 
		 	//$('#slide_msg').html(result);
		 	$("#StartEvent").val(result);
		},
		error: function( result ) {
			$('#slide_msg').html('Something goes wrong.');
		}
    });
}

/* Upload PPT */
$( "#upload-ppt-btn" ).click(function(){
   	$("#upload-ppt-form #ppt").click();
});

$( "#upload-ppt-form #ppt" ).change(function(){
   	$("#upload-ppt-form").submit();
   	$("#load_images").show();
});

/* Select PPT */
$( "#select-ppt-id" ).change(function(){
	var value = $(this).val();
	if(value !=''){
   		$("#select-ppt-form").submit();
	}
});
</script>
</body>
</html>