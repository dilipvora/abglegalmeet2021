<?php 

include '../config.php';

if(isset($_SESSION['admin_username']) && $_SESSION['admin_username'] !=''){
	header('location:dashboard.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login | Admin Panel | <?php echo COMPANY_NAME; ?></title>

	<!-- Global stylesheets -->
	<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<link href="../assets/css/webinar.css" rel="stylesheet" type="text/css">
	
	<!-- <link href="assets/css/style.css" rel="stylesheet" type="text/css"> -->
	
	<!-- Core JS files -->
	<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="../assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<span class="text-semibold" style="color: #fff"><?php echo COMPANY_NAME; ?></span>
				<!--<img height="50" src="../images/white_logo.png" alt="<?php echo COMPANY_NAME; ?>">-->
			</a>

			<!-- <ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul> -->
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container login-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Simple login form -->
					<form action="login_process.php" method="post" class="login-form">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object border-indigo-300 text-indigo-300"><i class="icon-reading"></i></div>
								<h5 class="content-group">Login to your account</h5>
							</div>
							
							<?php if(isset($_SESSION['error']) && $_SESSION['error'] !=''){?>
							<div class="alert alert-danger no-border">
								<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
								<span class="text-semibold"><?php echo $_SESSION['error'];?></span>
						    </div>
						    <?php }?>
							   
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Username" name="username" required="required">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Password" name="password" required="required">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>
							<input type="hidden" name="frmLogin" value="1">
							<div class="form-group">
								<button type="submit" class="btn bg-indigo-700 btn-block">Log in <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<!-- <div class="text-center">
								<a href="login_password_recover.html">Forgot password?</a>
							</div> -->
						</div>
					</form>
					<!-- /simple login form -->


					<!-- Footer -->
					<div class="footer text-muted">
						Powered by <a href="<?php echo POWERED_BY_URL; ?>" target="_blank"><?php echo POWERED_BY; ?></a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
