<?php

include 'header.php';
if(isset($_SESSION['status']) && $_SESSION['status'] != '1'){
    echo '<meta http-equiv="refresh" content="0; URL=index.php">';
}

if(isset($_GET['delete']) && isset($_GET['id']) && $_GET['id'] !=''){
    $q_id = $_GET['id'];
    $sql = "delete from team where id = '{$q_id}' AND `event_id` = '".EVENT_ID."'";
    mysql_query($sql);
    $_SESSION['success_msg'] = 'Team deleted successfully.';
    echo '<script type="text/javascript">window.location.href="team.php";</script>';
    exit();
}


?>

<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Team</span>
                            <a href="add_team.php" class="btn btn-primary float-right">
                                Add New
                            </a>
                        </h4>
                    </div>
                </div>
            </div>

            <!-- Content area -->
            <div class="content">
                <?php include 'messages.php';?>
                <div class="panel panel-flat">
                    <table class="table" id="team_datatable">
                        <thead>
                        <tr>
                            <th class="hidden"></th>
                            <th>Team Name</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody id="mydata">
                        <?php
                        $rs = mysql_query("SELECT * FROM team WHERE event_id = '".EVENT_ID."' ORDER BY team_name ASC");
                        $num_rows = mysql_num_rows($rs);
                        if($num_rows){
                            while ($row = mysql_fetch_object($rs)){
                                ?>
                                <tr>
                                    <td class="hidden"></td>
                                    <td><?php echo $row->team_name; ?></td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This Team?')){self.location='?delete&id=<?php echo $row->id;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
                                        <a href="add_team.php?id=<?=$row->id?>"  class="label label-success"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            <?php }}?>
                        </tbody>
                    </table>
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>

</body>
</html>
<script>
    $(document).ready(function () {
        $("#team_datatable").DataTable({
            autoWidth: false,
            columnDefs: [{
                targets: [ 1 ]
            }]
        });
        $('.dataTables_length select').select2({
            minimumResultsForSearch: "-1"
        });
    });

</script>