<?php 
include 'header.php'; 
$form_field_query = mysql_query("SELECT * from registration_field where event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span></h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row text-right">
								<a href="export-user.php" class="btn btn-primary">Download Report</a>
							</div>
						</div>
					</div>
						
					<!-- Dashboard content -->
					<div class="panel panel-flat">
						<table class="table" id="users_table">
							<thead>
								<tr>
                                    <?php
                                    $field_array = array();
									if($num_rows>0){
									    $count = 0;
										while($row = mysql_fetch_object($form_field_query)){
                                            $field_array['data'][] = $count;
                                            $count++;?>
											<th><?=$row->field_label?></th><?php
										}
									} ?>
								</tr>
							</thead>
                            <tbody id="mydata">
							</tbody>
						</table>
						
						<input type="hidden" name="total_users" id="total_users" value="<?php echo $num_rows;?>">
					</div>
					<!-- /default ordering -->
					<!-- /dashboard content -->
					<?php include 'footer.php';?>

				</div>
				<!-- /content area -->

			</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>

<script>
    var question_table;
    $(document).ready(function () {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [ 3 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        var user_table = $("#users_table").DataTable({
 		   "processing": true,
		   "ajax": {
	            "url": "process.php?action=get_user",
 	            "type": "POST"
	        },
	        "columns": [
	            <?php
                if(!empty($field_array['data'])){
                    foreach ($field_array['data'] as $field_item) { ?>
                            { "data": <?=$field_item?>}, <?php
                    }
                } ?>

	        ],
	        "ordering": false,
			"lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
		});
		$("#users_table_length select").select2();
	});
</script>