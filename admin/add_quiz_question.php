<?php include 'header.php';
if(isset($_GET['quiz_id'])){
    $quiz_id = $_GET['quiz_id'];
}else{
    $_SESSION['error_msg'] = "Quiz not found!";
    echo "<script>window.location.href = 'quiz.php'</script>";
    //header("Location:feedbacks.php");
    exit();
}
if(isset($_POST['save_quiz_form_btn'])){

    $quiz_id = addslashes(trim($_POST['quiz_id']));
    $title = addslashes(trim($_POST['title']));
    $is_objective = isset($_POST['is_objective'])?1:0;
    $option1 = addslashes(trim($_POST['option1']));
    $option2 = addslashes(trim($_POST['option2']));
    $option3 = addslashes(trim($_POST['option3']));
    $option4 = addslashes(trim($_POST['option4']));
    $option5 = addslashes(trim($_POST['option5']));
    $option6 = addslashes(trim($_POST['option6']));
    $option7 = addslashes(trim($_POST['option7']));
    $correct_answer = addslashes(trim($_POST['correct_answer']));
    $image_name = addslashes(trim($_POST['image_name']));
    $correct_option = empty($_POST['correct_option'])?0:trim($_POST['correct_option']);


    $sql = "INSERT INTO `quiz_questions` SET
                        `event_id` = '".EVENT_ID."',
						`quiz_id` = '{$quiz_id}',
						`title` = '{$title}',
						`is_objective` = '{$is_objective}',
						`option1` = '{$option1}',
						`option2` = '{$option2}',
						`option3` = '{$option3}',
						`option4` = '{$option4}',
						`option5` = '{$option5}',
						`option6` = '{$option6}',
						`option7` = '{$option7}',
						`correct_answer` = '{$correct_answer}',
						`image` = '{$image_name}',
						`correct_option` = '{$correct_option}'";
    $res = mysql_query($sql);

    if($res){
        $_SESSION['success_msg'] = "Question saved successfully.";
        echo "<script>window.location.href = 'quiz_questions.php?quiz_id=".$quiz_id."'</script>";
        //header("Location:feedbacks.php");
        exit();
    }else{
        $_SESSION['error_msg'] = "Something goes wrong try again.";
    }
}
?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add Quiz Question</span></h4>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">

                <?php include 'messages.php';?>

                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post" name="save_feedback_form">
                            <input type="hidden" name="quiz_id" value="<?=$quiz_id?>">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="col-md-12 form-group">
                                        <label>Question</label>
                                        <textarea name="title" class="form-control" placeholder="Question" required></textarea>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Is Objective Question</label>
                                        <input type="checkbox" name="is_objective" class="is_objective" value="1" checked="checked" />
                                    </div>
                                    <div class="option_section">
                                        <div class="col-md-6 form-group">
                                            <label>Option 1</label>
                                            <input type="text" name="option1" class="form-control options" placeholder="Option 1" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 2</label>
                                            <input type="text" name="option2" class="form-control options" placeholder="Option 2" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 3</label>
                                            <input type="text" name="option3" class="form-control options" placeholder="Option 3" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 4</label>
                                            <input type="text" name="option4" class="form-control options" placeholder="Option 4" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 5</label>
                                            <input type="text" name="option5" class="form-control options" placeholder="Option 5" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 6</label>
                                            <input type="text" name="option6" class="form-control options" placeholder="Option 6" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 7</label>
                                            <input type="text" name="option7" class="form-control options" placeholder="Option 7" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>True Answer</label>
                                            <select class="form-control" name="correct_option">
                                                <option value="1">Option 1</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="5">Option 5</option>
                                                <option value="6">Option 6</option>
                                                <option value="7">Option 7</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>Correct Answer (Only for non Objective que)</label>
                                        <input type="text" name="correct_answer" class="form-control correct_answer" placeholder="Correct Answer" >
                                        <p class="text-danger">*One word only</p>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Image Name</label>
                                        <input type="text" name="image_name" class="form-control" placeholder="Image Name" value="">
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-primary" name="save_quiz_form_btn" value="save">Add</button>
                                        <a href="quiz_questions.php?quiz_id=<?=$quiz_id?>" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /Content area -->
        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<script>
    $(document).ready(function(){
        $(document).on('change','.is_objective',function() {
            if ($(this).is(":checked")) {
                $(".option_section").removeClass('hidden');
                //$('input.options').prop('required',true);
            } else {
                $(".option_section").addClass('hidden');
                $('input.options').prop('required',false);
            }
        });
    });
</script>
<!-- End Page container -->
</body>
</html>