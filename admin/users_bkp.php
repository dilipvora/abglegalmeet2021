<?php include 'header.php'; ?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Users</span></h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row text-right">
	                        <!--    <?php
							$hidden_date = '';
							if(isset($_POST['date']) && $_POST['date'] !='' && $_POST['date'] !='1970-01-01'){
								$hidden_date = $_POST['date'];
							}?>
	                                    <input type="hidden" name="user_hidden_date" id="user_hidden_date" value="<?php echo $hidden_date;?>">
								<form class="form-inline" role="form" name="view-users-form" method="post" action="" style="display: inline-block;">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar"></i></span>
											<input type="text" name="date" class="form-control datepicker-menus" value="<?php if(isset($_POST['date'])){echo $_POST['date'];}?>" placeholder="Select Date">
										</div>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-primary" name="submit-view-users-form-btn" value="save">View Users</button>
									</div>
								</form>-->
								<a href="export-user.php" class="btn btn-primary">Download Report</a>
							</div>
						</div>
					</div>
						
					<!-- Dashboard content -->
					<div class="panel panel-flat">
						<table class="table" id="user_table">
							<thead>
								<tr>
									<th class="hidden"></th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Location</th>
									<th>Contact Number</th>
									<th>Created Date</th>
								</tr>
							</thead>
							<tbody id="mydata">
								
							</tbody>
						</table>
						
						<input type="hidden" name="total_users" id="total_users" value="<?php echo $num_rows;?>">
					</div>
					<!-- /default ordering -->
					<!-- /dashboard content -->
					<?php include 'footer.php';?>

				</div>
				<!-- /content area -->

			</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>