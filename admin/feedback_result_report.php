<?php include '../config.php';
$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);

$rs_sel_feedback = get_selected_with_where('options', "event_id = '" . EVENT_ID . "' AND option_name='feedback_id'");
$feedback_id = $rs_sel_feedback[0]->option_value;
$rs_question_feedback = get_selected_with_where('feedback_questions', "event_id = '" . EVENT_ID . "' AND feedback_id = '{$feedback_id}'");

$header = '';
$result = '';
$filed_array = array();
if ($num_rows > 0) {
    while ($row = mysql_fetch_array($form_field_query)) {
        array_push($filed_array, 'u.' . $row['field_name']);
        $header .= $row['field_label'] . "\t";
    }
}

$fields = implode(',', $filed_array);
$final_result = array();
if (!empty($rs_question_feedback)) {
    $counter = 1;
    foreach ($rs_question_feedback as $rows) {
        $header .= $rows->title . "\t";
        $question_id = $rows->id;
        $rs = mysql_query("SELECT r.*, $fields FROM users_feedbacks r, new_users u WHERE u.uid = r.uid AND r.event_id = '" . EVENT_ID . "' AND r.question_id = $question_id");
        $num_rows1 = mysql_num_rows($rs);

        if ($num_rows1) {
            $result_data = array();
            while ($row = mysql_fetch_object($rs)) {
                $result_data[] = $row;
            }
        }

        $final_result["Q" . $counter] = $result_data;
        $counter++;
    }
}

foreach ($final_result["Q1"] as $key => $item_row) {
    $fields_array = explode(',', $fields);
    $line = '';
    if (!empty($fields_array)) {
        foreach ($fields_array as $item) {
            $field = str_replace('u.', '', $item);
            $line .= $item_row->$field . "\t";
        }
    }

    $question_id = $item_row->question_id;
    $option = $item_row->answer;

    $is_objective = get_values("feedback_questions", "is_objective", "event_id='" . EVENT_ID . "' AND id=$question_id");
    if ($is_objective == 1) {
        $feedback_answer = get_values("feedback_questions", "option$option", "event_id='" . EVENT_ID . "' AND id=$question_id");
    } elseif ($is_objective == 2) {
        $options_array = explode(",", $option);
        $get_feedback_answer = array();
        foreach ($options_array as $value) {
            $get_feedback_answer[] = get_values("feedback_questions", "option$value", "event_id='" . EVENT_ID . "' AND id=$question_id");
        }
        $feedback_answer = implode(", ", $get_feedback_answer);
        //$feedback_answer = get_values("feedback_questions", "option$option", "event_id='" . EVENT_ID . "' AND id=$question_id");
    } else {
        $feedback_answer = $option;
    }
    $line .= $feedback_answer . "\t";

    if ($counter >= 2) {
        for ($q_counter = 2; $q_counter <= $counter; $q_counter++) {
            $que_q = "Q$q_counter";
            if (isset($final_result["$que_q"][$key])) {
                $question_id = $final_result["$que_q"][$key]->question_id;
                $option = $final_result["$que_q"][$key]->answer;
                $is_objective = get_values("feedback_questions", "is_objective", "event_id='" . EVENT_ID . "' AND id=$question_id");
                if ($is_objective == 1) {
                    $feedback_answer = get_values("feedback_questions", "option$option", "event_id='" . EVENT_ID . "' AND id=$question_id");
                } elseif ($is_objective == 2) {
                    $options_array = explode(",", $option);
                    $get_feedback_answer = array();
                    foreach ($options_array as $value) {
                        $get_feedback_answer[] = get_values("feedback_questions", "option$value", "event_id='" . EVENT_ID . "' AND id=$question_id");
                    }
                    $feedback_answer = implode(", ", $get_feedback_answer);
                    //$feedback_answer = get_values("feedback_questions", "option$option", "event_id='" . EVENT_ID . "' AND id=$question_id");
                } else {
                    $feedback_answer = $option;
                }
                $line .= $feedback_answer . "\t";
            }
        }
    }

    $result .= trim($line) . "\n";
}

//die;

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Feedback Result.xls");
header('Cache-Control: max-age=0');
header("Pragma: no-cache");
header("Expires: 0");
echo "$header\n$result";
?>