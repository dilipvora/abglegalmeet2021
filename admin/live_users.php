<?php 
include 'header.php';

$result = mysql_query("SELECT count(uid) as total_uid,location FROM `new_users` where `event_id` = '".EVENT_ID."' ORDER BY `total_uid` DESC");
$num_row = mysql_num_rows($result);

$login_count = mysql_query("SELECT count(uid) as total_login from `new_users` where `event_id` = '".EVENT_ID."' and is_login = 1");
$count_result = mysql_fetch_array($login_count);
$count_rows = mysql_num_rows($login_count);
if($count_rows>0){
	$total_login_users = $count_result['total_login'];
}else{
	$total_login_users = 0;
}

if(1 || $_SESSION['status'] == 1){

	$query = "select count(users_login_detail.id) as total_users,users_login_detail.logout_time from users_login_detail INNER JOIN new_users ON new_users.uid = users_login_detail.uid where new_users.event_id = '".EVENT_ID."' and users_login_detail.logout_time >= DATE_SUB(NOW(),INTERVAL 2 HOUR) GROUP BY ((60/5) * HOUR(users_login_detail.logout_time ) + FLOOR( MINUTE(users_login_detail.logout_time) / 5 )) ORDER BY users_login_detail.logout_time ASC";

	$result_data = mysql_query($query);
	$result_rows = mysql_num_rows($result_data);
	$chart_array = array();
	$chart_label_array = array();
	if($result_rows>0){
		while($row = mysql_fetch_array($result_data)){
			
			array_push($chart_array,(int)$row['total_users']);
			array_push($chart_label_array,$row['logout_time']);
		}
	}
}

$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
$next_count = $num_rows;
?>
<style>
	text.highcharts-credits{
		display:none;
	}
</style>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-body">
									<h3>Concurrent Users: <span class="total_login_users"><?=$total_login_users?></span></h3>	
								</div>
							</div>
						</div>
						<?php if($_SESSION['status'] == 1){?>
							<div class="col-md-6">
								<div class="panel panel-flat">
									<div class="panel-body">
										<button type="button" class="btn btn-primary send_mail">Send Settings <i class="fa fa-spinner fa-spin loader" style="font-size:18px;display:none"></i></button><br/><br/>
										
										<div class="alert alert-success alert-bordered successmsg" style="display:none">
											<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
											<span class="text-semibold successmessage"></span>
										</div>
										<div class="alert alert-danger alert-bordered errormsg" style="display:none">
											<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
											<span class="text-semibold errmessage"></span>
										</div>
									</div>
								</div>
							</div><?php
						} ?>
					</div>
					<?php if(1 || $_SESSION['status'] == 1){?>
						<div class="row">
							<div class="col-md-12">
								<div id="chart_div"></div>
							</div>
						</div>
					<?php } ?>
					
					<!-- Dashboard content -->
					<div class="row">
						<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
							<div class="page-header">
								<div class="page-header-content">
									<div class="page-title">
										<h4>
											<i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Location Wise Users Count</span>
										</h4>
									</div>
								</div>
							</div>
							<div class="panel panel-flat">
								<table class="table" id="location_datatable">
									<thead>
										<tr>
											<th class="hidden"></th>
											<th><b>Location</b></th>
											<th><b>Count</b></th>
										</tr>
									</thead>
									<tbody id="mydata"><?php
										if($num_row){
											while ($row = mysql_fetch_object($result)){ ?>
												<tr>
													<td><?=$row->location?></td>
													<td><?=$row->total_uid?></td>
												</tr><?php
											}
										} ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
							<div class="page-header">
								<div class="page-header-content">
									<div class="page-title">
										<h4>
											<i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Concurrent Users</span>
										</h4>
									</div>
								</div>
							</div>
							<div class="panel panel-flat">
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table" id="dashboard_concurrent_user_datatable">
											<thead>
												<tr>
													<th class="hidden"></th>
                                                    <?php
                                                    $filed_array = array();
                                                    if($num_rows>0){
                                                        while ($row = mysql_fetch_array($form_field_query)){
                                                            array_push($filed_array,$row['field_name']);?>
                                                            <th><?=$row['field_label']?></th><?php
                                                        }
                                                    }
                                                    ?>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<!-- /dashboard content -->
					<?php include 'footer.php';?>

				</div>
				<!-- /content area -->

			</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>

<script>
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
            orderable: false,
            width: '100px',
            targets: [ 2 ]
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
    var dashboard_user_datatable = $("#dashboard_concurrent_user_datatable").DataTable({
        "ajax": {
            "url": "process.php",
            "method":"POST",
            "data":{user_data:true,field:'<?=implode(',',$filed_array)?>'}
        },
        "columns": [
            { "data": 0, className: "hidden" },
            <?php $count = 1; for($i=0;$i<$next_count;$i++){ ?>
            { "data": <?=$count?> },
            <?php $count++; } ?>
        ],
        "columnDefs": [{
            "targets": [ 0 ]
        }],
        "ordering": false,
        "bLengthChange" : false,
        "lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
    } );
</script>

<script>
$(document).ready(function(){
	
	/* Location Datatable in Dashboard */
		
	$("#location_datatable").DataTable({
		"columns": [
 	            { "data": 0 },
 	            { "data": 1 }
 	        ],
		"columnDefs": [{ 
			"targets": [ 1 ]
		}],
		"bLengthChange" : false,
	});
	
	setInterval(function(){ check_login_time(); }, 60000);
	check_login_time();
	var category_array = <?=json_encode($chart_label_array)?>;	
	var chart_data_array = <?=json_encode($chart_array)?>;	
	<?php if(1 || $_SESSION['status'] == 1){?>
		draw_chart(category_array,chart_data_array);
		setInterval(function(){ check_chart_data(); }, 30000);
	<?php } ?>
	
	$(document).on("click",".send_mail",function(){
		$(".loader").show();
		$.ajax({
			type: "POST",
			url: "send_mail.php",
			async: true,
			cache: false,
			dataType:"json",
			success: function(result){
				$(".loader").hide();
				if(result.success == true){
					$(".successmessage").html(result.message);
					$(".successmsg").show();
				}else{
					$(".errmessage").html(result.message);
					$(".errormsg").show();
				}
			},
			error: function( result ) {
				
			}
		});
	});
});

function check_chart_data(){
	$.ajax({
		type: "POST",
		async: true,
		url: "ajax_draw_chart.php",
		cache: false,
		dataType:"json",
		success: function(result){
			draw_chart(result.chart_label,result.chart_data);
		},
		error: function( result ) {
			
		}
	});
}

function check_login_time(){
	$.ajax({
		type: "POST",
		async: true,
		url: "check_login.php",
		cache: false,
		success: function(result){
			$(".total_login_users").html(result)
		},
		error: function( result ) {
			
		}
	});
}

function draw_chart(category_array,chart_data_array){
	Highcharts.chart('chart_div', {

		title: {
			text: 'Concurrent Users Report'
		},
		xAxis: {
			categories: category_array
		},
		yAxis: {
			title: {
				text: 'Number of Users'
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},

		plotOptions: {
			series: {
				label: {
					connectorAllowed: false
				}
			}
		},

		series: [{
			name: 'Users',
			data: chart_data_array
		}],
		exporting: { enabled: false },
		responsive: {
			rules: [{
				condition: {
					maxWidth: 500
				},
				chartOptions: {
					legend: {
						layout: 'horizontal',
						align: 'center',
						verticalAlign: 'bottom'
					}
				}
			}]
		}

	});
}
</script>