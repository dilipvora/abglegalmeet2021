            	<?php if(isset($_SESSION['success_msg']) && $_SESSION['success_msg'] !=''):?>
					   <div class="alert alert-success alert-bordered">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><?php echo $_SESSION['success_msg'];?></span>
					    </div>
	             	<?php $_SESSION['success_msg']=''; ?>
	             <?php endif;?>
	             	
	             
	             <?php if(isset($_SESSION['error_msg']) && $_SESSION['error_msg'] !=''):?>
					   	<div class="alert alert-danger alert-bordered">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><?php echo $_SESSION['error_msg'];?></span>
					    </div>
	                <?php $_SESSION['error_msg']=''; ?>
                 <?php endif;?>