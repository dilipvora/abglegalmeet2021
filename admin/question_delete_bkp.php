<?php include 'header.php';
if(isset($_SESSION['status']) && $_SESSION['status'] != '1'){
	echo '<meta http-equiv="refresh" content="0; URL=index.php">';
}

if(isset($_GET['delete']) && isset($_GET['q_id']) && $_GET['q_id'] !=''){
	$q_id = $_GET['q_id'];
	$sql = "delete from question where q_id = '{$q_id}' AND `event_id` = '".EVENT_ID."'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'Question deleted successfully.';
	echo '<script type="text/javascript">window.location.href="question_delete.php";</script>';
	exit();
}

if(isset($_GET['delete_all']) && $_GET['delete_all'] =='true'){
	//$sql = "truncate table question";
	$sql = "delete from question where `event_id` = '".EVENT_ID."'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'All Questions deleted successfully.';
	echo '<script type="text/javascript">window.location.href="question_delete.php";</script>';
	exit();
}

?>

<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Question Delete</span>
							<button onClick="if(confirm('Would You Like To Permanently Delete All Questions?')){self.location='?delete_all=true';}" type="button" class="btn btn-warning float-right">
								Delete All <i class="fa fa-trash-o position-right"></i>
							</button>
						</h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				<div class="panel panel-flat">
					<table class="table datatable-basic">
							<thead>
								<tr>
                                    <th class="hidden"></th>
                                    <th>User Name</th>
                                    <th>Location</th>
                                    <th>Question</th>
                                    <th>Team</th>
                                    <th>Status</th>
                                    <th>Action</th>
								</tr>
							</thead>
							<tbody id="mydata">
								<?php 
								$rs = mysql_query("SELECT q.*, u.name, u.location FROM question q, users u WHERE u.uid = q.uid AND q.event_id = '".EVENT_ID."' ORDER BY q_id DESC");
								$num_rows = mysql_num_rows($rs);
								if($num_rows){
									while ($row = mysql_fetch_object($rs)){
                                        if(!empty($row->team_id)) {
                                            $team_id = $row->team_id;
                                            $team_rs = mysql_query("SELECT `team_name` from `team` where `id` = $team_id and `event_id` = '".EVENT_ID."'");
                                            $num = mysql_num_rows($team_rs);
                                            if($num > 0){
                                                $row1=mysql_fetch_object($team_rs);

                                                $team_name = $row1->team_name;
                                            }else{
                                                $team_name = '';
                                            }
                                        }else{
                                            $team_name = '';
                                        }
									?>
									<tr>
										<td class="hidden"></td>
										<td><?php echo $row->name; ?></td>
										<td><?php echo $row->location; ?></td>
										<td><?php echo $row->q_question; ?></td>
                                        <td><?php echo $team_name; ?></td>
										<td>
											<?php 
											if($row->status == 1){ ?>
											<span class="label label-success">Approve</span>
											<?php }else if($row->status == 2){?>
											<span class="label label-info">Reject</span>
											<?php }else{?>
											<span class="label label-danger">Pending</span>
											<?php }?>
										</td>
										<td>
											<a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This Question?')){self.location='?delete&q_id=<?php echo $row->q_id;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
								<?php }}?>
							</tbody>
						</table>
						<input type="hidden" name="total_question" id="total_question" value="<?php echo $num_rows;?>">
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
<script type="text/javascript">

var question_ajax_call = function() {
	var total_question = $("#total_question").val();
	var dataString = "action=get_question_delete&total_question="+total_question;
	$.ajax({
		async: true,
		url: 'process.php',
		type: "POST",
		data: dataString,
		//contentType: false,
		cache: false,
		//processData:false,
		//dataType: "json",
		success: function(data){
			var count = data.split('<tr>').length - 1;
			$("#total_question").val(parseInt(total_question)+parseInt(count));
			$('#mydata').prepend(data);
		}
	});
};
var interval = 30000;
setInterval(question_ajax_call, interval);

</script>
</body>
</html>