<?php 
	include 'header.php';

	if(isset($_GET['feedback_id'])){
		$feedback_id = $_GET['feedback_id'];
	}else{
		$_SESSION['error_msg'] = "Feedback not found!";
		echo "<script>window.location.href = 'feedbacks.php'</script>";
		//header("Location:feedbacks.php");
		exit();
	}
?>

<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4>
							<i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Feedback Results</span>
						</h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				<div class="panel panel-flat">
					<table class="table datatable-basic">
							<thead>
								<tr>
									<th class="hidden"></th>
									<th>Question</th>
									<th>Option 1</th>
									<th>Option 2</th>
									<th>Option 3</th>
									<th>Option 4</th>
									<th>Option 5</th>
									<th>Option 6</th>
									<th>Option 7</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="mydata">
								<?php 
								$rs = mysql_query("SELECT * FROM feedback_questions WHERE event_id='".EVENT_ID."' AND feedback_id = '$feedback_id'");
								$num_rows = mysql_num_rows($rs);
								if($num_rows){
									while ($row = mysql_fetch_object($rs)){
										if($row->is_objective == 1) {
										    ?>
                                            <tr>
                                                <td class="hidden"></td>
                                                <td><?= $row->title; ?></td>
                                                <td><?= $row->option1; ?></td>
                                                <td><?= $row->option2; ?></td>
                                                <td><?= $row->option3; ?></td>
                                                <td><?= $row->option4; ?></td>
                                                <td><?= $row->option5; ?></td>
                                                <td><?= $row->option6; ?></td>
                                                <td><?= $row->option7; ?></td>
                                                <td></td>
                                            </tr>
                                            <?php
										    $rs_r = mysql_query("SELECT id, COUNT(*) as total,
                                                                    COUNT(CASE WHEN answer = 1 THEN 1 END) AS count1,
                                                                    COUNT(CASE WHEN answer = 2 THEN 1 END) AS count2,
                                                                    COUNT(CASE WHEN answer = 3 THEN 1 END) AS count3,
                                                                    COUNT(CASE WHEN answer = 4 THEN 1 END) AS count4,
                                                                    COUNT(CASE WHEN answer = 5 THEN 1 END) AS count5,
                                                                    COUNT(CASE WHEN answer = 6 THEN 1 END) AS count6,
                                                                    COUNT(CASE WHEN answer = 7 THEN 1 END) AS count7
                                                                    FROM `users_feedbacks` where event_id='".EVENT_ID."' AND question_id= '$row->id'");

											$num_rows_r = mysql_num_rows($rs_r);
											if($num_rows_r){
												while ($row_r = mysql_fetch_object($rs_r)){
												?>
												<tr>
													<td class="hidden"></td>
													<td></td>
													<td><b><?= $row_r->count1; ?></b></td>
													<td><b><?= $row_r->count2; ?></b></td>
													<td><b><?= $row_r->count3; ?></b></td>
													<td><b><?= $row_r->count4; ?></b></td>
													<td><b><?= $row_r->count5; ?></b></td>
													<td><b><?= $row_r->count6; ?></b></td>
													<td><b><?= $row_r->count7; ?></b></td>
													<td><b><?= $row_r->total; ?></b></td>
												</tr>
												<?php
												}
											} else {
												?>
												<tr>
													<td class="hidden"></td>
													<td></td>
													<td>0</td>
													<td>0</td>
													<td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
                                                    <td>0</td>
												</tr>
												<?php
											}
										} else {
                                            ?>
                                            <tr>
                                                <td class="hidden"></td>
                                                <td><?= $row->title; ?></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td colspan="2"><a href="feedback_question_results.php?question_id=<?= $row->id;?>"  class="label label-primary">Question Results</a></td>
                                            </tr>
                                            <?php
                                        }
									?>
								<?php }}?>
							</tbody>
						</table>
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>