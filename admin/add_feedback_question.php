<?php include 'header.php';
if(isset($_GET['feedback_id'])){
    $feedback_id = $_GET['feedback_id'];
}else{
    $_SESSION['error_msg'] = "Feedback not found!";
    echo "<script>window.location.href = 'feedbacks.php'</script>";
    //header("Location:feedbacks.php");
    exit();
}
if(isset($_POST['save_feedback_form_btn'])){

    if (isset($_POST["is_multi_choose"])) {
        $is_objective_or_multi_choose = 2;
    } elseif (isset($_POST['is_objective'])) {
        $is_objective_or_multi_choose = 1;
    } else {
        $is_objective_or_multi_choose = 0;
    }

    $feedback_id = addslashes(trim($_POST['feedback_id']));
    $title = addslashes(trim($_POST['title']));
    //$is_objective = isset($_POST['is_objective'])?1:0;
    $is_objective = $is_objective_or_multi_choose;
    $option1 = addslashes(trim($_POST['option1']));
    $option2 = addslashes(trim($_POST['option2']));
    $option3 = addslashes(trim($_POST['option3']));
    $option4 = addslashes(trim($_POST['option4']));
    $option5 = addslashes(trim($_POST['option5']));
    $option6 = addslashes(trim($_POST['option6']));
    $option7 = addslashes(trim($_POST['option7']));
    $correct_option = empty($_POST['correct_option'])?0:trim($_POST['correct_option']);

    $sql = "INSERT INTO `feedback_questions` SET
                        `event_id` = '".EVENT_ID."',
						`feedback_id` = '{$feedback_id}',
						`title` = '{$title}',
						`is_objective` = '{$is_objective}',
						`option1` = '{$option1}',
						`option2` = '{$option2}',
						`option3` = '{$option3}',
						`option4` = '{$option4}',
						`option5` = '{$option5}',
						`option6` = '{$option6}',
						`option7` = '{$option7}',
						`correct_option` = '{$correct_option}'";
    $res = mysql_query($sql);

    if($res){
        $_SESSION['success_msg'] = "Question saved successfully.";
        echo "<script>window.location.href = 'feedback_questions.php?feedback_id=".$feedback_id."'</script>";
        //header("Location:feedbacks.php");
        exit();
    }else{
        $_SESSION['error_msg'] = "Something goes wrong try again.";
    }
}
?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Add Feedback Question</span></h4>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">

                <?php include 'messages.php';?>

                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post" name="save_feedback_form">
                            <input type="hidden" name="feedback_id" value="<?=$feedback_id?>">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="col-md-12 form-group">
                                        <label>Question</label>
                                        <textarea name="title" class="form-control" placeholder="Question" required></textarea>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Is Objective Question</label>
                                        <input type="checkbox" name="is_objective" class="is_objective" value="1" checked="checked" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Is Multi Choose Question</label>
                                        <input type="checkbox" name="is_multi_choose" class="is_multi_choose" value="2" checked="checked" />
                                    </div>
                                    <div class="option_section">
                                        <div class="col-md-6 form-group">
                                            <label>Option 1</label>
                                            <input type="text" name="option1" class="form-control options" placeholder="Option 1" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 2</label>
                                            <input type="text" name="option2" class="form-control options" placeholder="Option 2" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 3</label>
                                            <input type="text" name="option3" class="form-control options" placeholder="Option 3" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 4</label>
                                            <input type="text" name="option4" class="form-control options" placeholder="Option 4" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 5</label>
                                            <input type="text" name="option5" class="form-control options" placeholder="Option 5" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 6</label>
                                            <input type="text" name="option6" class="form-control options" placeholder="Option 6" value="">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 7</label>
                                            <input type="text" name="option7" class="form-control options" placeholder="Option 7" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-primary" name="save_feedback_form_btn" value="save">Add</button>
                                        <a href="feedback_questions.php?feedback_id=<?=$feedback_id?>" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /Content area -->
        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<script>
    $(document).ready(function(){
        $(document).on('change','.is_objective',function() {
            if ($(this).is(":checked")) {
                $(".option_section").removeClass('hidden');
                $(".is_multi_choose").attr("checked","checked");
                //$('input.options').prop('required',true);
            } else {
                $(".option_section").addClass('hidden');
                $(".is_multi_choose").removeAttr("checked","checked");
                //$('input.options').prop('required',false);
            }
        });
    });
</script>
<!-- End Page container -->
</body>
</html>