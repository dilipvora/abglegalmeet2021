<?php include '../config.php';
include_once '../src/firebaseLib.php';

if (isset($_GET['action']) && $_GET['action'] == 'get_score') {

    $q = "SELECT u.uid, u.f2, u.f1, gs.score FROM `new_users` as u INNER JOIN game_score as gs ON u.uid = gs.uid WHERE u.event_id='" . EVENT_ID . "' ORDER BY gs.score DESC";

    $users = array();
    $rs = mysql_query($q);
    $num_row = mysql_num_rows($rs);

    if ($num_row) {
        while ($row = mysql_fetch_row($rs)) {
            $users['data'][] = $row;
        }

        echo(json_encode($users));
    } else {
        echo '{"data":[{"0":"","1":"","2":"","3":""}]}';
    }
}

if(isset($_POST['frmAnnounce'])){
	$rs = mysql_query("SELECT * FROM `announcement` WHERE `event_id` = '".EVENT_ID."'");
	$num_row = mysql_num_rows($rs);
	if($num_row){
		mysql_query("UPDATE `announcement` SET `announcement_text` = '{$_POST['announcement_text']}' WHERE `event_id` = '".EVENT_ID."'");
	}else{
		mysql_query("INSERT INTO `announcement` SET `announcement_text` = '{$_POST['announcement_text']}', `event_id` = '".EVENT_ID."'");
	}

    $json_data = file_get_contents("../current_slide.json");
    $json_array = json_decode($json_data);
    $json_array->announcement_text = $_POST['announcement_text'];
    file_put_contents("../current_slide.json",json_encode($json_array));


    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/current_announcement', $_POST['announcement_text']);

	echo $_POST['announcement_text'];
}

if(isset($_POST['action']) && $_POST['action'] == 'get_question'){
	$total_question = $_POST['total_question'];
	$rs = mysql_query("SELECT q.*, u.name, u.location FROM question q, users u WHERE u.uid = q.uid AND q.event_id = '".EVENT_ID."' ORDER BY q_id ASC LIMIT {$total_question},10");
	$num_row = mysql_num_rows($rs);
	$question='';
	if($num_row){
		while($row = mysql_fetch_object($rs)){
			$question .='<tr>
				<td class="hidden"></td>
				<td>'.$row->name.'</td>
				<td>'.$row->location.'</td>
				<td>'.$row->q_question.'</td>
				<td class="hidden"></td>
			</tr>';
		}
	}
	echo $question;
}

if(isset($_POST['action']) && $_POST['action'] == 'get_question_delete'){
    $total_question = $_POST['total_question'];
    $fields = $_POST['fields'];
    $fields_array = explode(',',$fields);
    $rs = mysql_query("SELECT q.*, $fields FROM question q, new_users u WHERE u.uid = q.uid AND q.event_id = '".EVENT_ID."' ORDER BY q_id ASC LIMIT {$total_question},10");
    $num_row = mysql_num_rows($rs);
    $question='';
    if($num_row){
        while($row = mysql_fetch_object($rs)){

            if($row->status == 1){
                $status = '<span class="label label-success">Approve</span>';
            }else if($row->status == 2){
                $status = '<span class="label label-info">Reject</span>';
            }else{
                $status = '<span class="label label-danger">Pending</span>';
            }
            $delete = "if(confirm('Would You Like To Permanently Delete This Question')){self.location='?delete&q_id=$row->q_id';}";

            $question .='<tr>
			<td class="hidden"></td>
			<td><input type="checkbox" name="delete_question[]" class="delete_chk" value="'.$row->q_id.'"></td>';

            if(!empty($fields_array)) {
                foreach ($fields_array as $item) {
                    $field = str_replace('u.', '', $item);
                    $question .= '<td>'.$row->$field.'</td>';
                }
            }
            $question .='<td>'.$row->q_question.'</td>
			<td>'.$row->team_id.'</td>
			<td>'.$status.'</td>
			<td><a href="javascript:void(0)" onClick="'.$delete.'" class="label label-danger"><i class="fa fa-trash-o"></i></a></td>
			</tr>';
        }
    }
    echo $question;
}

if(isset($_GET['action']) && $_GET['action'] == 'get_nominee'){
//    echo "SELECT * from `users` LEFT JOIN `extra_form` ON users.alias = extra_form.fid where `event_id` = '".EVENT_ID."'";die();
//    $users_result = mysql_query("SELECT * from `users` inner JOIN extra_form ON users.alias = extra_form.fid where users.event_id = '".EVENT_ID."'")or die(mysql_error());
    $users_result = mysql_query("SELECT * FROM `extra_form` WHERE `role` IN ('above-and-beyond','young-achievers-award','best-legal-innovator','best-in-house-team-of-the-year','dealmaker-of-the-year','student-of-the-year','collaborator-of-the-year','gc-of-the-year') AND event_id = '".EVENT_ID."'") or die(mysql_error());
    $num_of_rows = mysql_num_rows($users_result);
    $users=array();
    if($num_of_rows>0){
        //echo "<pre/>";
        while($item = mysql_fetch_array($users_result)){
            //print_r($item);
            $item['f1'] = '<img src="../upload/nominate/'.$item['f1'].'" class="img-responsive img-rounded" />';
            $confirm = "if(confirm('Would You Like To Permanently Delete This Record?')){self.location='?delete&fid=".$item['fid']."'}";
            $item['fid'] = '<div class="text-center">
                                <br/><a href="javascript:void(0)" data-id="'.$item['fid'].'" id="view-users" class="label bg-indigo">View Nominee</a>
                                <br/><a href="javascript:void(0)" data-id="'.$item['fid'].'" id="view-like-comments" class="label bg-pink">View Likes & Comments</a>
                                <a href="https://www.abglegalmeet2021.com/nominate.php?nid='.base64_encode($item['fid']).'" target="_blank" class="label bg-info">View Link</a>
                                <a href="javascript:void(0)" onClick="'.$confirm.'" class="label bg-danger">Delete</a>
                            </div>';
            $item['role'] = ucwords(str_replace(array('-','_'), " ", $item['role']));
            $users['data'][] = $item;
        }
        //die;
        echo (json_encode($users));
    }
}

if(isset($_GET['action']) && $_GET['action'] == 'get_user'){

    $date = date('Y-m-d',strtotime($_GET['date']));
    $date_string = "";
    if($date != "" && $date != '1970-01-01'){
        $date = date('Y-m-d',strtotime($_GET['date']));
        $date_string = "date(created_date) LIKE '{$date}%' AND ";
    }

    $form_field_query = mysql_query("SELECT * from registration_field where event_id = '".EVENT_ID."' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);
    $field_name_array = array();
    if($num_rows>0){
        while ($row = mysql_fetch_array($form_field_query)){
            array_push($field_name_array,$row['field_name']);
        }
    }

    $fiel_name = implode(',',$field_name_array);

    //echo "SELECT $fiel_name from `new_users` where ".$date_string." `event_id` = '".EVENT_ID."' group by `f3` order by `uid` desc"; die();

    $users_result = mysql_query("SELECT $fiel_name from `new_users` where ".$date_string." `event_id` = '".EVENT_ID."'");
    $num_of_rows = mysql_num_rows($users_result);
    $users=array();
    if($num_of_rows>0){
        while($item = mysql_fetch_array($users_result)){
            for($i=0; $i<=3; $i++) {
                if(!isset($item[$i])) {
                    $item[$i] = "";
                }
            }
            $users['data'][] = $item;
        }
        echo (json_encode($users));
    }
    else{

        $form_field_query = mysql_query("SELECT * from registration_field where event_id = '".EVENT_ID."' ORDER BY display_order ASC");
        $num_rows = mysql_num_rows($form_field_query);

        $data = array();
        if($num_rows>0){
            for($i=0;$i<$num_rows;$i++){
                array_push($data,"");
            }
        }
        $data1['data'][] = (object)$data;
        print json_encode($data1);
        //echo '{"data":[{"0":"","1":"","2":"","3":"","4":""}]}';
    }
}

if(isset($_POST['action']) && $_POST['action'] == 'get_user_delete'){

    $form_field_query = mysql_query("SELECT * from registration_field where event_id = '".EVENT_ID."' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);

    $field_name_array = array();
    if($num_rows>0){
        while($row = mysql_fetch_object($form_field_query)){
            array_push($field_name_array,$row->field_name);
        }
    }

	$total_users = $_POST['total_users'];
	$rs = mysql_query("SELECT * FROM new_users WHERE `event_id` = '".EVENT_ID."' ORDER BY uid ASC LIMIT {$total_users},10");
	$num_row = mysql_num_rows($rs);
	$users='';
	if($num_row){
		while($row = mysql_fetch_object($rs)){
			$delete = "if(confirm('Would You Like To Permanently Delete This User')){self.location='?delete&uid=$row->uid';}";
			$users .='<tr>';
             if(!empty($field_name_array)){
                foreach ($field_name_array as $field_row) {
                    $users .='<td>'.$row->$field_row.'</td>';
                }
             }
			$users .='<td><a href="javascript:void(0)" onClick="'.$delete.'" class="label label-danger"><i class="fa fa-trash-o"></i></a></td>
			</tr>';
		}
	}
	echo $users;
}

/**
 * get attendees
 */
if(isset($_GET['action']) && $_GET['action'] == 'get_attendees'){
    $event_id = EVENT_ID;

    $event_start_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_start_date_time' and `event_id` = '{$event_id}'");
    $event_start_date_time_data = mysql_fetch_object($event_start_date_time);
    if(mysql_num_rows($event_start_date_time)>0){
        $event_start_date_time_value = date('Y-m-d H:i:s',strtotime($event_start_date_time_data->option_value));
        $converted_start_time = date('Y-m-d H:i:s',strtotime('-10 minutes',strtotime($event_start_date_time_value)));
    }else{
        $converted_start_time = '';
    }

    $event_end_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_end_date_time' and `event_id` = '{$event_id}'");
    $event_end_date_time_data = mysql_fetch_object($event_end_date_time);
    if(mysql_num_rows($event_end_date_time)>0){
        $event_end_date_time_value = date('Y-m-d H:i:s',strtotime($event_end_date_time_data->option_value));
    }else{
        $event_end_date_time_value = '';
    }

    if(!empty($converted_start_time) && !empty($event_end_date_time_value)) {
        $form_field_query = mysql_query("SELECT * from registration_field where event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
        $num_rows = mysql_num_rows($form_field_query);
        $field_name_array = array();
        if ($num_rows > 0) {
            while ($row = mysql_fetch_array($form_field_query)) {
                array_push($field_name_array, "u.".$row['field_name']);
            }
        }

        $fiel_name = implode(', ', $field_name_array);

        //$users_result = mysql_query("SELECT $fiel_name from `new_users` where `event_id` = '" . EVENT_ID . "'");
        $users_result = mysql_query("SELECT $fiel_name FROM `users_login_detail` as `ul` INNER JOIN `new_users` as `u` ON ul.uid = u.uid WHERE u.event_id = '{$event_id}' AND ((ul.login_time >= '{$converted_start_time}' AND ul.logout_time <= '{$event_end_date_time_value}') OR (ul.login_time <= '{$converted_start_time}' AND ul.logout_time >= '{$event_end_date_time_value}')) GROUP BY ul.uid");
        $num_of_rows = mysql_num_rows($users_result);
        $users = array();
        if ($num_of_rows > 0) {
            while ($item = mysql_fetch_array($users_result)) {
                $users['data'][] = $item;
            }
            echo(json_encode($users));
        } else {

            $form_field_query = mysql_query("SELECT * from registration_field where event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
            $num_rows = mysql_num_rows($form_field_query);

            $data = array();
            if ($num_rows > 0) {
                for ($i = 0; $i < $num_rows; $i++) {
                    array_push($data, "");
                }
            }
            $data1['data'][] = (object)$data;
            print json_encode($data1);
            //echo '{"data":[{"0":"","1":"","2":"","3":"","4":""}]}';
        }
    } else {
        $form_field_query = mysql_query("SELECT * from registration_field where event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
        $num_rows = mysql_num_rows($form_field_query);

        $data = array();
        if ($num_rows > 0) {
            for ($i = 0; $i < $num_rows; $i++) {
                array_push($data, "");
            }
        }
        $data1['data'][] = (object)$data;
        print json_encode($data1);
    }
}

/**
 * get live attendees
 */
if(isset($_GET['action']) && $_GET['action'] == 'get_live_attendees') {
    $form_field_query = mysql_query("SELECT * from registration_field where event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
    $num_rows = mysql_num_rows($form_field_query);
    $field_name_array = array();
    if ($num_rows > 0) {
        while ($row = mysql_fetch_array($form_field_query)) {
            array_push($field_name_array, $row['field_name']);
        }
    }

    $fiel_name = implode(', ', $field_name_array);

    $users_result = mysql_query("SELECT $fiel_name from `new_users` where `event_id` = '".EVENT_ID."' and is_login = 1");
    $num_of_rows = mysql_num_rows($users_result);
    $users = array();
    if ($num_of_rows > 0) {
        while ($item = mysql_fetch_array($users_result)) {
            $users['data'][] = $item;
        }
        echo(json_encode($users));
    } else {

        $form_field_query = mysql_query("SELECT * from registration_field where event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
        $num_rows = mysql_num_rows($form_field_query);

        $data = array();
        if ($num_rows > 0) {
            for ($i = 0; $i < $num_rows; $i++) {
                array_push($data, "");
            }
        }
        $data1['data'][] = (object)$data;
        print json_encode($data1);
        //echo '{"data":[{"0":"","1":"","2":"","3":"","4":""}]}';
    }
}

/**
 * Upload PPT
 */

if (isset ( $_POST ['submit-upload-ppt-form'] ) && $_POST ['submit-upload-ppt-form'] == 'true') {
	$ppt = '';
	
	if (isset ( $_FILES ['ppt'] ) && $_FILES ['ppt'] ['name'] != '') {
		//type validation
        $extension = pathinfo($_FILES ['ppt']['name'], PATHINFO_EXTENSION);
		$allow_types = array("pptx","ppt");
		if (!in_array($extension,$allow_types)) {
			$_SESSION ['error_msg'] = "Upload ppt file only.";
			header ( "Location:webcast.php"); exit;
		}
		//end type validation
		$ppt_name = $_FILES ['ppt'] ['name'];
		$ppt_tmp = $_FILES ['ppt'] ['tmp_name'];
		$name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $ppt_name);
        $ppt_name_remove_space = preg_replace('/\s+/', '_', $name);

		$destination = '../upload/ppt-sources/';
		$ext = pathinfo ( $ppt_name, PATHINFO_EXTENSION );
		$ppt_name = 'ppt_'.$ppt_name_remove_space. '_' . uniqid ().'.' . $ext;
		move_uploaded_file ( $ppt_tmp, $destination . '' . $ppt_name );
		$ppt = $ppt_name;

		$sql = "INSERT INTO `ppt` SET
							`name` = '{$name}',
							`event_id` = '".EVENT_ID."',
							`filename` = '{$ppt_name}'";
		$res = mysql_query ( $sql );
		if ($res) {
			$ppt_id = mysql_insert_id($db);
			//rename($destination . '' . $ppt_name,$destination . '' .$ppt_id.'_'. $ppt_name);
			$destination_with_id = '../upload/ppt/'.$ppt_id."/";
			if(!is_dir($destination_with_id))
				mkdir($destination_with_id, 0777);
			copy ($destination . '' . $ppt_name, $destination_with_id . '' . $ppt_name);

            /* PPT to JPG Code */
            $ch = curl_init();
            $data = "pptpath=".PPTPATH.$ppt_id."&pptname=".$ppt_name;
            curl_setopt($ch, CURLOPT_URL,PPT_CONVERT_URL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);
            //var_dump($output);

            unlink( $destination_with_id.''.$ppt_name);
            $images_array = glob($destination_with_id . "*.*");
            $img_length = sizeof($images_array);
            if($img_length > 0){
	            foreach($images_array as $filename){
		            $old_img = basename($filename);
		            $ext = pathinfo($old_img, PATHINFO_EXTENSION);
		            $file = basename($old_img, ".".$ext);
		            $file_num = substr($file, 5);
		            $length = strlen($file_num);

		            if($length == 1){
			            $new_name = 'Slide00'.$file_num.'.'.$ext;
		            }elseif($length == 2){
			            $new_name = 'Slide0'.$file_num.'.'.$ext;
		            }else{
			            $new_name = 'Slide'.$file_num.'.'.$ext;
		            }
		            rename($destination_with_id.$old_img,$destination_with_id.$new_name);
	            }
            }
            /* End PPT to JPG Code */

			$_SESSION ['success_msg'] = "PPT upload successfully.";
			header ( "Location:webcast.php");
		} else {
			$_SESSION ['error_msg'] = "Something goes wrong try again.";
			header ( "Location:webcast.php");
		}
	}
}

/**
* Select PPT
*/

if (isset ( $_POST ['submit-select-ppt-form'] ) && $_POST ['submit-select-ppt-form'] == 'true') {
	$ppt_id = trim($_POST['ppt-id']);
	
	if($ppt_id !=''){
		$webinar_id = $_POST['webinar_id'];
		$current_slide = 'upload/ppt/'.$ppt_id.'/Slide001.JPG';
		$slide_no = 1;

        $json_data = file_get_contents("../current_slide.json");
        if(!empty($json_data)) {
            $json_array = json_decode($json_data);
            $json_array->slide = $current_slide;
            $json_array->slide_no = $slide_no;
            $json_array->time = date('Y-m-d H:i:s');
        }else{
            $json_array['slide'] = $current_slide;
            $json_array['slide_no'] = $slide_no;
            $json_array['time'] = date('Y-m-d H:i:s');
        }
        file_put_contents("../current_slide.json",json_encode($json_array));
		
		$firebase = new \Firebase\FirebaseLib($firebase_url,API_KEY);
        $firebase->set(EVENT_ID.'/current_slide', SITE_URL.$current_slide);

		$rs = mysql_query("SELECT * FROM `webinar` WHERE `event_id` = '".EVENT_ID."'");
		$num_row = mysql_num_rows($rs);
		if($num_row){
			$sql = "UPDATE `webinar` SET
						`current_ppt` = '{$ppt_id}',
						`current_slide` = '{$current_slide}',
						`slide_no` = '{$slide_no}'
						WHERE `event_id` = '".EVENT_ID."'";
		}else{
			$sql = "INSERT INTO `webinar` SET 
						`event_id` = '".EVENT_ID."',
						`current_ppt` = '{$ppt_id}',
						`current_slide` = '{$current_slide}',
						`slide_no` = '{$slide_no}'";
		}
		
		$res = mysql_query ( $sql );
		if ($res) {
			$_SESSION ['success_msg'] = "Select PPT successfully.";
		
			if(isset($_SESSION['status']) && $_SESSION['status'] == '2'){
				header ( "Location:presentation.php");
			}else{
				header ( "Location:webcast.php");
			}
		} else {
			$_SESSION ['error_msg'] = "Something goes wrong try again.";
			if(isset($_SESSION['status']) && $_SESSION['status'] == '2'){
				header ( "Location:presentation.php");
			}else{
				header ( "Location:webcast.php");
			}
		}
	}else{
		$_SESSION ['error_msg'] = "Please Select PPT.";
		if(isset($_SESSION['status']) && $_SESSION['status'] == '2'){
			header ( "Location:presentation.php");
		}else{
			header ( "Location:webcast.php");
		}
	}
}

if(isset($_POST['user_data']) && $_POST['user_data']==true){
    $fields = $_POST['field'];

    $rs = mysql_query("SELECT $fields from `new_users` where `event_id` = '".EVENT_ID."' and is_login = 1");
    $num_row = mysql_num_rows($rs);

    $field_arrary_count = count(explode(',',$fields)) + 1;
    $user = array();
	$i=0;
	$fields_array = explode(',',$fields);
	if($num_row){
		$i = 0;
		while($row = mysql_fetch_array($rs)){
			$user['data'][$i][0] = "";
			$count = 1;
            foreach ($fields_array as $item) {
                $user['data'][$i][$count] = $row[$item];
                $count++;
			}
			$i++;
		}
		echo (json_encode($user));
	}else{
        $data = array();
        for ($i=0;$i<$field_arrary_count;$i++){
            $data[$i] = '';
        }
        $findal_array['data'] = array((object)$data);
        print(json_encode($findal_array));
	}
}

if(isset($_POST['frmaddteam'])){
    $team_name = $_POST['team_name'];
    $id = $_POST['id'];
    if(empty($id)) {
        $sql = "INSERT INTO `team` SET 
						`event_id` = '" . EVENT_ID . "',
						`team_name` = '{$team_name}'";
        $res = mysql_query ( $sql );
        $_SESSION ['success_msg'] = "Team add successfully.";
    }else{
        $sql = "UPDATE `team` SET
						`team_name` = '{$team_name}'
						WHERE `event_id` = '".EVENT_ID."' and `id` = $id";
        $res = mysql_query ( $sql );
        $_SESSION ['success_msg'] = "Team update successfully.";
    }

    header ( "Location:team.php");

}

// Submit ppt delay time
if(isset($_POST['submit_ppt_delay_time']) && $_POST['submit_ppt_delay_time'] == "true"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/ppt_delay', $_POST['ppt_delay_time']);
}

// Feedback on
if(isset($_POST['btn_feedback_on']) && $_POST['btn_feedback_on'] == "true"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/feedback_on_off', 'on');
}

// Feedback off
if(isset($_POST['btn_feedback_off']) && $_POST['btn_feedback_off'] == "true"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/feedback_on_off', 'off');
}

// Feedback result on
if(isset($_POST['btn_feedback_result_on']) && $_POST['btn_feedback_result_on'] == "true"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/feedback_result_on_off', 'on');
}

// Feedback result off
if(isset($_POST['btn_feedback_result_off']) && $_POST['btn_feedback_result_off'] == "true"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/feedback_result_on_off', 'off');
}

// Quiz on
if(isset($_POST['btn_quiz_on']) && $_POST['btn_quiz_on'] == "true"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/quiz_on_off', 'on');
}

// Quiz off
if(isset($_POST['btn_quiz_off']) && $_POST['btn_quiz_off'] == "true"){
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL,API_KEY);
    $firebase->set(EVENT_ID.'/quiz_on_off', 'off');
}

// Event Start
if (isset($_POST['btn_start_event']) && $_POST['btn_start_event'] == "true") {
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL, API_KEY);
    $firebase->set(EVENT_ID . '/event_start_stop', 'start');
}
if (isset($_POST['btn_stop_event']) && $_POST['btn_stop_event'] == "true") {
    $firebase = new \Firebase\FirebaseLib(FIREBASE_URL, API_KEY);
    $firebase->set(EVENT_ID . '/event_start_stop', 'stop');
}

if(isset($_POST['action']) && $_POST['action'] == "multiple_delete_question"){
    $redirect = $_POST['redirect'];

    if(isset($_POST['delete_question']) && !empty($_POST['delete_question'])){
        if($redirect == "question-pending.php"){
            $delete_question_id = implode(',',$_POST['delete_question']);
            mysql_query("UPDATE question SET status='2' WHERE `q_id` IN ($delete_question_id) AND `event_id` = '".EVENT_ID."'");
            $_SESSION['success_msg'] = 'Question deleted successfully.';
        }else{
            $delete_question_id = implode(',',$_POST['delete_question']);
            $sql = "delete from `question` where `q_id` IN ($delete_question_id) AND `event_id` = '".EVENT_ID."'";
            mysql_query($sql);
            $_SESSION['success_msg'] = 'Question deleted successfully.';
        }

        echo '<script type="text/javascript">window.location.href="'.$redirect.'";</script>';
        exit();
    }else{
        $_SESSION['error_msg'] = "Please select question";
        echo '<script type="text/javascript">window.location.href="'.$redirect.'";</script>';
    }
}

if(isset($_POST['submit-select-ppt-image']) && $_POST['submit-select-ppt-image'] == 'true'){

    $ppt_id = $_POST['ppt-id'];
    $destination = '../upload/ppt/'.$ppt_id."/";
    if(!empty($_FILES['images']) && isset($_FILES['images']['name']) && !empty($_FILES['images']['name'])){
        foreach ($_FILES['images']['name'] as $key=>$image) {
            $ext = pathinfo($image, PATHINFO_EXTENSION);

            $file = basename($image, ".".$ext);
            $file_num = substr($file, 5);
            $length = strlen($file_num);

            if($length == 1){
                $slide_name = 'Slide00'.$file_num.'.'.$ext;
            }elseif($length == 2){
                $slide_name = 'Slide0'.$file_num.'.'.$ext;
            }else{
                $slide_name = 'Slide'.$file_num.'.'.$ext;
            }

            $targetFilePath = $destination . $slide_name;
            move_uploaded_file($_FILES["images"]["tmp_name"][$key], $targetFilePath);
        }
    }
    $_SESSION ['success_msg'] = "PPT images upload successfully.";
    header ( "Location:manual-ppt.php");
}

if(isset($_POST['create_ppt_folder']) && $_POST['create_ppt_folder'] == 'true'){

    $name = $_POST['folder_name'];
    $sql = "INSERT INTO `ppt` SET
							`name` = '{$name}',
							`event_id` = '".EVENT_ID."',
							`filename` = ''";
    $res = mysql_query ( $sql );
    if ($res) {
        $ppt_id = mysql_insert_id($db);
        //rename($destination . '' . $ppt_name,$destination . '' .$ppt_id.'_'. $ppt_name);
        $destination_with_id = '../upload/ppt/' . $ppt_id . "/";
        if (!is_dir($destination_with_id))
            mkdir($destination_with_id, 0777);
    }
    $_SESSION ['success_msg'] = "PPT create successfully.";
    header ( "Location:manual-ppt.php");
}
?>