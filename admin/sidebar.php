<!-- Main sidebar -->
<div class="sidebar sidebar-main">
	<div class="sidebar-content">
		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">
					<!-- Main -->
					<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
					<li <?php if($page == 'dashboard.php'){?>class="active"<?php }?>><a href="dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
					<li <?php if($page == 'live_users.php'){?>class="active"<?php }?>><a href="live_users.php"><i class="icon-chart"></i> <span>Live Users</span></a></li>
                    <li <?php if($page == 'upload_images.php'){?>class="active"<?php }?>><a href="upload_images.php" target="_blank"><i class="icon-cog"></i> <span>Upload Images</span></a></li>
                        
                    <?php if ($_SESSION['status'] == 1) { ?>
                        <li <?php if($page == 'setting.php'){?>class="active"<?php }?>><a href="setting.php"><i class="icon-cog"></i> <span>Setting</span></a></li>
                        <li <?php if($page == 'form-fields.php'){?>class="active"<?php }?>><a href="form-fields.php"><i class="icon-list"></i> <span>Form Fields</span></a></li>
					<?php } ?>

                    <li <?php if($page == 'users.php'){?>class="active"<?php }?>><a href="users.php"><i class="icon-user-plus"></i> <span>Users</span></a></li>

                    <?php if ($_SESSION['status'] == 1) { ?>
                        <li <?php if($page == 'users_delete.php'){?>class="active"<?php }?>><a href="users_delete.php"><i class="icon-user-plus"></i> <span>Users Delete</span></a></li>
                        <li <?php if($page == 'team.php'){?>class="active"<?php }?>><a href="team.php"><i class="icon-user-plus"></i> <span>Team</span></a></li>
					<?php } ?>

                    <li <?php if($page == 'webcast.php'){?>class="active"<?php }?>><a href="webcast.php"><i class="icon-play"></i> <span>Webcast</span></a></li>
					<li <?php if($page == 'delete-ppt.php'){?>class="active"<?php }?>><a href="delete-ppt.php"><i class="fa fa-trash-o"></i> <span>Delete PPT</span></a></li>
					<li <?php if($page == 'manual-ppt.php'){?>class="active"<?php }?>><a href="manual-ppt.php"><i class="fa fa-file-powerpoint-o"></i> <span>Manual PPT</span></a></li>

                    <li>
                        <a href="javascript:void(0)"><i class="icon-comment"></i> <span>Feedback</span></a>
                        <ul>
                            <li <?php if($page == 'feedbacks.php'){?>class="active"<?php }?>><a href="feedbacks.php"><i class="icon-comment"></i> <span>Feedbacks</span></a></li>
                            <li <?php if($page == 'live_feedback_result.php'){?>class="active"<?php }?>><a href="live_feedback_result.php"><i class="icon-chart"></i> <span>Live Feedback Results</span></a></li>
                            <li <?php if($page == 'feedback_result_report.php'){?>class="active"<?php }?>><a href="feedback_result_report.php"><i class="icon-notebook"></i> <span>Feedback Result Report</span></a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:void(0)"><i class="icon-stack2"></i> <span>Form Filling</span></a>
                        <ul>
                            <li <?php if($page == 'work-experience.php'){?>class="active"<?php }?>><a target="_blank" href="work-experience.php"><i class="icon-typewriter"></i> <span>Work Experience</span></a></li>
                            <li <?php if($page == 'quotes.php'){?>class="active"<?php }?>><a target="_blank" href="quotes.php"><i class="icon-book3"></i> <span>Quotes</span></a></li>
                            <li <?php if($page == 'nominee-list.php'){?>class="active"<?php }?>><a target="_blank" href="nominee-list.php"><i class="icon-magazine"></i> <span>Nominee List</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="icon-comment"></i> <span>Quiz</span></a>
                        <ul>
                            <li <?php if($page == 'quiz.php'){?>class="active"<?php }?>><a href="quiz.php"><i class="icon-comment"></i> <span>Quiz</span></a></li>
                            <li <?php if($page == 'quiz-result.php'){?>class="active"<?php }?>><a href="quiz-result.php" target="_blank"><i class="icon-gift"></i> <span>Quiz Result</span></a></li>
                        </ul>
                    </li>

                    <?php if ($_SESSION['status'] == 1) { ?>
					    <li <?php if($page == 'generate-vod.php'){?>class="active"<?php }?>><a href="generate-vod.php"><i class="icon-play"></i> <span>Generate VOD</span></a></li>
					<?php } ?>

                    <li>
                        <a href="javascript:void(0)"><i class="icon-pencil7"></i> <span>Questions</span></a>
                        <ul>
                            <li <?php if($page == 'question.php'){?>class="active"<?php }?>><a href="question.php"><i class="icon-pencil7"></i> <span>Question</span></a></li>
                            <li <?php if($page == 'question-pending.php'){?>class="active"<?php }?>><a href="question-pending.php"><i class="icon-pencil7"></i> <span>Pending Question</span></a></li>
                        </ul>
                    </li>

                    <?php if ($_SESSION['status'] == 1) { ?>
					    <li <?php if($page == 'question_delete.php'){?>class="active"<?php }?>><a href="question_delete.php"><i class="icon-pencil7"></i> <span>Question Delete</span></a></li>
					<?php } ?>

                    <li <?php if($page == 'selfie-corner.php'){?>class="active"<?php }?>><a href="selfie-corner.php"><i class="icon-camera"></i> <span>Selfie Corner</span></a></li>
                    <li <?php if($page == 'announcement.php'){?>class="active"<?php }?>><a href="announcement.php"><i class="icon-play"></i> <span>Announcement</span></a></li>
                    <li <?php if($page == 'event-info.php'){?>class="active"<?php }?>><a href="event-info.php"><i class="icon-info22"></i> <span>Event Info</span></a></li>
                    <li <?php if($page == 'get-attendee-report.php'){?>class="active"<?php }?>><a href="get-attendee-report.php"><i class="icon-info22"></i> <span>Get Attendee Report</span></a></li>
                    <li><a href="reports.php" target="_blank"><i class=" icon-file-presentation"></i> <span>Reports</span></a></li>
                    <li>
                        <a href="javascript:void(0)"><i class="icon-notebook"></i> <span>Excel Reports</span></a>
                        <ul>
                            <li><a href="export-data.php?users_list"><i class="icon-notebook"></i> <span>Users Report</span></a></li>
                            <li><a href="export-data.php?questions_list"><i class="icon-notebook"></i> <span>Questions Report</span></a></li>
                            <li><a href="export-data.php?login_report"><i class="icon-notebook"></i> <span>Login Report</span></a></li>
                        </ul>
                    </li>
				</ul>
			</div>
		</div>
		<!-- /main navigation -->
	</div>
</div>
<!-- /main sidebar -->