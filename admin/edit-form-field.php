<?php include 'header.php'; 
$id = $_GET['id'];

$get_field_data = mysql_query("SELECT * from `registration_field` where `id` = '{$id}' and `event_id` = '".EVENT_ID."'");
$field_result = mysql_fetch_object($get_field_data);

if(isset($_POST['edit_form_field_data'])){
	
	$type = $_POST['type'];
	$label = $_POST['label'];
	$name = $_POST['name'];
	$options = addslashes(trim($_POST['options']));
	$is_required = $_POST['is_required'];
	$is_unique = $_POST['is_unique'];
	$display_order = $_POST['display_order'];
    $display_question = $_POST['display_question'];
	$event_id = EVENT_ID;
	$id = $_POST['id'];

	/*if($is_unique == "yes"){
        $update = "UPDATE `registration_field` SET `is_unique` = 'no' WHERE `id` != '".$id."' and `event_id` = '".EVENT_ID."'";
	    mysql_query($update);
    }*/
	
	$sql = "UPDATE `registration_field` SET
							`event_id` = '{$event_id}',
							`field_label` = '{$label}',
							`field_name` = '{$name}',
							`type` = '{$type}',
							`is_required` = '{$is_required}',
							`is_unique` = '{$is_unique}',
							`display_question` = '{$display_question}',
							`display_order` = '{$display_order}',
							`options` = '{$options}' WHERE `id` = '".$id."'";
		$res = mysql_query($sql);
		
	if($res){
		$_SESSION['success_msg'] = "Field update successfully.";
	}else{
		$_SESSION['error_msg'] = "Something goes wrong try again.";
	}	
	echo '<script>window.location = "form-fields.php"</script>';
}

?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Edit Form Field</span></h4>
						</div>
					</div>
				</div>
				<!-- /page header -->
				
			<!-- Content area -->
			<div class="content">

				<?php include 'messages.php';?>
				
				<div class="row">
					<div class="col-md-6">
						<form action="" method="post" name="savesettingform">
							<input type="hidden" name="id" value="<?=$id?>">
	                        <br/>
							<div class="panel panel-flat">
				                <div class="panel-body">
									<div class="form-group">
										<label>Type</label>
										<select name="type" class="form-control" required>
											<option value="">Select</option>
											<option value="1" <?=($field_result->type == "1")?"selected":""?>>Textbox</option>
											<option value="2" <?=($field_result->type == "2")?"selected":""?>>Textarea</option>
											<option value="3" <?=($field_result->type == "3")?"selected":""?>>Radio</option>
											<option value="4" <?=($field_result->type == "4")?"selected":""?>>Checkbox</option>
											<option value="5" <?=($field_result->type == "5")?"selected":""?>>Select</option>
											<option value="6" <?=($field_result->type == "6")?"selected":""?>>Number</option>
										<select>
									</div>
									<div class="form-group">
										<label>Label</label>
										<input type="text" name="label" value="<?=$field_result->field_label?>" placeholder="Enter Label" required class="form-control">
									</div>
									<div class="form-group">
										<label>Name</label>
                                        <select name="name" class="form-control" required="">
                                            <option value="">Select</option>
                                            <option value="f1" <?=($field_result->field_name == "f1")?"selected":""?>>F1</option>
                                            <option value="f2" <?=($field_result->field_name == "f2")?"selected":""?>>F2</option>
                                            <option value="f3" <?=($field_result->field_name == "f3")?"selected":""?>>F3</option>
                                            <option value="f4" <?=($field_result->field_name == "f4")?"selected":""?>>F4</option>
                                            <option value="f5" <?=($field_result->field_name == "f5")?"selected":""?>>F5</option>
                                            <option value="f6" <?=($field_result->field_name == "f6")?"selected":""?>>F6</option>
                                            <option value="f7" <?=($field_result->field_name == "f7")?"selected":""?>>F7</option>
                                            <option value="f8" <?=($field_result->field_name == "f8")?"selected":""?>>F8</option>
                                            <option value="f9" <?=($field_result->field_name == "f9")?"selected":""?>>F9</option>
                                            <option value="f10" <?=($field_result->field_name == "f10")?"selected":""?>>F10</option>
                                            <option value="f11" <?=($field_result->field_name == "f11")?"selected":""?>>F11</option>
                                            <option value="f12" <?=($field_result->field_name == "f12")?"selected":""?>>F12</option>
                                            <option value="f13" <?=($field_result->field_name == "f13")?"selected":""?>>F13</option>
                                            <option value="f14" <?=($field_result->field_name == "f14")?"selected":""?>>F14</option>
                                            <option value="f15" <?=($field_result->field_name == "f15")?"selected":""?>>F15</option>
                                        </select>
									</div>
									<div class="form-group">
										<label>Options</label>
										<textarea name="options" placeholder="Enter Options(comma separated)" class="form-control"><?=$field_result->options?></textarea>
									</div>
									<div class="form-group">
										<label>Display Order</label>
										<input type="text" name="display_order" value="<?=$field_result->display_order?>" placeholder="Display Order" class="form-control" required></textarea>
									</div>
									<div class="form-group">
                                        <label>Is Required </label> <br>
                                        <label class="radio-inline"><input type="radio" name="is_required" value="yes" <?=($field_result->is_required == "yes")?"checked":""?> required>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="is_required" value="no" <?=($field_result->is_required == "no")?"checked":""?> required>No</label>
                                    </div>
									<div class="form-group">
                                        <label>Is Unique </label> <br>
                                        <label class="radio-inline"><input type="radio" name="is_unique" value="yes" <?=($field_result->is_unique == "yes")?"checked":""?> required>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="is_unique" value="no" <?=($field_result->is_unique == "no")?"checked":""?> required>No</label>
                                    </div>
                                    <div class="form-group">
                                        <label>Display in Question</label> <br>
                                        <label class="radio-inline"><input type="radio" name="display_question" value="1" <?=($field_result->display_question == "1")?"checked":""?> required>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="display_question" value="0" <?=($field_result->display_question == "0")?"checked":""?> required>No</label>
                                    </div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" name="edit_form_field_data" value="save">Save</button>
										<a href="form-fields.php" class="btn btn-danger">Cancel</a>
									</div>
				                </div>
				            </div>
							
			            </form>
					</div>
					</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /Content area -->
		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>
		