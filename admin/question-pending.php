<?php include 'header.php';

if(isset($_GET['action']) && $_GET['action'] == 'approve' && isset($_GET['q_id']) && $_GET['q_id'] !=''){
	$q_id = $_GET['q_id'];
	$res = mysql_query("SELECT q_id, status FROM question WHERE q_id = ".$q_id." AND `event_id` = '".EVENT_ID."'");
	if(mysql_num_rows($res)>0){
		mysql_query("UPDATE question SET status='1' WHERE q_id = ".$q_id." AND `event_id` = '".EVENT_ID."'");
	}
}

if(isset($_GET['action']) && $_GET['action'] == 'approve_print' && isset($_GET['q_id']) && $_GET['q_id'] !=''){
	$q_id = $_GET['q_id'];
	$res = mysql_query("SELECT q_id, status FROM question WHERE q_id = ".$q_id." AND `event_id` = '".EVENT_ID."'");
	if(mysql_num_rows($res)>0){
		mysql_query("UPDATE question SET status='1', print_status='2' WHERE q_id = ".$q_id." AND `event_id` = '".EVENT_ID."'");
	}
}

if(isset($_GET['action']) && $_GET['action'] == 'reject' && isset($_GET['q_id']) && $_GET['q_id'] !=''){
	$q_id = $_GET['q_id'];
	$res = mysql_query("SELECT q_id, status FROM question WHERE q_id = ".$q_id." AND `event_id` = '".EVENT_ID."'");
	if(mysql_num_rows($res)>0){
		mysql_query("UPDATE question SET status='2' WHERE q_id = ".$q_id." AND `event_id` = '".EVENT_ID."'");
	}
}

$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '".EVENT_ID."' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
$next_count = $num_rows + 3;
?>
<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pending Question</span>
                        <button type="button" class="btn btn-danger multipledelete float-right">Multiple Delete</button></h4>
                    </div>
                </div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				<div class="panel panel-flat">
					<!-- datatable-basic -->
                    <form name="delete_frm" id="delete_frm" action="process.php" method="post">
                        <input type="hidden" name="action" value="multiple_delete_question">
                        <input type="hidden" name="redirect" value="question-pending.php">
					    <table class="table" id="pending_question_table1">
							<thead>
								<tr>
                                    <th class="hidden"></th>
                                    <th>
                                        <input type="checkbox" class="delete_all_question_chk">
                                    </th>
                                    <?php
                                    $filed_array = array();
                                    if($num_rows>0){
                                        while ($row = mysql_fetch_array($form_field_query)){
                                            array_push($filed_array,'u.'.$row['field_name']);?>
                                            <th><?=$row['field_label']?></th><?php
                                        }
                                    }
                                    ?>
                                    <th>Question</th>
                                    <th>Team</th>
                                    <th>Status</th>
                                    <th class="hidden"></th>
								</tr>
							</thead>
							<tbody id="mydata">
                            <?php
                            $fields = implode(',',$filed_array);
                            $rs = mysql_query("SELECT q.q_id, $fields, q.q_question,q.team_id, q.status, q.created_date, u.uid FROM question q, new_users u WHERE q.status = '0' AND u.uid = q.uid AND q.event_id = '".EVENT_ID."' ORDER BY q_id DESC LIMIT 100");
                            $num_rows1 = mysql_num_rows($rs);
                            if($num_rows1){
                                while ($row = mysql_fetch_object($rs)){
                                    $fields_array = explode(',',$fields);
                                    ?>
                                    <tr>
                                        <td class="hidden"></td>
                                        <td><input type="checkbox" name="delete_question[]" class="delete_chk" value="<?=$row->q_id?>"> </td>
                                        <?php
                                        if(!empty($fields_array)){
                                            foreach ($fields_array as $item) {
                                                $field = str_replace('u.','',$item);?>
                                                <td><?php echo $row->$field; ?></td><?php
                                            }
                                        }
                                        ?>
                                        <td><?php echo $row->q_question; ?></td>
                                        <td><?php echo $row->team_id; ?></td>
                                        <td>
                                            <a href="?action=approve&q_id=<?=$row->q_id?>" class="label label-success">Approve</a>
                                            <a href="?action=approve_print&q_id=<?=$row->q_id?>" class="label label-success">Approve & Print</a>
                                            <a href="?action=reject&q_id=<?=$row->q_id?>" class="label label-danger">Delete</a>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                <?php }
                            }else{ ?>
                                <td colspan="<?=$num_rows + 6?>" align="center" class="no_data_row">No data available in table</td><?php
                            }?>
							</tbody>
						</table>
                    </form>
						<input type="hidden" name="total_question" id="total_question" value="<?php echo $num_rows1;?>">
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>
<?php
$fields = implode(',',$filed_array);
?>
<script type="text/javascript">
    var pending_datatable = '';
    $(document).ready(function () {
        $('.delete_all_question_chk').on('click',function(){
            $(".delete_chk").prop('checked', $(this).prop('checked'));
            /*if(this.checked){
                $('.delete_chk').each(function(){
                    this.checked = true;

                });
            }else{
                $('.delete_chk').each(function(){
                    this.checked = false;
                });
            }*/
        });

        $(document).on('click','.delete_chk',function(){
            if($('.delete_chk:checked').length == $('.delete_chk').length){
                $('.delete_all_question_chk').prop('checked',true);
            }else{
                $('.delete_all_question_chk').prop('checked',false);
            }

        });

        $(document).on("click",".multipledelete",function () {
            $("#delete_frm").submit();
        });

    });
    var fields = '<?=$fields?>';



    var question_ajax_call = function() {
        var total_question = $("#total_question").val();
        var fields = '<?=$fields?>';
        var dataString = "action=pending_question?&total_question="+total_question+"&fields="+fields;
        $.ajax({
            async: true,
            url: "get_question_data.php?pending_question",
            type: "POST",
            data: dataString,
            //contentType: false,
            cache: false,
            //processData:false,
            //dataType: "json",
            success: function(data){
                var count = data.split('<tr>').length - 1;
                $("#total_question").val(parseInt(total_question)+parseInt(count));
                $(".no_data_row").remove();
                $('#mydata').prepend(data);
            }
        });
    };
    var interval = 10000;
    setInterval(question_ajax_call, interval);

    /*var pending_question_table = $("#pending_question_table1").DataTable({
        "ajax": {
            "url": "get_question_data.php?pending_question&fields="+fields
        },
        "columns": [
            { "data": 0, className: "hidden" },
            <?php $count = 1; for($i=0;$i<$next_count;$i++){ ?>
            { "data": <?=$count?> },
            <?php $count++; } ?>
            { "data": <?=$count?>, className: "hidden"},
        ],
        "ordering": false,
        "lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
    });
    setInterval(function () {
        pending_question_table.ajax.reload();
    }, 10000);*/
</script>