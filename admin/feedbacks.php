<?php 
include 'header.php';

$check_feedback_available =  mysql_query("SELECT * FROM feedbacks where event_id='".EVENT_ID."' LIMIT 1");
if(!mysql_num_rows($check_feedback_available) > 0){
	echo "<script>window.location.href = 'add_feedback.php'</script>";
}
if(isset($_GET['delete']) && isset($_GET['id']) && $_GET['id'] !=''){
	$id = $_GET['id'];
	$sql = "delete from feedbacks where id = '{$id}' AND event_id='".EVENT_ID."'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'Feedback deleted successfully.';
} 

if(isset($_GET['set_current_feedback']) && isset($_GET['id']) && $_GET['id'] !=''){

	$sel_res = mysql_query("SELECT * FROM `options` WHERE `event_id`='".EVENT_ID."' AND `option_name` = 'feedback_type'");
	if(mysql_num_rows($sel_res)>0){
		$start_date = date('Y-m-d H:i:s');
		update_details("options", "option_value = 'feedback'", "option_name = 'feedback_type' AND `event_id`='".EVENT_ID."'");
		update_details("options", "option_value = '{$_GET['id']}'", "option_name = 'feedback_id' AND `event_id`='".EVENT_ID."'");
		update_details("options", "option_value = '{$start_date}'", "option_name = 'feedback_start' AND `event_id`='".EVENT_ID."'");
	}else{
		$start_date = date('Y-m-d H:i:s');
		//$status = 'true';
		$created_date = date('Y-m-d H:i:s');

		insert_details("options", "`event_id`='".EVENT_ID."', option_value = 'feedback', option_name = 'feedback_type', created_date = '{$created_date}'");
		insert_details("options", "`event_id`='".EVENT_ID."', option_value = '{$_GET['id']}', option_name = 'feedback_id', created_date = '{$created_date}'");
		insert_details("options", "`event_id`='".EVENT_ID."', option_value = '{$start_date}', option_name = 'feedback_start', created_date = '{$created_date}'");
	}

	$_SESSION['success_msg'] = 'Set Current Feedback save successfully.';
}


?>

<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4>
							<i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Feedback List</span>
                            <a href="add_feedback.php" class="btn btn-primary float-right" style="margin-right: 10px;">Add Feedback</a>
                            <?php
                            //if($_SESSION['status'] == 1){
                            ?>
                            <button id="btnFeedbackOn" type="button" class="btn btn-success float-right" style="margin-right: 20px;" name="FeedbackOn">Show Feedback</button>
                            <button id="btnFeedbackOff" type="button" class="btn btn-danger float-right" style="margin-right: 20px; display: none;" name="FeedbackOff">Hide Feedback</button>
                            <button id="btnFeedbackResultOn" type="button" class="btn btn-success float-right" style="margin-right: 20px;" name="FeedbackResultOn">Show Feedback Result</button>
                            <button id="btnFeedbackResultOff" type="button" class="btn btn-danger float-right" style="margin-right: 20px; display: none;" name="FeedbackResultOff">Hide Feedback Result</button>
                            <?php //} ?>
						</h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				<div class="panel panel-flat">
					<table class="table datatable-basic">
							<thead>
								<tr>
									<th class="hidden"></th>
									<th>Feedback</th>
									<th width="100">Status</th>
									<th width="310">Action</th>
									<th class="hidden"></th>
									<th class="hidden"></th>
								</tr>
							</thead>
							<tbody id="mydata">
								<?php 
								$rs = mysql_query("SELECT * FROM feedbacks where event_id='".EVENT_ID."'");
								$num_rows = mysql_num_rows($rs);
								if($num_rows){
									while ($row = mysql_fetch_object($rs)){
									?>
									<tr>
										<td class="hidden"></td>
										<td><?php echo $row->title; ?></td>
										<td>
											<?php
											$feedback_id = '';
											$feedback_type = get_values("options","option_value","event_id='".EVENT_ID."' AND option_name='feedback_type'");
											if($feedback_type == 'feedback'){
												$feedback_id = get_values("options","option_value","event_id='".EVENT_ID."' AND option_name='feedback_id'");
											}
											if($row->id == $feedback_id){?>
												<a href="?set_current_feedback&id=<?php echo $row->id;?>" class="btn btn-primary btn-icon legitRipple" title="Click to Off">On</a>
											<?php }else{?>
												<a href="?set_current_feedback&id=<?php echo $row->id;?>" class="btn border-slate text-slate-800 btn-icon legitRipple" title="Click to On">Off</a>
											<?php }?>
										</td>
										<td>
											<a href="feedback_questions.php?feedback_id=<?php echo $row->id;?>"  class="label label-primary">Manage Questions</a>
											<a href="feedback_results.php?feedback_id=<?php echo $row->id;?>"  class="label label-primary">View Results</a>
											<a href="edit_feedback.php?edit&id=<?php echo $row->id;?>"  class="label label-success"><i class="fa fa-pencil"></i></a>
											<a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This Feedback?')){self.location='?delete&id=<?php echo $row->id; ?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
										</td>
										<td class="hidden"></td>
										<td class="hidden"></td>
									</tr>
								<?php }}?>
							</tbody>
						</table>
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
<script type="text/javascript">
    //var firebase_redik_webinar = new Firebase(firebase_url);
    //var firebase_feedback_on_off = firebase_redik_webinar.child('feedback_on_off');
    $(document).ready(function(){
		
		$("#btnFeedbackOn").click(function(){
			var send_data = { btn_feedback_on:'true' };
			$.ajax({
				type: "POST",
				async: true,
				url: "process.php",
				data: send_data,
				cache: false,
				success: function(result){
					$('#btnFeedbackOn').hide();
					$('#btnFeedbackOff').show();
				},
				error: function( result ) {

				}
			});
		});
		
		$("#btnFeedbackOff").click(function(){
			var send_data = { btn_feedback_off:'true' };
			$.ajax({
				type: "POST",
				async: true,
				url: "process.php",
				data: send_data,
				cache: false,
				success: function(result){
					$('#btnFeedbackOff').hide();
					$('#btnFeedbackOn').show();
				},
				error: function( result ) {

				}
			});
		});
		
		
		$("#btnFeedbackResultOn").click(function(){
			var send_data = { btn_feedback_result_on:'true' };
			$.ajax({
				type: "POST",
				async: true,
				url: "process.php",
				data: send_data,
				cache: false,
				success: function(result){
					$('#btnFeedbackResultOn').hide();
					$('#btnFeedbackResultOff').show();
				},
				error: function( result ) {

				}
			});
		});
		
		$("#btnFeedbackResultOff").click(function(){
			var send_data = { btn_feedback_result_off:'true' };
			$.ajax({
				type: "POST",
				async: true,
				url: "process.php",
				data: send_data,
				cache: false,
				success: function(result){
					$('#btnFeedbackResultOff').hide();
					$('#btnFeedbackResultOn').show();
				},
				error: function( result ) {

				}
			});
		});
		
    });
</script>
</body>
</html>