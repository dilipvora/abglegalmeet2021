<?php include 'header.php';
if(isset($_GET['quiz_id']) && isset($_GET['question_id'])){
    $quiz_id = $_GET['quiz_id'];
    $question_id = $_GET['question_id'];
}else{
    $_SESSION['error_msg'] = "Quiz not found!";
    echo "<script>window.location.href = 'quiz.php'</script>";
    exit();
}

if(isset($_POST['save_quiz_form_btn'])){

    $question_id = trim($_POST['question_id']);
    $quiz_id = trim($_POST['quiz_id']);
    $title = addslashes(trim($_POST['title']));
    $is_objective = isset($_POST['is_objective'])?1:0;
    $option1 = addslashes(trim($_POST['option1']));
    $option2 = addslashes(trim($_POST['option2']));
    $option3 = addslashes(trim($_POST['option3']));
    $option4 = addslashes(trim($_POST['option4']));
    $option5 = addslashes(trim($_POST['option5']));
    $option6 = addslashes(trim($_POST['option6']));
    $option7 = addslashes(trim($_POST['option7']));
    $image_name = addslashes(trim($_POST['image_name']));
    $correct_answer = addslashes(trim($_POST['correct_answer']));
    $correct_option = empty($_POST['correct_option'])?0:trim($_POST['correct_option']);
    if($is_objective == 0) {
        $option1 = $option2 = $option3 = $option4 = '';
        $correct_option = 0;
    }


    $sql = "UPDATE `quiz_questions` SET
						`title` = '{$title}',
						`is_objective` = '{$is_objective}',
						`option1` = '{$option1}',
						`option2` = '{$option2}',
						`option3` = '{$option3}',
						`option4` = '{$option4}',
						`option5` = '{$option5}',
						`option6` = '{$option6}',
						`option7` = '{$option7}',
						`image` = '{$image_name}',
						`correct_answer` = '{$correct_answer}',
						`correct_option` = '{$correct_option}'
						WHERE `event_id` = '".EVENT_ID."' AND `id` = '{$question_id}'";
    $res = mysql_query($sql);

    if($res){
        $_SESSION['success_msg'] = "Question saved successfully.";
        echo "<script>window.location.href = 'quiz_questions.php?quiz_id=".$quiz_id."'</script>";
        //header("Location:feedbacks.php");
        exit();
    }else{
        $_SESSION['error_msg'] = "Something goes wrong try again.";
    }
}
$rs = mysql_query("SELECT * FROM quiz_questions WHERE event_id='".EVENT_ID."' AND id = '{$question_id}' LIMIT 1");
$num_rows = mysql_num_rows($rs);
if(!$num_rows > 0){
    $_SESSION['error_msg'] = "Quiz question not found!";
    echo "<script>window.location.href = 'quiz_questions.php?quiz_id=".$quiz_id."'</script>";
    exit();
}
$row = mysql_fetch_object($rs)
?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php';?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Update Quiz Question</span></h4>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">

                <?php include 'messages.php';?>

                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post" name="save_feedback_form">
                            <input type="hidden" name="quiz_id" value="<?=$quiz_id?>">
                            <input type="hidden" name="question_id" value="<?=$row->id?>">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="col-md-12 form-group">
                                        <label>Question</label>
                                        <textarea name="title" class="form-control" placeholder="Question" required><?=$row->title?></textarea>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Is Objective Question</label>
                                        <input type="checkbox" name="is_objective" class="is_objective" value="1" <?=$row->is_objective == 1?'checked="checked"':''?> />
                                    </div>
                                    <div class="option_section <?=$row->is_objective == 1?'':'hidden'?>">
                                        <div class="col-md-6 form-group">
                                            <label>Option 1</label>
                                            <input type="text" name="option1" class="form-control options" placeholder="Option 1" value="<?=$row->option1?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 2</label>
                                            <input type="text" name="option2" class="form-control options" placeholder="Option 2" value="<?=$row->option2?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 3</label>
                                            <input type="text" name="option3" class="form-control options" placeholder="Option 3" value="<?=$row->option3?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 4</label>
                                            <input type="text" name="option4" class="form-control options" placeholder="Option 4" value="<?=$row->option4?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 5</label>
                                            <input type="text" name="option5" class="form-control options" placeholder="Option 5" value="<?=$row->option5?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 6</label>
                                            <input type="text" name="option6" class="form-control options" placeholder="Option 6" value="<?=$row->option6?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Option 7</label>
                                            <input type="text" name="option7" class="form-control options" placeholder="Option 7" value="<?=$row->option7?>">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>True Answer</label>
                                            <select class="form-control" name="correct_option">
                                                <option value="1" <?=($row->correct_option == 1)?'selected':''?>>Option 1</option>
                                                <option value="2" <?=($row->correct_option == 2)?'selected':''?>>Option 2</option>
                                                <option value="3" <?=($row->correct_option == 3)?'selected':''?>>Option 3</option>
                                                <option value="4" <?=($row->correct_option == 4)?'selected':''?>>Option 4</option>
                                                <option value="5" <?=($row->correct_option == 5)?'selected':''?>>Option 5</option>
                                                <option value="6" <?=($row->correct_option == 6)?'selected':''?>>Option 6</option>
                                                <option value="7" <?=($row->correct_option == 7)?'selected':''?>>Option 7</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>Correct Answer (Only for non Objective que)</label>
                                        <input type="text" name="correct_answer" class="form-control correct_answer" placeholder="Correct Answer" value="<?=$row->correct_answer?>">
                                        <p class="text-danger">*One word only</p>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Image Name</label>
                                        <input type="text" name="image_name" class="form-control" placeholder="Image Name" value="<?=$row->image_name?>">
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-primary" name="save_quiz_form_btn" value="save">Update</button>
                                        <a href="quiz_questions.php?quiz_id=<?=$quiz_id?>" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php include 'footer.php';?>
            </div>
            <!-- /Content area -->
        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<script>
    $(document).ready(function(){
        $(document).on('change','.is_objective',function() {
            if ($(this).is(":checked")) {
                $(".option_section").removeClass('hidden');
                //$('input.options').prop('required',true);
            } else {
                $(".option_section").addClass('hidden');
                $('input.options').prop('required',false);
            }
        });
    });
</script>
<!-- End Page container -->
</body>
</html>