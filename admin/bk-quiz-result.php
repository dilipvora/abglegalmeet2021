<?php include '../config.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quiz Result</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	
	<!-- Core JS files -->
	<script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
	<!-- /core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="../../assets/js/pages/datatables_basic.js"></script>
	<script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	
</head>

<body>
	
<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<!-- Main content -->
        <?php $quiz_id = get_values("options","option_value","event_id = '".EVENT_ID."' AND option_name='quiz_id'");
        $title = get_values("quiz","title","event_id = '".EVENT_ID."' AND id='{$quiz_id}'"); ?>

		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
                        <h4>
                            <i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><b><?=$title?></b> - Result</span>
                        </h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<div class="panel panel-flat">
                    <?php

                    /*$rs = mysql_query("SELECT * FROM `quiz_result` WHERE `quiz_id` = '{$quiz_id}'");

                    echo "<pre/>";
                    print_r(mysql_fetch_object($rs));

                    die;*/

                    $rs = mysql_query("SELECT `r`.`answer`, `r`.`answer_status`, `u`.`f1`, `u`.`uid` FROM `quiz_result` `r` JOIN `new_users` `u` ON `u`.`uid` = `r`.`uid` WHERE `r`.`quiz_id` = '{$quiz_id}' ORDER BY `uid` ASC, `question_id` ASC") or die(mysql_error()); ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Answer</th>
                                <th>Answer is</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if (mysql_num_rows($rs) > 0) {
                            while ($rows = mysql_fetch_object($rs)) { ?>
                                <tr>
                                    <td><?= $rows->f1 ?></td>
                                    <td><?= $rows->answer ?></td>
                                    <td><?= ($rows->answer_status == 1) ? "<span class='label bg-success'>Right</span>" : "<span class='label bg-danger'>Wrong</span>" ?></td>
                                </tr>
                            <?php }
                        } ?>
                        </tbody>
                    </table>

				</div>
				
				<!-- Footer -->
				<div class="footer text-muted">
					&copy; Copyright 2016 <a href="<?php echo POWERED_BY_URL; ?>" target="_blank"><?php echo POWERED_BY; ?></a> All right reserved.
				</div>
				<!-- /footer -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>