<?php include 'header.php';
$event_id = EVENT_ID;

$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '{$event_id}' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);
$next_count = $num_rows + 1;

if (isset($_GET['action']) && $_GET['action'] == 'delete' && isset($_GET['uid']) && $_GET['uid'] != '') {
    $uid = $_GET['uid'];
    $res = mysql_query("SELECT `img` FROM `users_images` WHERE `uid` = {$uid} AND `event_id` = '{$event_id}'");
    if (mysql_num_rows($res) > 0) {
        $file_path = "../selfie-corner/upload/image/";
        while ($row = mysql_fetch_object($res)) {
            $user_image = $file_path.$row->img;
            if (file_exists($user_image)) {
                unlink($user_image);
            }
        }
        mysql_query("DELETE FROM `users_images` WHERE `uid` = {$uid} AND `event_id` = '{$event_id}'");
        $_SESSION['success_msg'] = 'User Images Delete successfully';
        echo '<script type="text/javascript">window.location.replace("selfie-corner.php");</script>';
        exit();
    }
}

//$get_all_selfie = mysql_query("SELECT `id` FROM `users_images` WHERE `event_id` = '{$event_id}'");
//$selfie_count = mysql_num_rows($get_all_selfie);
$selfie_count = count(glob("../selfie-corner/upload/image/*"));

?>
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php'; ?>
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title row">
                        <h4 class="h4 text-left col-md-6">
                            <i class="icon-arrow-left52"></i> <span class="text-semibold">Selfie Corner</span>
                        </h4>
                        <h4 class="h4 text-right col-md-6">Total Selfie Clicked: <?=$selfie_count?></h4>
                    </div>
                </div>
            </div>

            <!-- Content area -->
            <div class="content">
                <?php include 'messages.php'; ?>
                <div class="panel panel-flat">
                    <!-- datatable-basic -->
                    <table class="table" id="selfie_corner_table">
                        <thead>
                        <tr>
                            <th class="hidden"></th>
                            <?php $filed_array = array();
                            if ($num_rows > 0) {
                                while ($row = mysql_fetch_array($form_field_query)) {
                                    array_push($filed_array, 'u.' . $row['field_name']); ?>
                                    <th><?= $row['field_label'] ?></th>
                                <?php }
                            } ?>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <?php include 'footer.php'; ?>
            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->

<div id="show_images"></div>

<?php $fields = implode(',', $filed_array); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '#delete_user_selfie', function () {
            if (confirm("Would You Like To Permanently Delete This Records")) {
                self.location=$(this).data("href");
            }
        });

        $(document).on("click", '#view-image', function () {
            var uid = $(this).data("id");
            $.ajax({
                method: "POST",
                url: "get_selfie_data.php",
                data: {uid},
                dataType: "JSON",
                success: function (response) {
                    $("#show_images").html(response.data);
                    $("#show_images_modal").modal("show");
                }
            })
        });

        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        var fields = '<?=$fields?>';
        var user_selfie_ajax_call = $("#selfie_corner_table").DataTable({
            "ajax": {
                "url": "get_selfie_data.php?selfie-corner&fields=" + fields
            },
            "columns": [
                {"data": 0, className: "hidden"},
                <?php $count = 1; for($i = 0;$i < $next_count;$i++){ ?>
                {"data": <?=$count?>},
                <?php $count++; } ?>
            ],
            "ordering": false,
            "lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
        });
        $("#selfie_corner_table_length select").select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });

        var interval_time = 10000;
        setInterval(function () {
            user_selfie_ajax_call.ajax.reload();
        }, interval_time);

		$("#btnshow").click(function(){
            var send_data = { selfe_btn_show:'true' };
            $.ajax({
                type: "POST",
                async: true,
                url: "process.php",
                data: send_data,
                cache: false,
                success: function(result){
                    $('#btnshow').hide();
                    $('#btnhide').show();
                },
                error: function( result ) {

                }
            });
        });

        $("#btnhide").click(function(){
            var send_data = { selfe_btn_hide:'true' };
            $.ajax({
                type: "POST",
                async: true,
                url: "process.php",
                data: send_data,
                cache: false,
                success: function(result){
                    $('#btnhide').hide();
                    $('#btnshow').show();
                },
                error: function( result ) {

                }
            });
        });

    });
</script>
</body>
</html>