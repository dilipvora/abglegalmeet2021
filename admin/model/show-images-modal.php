<?php $data = null;
$data .= '<div id="show_images_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-indigo">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Users Selfie</h5>
            </div>

            <div class="modal-body">
                <div class="row">'; ?>

<?php foreach ($model_data as $value) {
    $img = SITE_URL."selfie-corner/upload/image/$value->img";
    $data .= "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3'>
                    <a href='$img' class='text-center' disabled download>
                        <img src='$img' class='img-responsive' />
                    </a>
                </div>";
} ?>

<?php $data .= '</div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-xs btn-raised legitRipple" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>';