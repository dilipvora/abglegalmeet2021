<?php $data = '<div id="show_likeComments_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-indigo">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Nominee Likes & Comments</h5>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
                    if (mysql_num_rows($users_like_data_q) || mysql_num_rows($users_comments_data_q)) {
                        $likes = $comments = 1;
                        if (mysql_num_rows($users_like_data_q)) {
                            $data .= '<div class="table-responsive">
                                <table class="table table-xs table-striped table-framed">
                                    <thead>
                                        <tr>
                                            <th colspan="6" class="h3 text-center">Users Like Data</th>
                                        </tr>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Designation</th>
                                            <th>Like Count</th>
                                            <th>Registered Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>';
                                        while ($value = mysql_fetch_object($users_like_data_q)) {
                                            $data .= "<tr>";
                                            $data .= "<td>".$likes++."</td>";
                                            $data .= "<td>$value->name</td>";
                                            $data .= "<td>$value->email</td>";
                                            $data .= "<td>$value->location</td>";
                                            $data .= "<td>$value->count</td>";
                                            $data .= "<td>".date('D, d F, Y h:i A', strtotime($value->created_date))."</td>";
                                            $data .= "</tr>";
                                        }
                                        $data .= '</tbody>
                                </table>
                            </div>';
                        }
                        $data .= '</div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br/></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
                        if (mysql_num_rows($users_comments_data_q)) {
                            $data .= '<div class="table-responsive">
                                <table class="table table-xs table-striped table-framed">
                                    <thead>
                                        <tr>
                                            <th colspan="6" class="h3 text-center">Users Comment Data</th>
                                        </tr>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Designation</th>
                                            <th>Comments</th>
                                            <th>Registered Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>';
                                        while ($value = mysql_fetch_object($users_comments_data_q)) {
                                            $data .= "<tr>";
                                            $data .= "<td>".$comments++."</td>";
                                            $data .= "<td>$value->name</td>";
                                            $data .= "<td>$value->email</td>";
                                            $data .= "<td>$value->location</td>";
                                            $data .= "<td>$value->q_question</td>";
                                            $data .= "<td>".date('D, d F, Y h:i A', strtotime($value->created_date))."</td>";
                                            $data .= "</tr>";
                                        }
                                        $data .= '</tbody>
                                </table>
                            </div>';
                        }
                    } else {
                        $data .= '<h4 class="h4 text-center"><b>No record found</b></h4>';
                    }
                    $data .= '</div>
                </div>
            </div>
        </div>
    </div>';