<?php $data = '<div id="show_nominee_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-indigo">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Registered Nominee</h5>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-xs table-striped table-framed">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Designation</th>
                                    <th>Registered Date</th>
                                </tr>
                                </thead>
                                <tbody>';
                                if (!empty($model_data)) {
                                    foreach ($model_data as $value) {
                                        $data .= "<tr>";
                                        $data .= "<td>$value->name</td>";
                                        $data .= "<td>$value->email</td>";
                                        $data .= "<td>$value->location</td>";
                                        $data .= "<td>$value->created_date</td>";
                                        $data .= "</tr>";
                                    }
                                } else {
                                    $data .= "<tr><td colspan='4' class='text-center h4'><b>No record found</b></td></tr>";
                                }
                                $data .= '</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>';