<?php include 'header.php';

//print_r($_POST); die;

$created_date = date("Y-m-d H:i:s");
$event_id = EVENT_ID;

$event_start_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_start_date_time' and `event_id` = '{$event_id}'");
$event_start_date_time_data = mysql_fetch_object($event_start_date_time);

$event_end_date_time = mysql_query("SELECT `option_value` from `options` where `option_name` = 'event_end_date_time' and `event_id` = '{$event_id}'");
$event_end_date_time_data = mysql_fetch_object($event_end_date_time);

if (isset($_POST["save_attendee_report_btn"])) {
    $exist_num = mysql_num_rows($event_start_date_time);
    $event_start_date_time_value = (isset($_POST['event_start_date_time']) && !empty($_POST['event_start_date_time'])) ? $_POST['event_start_date_time'] : null;
    if($exist_num>0){
        $res = mysql_query("UPDATE `options` set `option_value` = '{$event_start_date_time_value}' where `option_name` = 'event_start_date_time' and `event_id` = '{$event_id}'");
    }else{
        $res = mysql_query("INSERT into `options` set `option_name` = 'event_start_date_time',`option_value`= '{$event_start_date_time_value}',`event_id`='{$event_id}',`created_date`='{$created_date}'");
    }


    $exist_num = mysql_num_rows($event_end_date_time);
    if (isset($_POST['event_end_date_time']) && !empty($_POST['event_end_date_time'])) {
        $event_end_date_time_value = $_POST['event_end_date_time'];
    } else {
        // for event end time is not set
        $event_end_date_time_value = date('Y-m-d\TH:i', strtotime('next day', strtotime('midnight -1 minutes', strtotime($event_start_date_time_value))));
    }
    if($exist_num>0){
        $res = mysql_query("UPDATE `options` set `option_value` = '{$event_end_date_time_value}' where `option_name` = 'event_end_date_time' and `event_id` = '{$event_id}'");
    }else{
        $res = mysql_query("INSERT into `options` set `option_name` = 'event_end_date_time',`option_value`= '{$event_end_date_time_value}',`event_id`='{$event_id}',`created_date`='{$created_date}'");
    }

    if($res){
        $_SESSION['success_msg'] = "Event Info saved successfully.";
    }else{
        $_SESSION['error_msg'] = "Something goes wrong try again.";
    }

    echo '<script type="text/javascript">window.location.href="get-attendee-report.php";</script>';
    exit();
}
$event_start_date_time_data = !empty($event_start_date_time_data->option_value) ? $event_start_date_time_data->option_value : null;
$event_end_date_time_data = !empty($event_end_date_time_data->option_value) ? $event_end_date_time_data->option_value : null;

//echo date('Y-m-d H:i:s', strtotime('next day', strtotime('midnight -1 minutes', strtotime($event_start_date_time_data))));

?>

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php'; ?>
        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">
                <div class="row">
                    <div class="col-md-6">

                        <?php include 'messages.php'; ?>

                        <form action="" method="post" name="save_attendee_report">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>Event Start Date & Time</label>
                                                <input type="datetime-local" name="event_start_date_time" placeholder="Event Start Date & Time" class="form-control" value="<?=$event_start_date_time_data?>" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>Event End Date & Time</label>
                                                <input type="datetime-local" name="event_end_date_time" class="form-control" placeholder="Event End Date & Time" value="<?=$event_end_date_time_data?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <input type="submit" class="btn bg-primary" name="save_attendee_report_btn" value="save">
                                        <a href="export-data.php?attendee_report" class="btn bg-indigo">Export Report</a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <?php include 'footer.php'; ?>
            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>