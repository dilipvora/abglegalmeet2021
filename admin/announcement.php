<?php include 'header.php';
$rs = mysql_query("SELECT * FROM `announcement` WHERE `event_id` = '".EVENT_ID."'");
$num_row = mysql_num_rows($rs);
$announce_text = '';
if($num_row){
	$announce = mysql_fetch_assoc($rs);
	$announce_text = $announce['announcement_text'];
}
?>
<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<div class="panel panel-flat">
					
						<div id="announce_msg"></div>
						<div class="panel-heading">
							<h5 class="panel-title">Announcement</h5>
							<!-- <div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="reload"></a></li>
			                	</ul>
		                	</div> -->
						</div>
						

						<div class="panel-body">
							<form class="form-horizontal" action="process.php" method="post" id="frmAnnounce">
								<fieldset class="content-group">
									<div class="form-group">
										<div class="col-lg-10">
											<textarea rows="5" cols="5" class="form-control" name="announcement_text" placeholder="Enter Announcement Text here"><?php echo $announce_text;?></textarea>
										</div>
									</div>
									<input type="hidden" name="frmAnnounce" value="1">
								</fieldset>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" id="btnSubmit">Save <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</form>
						</div>
					</div>
					<?php include 'footer.php';?>
				</div>
				<!-- /content area -->
			</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>

<script type="text/javascript">
var firebase_redik_webinar = new Firebase(firebase_url);
var firebase_current_announcement = firebase_redik_webinar.child('current_announcement');
$(document).ready(function(){
	$("#frmAnnounce").on('submit',(function(e) {
		e.preventDefault();
		$('#announce_msg').html('');
		$.ajax({
			url: $(this).attr('action'),
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				firebase_current_announcement.set({ text: data});
				$('#announce_msg').html('<div class="alert bg-green-600 alert-styled-left"><button type="button" class="close" data-dismiss="alert"><span>X</span><span class="sr-only">Close</span></button><span class="text-semibold">Announcement save successfully.</span></div>');
			},
			error: function( result ) {
				alert('Something goes wrong.');
			}
		});
	}));
});

</script>