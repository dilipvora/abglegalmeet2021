<?php include 'header.php';
//header('Content-Type: text/html; charset=utf-8');

$form_field_query = mysql_query("SELECT `field_name`, `field_label` from registration_field where event_id = '" . EVENT_ID . "' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);

$filed_array = array();
if ($num_rows > 0) {
    while ($row = mysql_fetch_object($form_field_query)) {
        $filed_array[] = $row->field_name;
    }
}
$fields_count = count($filed_array);
$fields = implode(',',$filed_array);
?>

<style type="text/css">
    .table tbody tr th {
        min-width: 250px;
        max-width: 350px;
        width: auto;
    }

    .table tbody tr td {
        min-width: 250px;
        max-width: 350px;
        width: auto;
    }

    .table-responsive {
        max-height: 500px;
        height: auto;
        margin-bottom: 20px;
    }
    .form-group {
        margin: 0;
    }
</style>

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->

        <!-- /main sidebar -->
        <?php include 'sidebar.php'; ?>
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Import Users</span></h4>
                    </div>
                </div>
            </div>

            <!-- Content area -->
            <div class="content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Select Only CSV File</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" id="upload_area">
                            <form method="post" id="upload_form" enctype="multipart/form-data">
                                <input type="hidden" name="action" value="upload_csv" />
                                <div class="col-md-4">
                                    <div id="message"></div>
                                    <b>Select File:</b>
                                    <div class="input-group form-group">
                                        <input type="file" name="file" id="csv_file" class="form-control"/>
                                        <div class="input-group-btn">
                                            <input type="submit" name="upload_file" id="upload_file" class="btn btn-primary" value="Upload"/>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        </div>
                        <div id="process_area">

                        </div>
                    </div>
                </div>
            </div>
            <!-- /content area -->
            <?php include 'footer.php'; ?>

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->

<script type="text/javascript">
    $(document).ready(function () {

        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        $("#users_import_table").DataTable({
            "processing": true,
            "ordering": false,
            "lengthMenu": [[100, 50, 25, 10, -1], [100, 50, 25, 10, "All"]]
        });
        $("#users_import_table_length select").select2();


        var <?=$fields?> = "null";

        $(document).on('submit', '#upload_form', function (event) {
            event.preventDefault();
            $.ajax({
                url: "import-users-process.php",
                method: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.error != '') {
                        $('#message').html('<div class="alert alert-danger">' + data.error + '</div>');
                    } else {
                        $('#process_area').html(data.output);
                        $('#upload_area').css('display', 'none');
                    }
                }
            });
        });

        var total_selection = 0;

        var column_data = [];

        $(document).on('change', '.set_column_data', function () {
            var column_name = $(this).val();
            var column_number = $(this).data('column_number');
            if (column_name in column_data) {
                alert('You have already define this column');
                $(this).val('');
                return false;
            }

            if (column_name != '') {
                column_data[column_name] = column_number;
            } else {
                const entries = Object.entries(column_data);
                for (const [key, value] of entries) {
                    if (value == column_number) {
                        delete column_data[key];
                    }
                }
            }

            total_selection = Object.keys(column_data).length;

            if (total_selection == <?=$fields_count?>) {
                $('#import').attr('disabled', false);

                <?php foreach ($filed_array as $value) { ?>
                    <?=$value?> = column_data.<?=$value?>;
                <?php } ?>

                /*first_name = column_data.first_name;
                last_name = column_data.last_name;
                email = column_data.email;*/
            } else {
                $('#import').attr('disabled', 'disabled');
            }

        });

        $(document).on('click', '#import', function (event) {
            event.preventDefault();
            var file_data = $("#file_data").val();

            $.ajax({
                url: "import-users-process.php",
                method: "POST",
                data: {"action":"import", "file_data":file_data, <?=$fields?>},
                beforeSend: function () {
                    $('#import').attr('disabled', 'disabled');
                    $('#import').text('Importing...');
                },
                success: function (data) {
                    $('#import').attr('disabled', false);
                    $('#import').text('Import');
                    $('#process_area').css('display', 'none');
                    $('#upload_area').css('display', 'block');
                    $('#upload_form')[0].reset();
                    $('#message').html(data);
                }
            });
        });

    });
</script>

</body>
</html>