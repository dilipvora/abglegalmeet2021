<?php include 'header.php';

$event_id = EVENT_ID;

if(isset($_GET['feedback_id'])){
	$feedback_id = $_GET['feedback_id'];
}else{
	$_SESSION['error_msg'] = "Feedback not found!";
	echo "<script>window.location.href = 'feedbacks.php'</script>";
	//header("Location:feedbacks.php");
	exit();
}

if (isset($_GET['set_default_question'])) {
    $que_title_array = array(
        "How would you rate the relevance of the webinar?",
        "How would you rate the quality of information provided?",
        "How would you rate the format of the webinar?",
        "During this session, I learnt something new (Please rate from 1=Strongly disagree to 5=Strongly agree)",
        "Information presented is relevant to my practice (Please rate from 1=Strongly disagree to 5=Strongly agree)",
        "TOPICS THAT YOU WOULD LIKE TO SEE IN FUTURE WEBINARS"
    );
    $is_objective_array = array("1", "1", "1", "1", "1", "0");

    $option_one_array = array("Excellent", "Excellent", "Excellent", "1", "1");
    $option_two_array = array("Very Good", "Very Good", "Very Good", "2", "2");
    $option_three_array = array("Good", "Good", "Good", "3", "3");
    $option_four_array = array("Satisfactory", "Satisfactory", "Satisfactory", "4", "4");
    $option_five_array = array("Poor", "Poor", "Poor", "5", "5");

    $date = date('Y-m-d H:i:s');

    foreach ($que_title_array as $key => $value) {
        $title = addslashes(trim($value));
        $is_objective = trim($is_objective_array[$key]);
        $option_one = isset($option_one_array[$key]) ? addslashes($option_one_array[$key]) : null;
        $option_two = isset($option_two_array[$key]) ? addslashes($option_two_array[$key]) : null;
        $option_three = isset($option_three_array[$key]) ? addslashes($option_three_array[$key]) : null;
        $option_four = isset($option_four_array[$key]) ? addslashes($option_four_array[$key]) : null;
        $option_five = isset($option_five_array[$key]) ? addslashes($option_five_array[$key]) : null;

        $sql = "INSERT INTO `feedback_questions` SET
                        `event_id` = '{$event_id}',
						`feedback_id` = '{$feedback_id}',
						`title` = '{$title}',
						`is_objective` = '{$is_objective}',
						`option1` = '{$option_one}',
						`option2` = '{$option_two}',
						`option3` = '{$option_three}',
						`option4` = '{$option_four}',
						`option5` = '{$option_five}',
						`correct_option` = '0'";

        //echo $sql."<hr/>";
        $res = mysql_query($sql);
    }
    echo '<script>window.location = "feedback_questions.php?feedback_id='.$feedback_id.'"</script>';
}

if(isset($_GET['delete']) && isset($_GET['question_id']) && $_GET['question_id'] !=''){
	$id = $_GET['question_id'];
	$sql = "delete from feedback_questions where id = '{$id}' AND event_id='{$event_id}'";
	mysql_query($sql);
	$_SESSION['success_msg'] = 'Questions deleted successfully.';
} ?>

<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="page-header">
				<div class="page-header-content">
					<div class="page-title">
						<h4>
							<i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Feedback Questions List</span>
                            <a href="javascript:void(0)" class="btn btn-primary float-right" style="margin-right: 10px;" onClick="if(confirm('Would You Like To Create Default Question?')){self.location='?feedback_id=<?=$feedback_id?>&set_default_question';}">Set Default Question</a>
							<a href="add_feedback_question.php?feedback_id=<?=$feedback_id?>" class="btn btn-primary float-right" style="margin-right: 10px;">Add Question</a>
						</h4>
					</div>
				</div>
			</div>
			
			<!-- Content area -->
			<div class="content">
				<?php include 'messages.php';?>
				<div class="panel panel-flat">
					<table class="table datatable-basic">
							<thead>
								<tr>
									<th class="hidden"></th>
									<th>Question</th>
									<th>Option Is</th>
									<th>Option 1</th>
									<th>Option 2</th>
									<th>Option 3</th>
									<th>Option 4</th>
									<th>Option 5</th>
									<th>Option 6</th>
									<th>Option 7</th>
									<!--<th>Correct Option</th>-->
									<th width="150">Action</th>
								</tr>
							</thead>
							<tbody id="mydata">
								<?php 
								$rs = mysql_query("SELECT * FROM feedback_questions WHERE event_id='{$event_id}' AND feedback_id = '$feedback_id'");
								$num_rows = mysql_num_rows($rs);
								if($num_rows){
									while ($row = mysql_fetch_object($rs)){
									    if ($row->is_objective == 1) {
									        $is_objective_or_multi_choose = "Single Choose";
                                        } elseif ($row->is_objective == 2) {
                                            $is_objective_or_multi_choose = "Multi Choose";
                                        } else {
                                            $is_objective_or_multi_choose = "Open Box";
                                        }
									?>
									<tr>
										<td class="hidden"></td>
										<td><?php echo $row->title; ?></td>
										<td><?php echo $is_objective_or_multi_choose ?></td>
										<td><?php echo $row->option1; ?></td>
										<td><?php echo $row->option2; ?></td>
										<td><?php echo $row->option3; ?></td>
										<td><?php echo $row->option4; ?></td>
										<td><?php echo $row->option5; ?></td>
										<td><?php echo $row->option6; ?></td>
										<td><?php echo $row->option7; ?></td>
										<!--<td><?php /*echo $row->correct_option != 0?$row->correct_option:''; */?></td>-->
										<td>
											<a href="edit_feedback_question.php?feedback_id=<?php echo $feedback_id;?>&question_id=<?php echo $row->id;?>"  class="label label-success"><i class="fa fa-pencil"></i></a>
											<a href="javascript:void(0)" onClick="if(confirm('Would You Like To Permanently Delete This Feedback?')){self.location='?delete&feedback_id=<?php echo $feedback_id;?>&question_id=<?php echo $row->id;?>';}" class="label label-danger"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
								<?php }}?>
							</tbody>
						</table>
				</div>
				<?php include 'footer.php';?>
			</div>
			<!-- /content area -->

		</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>