<?php include '../config.php';
require_once '../PHPExcel/Classes/PHPExcel.php';
$event_id = EVENT_ID;

if (!empty($_GET['qid'])) {
    $quiz_id = $_GET['qid'];
} else {
    $quiz_id = get_values("options", "option_value", "event_id = '{$event_id}' AND option_name='quiz_id'");
}

$objPHPExcel = new PHPExcel();
$bold_font = array('font' => array('bold' => true, 'size' => 13));

$form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '{$event_id}' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($form_field_query);

if (isset($_GET["date"])) {
    $date = $_GET["date"];
} else {
    $date = date("Y/m/d");
}

$title = get_values("quiz", "title", "event_id = '" . EVENT_ID . "' AND id='{$quiz_id}'");
$file_title = $event_id." Quiz Result | $title";

$filed_array = $final_result = array();

if ($num_rows > 0) {
    $alphabet = "A";
    while ($row = mysql_fetch_object($form_field_query)) {
        array_push($filed_array, $row->field_name);
        $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", $row->field_label)->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
        $alphabet++;
    }
    $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Total True Answer")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

    $alphabet++;
    $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Total Time")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);

    $alphabet++;
    $objPHPExcel->getActiveSheet()->setCellValue("$alphabet" . "1", "Date")->getStyle("$alphabet" . "1:$alphabet" . "1")->applyFromArray($bold_font);
}

$no_col = count($filed_array) + 3;
$fields = implode(',', $filed_array);
$date = date('Y-m-d', strtotime($_REQUEST['date']));
$date_string = "";
if ($date != "" && $date != '1970-01-01') {
    $date = date('Y-m-d', strtotime($_REQUEST['date']));
    $date_string = "date(created_date) LIKE '{$date}%' AND ";
}

/*$rs = mysql_query("SELECT SUM(r.answer_status) as total, $fields, `u`.`uid`, `r`.`created_at` FROM `quiz_result` `r`
                                INNER JOIN `new_users` `u` ON u.uid = r.uid
                                WHERE $quiz_str `r`.`event_id` = '{$event_id}'
                                GROUP BY r.uid ORDER BY `total` DESC LIMIT $length");*/
$rs = mysql_query("SELECT SUM(r.answer_status) as total, $fields, `u`.`uid`, `r`.`created_at` FROM `quiz_result` `r`
                                INNER JOIN `new_users` `u` ON u.uid = r.uid
                                WHERE `r`.`quiz_id` = '{$quiz_id}' AND `r`.`event_id` = '{$event_id}' GROUP BY r.uid HAVING `total` <= 5 ORDER BY `total` DESC");

$i = 0;
if (mysql_num_rows($rs) > 0) {
    while ($rows = mysql_fetch_object($rs)) {
        $result = mysql_query("SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(`q`.`total_time`)))  as `total_time` FROM `quiz_time` `q` WHERE `q`.`event_id` = '{$event_id}' AND `q`.`uid` = '{$rows->uid}'");
        $total_time = '';
        if (mysql_num_rows($result) > 0) {
            $total_time = mysql_fetch_object($result)->total_time;
        }

        $form_field_query = mysql_query("SELECT * from registration_field where display_question = 1 AND event_id = '{$event_id}' ORDER BY display_order ASC");
        while ($row = mysql_fetch_object($form_field_query)) {
            $field_name = $row->field_name;
            $final_result[$rows->total][$i][$field_name] = $rows->$field_name;
        }
        $final_result[$rows->total][$i]['total'] = $rows->total;
        $final_result[$rows->total][$i]['total_time'] = $total_time;
        $final_result[$rows->total][$i]['date'] = date("d-m-Y", strtotime($rows->created_at));
        $i++;
    }
}

//echo "<pre>"; print_r($final_result);
function result_sort($result_a, $result_b)
{
    return strtotime($result_a["total_time"]) - strtotime($result_b["total_time"]);
}

$final_result_temp = array_slice($final_result, 0, 100, true);
//echo "<pre>"; print_r($final_result_temp); exit;

if (!empty($final_result_temp)) {
    foreach ($final_result_temp as &$sresult) {
        usort($sresult, "result_sort");
    }
}
$final_result_temp = call_user_func_array('array_merge', $final_result_temp);
//echo "<pre>"; print_r($final_result_temp); exit;
$fields_array = explode(',', $fields);

if (!empty($final_result_temp)) {
    $h = 2;
    foreach ($final_result_temp as $subresult) {
        $lines = array();
        //foreach ($subresult as $fresult) { ?>
            <?php if (!empty($fields_array)) {
                foreach ($fields_array as $item) {
                    $field = str_replace('u.', '', $item);
                    $lines[] = $subresult[$field];
                }
            }
            $lines[] = $subresult['total'];
            $lines[] = $subresult['total_time'];
            $lines[] = $subresult['date'];
        //}
        $alphabet_new = "A";
        for ($j = 0; count($lines) > $j; $j++) {
            $objPHPExcel->getActiveSheet()->setCellValue("$alphabet_new" . "$h", $lines[$j]);
            $alphabet_new++;
        }
        $h++;
    }
} else {
    $style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->setCellValue("A2", "No matching records found")->getStyle("A2:A2")->applyFromArray($style);
    $objPHPExcel->setActiveSheetIndex()->mergeCells("A2:$alphabet" . "2");
}

//die;
/*echo "<pre/>";
print_r($lines);*/

$filename = "$file_title.xlsx"; //save our workbook as this file name
ob_clean();
ob_end_clean();
header("Content-Type: application/vnd.ms-excel"); //mime type
header("Content-Disposition: attachment;filename=$filename"); //tell browser what's the file name
header("Cache-Control: max-age=0"); //no cache
ob_flush();
flush();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

//force user to download the Excel file without writing it to server's HD
$objWriter->save("php://output");