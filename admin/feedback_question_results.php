<?php include 'header.php';

if (isset($_GET['question_id'])) {
    $question_id = $_GET['question_id'];
} else {
    $_SESSION['error_msg'] = "Question not found!";
    echo "<script>window.location.href = 'feedback_results.php'</script>";
    exit();
}
?>

<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <?php include 'sidebar.php'; ?>
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <?php $rs_question_feedback = get_selected('feedback_questions', "id = '{$question_id}' AND is_objective = 0");?>
                        <h5 class="panel-title"><i><?= $rs_question_feedback[0]->title ?></i></h5>
                    </div>
                </div>
            </div>

            <!-- Content area -->
            <div class="content">
                <?php include 'messages.php'; ?>
                <div class="panel panel-flat">
                    <table class="table datatable-basic">
                        <thead>
                        <tr>
                            <th class="hidden"></th>
                            <th class="hidden"></th>
                            <th class="hidden"></th>
                            <th>User</th>
                            <th>Answer</th>
                            <th></th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody id="mydata">
                        <?php $rs_r = mysql_query("SELECT u.f1 as name, uf.answer, uf.question_id, uf.created_at FROM `users_feedbacks` as uf LEFT JOIN `new_users` as u ON u.uid = uf.uid where question_id= '{$question_id}'");
                        $num_rows_r = mysql_num_rows($rs_r);
                        if ($num_rows_r) {
                            while ($row_r = mysql_fetch_object($rs_r)) {
                                $is_objective = get_values("feedback_questions", "is_objective", "event_id='" . EVENT_ID . "' AND id=$row_r->question_id");
                                if ($is_objective == 2) {
                                    $options_array = explode(",", $row_r->answer);
                                    $get_feedback_answer = array();
                                    foreach ($options_array as $value) {
                                        $get_feedback_answer[] = get_values("feedback_questions", "option$value", "event_id='" . EVENT_ID . "' AND id=$row_r->question_id");
                                    }
                                    $feedback_answer = implode(",<br/>", $get_feedback_answer);
                                } else {
                                    $feedback_answer = $row_r->answer;
                                } ?>
                                <tr>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td><?= $row_r->name ?></td>
                                    <td><?= $feedback_answer ?></td>
                                    <td></td>
                                    <td><?= $row_r->created_at ?></td>
                                </tr>
                            <?php }
                        } ?>
                        </tbody>
                    </table>
                </div>
                <?php include 'footer.php'; ?>
            </div>
            <!-- /content area -->

        </div>
        <!-- /Main content -->
    </div>
    <!-- End Page content -->
</div>
<!-- End Page container -->
</body>
</html>