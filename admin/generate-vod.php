<?php include 'header.php'; 

if(isset($_POST['submit-view-vod-form-btn'])){
	$date = date('Y-m-d',strtotime($_POST['date']));
	if($date =='' || $date == '1970-01-01'){
		$date = date('Y-m-d');	
	}
}else{
	$date = date('Y-m-d');
}

$results = mysql_query("SELECT * FROM `ppt_time` WHERE `created_date` LIKE '{$date}%' AND `event_id` = '".EVENT_ID."'");

//$results = mysql_query("SELECT * FROM ppt_time");

$vod_data = '';
$img_arr = array();
$i = 0;
if(mysql_num_rows($results)>0){
	while($row = mysql_fetch_object($results)) {
		$img_arr[$i]['start'] = $row->start;
		$img_arr[$i]['src'] = $row->src;
		$i++;
	}
	
	$vod_data = 'if(position == 0){$("#slide-img").attr("src", "upload/ppt/Slide001.JPG");}'.PHP_EOL;
	for ($i=1; $i < count($img_arr) ; $i++) {
		/*$image_js .= '.image({<br>'.
		 '"start": '.$img_arr[$i-1]['start'].',<br>'.
				'"end": '.$img_arr[$i]['start'].',<br>'.
				'"target": "popcorn-container",'.
				'"src": "'.$img_arr[$i-1]['src'].'"<br>'.
				'})<br>';*/
	
		$start = $img_arr[$i-1]['start'];
		$end = $img_arr[$i]['start'];
		//$start -= 615;
		//$end -= 615;
		//$start -= 629;
		//$end -= 629;
		$vod_data .= 'else if(position >= '.($start).' && position < '.($end).'){$("#slide-img").attr("src", "'.$img_arr[$i-1]['src'].'"); }'.PHP_EOL;
	}
	$vod_data .= 'else if(position >= '.($img_arr[$i-1]['start']).' && position < '.( $img_arr[$i-1]['start'] + 100).'){$("#slide-img").attr("src", "'.$img_arr[$i-1]['src'].'"); }'.PHP_EOL;
}
?>

<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		<!-- main sidebar -->
		
		<!-- /main sidebar -->
		<?php include 'sidebar.php';?>
		<!-- Main content -->
		<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Generate VOD</span></h4>
						</div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row text-right">
	                            <?php
							$hidden_date = date('m/d/Y');
							if(isset($_POST['date']) && $_POST['date'] !='' && $_POST['date'] !='1970-01-01'){
								$hidden_date = $_POST['date'];
							}?>
								<div class="col-lg-4">
		                            <input type="hidden" name="vod_hidden_date" id="vod_hidden_date" value="<?php echo $hidden_date;?>">
									<form class="form-inline" role="form" name="view-vods-form" method="post" action="#" style="display: inline-block;">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
												<input type="text" name="date" class="form-control datepicker-menus" value="<?php if(isset($_POST['date'])){echo $_POST['date'];}else{ echo date('m/d/Y');}?>" placeholder="Select Date">
											</div>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-primary" name="submit-view-vod-form-btn" value="save">View Code</button>
										</div>
									</form>
								</div>
								<div class="col-lg-8">
									<div class="form-group col-sm-10" style="margin-bottom: 0px;">
											<div class="col-sm-6">
												<input type="text" placeholder="Find" id="find-string" class="form-control">
											</div>
							
											<div class="col-sm-6">
												<input type="text" placeholder="Replace" id="replace-string" class="form-control">
											</div>
									</div>
									<div class="form-group col-sm-2" style="margin-bottom: 0px;">
										<button type="button" class="btn btn-primary" id="submit-find-replace">Replace All</button>
									</div>
									
								</div>
								
							</div>
						</div>
					</div>
						
					<!-- Dashboard content -->
					<div class="panel panel-flat">
						<div class="panel-body">
							<div id="javascript_editor"><?php echo $vod_data;?></div>
						</div>
					</div>
					<!-- /default ordering -->
					<!-- /dashboard content -->
					<?php include 'footer.php';?>

				</div>
				<!-- /content area -->

			</div>
		<!-- /Main content -->
	</div>
	<!-- End Page content -->
</div>
<!-- End Page container -->
<script>
$(function() {
	// Javascript editor
    var js_editor = ace.edit("javascript_editor");
        js_editor.setTheme("ace/theme/monokai");
        js_editor.getSession().setMode("ace/mode/javascript");
        js_editor.setShowPrintMargin(false);

        
        
    $( "#submit-find-replace" ).on( "click", function() {
    	var find = $("#find-string").val();
    	var replace = $("#replace-string").val();
    	if(find !=''){
    		js_editor.find(find);
        	js_editor.replaceAll(replace);
    	}
    	$("#find-string").val('');
    	$("#replace-string").val('');
    	  //console.log( js_editor.getValue());
    });
});



</script>
</body>
</html>