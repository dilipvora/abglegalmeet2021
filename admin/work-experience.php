<?php include 'header1.php';
$event_id = EVENT_ID;
$form_query = mysql_query("SELECT ef.f1, ef.f2, ef.f3, ef.f4, u.f1 as user_name FROM `extra_form` as `ef` INNER JOIN `new_users` as `u` ON ef.uid = u.uid WHERE ef.role = 'work experience' AND ef.event_id = '{$event_id}'");
//$form_query = mysql_query("SELECT ef.f1, ef.f2, ef.f3, ef.f4, u.f1 as user_name FROM `extra_form` as `ef` INNER JOIN `new_users` as `u` ON ef.uid = u.uid WHERE ef.event_id = '{$event_id}'");
$num_rows = mysql_num_rows($form_query); ?>

<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- main sidebar -->
        <!-- /main sidebar -->
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Work Experience</span></h4>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="export-data.php?work-experience" class="btn btn-primary">Download Report</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Dashboard content -->
                <div class="panel panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-condensed" id="work_table">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th class="col-md-1">User Name</th>
                                <th class="col-md-2">Within Aditya Birla Group</th>
                                <th class="col-md-1">Overall</th>
                                <th class="col-md-2">Area/Subject expertise in Legal Domain</th>
                                <th class="col-md-6">Introduce about yourself</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($num_rows) {
                                $i = 1;
                                while ($rows = mysql_fetch_object($form_query)) { ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $rows->user_name ?></td>
                                        <td><?= $rows->f1 ?></td>
                                        <td><?= $rows->f2 ?></td>
                                        <td><?= $rows->f3 ?></td>
                                        <td><?= $rows->f4 ?></td>
                                    </tr>
                                    <?php $i++;
                                } ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="6"></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: true
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });
        $("#work_table").DataTable();
        $("#work_table_length select").select2();
    });
</script>

</body>
</html>