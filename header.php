<?php include 'config.php';
/*if (isset($_SESSION['uid']) && $_SESSION['event_id'] == EVENT_ID) {
    header("location:" . SITE_URL . "welcome.php");
}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta http-equiv="x-dns-prefetch-control" content="on"/>
    <meta property="og:type" content="website"/>
    <meta name="og_site_name" property="og:site_name" content="<?=SITE_URL?>"/>
    <meta property="og:url" content="<?=SITE_URL?>"/>
    <meta name="theme-color" content="#a32421">

	<title><?= COMPANY_NAME ?> Live Webcast</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap" rel="stylesheet">
    <link href="fonts/font-awesome/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/sweetalert2.min.css" >
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/imageMapResizer.min.js"></script>

    <link rel="stylesheet" href="css/select2.min.css">
    <link rel="stylesheet" href="css/custom.css?v=1">
</head>

<body>