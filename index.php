<?php include 'header.php';
if (isset($_SESSION['uid']) && $_SESSION['event_id'] == EVENT_ID) {
    header("location:" . SITE_URL . "lobby.php#lobby");
    exit();
}

$event_id = EVENT_ID;

$get_field_data = mysql_query("SELECT * from `registration_field` where `event_id` = '{$event_id}' ORDER BY display_order ASC");
$num_rows = mysql_num_rows($get_field_data); ?>

<style type="text/css">
    html, body {
        height: 100%;
    }
    body {
        background-image: url("img/bg.jpg");
        background-position: center;
        background-attachment: fixed;
        background-repeat: no-repeat;
        background-size: cover;
        background-color: #fff;
    }
    .homeslide {
        width: 50%;
    }
    .outer {
        display: table;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }
    .middle {
        display: table-cell;
        vertical-align: middle;
    }
    .inner {
        margin: auto;
        width: auto;
    }

    .right_side, .right_side .card {
        background: transparent;
        border: unset;
    }

    .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show>.btn-primary.dropdown-toggle,
    .theme_button {
        font-size: 1.25rem;
        line-height: 1.6;
        letter-spacing: 1.5px;
        background: #ea5725;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #6e090c;
        border-radius: 8px;
    }

    .theme_button:hover, .theme_button:focus, .theme_button:active {
        background: #6e090c;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #ea5725;
    }

    .form-control {
        color: #fff;
        font-size: 1.25rem;
        line-height: 1.6;
        background: #a32421;
        border: none;
        border-bottom: 3px solid #c5491f;
        border-radius: 8px;
    }
    .form-control:focus {
        background: #c5491f;
        border: none;
        color: white;
        border-bottom: 5px solid #a32421;
    }

    @media screen and (max-width: 767px) {
        .homeslide {
            width: 75%;
        }
        .outer {
            display: unset;
            position: unset;
        }
        .middle {
            display: unset;
            vertical-align: unset;
        }
        .inner {
            width: 100%;
        }
    }
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #ffffff !important;
        font-weight: 500;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        color: #ffffff !important;
        font-weight: 500;
    }

    :-ms-input-placeholder { /* IE 10+ */
        color: #ffffff !important;
        font-weight: 500;
    }

    :-moz-placeholder { /* Firefox 18+ */
        color: #ffffff !important;
        font-weight: 500;
    }
</style>

<div class="container-fluid h-100">
    <div class="row">
        <?php if(file_exists('./img/banner.png')){ ?>
            <div class="col-12 col-md-12 pr-0 pl-0 pt-2">
                <img src="img/banner.png" class="img-fluid w-100" />
            </div>
        <?php } ?>
    </div>
    <div class="row h-75">
        <?php if(file_exists('./img/homeslide.png')){ ?>
            <div class="col-12 col-md-7 p-0">
                <div class="text-center">
                    <div class="outer">
                        <div class="middle">
                            <div class="inner">
                                <img src="img/homeslide.png" class="img-fluid homeslide" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-12 col-md-4 p-0 mx-auto">
            <div class="outer right_side">
                <div class="middle card">
                    <div class="inner card-body">
                        <?php if (SHOW_LOGIN) { ?>
                        <div class="col-md-12 p-0">
                            <?php if (isset($_SESSION['message'])) { ?>
                            <div class="alert alert-danger border-danger mb-2" role="alert">
                                <strong><?= $_SESSION['message'] ?></strong>
                            </div>
                            <?php unset($_SESSION['message']); } ?>
                        </div>
                        <form method="post" id="submit_user_login_form" action="process.php" autocomplete="on">
                            <input type="hidden" name="submit_user_login_form" value="true"/>
                            <h5 class="h5 text-dark font-weight-bold">Login here</h5>
                            <div class="form-group">
                                <input type="text" name="f2" class="form-control" id="login_email" placeholder="Email Address*" required />
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2">
                                    <div class="text-right">
                                        <input type="submit" class="btn btn-primary theme_button" value="Login" />
                                    </div>
                                </div>
                                <!--<div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                                    <h5 class="h5 text-dark text-sm-left text-md-center text-lg-right text-xl-right font-weight-bold">Registration has been closed</h5>
                                </div>-->
                            </div>
                        </form>
                        <?php }
                        //if (SHOW_REGISTER) { ?>
                        <div class="d-none">
                            <form method="post" action="process.php" id="submit_user_form" autocomplete="on">
                            <input type="hidden" name="submit_user_form" value="true"/>
                            <!--<h4 class="title">Not registered yet? Register here</h4>-->
                            <?php if($num_rows>0) {
                                while ($row = mysql_fetch_object($get_field_data)) {
                                    if ($row->type == "1") {
                                        $type = "text";
                                        $pattern = $msg = null;
                                        if ($row->field_name == "f2") {
                                            $type = "email";
                                            $pattern = 'pattern="[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$"';
                                        } ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="<?= $type ?>" name="<?= $row->field_name ?>" <?= $pattern ?> id="<?= $row->field_name ?>" class="form-control" placeholder="<?= $row->field_label ?>" <?= ($row->is_required == "yes") ? 'required="required"' : '' ?>>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif ($row->type == "6") { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" name="<?= $row->field_name ?>" id="<?= $row->field_name ?>" class="form-control" placeholder="<?= $row->field_label ?>" <?= ($row->is_required == "yes") ? 'required="required"' : '' ?>/>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif ($row->type == "2") { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" placeholder="<?= $row->field_label ?>" name="<?= $row->field_name ?>" id="<?= $row > field_name ?>" <?= ($row->is_required == "yes") ? 'required="required"' : '' ?>></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif ($row->type == "3") {
                                        $option_array = explode(',', $row->options); ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?= $row->field_label ?></label><br/>
                                                    <?php if (!empty($option_array)) {
                                                        foreach ($option_array as $option) { ?>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="<?= $row->field_name ?>" value="<?= $option ?>" <?= ($row->is_required == "yes") ? 'required="required"' : '' ?>> <?= $option ?>
                                                            </label>
                                                        <?php }
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif ($row->type == "4") {
                                        $option_array = explode(',', $row->options); ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php if (!empty($option_array)) {
                                                        foreach ($option_array as $option) { ?>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="<?= $row->field_name ?>" value="Yes" id="<?= $row->field_name ?>" <?= ($row->is_required == "yes") ? 'required="required"' : '' ?> checked />
                                                                <?= $row->field_label ?>
                                                            </label>
                                                        <?php }
                                                    } ?>
                                                    <!--<label class="mb-1"><?/*= $row->field_label */?></label>-->
                                                </div>
                                            </div>
                                        </div>
                                    <?php } elseif ($row->type == "5") {
                                        $option_array = explode(',', $row->options); ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select name="<?= $row->field_name ?>" id="<?= $row->field_name ?>" <?= ($row->is_required == "yes") ? 'required="required"' : '' ?> class="form-control select2" style="font-size: 16px; display:block; color: #9e9e9e;">
                                                        <option value="" selected> Select <?= $row->field_label ?></option>
                                                        <?php if (!empty($option_array)) {
                                                            foreach ($option_array as $option) { ?>
                                                                <option value="<?= $option ?>"><?= $option ?></option>
                                                            <?php }
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                }
                            } ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <input type="submit" name="submit_btn_add" class="btn btn-primary theme_button" value="Next" />
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <?php //} ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
