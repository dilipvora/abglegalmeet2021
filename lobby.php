<?php include_once "header1.php";
$event_id = EVENT_ID;
$get_users_q = mysql_query("SELECT `uid`, `f1`, `is_login`, (SELECT count(`is_login`) FROM `new_users` WHERE `event_id` = '{$event_id}' AND `uid` != '{$_SESSION['uid']}' AND `is_login` = '1') as `total_live`
                            FROM `new_users` WHERE `event_id` = '{$event_id}' AND (`uid` != '{$_SESSION['uid']}' AND `f1` != '') ORDER BY `is_login` DESC");
$no_of_users = mysql_num_rows($get_users_q); ?>

<style type="text/css">
    .video-container iframe, .video-container video {
        position: absolute;
        top: 14.4%;
        left: 36.8%;
        width: 28%;
        height: 28%;
        border-radius: 5px;
    }

    .theme_button {
        position: absolute;
        top: 0.25rem;
        right: 0.25rem;
        font-size: medium;
        line-height: unset;
    }

    /** ===================== Start Wave ===================== **/
    .red-bg-wave .dot, .red-bg-wave .wave {border-color: #fffdfd !important;}
    .white-bg-wave .dot, .white-bg-wave .wave {border-color: #a40d06 !important;}

    .auditorium1-wave {
        bottom: 43.5%; left: 43%;
    }
    .speaker-wall-wave {
        bottom: 50.5%; left: 3.65%;
    }
    .quiz-wave {
        bottom: 48.25%; left: 32.65%;
    }
    .photo-booth-wave {
        bottom: 48.25%; right: 30.65%;
    }
    .agenda-wave {
        bottom: 56.75%; right: 15.75%;
    }
    .quotes-wave {
        bottom: 56.75%; left: 17.25%;
    }

    .badge-of-honour-wave {
        bottom: 50.5%; right: 2%;
    }
    .chairman-message-wave {
        bottom: 51.75%; left: 43%;
    }

    /** ========== Start Know the team ========== **/
    .know-the-team01-wave {
        bottom: 78%; left: 0.75%;
    }
    .know-the-team02-wave {
        bottom: 71.75%; left: 6.5%;
    }
    .know-the-team03-wave {
        bottom: 79.75%; left: 12.6%;
    }
    .know-the-team04-wave {
        bottom: 63.5%; left: 12.5%;
    }
    .know-the-team05-wave {
        bottom: 70.6%; left: 18.6%;
    }
    .know-the-team06-wave {
        bottom: 61.5%; left: 24.55%;
    }
    .know-the-team07-wave {
        bottom: 59.25%; left: 31%;
    }
    .know-the-team08-wave {
        bottom: 59.25%; right: 34.15%;
    }
    .know-the-team09-wave {
        bottom: 61.5%; right: 28.3%;
    }
    .know-the-team10-wave {
        bottom: 70.25%; right: 22.6%;
    }
    .know-the-team11-wave {
        bottom: 78.75%; right: 16.75%;
    }
    .know-the-team12-wave {
        bottom: 63.15%; right: 16.75%;
    }
    .know-the-team13-wave {
        bottom: 71.75%; right: 11%;
    }
    .know-the-team14-wave {
        bottom: 77.25%; right: 5.25%;
    }
    /** ========== Start Know the team ==========
    ===================== End Wave ===================== **/

    #toggle{
        position: fixed;
        bottom: 0;
        left: 0;
        cursor: pointer;
        padding: 5px;
        background: #a40d06;
        color: snow;
        font-weight: bold;
        letter-spacing: 1.15px;
        font-size: 20px;
        border-radius: 4px;
        z-index: 99;

        max-width: 25rem;
        width: auto;
    }
    #dropdown{
        background: #000;
        color: #fff;
        padding: 10px;
        list-style-type: none;
        border-radius: 4px;
        overflow: auto;
        max-height: 12em;
        height: auto;
    }
    #arrow {
        float: right;
        padding-top: .35rem;
        padding-left: 1rem;
    }
    #dropdown a {
        color: #fff;
        text-decoration: none;
    }
    #dropdown li {
        width: 100%;
        display: flex;
        flex-wrap: wrap;
        align-content: stretch;
        justify-content: space-between;
        align-items: baseline;
        flex-direction: row;
        background: #69686d9f;
        margin: 0.5rem 0;
        padding: 5px;
        border-radius: 5px;
        font-size: 1rem;
    }
    .dropdown-hide {
        display: none;
    }
    .lobby-msg {
        background: #a40d06;
        padding: 2.5px 5px;
        border-radius: 5px;
        position: absolute;
        z-index: 9;
        width: 100%;
        margin-top: 0.4px;
    }

    ::-webkit-scrollbar-thumb {
        background-color: #939194;
        border: 2px solid transparent;
        border-radius: 5px;
        background-clip: padding-box;
    }
    ::-webkit-scrollbar {
        width: .65rem;
    }
</style>
<div id="warning-message">
    <img style="width: 100vw; margin-top: 20%;" src="<?= SITE_URL ?>img/rotatescreen.gif"/>
    <h4 style="text-align:center;">Please rotate your phone to landscape.</h4>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 p-0 m-0">
            <div class="lobby-msg">
                <marquee direction="left" onmouseover="this.stop();" onmouseout="this.start();" loop="true">
                    <h6 class="h6 m-0 font-weight-bold text-light" id="lbldata"></h6>
                </marquee>
            </div>

            <div class="video_lobby_img video-container pt-0 mt-0" style="display: none">
                <img src="<?=SITE_URL?>img/lobby.jpg?v=2.15" alt="virtual-expo" usemap="#image_map" class="img-fluid w-100" usemap="#image_map" />
                <iframe id="iframe" src="javascript:void(0)" width="1280px" height="960px" marginheight="0" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
                <map name="image_map">
                    <area alt="Speaker Wall" title="Speaker Wall" href="<?= SITE_URL?>speaker-wall.php" coords="62,530 289,540 287,602 60,604" shape="polygon" />
                    <area alt="Quotes" title="Quotes" href="<?=SITE_URL?>quotes.php" coords="330,462 325,593 482,595 487,477" shape="polygon" />
                    <area alt="Let's play Brain Teasers" title="Let's play Brain Teasers" href="<?=SITE_URL?>quiz/index.php" coords="623,551 794,556 790,604 624,607" shape="polygon" />
                    <area alt="Photo Booth" title="Photo Booth" href="<?=SITE_URL?>selfie-corner/" coords="1172,552 1351,554 1346,604 1173,604" shape="polygon" />
                    <area alt="Agenda" title="Agenda" href="javascript:void(0)" id="agenda" coords="1474,474 1640,462 1642,599 1478,597" shape="polygon" />
                    <area alt="Entry to Auditorium" title="Entry to Auditorium" href="<?=SITE_URL?>presentation1.php" coords="814,605,1143,725" shape="rect" />
                    <area alt="Badge of Honour" title="Badge of Honour" href="<?=SITE_URL?>nominate" coords="1671,535 1897,528 1898,609 1671,600" shape="polygon" />
                    <area alt="Logout" title="Logout" href="<?=SITE_URL?>logout.php" coords="896,1019,1064,1062" shape="rect" />
                    <area alt="Chairman's Message" title="Chairman's Message" href="<?=SITE_URL?>chairmans-message.php" coords="821,486,1139,599" shape="rect" />

                    <!-- Start Know the team -->
                    <area alt="Know The Team 01" title="Know The Team 01" href="upload/know-the-team/birla-capital.pdf" download coords="10,115,106,248" shape="rect" />
                    <area alt="Know The Team 02" title="Know The Team 02" href="upload/know-the-team/birla-novelis.pdf" download coords="121,186,217,320" shape="rect" />
                    <area alt="Know The Team 03" title="Know The Team 03" href="upload/know-the-team/birla-central-legal-cell.pdf" download coords="238,99,333,233" shape="rect" />
                    <area alt="Know The Team 04" title="Know The Team 04" href="upload/know-the-team/birla-carbon.pdf" download coords="352,198,448,332" shape="rect" />
                    <area alt="Know The Team 05" title="Know The Team 05" href="upload/know-the-team/birla-estates.pdf" download coords="467,296,563,429" shape="rect" />
                    <area alt="Know The Team 06" title="Know The Team 06" href="upload/know-the-team/birla-emil.pdf" download coords="593,322,689,455" shape="rect" />
                    <area alt="Know The Team 07" title="Know The Team 07" href="upload/know-the-team/birla-grasim.pdf" download coords="1262,320,1358,453" shape="rect" />
                    <area alt="Know The Team 08" title="Know The Team 08" href="upload/know-the-team/birla-hindalco.pdf?v=1.1" download coords="1375,294,1471,428" shape="rect" />
                    <area alt="Know The Team 09" title="Know The Team 09" href="upload/know-the-team/birla-fashion-and-retail.pdf" download coords="1483,200,1579,334" shape="rect" />
                    <area alt="Know The Team 10" title="Know The Team 10" href="upload/know-the-team/birla-ultratech.pdf" download coords="1592,111,1688,245" shape="rect" />
                    <area alt="Know The Team 11" title="Know The Team 11" href="upload/know-the-team/birla-century.pdf" download coords="1592,279,1688,412" shape="rect" />
                    <area alt="Know The Team 12" title="Know The Team 12" href="upload/know-the-team/birla-swiss-singapore.pdf" download coords="1703,188,1799,322" shape="rect" />
                    <area alt="Know The Team 13" title="Know The Team 13" href="upload/know-the-team/birla-vil.pdf" download coords="1815,123,1910,257" shape="rect" />
                    <!-- End Know the team -->
                    
                </map>
                <div class="red-bg-wave">
                    <a href="<?=SITE_URL?>presentation.php" class="auditorium1-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="speaker-wall-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="quiz-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="photo-booth-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="agenda-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="quotes-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="chairman-message-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="badge-of-honour-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                </div>
                <div class="white-bg-wave">
                    <!-- Start Know the team -->
                    <a href="javascript:void(0)" class="know-the-team01-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team02-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team03-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <!--<a href="javascript:void(0)" class="know-the-team04-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>-->
                    <a href="javascript:void(0)" class="know-the-team05-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team06-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team07-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team08-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team09-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team10-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team11-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team12-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team13-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <a href="javascript:void(0)" class="know-the-team14-wave wave-a">
                        <div class="dot"></div>
                        <div class="wave"></div>
                    </a>
                    <!-- End Know the team -->
                </div>

                <?php if ($no_of_users > 1) { ?>
                    <div id="toggle" class="m-1">
                        <ul id="dropdown" class="dropdown-hide m-0">
                            <?php $total_live = 0;
                            while ($user = mysql_fetch_object($get_users_q)) {
                                $total_live = $user->total_live; ?>
                                <li>
                                    <a href="javascript:void(0)" data-chatTo="<?= $user->uid ?>" id="openChatModal" class="fix-length"><?= $user->f1 ?></a>
                                    <i class="fa fa-dot-circle <?= ($user->is_login ? "text-success" : "text-danger") ?>"></i>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="mt-2">
                            <i class="fa fa-users"></i> Networking Zone (<?=$total_live?>) <i class="fa fa-angle-up" id="arrow"></i>
                        </div>
                    </div>
                    <?php include_once("open_chat.php");
                }

                include_once("agenda.php"); ?>
            </div>

            <div class="lobby_video" style="position: relative; z-index: 99;">
                <img src="img/welcome.jpg" style="display: none" class="video_intro_img img-fluid w-100" />
                <div class="play_main_video" style="display: none">Play Video</div>
                <video onloadeddata="this.play();" id="main_video" playsinline autoplay poster="<?=SITE_URL?>img/welcome.jpg" style="display: none" class="w-100">
                    <source id="main_video_source" src="https://abglegalmeet2021.s3.ap-south-1.amazonaws.com/main.mp4" type="video/mp4" />
                </video>
                <button class="btn theme_button" onclick="skipVideo();">Skip Video</button>
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php"; ?>
<script type="text/javascript" src="<?=SITE_URL?>js/imageMapResizer.min.js"></script>
<script type="text/javascript">
    function scroll_to_top() {
        var div_scroll_height = $(".chat_msg").prop("scrollHeight");
        var scroll_height = div_scroll_height + 200;

        $('.chat_msg').animate({ scrollTop: scroll_height }, 1200);
    }

    function skipVideo() {
        location.replace('lobby.php#lobby');
        setTimeout(function () {
            location.reload();
        }, 250)
    }

    $(document).ready(function () {
        $('map').imageMapResize();

        $(document).on('click', '.coming-soon', function (evt) {
            evt.preventDefault();
            swal.fire("Coming Soon", "", "success");
        });

        $(document).on("click", "#agenda", function () {
            $("#agendaModal").modal("show");
            //$('.modal-backdrop').addClass('show').removeClass('in');
        });

        /*$("html, body").animate({
            scrollTop: $('html, body').get(0).scrollHeight
        }, 1000);*/

        var slug = window.location.hash;
        var vid = document.getElementById("main_video");
        vid.onloadeddata = function () {
            $(".video_intro_img").hide();
            $("#main_video").show();
            if (slug != "") {
                $(".video_lobby_img").show();
            } else {
                setTimeout(function () {
                    //console.log("play");
                    $(".play_main_video").trigger("click");
                }, 5000);
            }
        };
        vid.onplay = function () {
            $(".loop_safari").hide();
        };
        if (slug != "") {
            $(".video_lobby_img").show();
            $('#main_video').attr('autoplay', false);
            if (slug == "#lobby") {
                $('#lobby_main_video').attr('autoplay', true);
                hide_show_control("lobby");
                //$('#iframe').attr('src','https://player.vimeo.com/video/653221235?h=207579214c&autoplay=1&loop=1&autopause=0');
                $('#iframe').attr('src','https://webcastlive.co.in/player/play_redik-vod.php?stream=abglegalmeet2021&autoplay=true&loop=true&controls=false');
            }
        } else {
            navigator.mediaDevices.getUserMedia({ audio: true });
            $(".video_intro_img").show();
            setTimeout(function () {
                //console.log("play");
                $(".play_main_video").trigger("click");
            }, 2500);
        }
        $(document).on("click", ".play_main_video", function () {
            //console.log("click");
            var main_video = document.getElementById("main_video");
            main_video.play();
        });

        document.getElementById('main_video').addEventListener('ended', mainVideoHanlder, false);

        function mainVideoHanlder(e) {
            hide_show_control("lobby");
            //$('#main_video').attr('muted', true);
            //$('#iframe').attr('src','https://player.vimeo.com/video/653221235?h=207579214c&autoplay=1&loop=1&autopause=0');
            $('#iframe').attr('src','https://webcastlive.co.in/player/play_redik-vod.php?stream=abglegalmeet2021&autoplay=true&loop=true&controls=false');
            window.location.hash = "lobby";
            $('#lobby_main_video').trigger('play');
            e.preventDefault();

            location.replace('lobby.php#lobby');
            setTimeout(function () {
                console.log("2", "reload");
                location.reload();
            }, 500);
        }
        function hide_show_control(action_name = "") {
            $(".video_lobby_img").hide();
            $(".lobby_video").hide();

            if (action_name === "lobby") {
                $(".video_lobby_img").show();
                /*var intro_video = document.getElementById("intro_video");
                intro_video.play();
                $('video').get(0).play();*/
            }
        }
    });

    <?php if ($no_of_users > 1) { ?>
    /**
     * Live chat
     * */
    $(document).on('click', '#toggle', function () {
        $("#dropdown").fadeToggle(" ", "linear");
        $("#arrow").toggleClass("fa-angle-up fa-angle-down", "linear");
    });

    /* ==== Chat Box Ajax Code ==== */
    var ajax_called = false;
    function message_ajax_call(outgoing_msg_id, incoming_msg_id, lastCid) {
        //alert("1 "+incoming_msg_id);
        //console.log("ajax call console");
        if (ajax_called) {
            return false;
        }
        ajax_called = true;
        var dataString = {"action": "get_ajax_message", outgoing_msg_id, incoming_msg_id, lastCid};
        //alert("3 "+incoming_msg_id);
        $.ajax({
            async: true,
            url: 'ajax.php',
            type: "POST",
            data: dataString,
            dataType: "JSON",
            cache: false,
            success: function(response){
                ajax_called = false;
                if (response.data == "d-none") {
                    $(".comments-list").addClass(response.data);
                } else {
                    var last_msg_is_append = $(".comments-list").find('.last_id_'+response.last_id).length;
                    if (response.last_id != 'all'){
                        if ( last_msg_is_append != 1) {
                            $(".comments-list").removeClass('d-none').append(response.data);
                        }
                    } else {
                        $(".comments-list").removeClass('d-none').html(response.data);
                    }
                    scroll_to_top();
                }
                $("#chatBoxModal").find('input[name="lastCid"]').val(lastCid)
            },
            error: function (jqXHR, textStatus, errorThrown) {
               ajax_called = false;
            }
        });
    }

    $(document).on("click", "#openChatModal", function () {
        var outgoing_msg_id = "<?=$_SESSION['uid']?>";
        var incoming_msg_id = $(this).data("chatto");
        var incoming_user = $(this).text();

        $("#chatBoxModal #incoming_user").text(incoming_user);
        $("#chatBoxModal #incoming_msg_id").val(incoming_msg_id);

        //console.log("click to open chat modal console");

        if (incoming_msg_id != $(".comments-list").attr('data_incoming_user_id')){
            $(".comments-list").attr('data_incoming_user_id',incoming_msg_id);
            $(".comments-list").html('');
            message_ajax_call(outgoing_msg_id, incoming_msg_id, 'all');
            scroll_to_top();
        }

        $("#chatBoxModal").modal('show');
        //$('.modal-backdrop').addClass('show').removeClass('in');
        //alert("2 "+incoming_msg_id);
    });

    $(document).on("submit", ".chat_frm", function (event) {
        //console.log("submit console");
        event.preventDefault();
        var self = $(this);
        self.find("#success_msg").html('');
        self.find("#error_msg").html('');
        var error = 0;
        var question = self.find('#msg').val();

        if (question == '') {
            self.find("#error_msg").html('Please enter message.');
            self.find("#msg").focus();
            error = 1;
            return false;
        }

        if (error == 0) {
            self.find("#success_msg").html('Submitting...');
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: $(this).serialize(),
                dataType: "JSON",
                success: function (result) {
                    if (result.status == 'fail') {
                        self.find("#success_msg").html('');
                        self.find("#error_msg").html("Your Message not submitted please try again.");
                    } else {
                        var incoming_msg_id = self.find("#incoming_msg_id").val();
                        var outgoing_msg_id = self.find("#outgoing_msg_id").val();
                        //console.log(outgoing_msg_id, incoming_msg_id);
                        $("#chatBoxModal").find('input[name="lastCid"]').val(lastCid);
                        /*var lastCid_temp = result.cid;
                        if (typeof lastCid_temp === "undefined") {
                            lastCid = "all";
                        } else {
                            lastCid = lastCid_temp;
                        }*/
                        console.log("on submit lastCid", result.cid);
                        message_ajax_call(outgoing_msg_id, incoming_msg_id, result.cid);

                        $(self)[0].reset();
                        self.find("#success_msg").html("");
                        setTimeout(function(){
                            self.find("#success_msg").html('');
                        }, 3000);
                    }
                },
                error: function (result) {
                    self.find("#success_msg").html('');
                    self.find('#error_msg').html('Something goes wrong.');
                }
            });
        }
    });
    <?php } ?>
</script>
</body>
</html>
