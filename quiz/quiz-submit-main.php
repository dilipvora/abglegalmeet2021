<?php include '../config.php';
$event_id = EVENT_ID;
$uid = $_SESSION['uid'];
//$quiz_id = get_values("options", "option_value", "`event_id` = '{$event_id}' AND `option_name` = 'quiz_id'");

$now_date = date('Y-m-d H:i:s');

if((isset($_POST['action']) && $_POST['action'] === "submit_final_quiz") && isset($_POST['quiz_id']) && $_POST['quiz_id'] > 0) {
    $quiz_id = $_POST['quiz_id'];
    $data = json_decode($_POST['data']);
    unset($_POST['question_ids']);
    unset($_POST['answer']);
    foreach ($data as $row){
        $_POST['question_ids'][] = $row->q_id;
        $_POST['answer'][] = $row->ans;
        $_POST['que_type'][] = $row->que_type;
    }

    $user_feedback_data = array();
    $user_feedback_form_error = false;
    $already_submit_quiz = true;

    if (user_is_access_quiz_page($quiz_id)) {
        $already_submit_quiz = false;
    }

    if ($already_submit_quiz) {
        foreach ($_POST['question_ids'] as $key => $question_id) {
            //if (isset($_POST['answer'][$key])) {
            $answer = (!empty($_POST['answer'][$key]) ? trim($_POST['answer'][$key]) : 0);
            if (!empty($_POST['que_type'][$key]) && $_POST['que_type'][$key] == 'que_ans'){
                $true_answer = get_values("quiz_questions", "correct_answer", "event_id = '" . EVENT_ID . "' AND quiz_id='{$quiz_id}' AND id='{$question_id}'");

                $user_ans = $_POST['answer'][$key];
                if (strpos(strtolower($user_ans), strtolower($true_answer)) !== false) {
                    $answer_status = 1;
                } else {
                    $answer_status = 0;
                }
            } else {
                $true_answer = get_values("quiz_questions", "correct_option", "event_id = '" . EVENT_ID . "' AND quiz_id='{$quiz_id}' AND id='{$question_id}'");
                if ($true_answer == $answer) {
                    $answer_status = 1;
                } else {
                    $answer_status = 0;
                }
            }

            $user_feedback_data[] = array(
                'user_id' => $_SESSION['uid'],
                'quiz_id' => $_POST['quiz_id'],
                'question_id' => $question_id,
                'answer' => $answer,
                'answer_status' => $answer_status,
            );
            /*} else {
                $user_feedback_form_error = true;
            }*/
        }

        /*echo "<PRE>";
        print_r($user_feedback_data); die;*/

        if ($user_feedback_form_error == false) {
            $res = array();
            $user_id = $_SESSION['uid'];
            $quiz_id = $_POST['quiz_id'];
            foreach ($user_feedback_data as $key => $user_feedback_row) {
                $question_id = trim($user_feedback_row['question_id']);
                $answer = addslashes(trim($user_feedback_row['answer']));
                $answer_status = $user_feedback_row['answer_status'];
                //$res = insert_details("quiz_result", "event_id = '".EVENT_ID."', uid = '$user_id' ,quiz_id = '$quiz_id', question_id = '{$question_id}', answer = '{$answer}', answer_status='{$answer_status}'");
                $res[] = "'" . EVENT_ID . "', $user_id, $quiz_id, $question_id, '{$answer}', '{$answer_status}'";
            }
            $fields = "`event_id`, `uid`, `quiz_id`, `question_id`, `answer`, `answer_status`";
            insert_batch("quiz_result", $fields, $res);
            header('location:'.SITE_URL.'quiz/quiz.php');
        } else {
            header('location:'.SITE_URL.'quiz/quiz.php');
        }

    } else {
        header('location:'.SITE_URL.'quiz/quiz.php');
    }
}