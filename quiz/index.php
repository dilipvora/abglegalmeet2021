<?php include '../config.php';
if (!isset($_SESSION['uid'])) {
    header("location:" . SITE_URL . "index.php");
    exit();
}

$user_id = $_SESSION['uid'];
$event_id = EVENT_ID;
if (!empty($_GET['qid'])) {
    $quiz_id = $_GET['qid'];
} else {
    $quiz_id = get_values("options", "option_value", "`event_id` = '{$event_id}' AND `option_name` = 'quiz_id'");
}

$get_quiz_timer = get_values("options", "option_value", "`event_id`='{$event_id}' AND `option_name` = 'quiz_time'");
$quiz_timer = gmdate("i:s", ($get_quiz_timer + 1));

$query = mysql_query("SELECT count(id) as total_question FROM `quiz_result` WHERE `event_id` = '{$event_id}' AND `uid` = '{$user_id}' AND `quiz_id` = '{$quiz_id}' ORDER BY id ASC LIMIT 1");
$total_question = 0;
if(mysql_num_rows($query)>0){
    $total_question = mysql_fetch_object($query)->total_question;
} ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!--<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
    <title><?= COMPANY_NAME ?> | Quiz</title>

    <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap" rel="stylesheet">
    <link href="<?=SITE_URL?>fonts/font-awesome/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=SITE_URL?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=SITE_URL?>css/sweetalert2.min.css">
    <link rel="stylesheet" href="<?=SITE_URL?>css/custom.css">

    <script src="<?=SITE_URL?>js/jquery.min.js"></script>
    <script src="<?=SITE_URL?>js/popper.min.js"></script>
    <script src="<?=SITE_URL?>js/bootstrap.min.js"></script>
    <script src="<?=SITE_URL?>js/sweetalert2.all.min.js"></script>

    <style type="text/css">
        html, body {
            height: 100%;
        }
        body {
            background-image: url("<?=SITE_URL?>img/video-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
            background-position: center;
            background-color: #e2bc83;
        }

        .tab {
            display: none;
        }

        .form-group.fail_answer {
            color: red;
        }

        .form-group.success_answer {
            color: green;
        }

        .right_side {
            background: transparent !important;
        }

        .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle,
        .theme_button {
            font-size: 1.25rem;
            line-height: 1.6;
            letter-spacing: 1.5px;
            background: #ea5725;
            color: #ffffff;
            font-weight: bolder;
            border: none;
            border-bottom: 3px solid #680502;
            border-radius: 8px;
        }
        .theme_button:hover, .theme_button:focus, .theme_button:active {
            background: #6e090c;
            color: #ffffff;
            font-weight: bolder;
            border: none;
            border-bottom: 3px solid #680502;
        }
        .form-control {
            text-align: center;
            background: #901412;
            font-size: 1.15rem;
            letter-spacing: 1.25px;
            color: white;
            border: none;
            border-bottom: 3px solid #c5491f;
            border-radius: 1rem;
        }
        .form-control:focus {
            background: #680502;
            color: white;
        }

        .score-box {
            border: 3px solid #c62b27;
            padding: 35px 0;
            color: #101;
            background: #ffffff60;
            border-radius: 5px;
            margin-bottom: 15px;
        }

        .score-bg {
            line-height: 1;
            font-weight: 900;
            background: #c62b27;
            border-radius: 5px;
            color: aliceblue;
            padding: 10px 15px;
            display: inline-block;
        }

        .radio-inline {
            color: #fff;
        }

        /*.radio_btn .radio-inline, .question {
            !* ff 3.6+ *!
            background: -moz-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
            !* safari 5.1+,chrome 10+ *!
            background: -webkit-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
            !* opera 11.10+ *!
            background: -o-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
            !* ie 6-9 *!
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#C5A14F', endColorstr='#DCAA61', GradientType=0);
            !* ie 10+ *!
            background: -ms-linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);
            !* global 94%+ browsers support *!
            background: linear-gradient(90deg, rgba(220, 170, 97, 1) 0%, rgba(116, 71, 50, 1) 25%, rgba(243, 231, 139, 1) 50%, rgba(184, 142, 65, 1) 70%, rgba(197, 161, 79, 1) 100%);

            border-radius: 50px;
            padding: 7px;
            font-size: 1.1rem;
            text-align: left;
            width: 90%;
            max-width: 95%;
            color: #fff;
            font-weight: bold;
            cursor: pointer;
            margin: 0 auto;
        }
        .radio-inline-inner, .question-inner {
            background: #a20e06;
            border-radius: 50px;
            padding: 10px 15px;
            width: 100%;
            margin: 0;
        }*/

        .selected-radio {
            /*border-bottom-width: thick !important;*/
            background: #ea5725;
            margin-bottom: .3rem !important;
        }
        .disabled {
            opacity: .7;
            cursor: not-allowed !important;
        }
        .question_option {
            display: none;
        }
        .radio_btn span, .question-inner span {
            color: #ea8b18;
            font-weight: bold;
        }

        .fail_answer {
            border-color: red !important;
        }
        .success_answer {
            border-color: green !important;
        }
        .form-group {
            margin-bottom: .25rem !important;
        }

        @media screen and (max-width: 990px) {
            body {
                background-image: url("<?=SITE_URL?>img/video-mobile-bg.jpg");
            }
        }

        @media only screen and (orientation: landscape) and (min-device-width: 480px) and (max-device-width: 880px) {
            body {
                background-image: url("<?=SITE_URL?>img/video-bg.jpg");
            }
        }

        .h3, h3 {
            font-size: 1.4rem;
        }

        ::-webkit-scrollbar-thumb {
            background-color: #939194;
            border: 2px solid transparent;
            border-radius: 5px;
            background-clip: padding-box;
        }
        ::-webkit-scrollbar {
            width: .65rem;
        }
        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            color: #ffffff !important;
            font-weight: 500;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            color: #ffffff !important;
            font-weight: 500;
        }
        :-ms-input-placeholder { /* IE 10+ */
            color: #ffffff !important;
            font-weight: 500;
        }
        :-moz-placeholder { /* Firefox 18+ */
            color: #ffffff !important;
            font-weight: 500;
        }
    </style>
</head>
<!--<body onkeydown="return (event.keyCode == 154)">-->
<body>
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12 p-0">
            <img src="<?=SITE_URL?>img/quiz-banner.png" class="img-fluid quiz-banner w-100" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 m-auto">
            <div class="row">
                <div class="col-md-12 p-0">
                    <?php $is_feedback_available = true;
                    $show_script = true;

                    $is_feedback_available = true;
                    if (check_quiz_is_access()) {
                        if (user_is_access_quiz_page($quiz_id)) {
                            $is_feedback_available = false;
                            $show_script = false;
                        }
                    } else {
                        $show_script = false;
                        $is_feedback_available = false; ?>
                        <div class="col-md-12">
                            <div id='feedback_success_msg' class="text-center">
                                <h5>No quiz available at this movement!</h5>
                            </div>
                        </div>
                        <?php die();
                    }

                    if ($is_feedback_available) {
                        $date = date('Y-m-d H:i:s');
                        $quiz_data = get_quiz_data($quiz_id);
                    if (!empty($quiz_data)) { ?>
                        <div class="container-fluid">
                            <div class="countdown text-center font-weight-bold mb-3 fa-2x" style="color:#a20e06;"></div>

                            <form id="QuizForm" action="quiz-submit.php" name="frm_submit_feedback" class="form-horizontal frm_submit_quiz" method="POST">
                                <input type="hidden" name="action" value="submit_final_quiz">
                                <input type="hidden" name="quiz_id" value="<?= $quiz_data->id; ?>">

                                <?php $quiz_question_data = quiz_question_data($quiz_id);
                                if (!empty($quiz_question_data)) {
                                    $counter = 1;
                                    foreach ($quiz_question_data as $key => $quiz_question_row) { ?>
                                        <div class="tab" id="tab_<?= $key ?>" style="background-image: url('img/question-<?= $counter ?>.png');background-size: contain;background-repeat: no-repeat;height: 332px;width: 768px;margin: 0 auto">

                                            <input type="hidden" name="is_objective" class="is_objective" value="<?= $quiz_question_row->is_objective ?>">
                                            <input type="hidden" name="tab_no" class="tab_no" value="<?= $key ?>">
                                            <input type="hidden" name="question_ids[<?= $key ?>]" class="question_id" value="<?= $quiz_question_row->id ?>">
                                            <input type="hidden" name="current_option_value" class="current_option_value" value="<?= $quiz_question_row->correct_option ?>">

                                            <div class="row" style="padding-top: 3px;padding-left: 10px;padding-bottom: 10px;">
                                                <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                    <div class="form-group">
                                                        <div class="question">
                                                            <!--<h3 class="question-inner m-0 pre" style="font-weight: bolder;color: #FFFFFF;line-height: 1.3;min-height: 165px"><?/*="<span>Q$counter:&nbsp;&nbsp;</span>" . $quiz_question_row->title; */?></h3>-->
                                                            <h3 class="h3 question-inner m-0 pre" style="font-weight: bolder;color: #FFFFFF;line-height: 1.3;min-height: 165px"><?=$quiz_question_row->title ?></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 3px;padding-left: 10px;padding-bottom: 10px;">
                                                <?php if ($quiz_question_row->is_objective == 1) { ?>
                                                    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                        <div class="q_options">
                                                            <div class="form-group radio_btn">
                                                                <div class="radio-inline">
                                                                    <label class="radio-inline-inner">
                                                                        <input type="radio" name='answer[<?= $key ?>]' class="question_option option1" value="1" />
                                                                        <span>A.&nbsp;&nbsp;</span> <?= $quiz_question_row->option1 ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                        <div class="q_options">
                                                            <div class="form-group radio_btn">
                                                                <div class="radio-inline">
                                                                    <label class="radio-inline-inner">
                                                                        <input type="radio" name='answer[<?= $key ?>]' class="question_option option2" value="2" />
                                                                        <span>B.&nbsp;&nbsp;</span> <?= $quiz_question_row->option2 ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php if (!empty($quiz_question_row->option3)) { ?>
                                                        <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                            <div class="q_options">
                                                                <div class="form-group radio_btn">
                                                                    <div class="radio-inline">
                                                                        <label class="radio-inline-inner">
                                                                            <input type="radio" name='answer[<?= $key ?>]' class="question_option option3" value="3" />
                                                                            <span>C.&nbsp;&nbsp;</span> <?= $quiz_question_row->option3 ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    if (!empty($quiz_question_row->option4)) { ?>
                                                        <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                            <div class="q_options">
                                                                <div class="form-group radio_btn">
                                                                    <div class="radio-inline">
                                                                        <label class="radio-inline-inner">
                                                                            <input type="radio" name='answer[<?= $key ?>]' class="question_option option4" value="4" />
                                                                            <span>D.&nbsp;&nbsp;</span> <?= $quiz_question_row->option4 ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($quiz_question_row->option5)) { ?>
                                                        <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                            <div class="q_options">
                                                                <div class="form-group radio_btn">
                                                                    <div class="radio-inline">
                                                                        <label class="radio-inline-inner">
                                                                            <input type="radio" name='answer[<?= $key ?>]' class="question_option option5" value="5" />
                                                                            <span>E.&nbsp;&nbsp;</span> <?= $quiz_question_row->option5 ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    if (!empty($quiz_question_row->option6)) { ?>
                                                        <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                            <div class="q_options">
                                                                <div class="form-group radio_btn">
                                                                    <div class="radio-inline">
                                                                        <label class="radio-inline-inner">
                                                                            <input type="radio" name='answer[<?= $key ?>]' class="question_option option6" value="6" />
                                                                            <span>F.&nbsp;&nbsp;</span> <?= $quiz_question_row->option6 ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($quiz_question_row->option7)) { ?>
                                                        <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                            <div class="q_options">
                                                                <div class="form-group radio_btn">
                                                                    <div class="radio-inline">
                                                                        <label class="radio-inline-inner">
                                                                            <input type="radio" name='answer[<?= $key ?>]' class="question_option option7" value="7" />
                                                                            <span>G.&nbsp;&nbsp;</span> <?= $quiz_question_row->option7 ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                } else { ?>
                                                    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                                                        <div class="q_options">
                                                            <textarea name='answer[<?= $key ?>]' rows="1" class="form-control textarea textBox_<?=$key?>" placeholder="Type your answer in one word"></textarea>
                                                            <!--<input type="text" name='answer[<?/*= $key */?>]' class="form-control textBox_<?/*=$key*/?>" placeholder="Please type your answer" />-->
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="text-danger text-center font-weight-bold fa-2x" id="error_<?=$key?>"></div>
                                        <?php $counter++;
                                    } ?>
                                    <div class="col-md-12 text-center" style="margin-top: 10px;">
                                        <div class="form-group">
                                            <button type="button" class="btn theme_button btn-primary quiz_submit_btn" id="nextBtn" onclick="nextPrev(1,0)">NEXT</button>
                                            <input type="submit" class="btn theme_button btn-primary" id="quiz_submit_btn" style="display: none" />
                                        </div>
                                    </div>
                                <?php } ?>

                                <input type="text" id="data_input" name="data" hidden>
                            </form>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-12">
                            <div id='quiz_success_msg' class="text-center text-white">
                                <h2>No quiz available at this movement!</h2>
                                <a href="<?=SITE_URL?>lobby.php#lobby" class="btn theme_button btn-primary">BACK</a>
                            </div>
                        </div>
                    <?php }
                    } else {
                    $total_score_query = mysql_query("SELECT SUM(answer_status) as `answer_status` FROM `quiz_result` WHERE `event_id` = '{$event_id}' AND `uid` = '{$user_id}' AND `quiz_id` = '{$quiz_id}'");
                    $total_score = 0;
                    if(mysql_num_rows($total_score_query)>0){
                        $total_score = mysql_fetch_object($total_score_query)->answer_status;
                    } ?>
                        <div class="col-md-12">
                            <div id='quiz_success_msg' class="text-center text-white">
                                <div class="col-11 col-sm-10 col-md-6 col-lg-4 col-xl-3" style="margin: 0 auto">
                                    <div class="score-box">
                                        <h3 class="h3" style="line-height: 2; font-weight: 900">Your Score</h3>
                                        <h1 class="h1 score-bg"><?=$total_score?></h1>
                                        <h3 class="h3" style="line-height: 2; font-weight: 900">Thank You</h3>
                                        <h4 class="h4" style="line-height: 1.2;">for your participation</h4>
                                    </div>
                                </div>
                                <a href="<?=SITE_URL?>lobby.php#lobby" class="btn theme_button back-btn">BACK</a>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                localStorage.clear();
                            });
                        </script>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($show_script) { ?>
    <script type="text/javascript">
        $(document).keypress('.textarea', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                //console.log(e);
                return false;
            }
        });

        $("html").css({
            "touch-action": "pan-down"
        });

        // Retrieve
        var lastTabTimer = localStorage.getItem("timer<?=$quiz_id?>");
        var QUIZ_TIMER = ((lastTabTimer) ? lastTabTimer : "<?=$quiz_timer?>");

        // Retrieve
        var lastActiveTab = localStorage.getItem("activeTab<?=$quiz_id?>");

        var currentTab = ((lastActiveTab) ? lastActiveTab : <?=$total_question?>);

        var is_next_tab = 0;
        let timerOn = true;
        var interval;
        var isCheckValidation = true;
        showTab(currentTab);
        timer(currentTab);


        var answerArray = [];
        // Parse the serialized data back into an aray of objects
        answerArray = JSON.parse(localStorage.getItem('answer<?=$quiz_id?>')) || [];

        function showTab(n) {
            $(document).on('change', '.question_option', function (evt) {
                evt.preventDefault();
                var active_tab = $("#tab_"+n);
                var this_option = $(this);
                if (this_option.is(":checked")) {
                    active_tab.find('.selected-radio').removeClass('selected-radio');
                    //active_tab.find('.question_option').attr('disabled', 'disabled');

                    active_tab.find(this_option).parent('label').addClass('selected-radio');
                    //this_option.removeAttr('disabled');
                }
            });

            $("#error").hide();
            $("#error").html("");
            $("#success").hide();
            $("#success").html("");
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            //console.log(x[n]);
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            /*if (n == 0) {
                document.getElementById("prevBtn").style.display = "";
            } else {
                document.getElementById("prevBtn").style.display = "";
            }*/
            if (n == (x.length - 1)) {
                /*$("#quiz_submit_btn").show();
                $("#nextBtn").hide();*/

                $("#nextBtn").text('Submit');
                //$("#nextBtn").html("Submit").attr("type", "submit");
                //document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "NEXT";
            }
            //... and run a function that will display the correct step indicator:.
        }

        function nextPrev(n,status) {
            QUIZ_TIMER = "<?=$quiz_timer?>";
            // This function will figure out which tab to display
            var active_tab = $("#tab_"+currentTab);

            if(status == 0){
                //console.log(active_tab.find('input:radio:disabled').length);

                if(!validate(currentTab) && ((active_tab.find('input:radio:disabled').length === 0 && active_tab.find('.is_objective').val() == 1) || (active_tab.find('textarea:disabled').length === 0 && active_tab.find('.is_objective').val() == 0))){
                    $("#error_"+currentTab).show();
                    $("#error_"+currentTab).html("Please select answer");
                }else{
                    $("#error_"+currentTab).hide();
                    $("#error_"+currentTab).html("");
                    var x = document.getElementsByClassName("tab");

                    // Hide the current tab:
                    x[currentTab].style.display = "none";
                    // Increase or decrease the current tab by 1:
                    currentTab = (+currentTab) + (+n);

                    // ***********************************************************************************************************
                    localStorage.setItem("activeTab<?=$quiz_id?>", currentTab);

                    var q_id = active_tab.find('.question_id').val();
                    var selected_answer;
                    var que_type = '';
                    if(active_tab.find('.is_objective').val() == 0) {
                        que_type = "que_ans";
                    }

                    if (active_tab.find('input:radio:checked').val()) {
                        selected_answer = active_tab.find('input:radio:checked').val();
                    } else if (active_tab.find('textarea').val()) {
                        selected_answer = active_tab.find('textarea').val();
                    } else {
                        selected_answer = 0;
                    }
                    //var selected_answer = active_tab.find('input:radio:checked').val() ? active_tab.find('input:radio:checked').val() : 0;

                    var data = {"q_id": q_id,"ans": selected_answer,"que_type":que_type};

                    // Push the new data (whether it be an object or anything else) onto the array
                    //console.log(data);
                    answerArray.push(data);
                    // Re-serialize the array back into a string and store it in localStorage
                    localStorage.setItem('answer<?=$quiz_id?>', JSON.stringify(answerArray));

                    $("#data_input").val(JSON.stringify(answerArray));
                    // ***********************************************************************************************************

                    // if you have reached the end of the form...
                    if (currentTab >= x.length) {
                        $("#prevBtn").hide();
                        $("#nextBtn").hide();
                        // ... the form gets submitted:
                        $("#QuizForm").submit();
                        return false;
                    }
                    // Otherwise, display the correct tab:
                    showTab(currentTab);
                    clearInterval(interval);
                    timer(currentTab);
                }
            }else{
                var x = document.getElementsByClassName("tab");
                // Hide the current tab:
                x[currentTab].style.display = "none";
                // Increase or decrease the current tab by 1:
                currentTab = (+currentTab) + (+n);

                // ***********************************************************************************************************
                localStorage.setItem("activeTab<?=$quiz_id?>", currentTab);

                var q_id = active_tab.find('.question_id').val();
                var selected_answer;
                var que_type = '';
                if(active_tab.find('.is_objective').val() == 0) {
                    que_type = "que_ans";
                }

                if (active_tab.find('input:radio:checked').val()) {
                    selected_answer = active_tab.find('input:radio:checked').val();
                } else if (active_tab.find('textarea').val()) {
                    selected_answer = active_tab.find('textarea').val();
                    que_type = "que_ans";
                } else {
                    selected_answer = 0;
                }
                //var selected_answer = active_tab.find('input:radio:checked').val() ? active_tab.find('input:radio:checked').val() : 0;

                var data = {"q_id": q_id,"ans": selected_answer,"que_type":que_type};

                // Push the new data (whether it be an object or anything else) onto the array
                answerArray.push(data);
                // Re-serialize the array back into a string and store it in localStorage
                localStorage.setItem('answer<?=$quiz_id?>', JSON.stringify(answerArray));

                $("#data_input").val(JSON.stringify(answerArray));
                // ***********************************************************************************************************

                // if you have reached the end of the form...
                if (currentTab >= x.length) {
                    // ... the form gets submitted:
                    $("#QuizForm").submit();
                    return false;
                }
                // Otherwise, display the correct tab:
                showTab(currentTab);
                clearInterval(interval);
                timer(currentTab);
            }
            //console.log(answerArray);
        }

        //console.log(answerArray);

        function validate(current_tab) {
            // This function deals with validation of the form fields
            var valid = true;
            if ($('input[name="answer[' + current_tab + ']"]').is(':checked')) {
                valid = true;
            } else if($('textarea[name="answer['+current_tab+']"]').val()){
                valid = true;
            } else {
                valid = false;
            }
            //console.log(valid);
            return valid; // return the valid status
        }

        function timer(tabno) {
            var timer2 = QUIZ_TIMER;

            if(timer2 == "0:00") {
                clearInterval(interval);
                $("#tab_" + tabno).find(".question_option").attr("disabled", "disabled").addClass("disabled");
                $("#tab_" + tabno).find(".question_option").parent().parent('div').addClass("disabled");
                $("#tab_" + tabno).find(".question_option").parent('label').addClass("disabled");
                $("#tab_" + tabno).find("textarea").attr("disabled", "disabled").addClass("disabled");
                $("#nextBtn").trigger("click");
            } else {
                interval = setInterval(function () {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var minutes = parseInt(timer[0], 10);
                    var seconds = parseInt(timer[1], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    //minutes = (minutes < 10) ?  minutes : minutes;
                    $('.countdown').html(minutes + ':' + seconds);
                    if (minutes < 0) clearInterval(interval);
                    //check if both minutes and seconds are 0
                    if ((seconds <= 0) && (minutes <= 0)) {
                        clearInterval(interval);
                        $("#tab_" + tabno).find(".question_option").attr("disabled", "disabled").addClass("disabled");
                        $("#tab_" + tabno).find(".question_option").parent().parent('div').addClass("disabled");
                        $("#tab_" + tabno).find(".question_option").parent('label').addClass("disabled");
                        $("#tab_" + tabno).find("textarea").attr("disabled", "disabled").addClass("disabled");
                        $("#nextBtn").trigger("click");
                    } else {
                        $('.countdown').html(minutes + ':' + seconds);
                    }
                    timer2 = minutes + ':' + seconds;

                    // Store
                    if ((seconds >= 0) && (minutes >= 0)) {
                        localStorage.setItem("timer<?=$quiz_id?>", timer2);
                    }
                }, 1000);
            }
        }
    </script>
<?php }
include_once "footer.php"; ?>
</body>
</html>