<?php include_once "../header2.php"; ?>
<style type="text/css">
    html, body {
        height: 100%;
    }
    body {
        background-image: url("<?=SITE_URL?>img/video-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        background-position: center;
        background-color: #e2bc83;
    }
    .theme_button {
        font-size: 1.25rem;
        line-height: 1.6;
        letter-spacing: 1.5px;
        background: #ea5725;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #680502;
        border-radius: 8px;

        position: absolute;bottom: 3%;right: 44%;
    }
    .theme_button:hover, .theme_button:focus, .theme_button:active {
        background: #6e090c;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #680502;
    }
    .outer {
        display: table;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }
    .middle {
        display: table-cell;
        vertical-align: middle;
    }
    .inner {
        margin: auto;
        width: auto;
        /*whatever width you want*/
    }

    @media screen and (max-width: 767px) {
        body {
            background-image: url("<?=SITE_URL?>img/video-mobile-bg.jpg");
        }
        .outer {
            display: block;
            position: unset;
        }
        .theme_button {
            top: 42%;
            right: 39%;
            bottom: unset;
        }
    }
    @media only screen and (orientation: landscape) and (min-device-width: 480px) and (max-device-width: 1080px) {
        .theme_button {
            top: unset;
            right: 42%;
            bottom: 0%;
        }
    }
    ::-webkit-scrollbar-thumb {
        background-color: #939194;
        border: 2px solid transparent;
        border-radius: 5px;
        background-clip: padding-box;
    }
    ::-webkit-scrollbar {
        width: .65rem;
    }
</style>
<div id="warning-message">
    <img style="width: 100vw; margin-top: 20%;" src="<?=SITE_URL?>img/rotatescreen.gif" />
    <h4 style="text-align:center;">Please rotate your phone to landscape.</h4>
</div>

<div class="container-fluid h-75">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 p-0 mt-3 mb-3">
            <img src="<?=SITE_URL?>img/nominee-banner.png" class="img-fluid w-100" />
        </div>
    </div>
    <div class="row h-100">
        <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center">
                            <img src="<?=SITE_URL?>img/quiz.png?v=1.4" class="img-fluid" style="width: 85%" usemap="#image_map" />
                            <map name="image_map">
                                <area alt="UNO" title="UNO" href="<?=SITE_URL?>quiz/quiz.php?qid=155" coords="107,23,407,327" shape="rect" />
                                <area alt="DOS" title="DOS" href="<?=SITE_URL?>quiz/quiz.php?qid=156" coords="530,23,830,327" shape="rect" />
                                <area alt="TRES" title="TRES" href="<?=SITE_URL?>quiz/quiz.php?qid=158" coords="953,22,1253,326" shape="rect" />
                                <area alt="CUATRO" title="CUATRO" href="<?=SITE_URL?>quiz/quiz.php" coords="318,468,618,772" shape="rect" />
                                <area alt="CINCO" title="CINCO" href="javascript:void(0)" coords="739,468,1042,772" shape="rect" />
                            </map>
                            <br/>
                            <div class="text-center">
                                <button type="button" onclick="window.location.href='<?=SITE_URL?>presentation.php'" class="theme_button">Home</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=SITE_URL?>js/imageMapResizer.min.js"></script>
<?php include_once "../footer.php"; ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('map').imageMapResize();
    });
</script>
</body>
</html>
