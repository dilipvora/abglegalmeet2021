<?php include '../config.php';
if (!isset($_SESSION['uid']) && $_SESSION['event_id'] != EVENT_ID) {
    session_destroy();
    header("location:" . SITE_URL . "index.php");
} ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta http-equiv="x-dns-prefetch-control" content="on"/>
        <meta property="og:type" content="website"/>
        <meta name="og_site_name" property="og:site_name" content="<?=SITE_URL?>"/>
        <meta property="og:url" content="<?=SITE_URL?>"/>
        <meta name="theme-color" content="#000000">

        <title><?= COMPANY_NAME ?> Live Webcast</title>
        <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap" rel="stylesheet">
        <link href="../fonts/font-awesome/css/all.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/bootstrap.min.css" >
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/custom.css?v=1" />

        <style type="text/css">
            body {
                background-image: url("<?=SITE_URL?>img/speaker-wall-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: 100% 100%;
                background-position: center;
                background-color: #e2bc83;
            }
            .theme_button {
                font-size: unset;
                line-height: unset;
            }
            #my_camera, #result_box {
                max-width: 100%;
                max-height: 100%;
                height: auto !important;
                width: auto !important;
            }

            video, #result {
                background: -webkit-linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
                background: -moz-linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
                background: linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);

                max-width: 100%;
                max-height: 100%;
                height: auto !important;
                width: auto !important;
                border: 5px solid #e2bc83;
                border-radius: 10px;
            }
            #result_box img {
                width: 100%;
                height: auto;
            }

            @media screen and (max-width: 768px){

            }
            @media (min-width: 590px) and (max-width: 767px) {
                .take_snapshot_btn {
                    position: fixed;
                    top: 35%;
                    right: 10%;
                }
            }
        </style>
    </head>

<body>
<div id="warning-message">
    <img style="width: 100vw; margin-top: 20%;" src="../img/rotatescreen.gif" />
    <h4 style="text-align:center;">Please rotate your phone to landscape.</h4>
</div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-12 p-0 mt-3">
                <img src="../img/nominee-banner.png" class="img-fluid w-100" />
            </div>
            <div class="col-3 col-md-4 col-lg-4"></div>
            <div class="col-6 col-md-4 col-lg-4">
                <div class="text-center">
                    <div id="my_camera"></div>
                    <div id="result_box">
                        <div id="result" style="display: none"></div>
                    </div>
                    <div class="mt-2">
					    <button type="button" class="btn btn-primary theme_button" style="margin-bottom: 24px" onclick="window.location.href = '<?=SITE_URL?>lobby.php#lobby'">Back</button>
                        <button type="button" class="btn btn-primary theme_button take_snapshot_btn" id="take_snapshot_btn" style="margin-bottom: 24px"><i class="fa fa-camera fa-2x cam-icon" aria-hidden="true"></i></button>

                        <a href="javascript:void(0)" target="_blank" id="share-whatsapp" style="display: none; font-size: 22px; color: #27a000"><i class="fab fa-whatsapp-square fa-2x" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)" class="btn btn-primary theme_button" style="display: none; margin-bottom: 22px" id="download">Download</a>
                    </div>
                </div>
            </div>
            <div class="col-3 col-md-4 col-lg-4"></div>
        </div>
    </div>

    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script type="text/javascript" src="webcamjs/webcam.min.js"></script>

    <!-- Code to handle taking the snapshot and displaying it locally -->
    <script type="text/javascript">
        $(document).ready(function () {
            window.onorientationchange = function () {
                location.reload();
            };

            /* Configure a few settings and attach camera */
            var isMobile = false;

			var width = 648;
			var height = 486;

			var win_width = window.innerWidth;
            var win_height =  window.innerHeight;

            configure();

            function configure() {

				win_width = window.innerWidth;
                win_height =  window.innerHeight;

				if(win_height > win_width) {
					width = 486;
                    height = 648;
					isMobile = true;
                }

                Webcam.set({
                    width: width,
                    height: height,
                    image_format: 'jpeg',
                    jpeg_quality: 100
                });
                Webcam.attach('#my_camera');
            }

            $(document).on("click", "#configure", function () {
                configure();
            });
            /* A button for taking snaps */

            $(document).on("click", "#take_snapshot_btn", function () {
                var self = $(this);
                $(".cam-icon").hide();
                self.attr("disabled", "disabled").text("Processing...");
                // take snapshot and get image data
                Webcam.snap(function (data_uri) {
                    $.ajax({
                        url: "save.php",
                        method: "POST",
                        dataType: "JSON",
                        data: {"image": data_uri},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#my_camera").hide();
                                $(".take_snapshot_btn").hide();
                                $("#result").show().html('<img src="<?=SITE_URL?>selfie-corner/upload/image/' + response.data + '" alt="profile image" />');

                                var imageURL = "<?= SITE_URL ?>selfie-corner/upload/image/"+response.data;
                                $('#share-whatsapp').attr('href', 'https://wa.me/?text='+encodeURIComponent(imageURL)).show();

                                $('#download').attr('href', '<?=SITE_URL?>selfie-corner/download_image.php?file=' + response.data).show();
                            } else {
                                Swal.fire('',response.message,'error');
                            }
                        }
                    });
                });
            });
        });
    </script>
<?php include_once "../footer.php"; ?>