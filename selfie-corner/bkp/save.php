<?php include_once "../config.php";
$uid = $_SESSION["uid"];
$event_id = EVENT_ID;
$now_time = date("Y-m-d H:i:s");

$image = $_POST["image"];
$my_selfie_frame = $_POST["my_selfie_frame"];
$bgImgPath = "./img/bg1.jpg";

$uniq_name = str_replace(".", "", microtime(true) . uniqid(rand(0000, 9999), rand(0000, 9999)));

if (isset($image) && !empty($image)) {
    $image_parts = explode(";base64,", $image);
    $image_type = explode("image/", $image_parts[0])[1];

    $image_base64 = base64_decode($image_parts[1]);
    $file_path = "./upload/image/";
    if (!is_dir($file_path)) {
        mkdir($file_path, 0777, true);
    }

    $image_file = $uniq_name . ".$image_type";
    $image_file_with_path = $file_path . $image_file;

    if (file_put_contents($image_file_with_path, $image_base64)) {
        show_edited_image($image_file, $my_selfie_frame, $bgImgPath);
        mysql_query("INSERT INTO `users_images` SET `uid` = '{$uid}', `event_id` = '{$event_id}', `img` = '{$image_file}', `created_at` = '{$now_time}'");
        echo json_encode(array("status" => "success", "message" => "image save successfully", "data" => $image_file));
    } else {
        echo json_encode(array("status" => "false", "message" => "plz try again"));
    }
}

function show_edited_image($image_file, $frame, $bgImgPath)
{
    header("Content-type: image/jpg");

    $imgPath = "./upload/image/$image_file";
    $image = imagecreatefromjpeg($imgPath);

    $frame_path = "./img/$frame";

    $frame_image = imagecreatefrompng($frame_path);
	/**** Set GB ***/
	$bg_image = imagecreatefromjpeg($bgImgPath);
	/**** End Set BG ***/
	
	$bg_width = imagesx($bg_image);
	$bg_height = imagesy($bg_image);
	
	$frame_width = imagesx($frame_image);
	$frame_height = imagesy($frame_image);
	
	$width  = imagesx($image);
	$height = imagesy($image);
	
	$pos_x = ($bg_width - $width)/2;
	$pos_y = ($bg_height - $height)/2;
	
	/**** Fill White BG ***/
	//$bg_image = imagecreatetruecolor($bg_width, $bg_height);
	//$white = imagecolorallocate($bg_image, 255, 255, 255); //white color
	//$white = imagecolorallocate($bg_image, 193, 223, 247);
	//imagefill($bg_image, 0, 0, $white);
	/**** End Fill White BG ***/
	
	imagecopyresized($bg_image, $image, $pos_x, $pos_y, 0, 0, $width, $height, $width, $height);
	//////////////
	/**** Fill White BG ***/
	$n_bg_image = imagecreatetruecolor($frame_width, $frame_height);
	$white = imagecolorallocate($n_bg_image, 255, 255, 255); //white color
	//$white = imagecolorallocate($bg_image, 193, 223, 247);
	imagefill($n_bg_image, 0, 0, $white);
	/**** End Fill White BG ***/
	////////////
	//imagecopyresized($n_bg_image, $bg_image, 355, 680, 0, 0, $bg_width, $bg_height, $bg_width, $bg_height);
	//imagecopyresized($n_bg_image, $bg_image, 268, 514, 0, 0, $bg_width, $bg_height, $bg_width, $bg_height);
	imagecopyresized($n_bg_image, $bg_image, 105, 522, 0, 0, $bg_width, $bg_height, $bg_width, $bg_height);
	
	//imagecopyresized($bg_image, $image, $pos_x, $pos_y, 0, 0, $width, $height, $width, $height);
	//imagecopyresized($bg_image, $image, $pos_x, $pos_y, 0, 0, $width, $height, $width, $height);
	
	imagecopyresampled($n_bg_image, $frame_image, 0, 0, 0, 0, $frame_width, $frame_height, $frame_width, $frame_height);

    # save image in dir
    $save_path = "./upload/image/";
    if (!is_dir($save_path)) {
        mkdir($save_path, 0777, true);
    }
    $path_temp = $save_path.$image_file;
    imagejpeg($n_bg_image, $path_temp, 100);
    imagedestroy($n_bg_image);
}

$image_name = $_REQUEST["file"];
if (isset($image_name) && !empty($image_name)) {
    if (preg_match('/^[^.][-a-z0-9_.]+[a-z]$/i', $image_name)) {
        $filepath = "upload/image/" . $image_name;

        // Process download
        if (file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            die();
        } else {
            http_response_code(404);
            die();
        }
    } else {
        die("Invalid file name!");
    }
}