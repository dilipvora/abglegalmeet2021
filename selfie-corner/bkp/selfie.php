<?php include_once "../header1.php";

if(isset($_GET['frame']) && !empty($_GET['frame'])) {
	$my_selfie_frame = $_GET['frame'];
} else {
	header("location: index.php");
}

?>
    <style type="text/css">
        body {
            background: #afded8;
			background-image: url("img/page-bg.jpg");
			background-size: 100% auto;
			background-repeat: no-repeat;
        }

        @media (min-width: 590px) and (max-width: 767px) {
            .take_snapshot_btn {
                position: fixed;
                top: 35%;
                right: 10%;
            }
        }

        #my_camera, #result_box {
            max-width: 100%;
            max-height: 100%;
            height: auto !important;
            width: auto !important;
        }

        video, #result {
            max-width: 100%;
            max-height: 100%;
            height: auto !important;
            width: auto !important;
            border: 5px solid #233f8c;
            border-radius: 10px;
        }

        #my_img {
            max-width: 100%;
            max-height: 100%;
            height: 100% !important;
            width: 100% !important;
            border-radius: 5px;
        }

        .btn-theme {
            background-color: #233f8c;
        }
		
		#result_box img {
			width: 100%;
			height: auto;
		}
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-4"></div>
            <div class="col-12 col-md-4 col-lg-4">

                <div class="text-center mt-2">
                    <img src="<?=SITE_URL?>img/logo.png?v=1" class="img-fluid" style="width: 35%; margin-bottom: 10px"/>
                    <div class="mt-2">
                        <div id="my_camera"></div>
                        <div id="result_box">
                            <div id="result" style="display: none"></div>
                        </div>
                        <div class="mt-2">
							<button type="button" class="btn btn-primary theme_button" style="margin-bottom: 24px" onclick="window.location.replace('index.php')">Back</button>
                            <button type="button" class="btn btn-primary theme_button take_snapshot_btn" id="take_snapshot_btn" style="margin-bottom: 24px"><i class="fa fa-camera fa-2x cam-icon" aria-hidden="true"></i></button>

                            <a href="javascript:void(0)" target="_blank" id="share-whatsapp" style="display: none; font-size: 22px; color: #27a000"><i class="fab fa-whatsapp-square fa-2x" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-primary theme_button" style="display: none; margin-bottom: 22px" id="download">Download</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-4"></div>
        </div>
    </div>

    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script type="text/javascript" src="webcamjs/webcam.min.js"></script>
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>

    <!-- Code to handle taking the snapshot and displaying it locally -->
    <script type="text/javascript">
        $(document).ready(function () {
            /* Configure a few settings and attach camera */
			var width = 480;
			var height = 360;
			
			var win_width = window.innerWidth;
            var win_height =  window.innerHeight;
			
            configure();

            function configure() {
				
				win_width = window.innerWidth;
                win_height =  window.innerHeight;
				
				if(win_height > win_width) {
					width = 360;
                    height = 480;
					isMobile = true;
                }
				
                Webcam.set({
                    width: width,
                    height: height,
                    image_format: 'jpeg',
                    jpeg_quality: 100
                });
                Webcam.attach('#my_camera');
            }

            $(document).on("click", "#configure", function () {
                configure();
            });
            /* A button for taking snaps */

            $(document).on("click", "#take_snapshot_btn", function () {
                var self = $(this);
                $(".cam-icon").hide();
                self.attr("disabled", "disabled").text("Processing...");
                // take snapshot and get image data
                Webcam.snap(function (data_uri) {
                    $.ajax({
                        url: "save.php",
                        method: "POST",
                        dataType: "JSON",
                        data: {"image": data_uri, "my_selfie_frame": "<?=$my_selfie_frame?>"},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#my_camera").hide();
                                $(".take_snapshot_btn").hide();
                                $("#result").show().html('<img src="<?=SITE_URL?>selfie-corner/upload/image/' + response.data + '" alt="profile image" />');

                                var imageURL = "<?= SITE_URL ?>selfie-corner/upload/image/"+response.data;
                                $('#share-whatsapp').attr('href', 'https://wa.me/?text='+encodeURIComponent(imageURL)).show();

                                $('#download').attr('href', '<?=SITE_URL?>selfie-corner/save.php?file=' + response.data).show();
                            } else {
                                Swal.fire('',response.message,'error');
                            }
                        }
                    });
                });
            });

        });
    </script>
<?php include_once "../footer.php"; ?>