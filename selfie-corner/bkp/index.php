<?php include_once "../header1.php"; ?>

<style type="text/css">
    body {
		background-image: url("img/page-bg.jpg");
        background-position: top center;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        background-attachment: fixed;
        background-color: #5cb3aa;
    }

    @media screen and (max-width: 1080px) {
        body {
            background-size: contain;
        }
    }

    .select-frame {
        font-family: system-ui, serif;
        font-weight: bold;
        color: #233f8c;
        margin-bottom: -5px;
    }

    .divider {
        border-color: #233f8c;
    }
</style>


<div class="container-fluid">
    <div class="row">
        <div class="col-1 col-md-1 col-lg-1"></div>
        <div class="col-10 col-md-10 col-lg-10">
            <div class="text-center mt-2">
                <img src="<?=SITE_URL?>img/logo.png?v=1" class="img-fluid" style="width: 15%; margin-bottom: 10px"/>
                <div class="mt-2">
                    <div class="card p-3">
                        <h3 class="h3 select-frame">Select your frame</h3>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                <a href="selfie.php?frame=frame1.png">
                                    <img src="img/frame1.png?v=2" class="img-fluid" />
                                </a>
                            </div>
							<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                <a href="selfie.php?frame=frame2.png">
                                    <img src="img/frame2.png?v=2" class="img-fluid" />
                                </a>
                            </div>
							<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                <a href="selfie.php?frame=frame3.png">
                                    <img src="img/frame3.png?v=2" class="img-fluid" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <button type="button" class="btn btn-primary theme_button" style="margin-bottom: 24px" onclick="window.location.replace('<?=SITE_URL?>')">Back</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-1 col-md-1 col-lg-1"></div>
    </div>
</div>

<?php include_once "../footer.php"; ?>
