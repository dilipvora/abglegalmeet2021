<?php include_once "../config.php";
$image_name = $_REQUEST["file"];
if (isset($image_name) && !empty($image_name)) {
    if (preg_match('/^[^.][-a-z0-9_.]+[a-z]$/i', $image_name)) {
        $filepath = "upload/image/" . $image_name;

        // Process download
        if (file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            die();
        } else {
            http_response_code(404);
            die();
        }
    } else {
        die("Invalid file name!");
    }
}
