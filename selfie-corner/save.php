<?php include_once "../config.php";
$uid = $_SESSION["uid"];
$event_id = EVENT_ID;
$now_time = date("Y-m-d H:i:s");

$image = $_POST["image"];
$my_selfie_frame = 'selfie_frame.png';

$uniq_name = str_replace(".", "", microtime(true) . uniqid(rand(0000, 9999), rand(0000, 9999)));

if (isset($image) && !empty($image)) {
    $image_parts = explode(";base64,", $image);
    $image_type = explode("image/", $image_parts[0])[1];

    $image_base64 = base64_decode($image_parts[1]);
    $file_path = "./upload/image/";
    if (!is_dir($file_path)) {
        mkdir($file_path, 0777, true);
    }

    $image_file = $uniq_name . ".$image_type";
    $image_file_with_path = $file_path . $image_file;

    if (file_put_contents($image_file_with_path, $image_base64)) {
        show_edited_image($image_file, $my_selfie_frame);
        mysql_query("INSERT INTO `users_images` SET `uid` = '{$uid}', `event_id` = '{$event_id}', `img` = '{$image_file}', `created_at` = '{$now_time}'");
        echo json_encode(array("status" => "success", "message" => "image save successfully", "data" => $image_file));
    } else {
        echo json_encode(array("status" => "false", "message" => "plz try again"));
    }
}

function show_edited_image($image_file, $frame)
{
//    header("Content-type: image/jpg");
    $imgPath = "./upload/image/$image_file";
    $image = imagecreatefromjpeg($imgPath);

    $frame_path = "./img/$frame";

    $frame_image = imagecreatefrompng($frame_path);
    $frame_width = imagesx($frame_image);
    $frame_height = imagesy($frame_image);

    $image_temp = imagecreatefrompng($frame_path);

    $width = imagesx($image);
    $height = imagesy($image);


    $pos_x = ($frame_width - $width) / 2;
    $pos_y = ($frame_height - $height) / 2;


    /**** Fill White BG ***/
    $bg_image = imagecreatetruecolor($frame_width, $frame_height);
    //$white = imagecolorallocate($bg_image, 255, 255, 255);
    $white = imagecolorallocate($bg_image, 225, 188, 130);
    imagefill($bg_image, 0, 0, $white);
    /**** End Fill White BG ***/

    imagecopyresized($bg_image, $image, $pos_x, $pos_y, 0, 0, $width, $height, $width, $height);
    //imagecopyresized($bg_image, $image, $pos_x, $pos_y, 0, 0, $width, $height, $bg_width, $bg_height);
    imagecopyresampled($bg_image, $image_temp, 0, 0, 0, 0, $frame_width, $frame_height, $frame_width, $frame_height);


    # save image in dir
    $save_path = "./upload/image/";
    if (!is_dir($save_path)) {
        mkdir($save_path, 0777, true);
    }
    $path_temp = $save_path.$image_file;
    imagejpeg($bg_image, $path_temp, 100);
    imagedestroy($bg_image);
}

/*$image_name = $_REQUEST["file"];
if (isset($image_name) && !empty($image_name)) {
    if (preg_match('/^[^.][-a-z0-9_.]+[a-z]$/i', $image_name)) {
        $filepath = "upload/image/" . $image_name;

        // Process download
        if (file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            die();
        } else {
            http_response_code(404);
            die();
        }
    } else {
        die("Invalid file name!");
    }
}*/