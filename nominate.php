<?php include_once "header.php";
if (isset($_GET['nid']) && !empty($_GET['nid'])) {
    $_GET['nid'] = base64_decode($_GET['nid']);

    if (isset($_SESSION['nominee_uid']) && ($_SESSION['event_id'] === EVENT_ID)) {
        $check_q = mysql_query("SELECT * FROM `extra_form` WHERE `fid` = '{$_GET['nid']}'") or die(mysql_error());
        if (mysql_num_rows($check_q)) {
            $nominationData = mysql_fetch_object($check_q);
        } else {
            header("location:" . SITE_URL);
            exit();
        }
    } else {
        header("location:" . SITE_URL . "nominee.php?nid=".base64_encode($_GET['nid']));
        exit();
    }
} else {
    header("location:" . SITE_URL);
    exit();
} ?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css" />
<style type="text/css">
    html, body {
        height: auto !important;
    }
    body {
        background-image: url("<?=SITE_URL?>img/video-bg.jpg");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        background-position: center;
        background-color: #e2bc83;
    }
    .rounded {
        border-radius: .75rem !important;
    }
    .form-control {
        background: #f7f7f7;
        border: 2px solid #b0b0b0;
        color: #a30e06;
        font-weight: bold;
        line-height: 2rem
    }
    .theme_button {
        font-size: 1.25rem;
        line-height: 1.6;
        letter-spacing: 1.5px;
        background: #ea5725;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #680502;
        border-radius: 8px;
    }
    .theme_button:hover, .theme_button:focus, .theme_button:active {
        background: #6e090c;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #680502;
    }

    /* ============================================ */
    /*.heart {
        font-size: 25px;
        color:black;
    }
    .heart:hover {
        color:black;
    }*/
    /* ============================================ */

    /* =================== Live Comments =================== */
    .comments-container h1 {
        font-size: 36px;
        color: #283035;
        font-weight: 400;
    }
    .comments-container h1 a {
        font-size: 18px;
        font-weight: 700;
    }
    .comments-list {
        margin: 0;
        position: relative;
        min-height: 15vh;
        height: auto;
        max-height: 23vh;
        overflow: auto;
        border-radius: 5px;
        background: transparent;
        padding: 0 !important;
    }
    .chatbox-div {
        border-radius: 10px;
        border: 2px solid #00ffff;
        background: linear-gradient(180deg, rgb(39 104 175) 0%, rgb(7 3 65) 60%, rgb(7 3 28) 100%);
    }
    .comments-list li {
        margin-bottom: 15px;
        display: block;
        position: relative;
    }
    .comments-list .comment-avatar {
        width: 65px;
        height: 65px;
        position: relative;
        z-index: 99;
        float: left;
        border: 3px solid #FFF;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        overflow: hidden;
    }
    .comments-list .comment-avatar img {
        width: 100%;
        height: 100%;
    }
    .reply-list .comment-avatar {
        width: 50px;
        height: 50px;
    }
    .comments-list .comment-box {
        margin-bottom: 10px;
        width: 100%;
        float: right;
        position: relative;
    }
    .reply-list .comment-box {
        width: 610px;
    }
    .comment-box .comment-head {
        background-color: #f7f7f7;
        padding: 0 0 0 12px;
        border-bottom: 2px solid #b0b0b0;
        overflow: hidden;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
    }
    .comment-box .comment-head i {
        float: right;
        margin-left: 14px;
        position: relative;
        top: 2px;
        color: #A6A6A6;
        cursor: pointer;
        -webkit-transition: color 0.3s ease;
        -o-transition: color 0.3s ease;
        transition: color 0.3s ease;
    }
    .comment-box .comment-head i:hover {
        color: #03658c;
    }
    .comment-box .comment-name {
        color: #283035;
        font-size: 14px;
        font-weight: 700;
        float: left;
        margin-right: 10px;
    }
    .comment-box .comment-name a {
        color: #283035;
    }
    .comment-box .comment-head span {
        /*float: left;*/
        color: #680502;
        font-size: 13px;
        position: relative;
        font-weight: bold;
        /*top: 1px;*/
        padding: 0 0 0 5px !important;
    }
    .comment-box .comment-content {
        padding: 12px;
        font-size: 15px;
        color: #fff;
        -webkit-border-radius: 0 0 4px 4px;
        -moz-border-radius: 0 0 4px 4px;
        border-radius: 0 0 4px 4px;
    }
    .comment-box .comment-content-replay {
        font-size: 12px;
        color: #201a16;
        font-weight: bolder;
    }
    .comment-replay {
        float: right !important;
        text-align: right !important;
        text-align: -moz-right !important;
        text-align: -webkit-right !important;
    }
    .comment-replay label {
        color: #fff;
        font-weight: normal;
        margin: 0;
    }
    .comment-box .comment-name.by-author, .comment-box .comment-name.by-author a {
        color: #a30e06;
        font-weight: bolder;
        font-size: 1.1rem;
        margin: 8px 0 !important;
    }

    @media screen and (max-width: 767px) {
        body {
            background-image: url("<?=SITE_URL?>img/video-mobile-bg.jpg");
        }
    }
    @media only screen and (orientation: landscape) and (min-device-width: 480px) and (max-device-width: 880px) {
        body {
            background-image: url("<?=SITE_URL?>img/video-bg.jpg");
        }
    }

    ::-webkit-scrollbar-thumb {
        background-color: #939194;
        border: 2px solid transparent;
        border-radius: 5px;
        background-clip: padding-box;
    }
    ::-webkit-scrollbar {
        width: .65rem;
    }
</style>
<div class="container-fluid d-none">
    <div class="row">
        <div class="col-md-12 p-0">
            <img src="<?=SITE_URL?>img/nominee-banner.png" class="img-fluid quiz-banner w-100" />
        </div>
    </div>
</div>
<div class="container mt-3">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7 mx-auto">
            <div class="text-center">
                <h3 class="h3 font-weight-bold mb-0">Nominated for - <?=ucwords(str_replace("-"," ", $nominationData->role))?></h3>
                <img src="<?= SITE_URL ?>upload/nominate/<?=$nominationData->f1?>" class="img-fluid rounded mx-auto m-2" />
                <div class="text-left mb-2">
                    <h4 class="h4 mb-0"><?=$nominationData->f3?></h4>
                    <span><?=$nominationData->f8?></span>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="text-left">
                            <div class="form-group">
                                <a href="javascript:void(0)" class="heart">
                                    <i class="fa fa-heart-o fa-3x text-dark" id="heart"></i>
                                </a>
                                <br/>
                                <b class="heart-count">0</b><b> Likes</b>
                            </div>
                            <form method="post" class="query_form" id="query_form" action="javascript:void(0)">
                                <input id="q_submit_query" type="hidden" name="q_submit_query" value="true">
                                <input id="q_submit_query" type="hidden" name="nid" value="<?=$_GET['nid']?>">
                                <div class="form-group mb-2 p-2" style="background: #ededed; border-radius: 0.75rem;">
                                    <!--<textarea class="form-control mb-2" placeholder="Add Comments" rows="2"></textarea>-->
                                    <ul id="comments-list" class="comments-list"></ul>
                                    <textarea rows="1" placeholder="Insert Your Comments" class="form-control" name="q_question" id="q_question"></textarea>
                                </div>
                                <div class="form-group text-center font-weight-bold">
                                    <div id="success_msg" class="text-success"></div>
                                    <div id="error_msg" class="text-danger"></div>
                                    <button type="submit" class="btn btn-primary theme_button submit_btn_add m-1">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var interval = 60000;

        $(document).on("click", "#heart", function () {
            $(this).toggleClass("fa-heart fa-heart-o");
            $(this).toggleClass("text-dark text-danger");
        });

        /* ==== Start Comments Ajax Code ==== */
        var ajax_called = false;
        var message_ajax_call = function () {
            if (ajax_called) {
                return false;
            }
            ajax_called = true;
            var dataString = "action=get_ajax_message&nid=<?=$_GET['nid']?>";
            $.ajax({
                async: true,
                url: 'nominee-process.php',
                type: "POST",
                data: dataString,
                cache: false,
                success: function (data) {
                    ajax_called = false;
                    if (data == "d-none") {
                        $(".comments-list").addClass(data);
                    } else {
                        $(".comments-list").removeClass('d-none').html(data);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ajax_called = false;
                }
            });
        };
        message_ajax_call();
        setInterval(message_ajax_call, interval);
        /* ==== End Comments Ajax Code ==== */

        /* ==== Start like Ajax Code ==== */
        var ajax_called_flag = false;
        var emojis_ajax_call = function() {
            if (ajax_called_flag) {
                return false;
            }
            ajax_called_flag = true;
            let sendData = {"action": "getset_emojis", "name": "heart", "nid": "<?=$_GET['nid']?>"};
            $.ajax({
                url: 'nominee-process.php',
                type: "POST",
                dataType: "JSON",
                data: sendData,
                success: function (response) {
                    ajax_called_flag = false;
                    $(".heart-count").text(response.total_count);
                    if (response.is_like === 'yes'){
                        $(".heart i.fa").toggleClass("fa-heart fa-heart-o");
                        $(".heart i.fa").toggleClass("text-dark text-danger");
                        $(".heart i.fa").toggleClass("id","");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ajax_called_flag = false;
                }
            });
        }
        /* ==== End like Ajax Code ==== */

        emojis_ajax_call();
        setInterval(emojis_ajax_call(), interval);

        $(document).on("click", ".heart", function (event) {
            event.preventDefault();
            //let sendData = "action=getset_emojis&role=set_data&name="+like_role;
            let sendData = {"action": "getset_emojis", "role": "set_data", "name": "heart", "nid": "<?=$_GET['nid']?>"};
            $.ajax({
                url: 'nominee-process.php',
                type: 'POST',
                dataType: "JSON",
                data: sendData,
                success: function (response) {
                    $(".heart-count").text(response.total_count);
                }
            });
        });

        /* Submit Message Form */
        $(".query_form").on("submit", function (event) {
            event.preventDefault();
            var self = $(this);
            self.find("#success_msg").html('');
            self.find("#error_msg").html('');
            var error = 0;
            var question = self.find('#q_question').val();

            if (question == '') {
                self.find("#error_msg").html('Please enter comment.');
                self.find("#q_question").focus();
                error = 1;
                return false;
            }

            if (error == 0) {
                self.find("#success_msg").html('Submitting...');
                $.ajax({
                    type: "POST",
                    async: true,
                    url: "nominee-process.php",
                    data: $(this).serialize(),
                    cache: false,
                    success: function (result) {
                        $(".query_form").focus();

                        if (result == 'success') {
                            self.find("#success_msg").html('Your comment has been submitted.');

                            message_ajax_call();
                            $(".comments-list").animate({scrollTop: 0}, 1000);

                            emojis_ajax_call();

                            $(self)[0].reset();
                            setTimeout(function () {
                                self.find("#success_msg").html('');
                            }, 3000);
                        } else {
                            self.find("#success_msg").html('');
                            self.find("#error_msg").html(result);
                        }
                    },
                    error: function (result) {
                        self.find("#success_msg").html('');
                        self.find('#error_msg').html('Something goes wrong.');
                    }
                });
            }
        });
    });
</script>

</body>
</html>
