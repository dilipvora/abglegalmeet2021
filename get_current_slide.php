<?php

date_default_timezone_set("asia/kolkata");
$json_data = file_get_contents("current_slide.json");
if(!empty($json_data)) {
    $json_array = json_decode($json_data);
}else{
    $json_array = array();
}

$current_slide = '';

$current_time = date('Y-m-d H:i:s');
$different_sec = 0;
if(!empty($json_array)) {

    $current_slide = $json_array->slide;
    $different_sec = strtotime($current_time) - strtotime($json_array->time);

}

$announcement_text = '';
if(isset($json_array->announcement_text) && !empty($json_array->announcement_text)){
    $announcement_text = $json_array->announcement_text;
}

$delay = (35 - $different_sec) * 1000;
print json_encode(array("src"=>$current_slide,"delay"=>$delay,"announcement"=>$announcement_text));

?>