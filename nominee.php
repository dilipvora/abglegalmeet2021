<?php include 'header.php';
if (isset($_GET['nid']) && !empty($_GET['nid'])) {
    $_GET['nid'] = base64_decode($_GET['nid']);
    if (isset($_SESSION['nominee_uid']) && ($_SESSION['event_id'] === EVENT_ID)) {
        header("location:" . SITE_URL . "nominate.php?nid=" . $_GET['nid']);
        exit();
    }
} else {
    header("location:" . SITE_URL);
    exit();
}
$event_id = EVENT_ID; ?>

<style type="text/css">
    html, body {
        height: 100%;
    }
    body {
        background-image: url("img/bg.jpg");
        background-position: center;
        background-attachment: fixed;
        background-repeat: no-repeat;
        background-size: cover;
        background-color: #fff;
    }
    .homeslide {
        width: 50%;
    }
    .outer {
        display: table;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }
    .middle {
        display: table-cell;
        vertical-align: middle;
    }
    .inner {
        margin: auto;
        width: auto;
    }

    .right_side, .right_side .card {
        background: transparent;
        border: unset;
    }

    .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show>.btn-primary.dropdown-toggle,
    .theme_button {
        font-size: 1.25rem;
        line-height: 1.6;
        letter-spacing: 1.5px;
        background: #ea5725;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #6e090c;
        border-radius: 8px;
    }

    .theme_button:hover, .theme_button:focus, .theme_button:active {
        background: #6e090c;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #ea5725;
    }

    .form-control {
        color: #fff;
        font-size: 1.25rem;
        line-height: 1.6;
        background: #a32421;
        border: none;
        border-bottom: 3px solid #c5491f;
        border-radius: 8px;
    }
    .form-control:focus {
        background: #c5491f;
        border: none;
        color: white;
        border-bottom: 5px solid #a32421;
    }

    @media screen and (max-width: 767px) {
        .homeslide {
            width: 75%;
        }
        .outer {
            display: unset;
            position: unset;
        }
        .middle {
            display: unset;
            vertical-align: unset;
        }
        .inner {
            width: 100%;
        }
    }
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #ffffff !important;
        font-weight: 500;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        color: #ffffff !important;
        font-weight: 500;
    }

    :-ms-input-placeholder { /* IE 10+ */
        color: #ffffff !important;
        font-weight: 500;
    }

    :-moz-placeholder { /* Firefox 18+ */
        color: #ffffff !important;
        font-weight: 500;
    }
</style>

<div class="container-fluid h-100">
    <div class="row">
        <?php if(file_exists('./img/banner.png')){ ?>
            <div class="col-12 col-md-12 pr-0 pl-0 pt-2">
                <img src="img/banner.png" class="img-fluid w-100" />
            </div>
        <?php } ?>
    </div>
    <div class="row h-75">
        <?php if(file_exists('./img/homeslide.png')){ ?>
            <div class="col-12 col-md-7 p-0">
                <div class="text-center">
                    <div class="outer">
                        <div class="middle">
                            <div class="inner">
                                <img src="img/homeslide.png" class="img-fluid homeslide" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-12 col-md-4 p-0 mx-auto">
            <div class="outer right_side">
                <div class="middle card">
                    <div class="inner card-body">
                        <h5 class="h5 text-dark font-weight-bold">Enter your details here</h5>
                        <form method="post" action="nominee-process.php" id="submit_nominee_form" autocomplete="on">
                            <input type="hidden" name="submit_nominee_form" value="true"/>
                            <input type="hidden" name="nid" value="<?=$_GET['nid']?>"/>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter Your Name" required />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="email" id="email" class="form-control" placeholder="Enter Your Email" required />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="designation" id="designation" class="form-control" placeholder="Enter Your Designation" required />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <input type="submit" name="submit_btn_add" class="btn btn-primary theme_button" value="Submit" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
