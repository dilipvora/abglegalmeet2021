<style type="text/css">
    .comments-list .incoming {
        display: flex;
        align-items: flex-end;
    }
    .comments-list .chat {
        margin: 0;
    }
    .comments-list .incoming .details {
        margin-right: auto;
        margin-left: 0;
        max-width: calc(100% - 10vw);
    }
    .incoming .details label {
        background: #fffdfd;
        color: #333;
        border-radius: 18px 18px 18px 0;
        margin: 0;
    }
    .comments-list .chat label {
        word-wrap: break-word;
        padding: 8px 16px;
        box-shadow: 0 0 32px #69686d9f, 0rem 16px 16px -16px rgb(0 0 0 / 10%);
    }
    .comments-list .outgoing {
        display: flex;
    }
    .comments-list .outgoing .details {
        margin-left: auto;
        max-width: calc(100% - 10vw);
    }
    .outgoing .details label {
        background: #333;
        color: #fffdfd;
        border-radius: 18px 18px 0 18px;
        margin: 0;
    }
    .comments-list .chat label {
        word-wrap: break-word;
        padding: 8px 16px;
        box-shadow: 0 0 32px rgb(0 0 0 / 8%), 0rem 16px 16px -16px rgb(0 0 0 / 10%);
    }
</style>

<div class="modal" id="chatBoxModal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered chant_modal_content" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="incoming_user">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times" aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body p-0 m-1">
                <div class="chat_msg">
                    <ul id="comments-list" class="comments-list"></ul>
                    <input type="hidden" name="lastCid" id="lastCid" value="all" />
                </div>

                <div class="type_msg">
                    <div class="input_msg_write">
                        <form name="chat_frm" class="chat_frm" id="chat_frm" method="post" autocomplete="off" action="javascript:void(0)">
                            <input type="hidden" name="chatForm" value="true" />
                            <input type="hidden" name="incoming_msg_id" id="incoming_msg_id" value="" />
                            <input type="hidden" name="outgoing_msg_id" id="outgoing_msg_id" value="<?=$_SESSION['uid']?>" />

                            <!--<input type="text" name="msg" id="msg" class="write_msg chatbox" placeholder="Type a message" required />-->
                            <textarea name="msg" id="msg" class="write_msg chatbox" rows="1" placeholder="Type a message" required></textarea>
                            <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>

                            <div class="text-center">
                                <div id="success_msg" class="text-success font-weight-bold"></div>
                                <div id="error_msg" class="text-danger font-weight-bold"></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-danger btn-rounded" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>