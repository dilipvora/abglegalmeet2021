<?php include_once "header1.php"; ?>
<style type="text/css">
    .chairmans-btn {
        position: absolute;
        top: 88%;
        right: 1%;
    }
</style>
<div id="warning-message">
    <img style="width: 100vw; margin-top: 20%;" src="<?= SITE_URL ?>img/rotatescreen.gif"/>
    <h4 style="text-align:center;">Please rotate your phone to landscape.</h4>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 p-0">
            <div class="text-center" style="position: relative">
                <img src="<?= SITE_URL ?>img/chairmans-message.jpg?V=1" class="img-fluid w-100" />
                <div class="chairmans-btn">
                    <input type="button" class="btn btn-primary theme_button" value="Back" onclick="window.location.href = '<?=SITE_URL?>lobby.php#lobby'">
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php"; ?>
</body>
</html>

