<?php include 'config.php';
if(!isset($_SESSION['uid'])){
    header("location:".SITE_URL."index.php");
}
if($_SESSION['event_id'] != EVENT_ID){
    header("location:".SITE_URL."index.php");
    session_destroy();
}

$video_iframe='';
$current_slide = '';
$results = mysql_query("SELECT * FROM webinar WHERE `event_id` = '".EVENT_ID."'");
if(mysql_num_rows($results)>0){
    while ($row = mysql_fetch_object($results)){
        $webinar_id = $row->id;
        $video_iframe = $row->video_url;
        $current_slide = $row->current_slide;
    }
}

$rs = mysql_query("SELECT * FROM `announcement` WHERE `event_id` = '".EVENT_ID."'");
$num_row = mysql_num_rows($rs);
$announce_text = '';
if($num_row){
    $announce = mysql_fetch_assoc($rs);
    $announce_text = $announce['announcement_text'];
}

$team_rs = mysql_query("SELECT * from team WHERE `event_id` = '".EVENT_ID."' ORDER BY `team_name` ASC");

$json_data = file_get_contents("firebase_ajax_setting.json");
if(!empty($json_data)) {
    $json_array = json_decode($json_data);
}else{
    $json_array = array();
}
$firebase_ajax = 1;
if(!empty($json_array)){
    $firebase_ajax = $json_array->firebase_ajax;
}

/*
$one_plus_active_inactive_check_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'one_plus_active_inactive' and `event_id` = '".EVENT_ID."'");
$one_plus_active_inactive_option_data = mysql_fetch_object($one_plus_active_inactive_check_option);
if(mysql_num_rows($one_plus_active_inactive_check_option)>0){
    $one_push_active = $one_plus_active_inactive_option_data->option_value;
}else{
    $one_push_active = 1;
}
*/

$video_ppt_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'video_ppt' and `event_id` = '".EVENT_ID."'");
$video_ppt_data = mysql_fetch_object($video_ppt_option);
if(mysql_num_rows($video_ppt_option)>0){
    $video_or_ppt = $video_ppt_data->option_value;
}else{
    $video_or_ppt = 2;
}
$cursor_image_option = mysql_query("SELECT `option_value` from `options` where `option_name` = 'cursor_image' and `event_id` = '".EVENT_ID."'");
$cursor_image_data = mysql_fetch_object($cursor_image_option);
if(mysql_num_rows($cursor_image_option)>0){
    $show_hide_cursor = $cursor_image_data->option_value;
}else{
    $show_hide_cursor = 2;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta http-equiv="x-dns-prefetch-control" content="on"/>
    <meta property="og:type" content="website"/>
    <meta name="og_site_name" property="og:site_name" content="<?=SITE_URL?>"/>
    <meta property="og:url" content="<?=SITE_URL?>"/>
    <meta name="theme-color" content="#000000">

    <title><?= COMPANY_NAME ?> Live Webcast</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital@1&display=swap" rel="stylesheet">
    <link href="fonts/font-awesome/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="css/custom.css?v=1">

    <!-- firebase -->
    <script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.7/firebase-database.js"></script>
    <script type="text/javascript" src="assets/js/firebase/firebase2.2.1.js"></script>

    <script src="assets/chart/highcharts.js"></script>
    <script src="assets/chart/highcharts-more.js"></script>
    <script src="assets/chart/modules/exporting.js"></script>

    <style type="text/css">
        .presentation{
            background-color: black;
        }
        .video-container {
            margin-top: unset;
            padding-top: unset;
            /*padding-bottom: 49.7%;*/
        }
        /*.video-container iframe{
            width: 85%;
            height: 100%;
            top: 0;
            left: 8%;
            !* border: 5px solid black;
             border-radius: 2px;*!
        }*/
        .video-container img{
            width: 85%;
        }
        /*#que-btn{
            position: absolute;
            bottom: 27%;
            left: 45%;
        }*/
        /*.backbtn {
            margin: unset;
            position: absolute;
            top: 8px;
            left: 8px;
            font-size: small;
            padding: 0 10px;
            height: auto;
            z-index: 9999;
        }*/

        .backicon {
            position: fixed;
            z-index: 999;
            top: .5%;
            left: .4%;
        }

        #toggle .messaging-box, #toggle .messaging-btn {
            position: fixed;
            bottom: 4.5%;
            right: 0;
            cursor: pointer;
            padding: 5px;
            background: #a40d06;
            color: snow;
            font-weight: bold;
            letter-spacing: 1.15px;
            font-size: 20px;
            border-radius: 5px;
            z-index: 99;
            max-width: 20rem;
            width: 100%;
        }
        .msg_send_btn {
            background: #a40d06;
            border: medium none;
            border-radius: 50%;
            color: #f7f5f5e0;
            cursor: pointer;
            font-size: 1rem;
            height: 1.75rem;
            width: 1.75rem;
            position: absolute;
            right: 0.35rem;
            bottom: 0.35rem;
            top: unset;
        }
        .emojis-img {
            width: 2.5rem;
        }
        .list-inline {
            position: fixed;
            top: 0;
            width: auto;
            right: 0
        }
        .list-inline-item {
            align-items: center;
            justify-content: space-between;
            align-content: space-around;
            flex-wrap: nowrap;
            flex-direction: column;
            display: inline-flex
        }

        @media screen and (max-width: 767px) {
            .list-inline {
                top: 30%;
            }
        }
        @media only screen and (orientation: landscape) and (min-device-width: 480px) and (max-device-width: 1080px) {
            .list-inline {
                top: 0;
            }
        }

        ::-webkit-scrollbar-thumb {
            background-color: #939194;
            border: 2px solid transparent;
            border-radius: 5px;
            background-clip: padding-box;
        }
        ::-webkit-scrollbar {
            width: .65rem;
        }
    </style>

</head>
<div id="warning-message">
    <img style="width: 100vw; margin-top: 20%;" src="<?= SITE_URL ?>img/rotatescreen.gif"/>
    <h4 style="text-align:center;">Please rotate your phone to landscape.</h4>
</div>

<body class="presentation">
<a href="<?= SITE_URL?>lobby.php#lobby" id="mybackid"><img src="img/back.png" width="40" class="backicon" /></a>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-12 col-md-12 col-lg-12 col-xl-12 p-0" id="video_section">
            <?php if (file_exists("img/thank-you.jpg")) { ?>
                <div class="video-container text-center">
                    <img src="img/thank-you.jpg?v=1" class="img-fluid"/>
                </div>
            <?php } else { ?>
                <div class="video-container text-center">
                    <!--<iframe src="https://webcastlive.co.in/player/play_redikpf.php?event_id=<?/*= FOLDER_NAME */?>" width="1280px" height="960px" marginheight="0" frameborder="0" scrolling="no"  allowfullscreen="allowfullscreen"></iframe>-->
                    <iframe src="https://webcastlive.co.in/cloud-player/player/player.php?event_id=2752" marginheight="0" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
                    <!--<iframe src="https://player.vimeo.com/video/653221235?h=207579214c&autoplay=1&loop=1&autopause=0" width="1280px" height="960px" marginheight="0" frameborder="0" scrolling="no"  allowfullscreen="allowfullscreen"></iframe>-->
                </div>
            <?php } ?>

            <div id="toggle" class="m-1">
                <a href="javascript:void(0)" class="messaging-btn w-auto" style="background: none !important;bottom: 4.75%; right: 0; left: unset;">
                    <img src="img/Q&A.png" class="img-fluid" width="65" />
                </a>
                <div class="messaging-box" style="display: none">
                    <div class="position-relative" style="z-index: 9999">
                        <h5 class="h5 float-left">Chat Box (Q&A) </h5>
                        <a href="javascript:void(0)" class="h5 float-right close-qna"><i class="fa fa-times"></i></a>
                    </div>
                    <div style="background: #a40d06; border-radius: 5px;">
                        <form method="post" class="query_form" id="query_form" action="javascript:void(0)" autocomplete="off">
                            <input id="q_submit_query" type="hidden" name="q_submit_query" value="true">
                            <div class="form-group mb-0 position-relative">
                                <textarea rows="3" placeholder="Type Your Question Here" class="write_msg chatbox form-control" name="q_question" id="q_question"></textarea>
                                <button type="submit" class="msg_send_btn submit_btn_add">
                                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div id="success_msg" class="text-light text-center"></div>
                            <div id="error_msg" class="text-light text-center"></div>
                        </form>
                    </div>
                </div>
            </div>

            <ul class="list-inline text-center m-1 p-0">
                <li class="list-inline-item text-center">
                    <a href="javascript:void(0)" class="emojis" data-name="clap">
                        <img src="img/emojis/clap.png" class="img-fluid rounded emojis-img" />
                    </a>
                    <span class="text-light h5" id="clap">0</span>
                </li>
                <li class="list-inline-item text-center">
                    <a href="javascript:void(0)" class="emojis" data-name="cool">
                        <img src="img/emojis/cool.png" class="img-fluid rounded emojis-img" />
                    </a>
                    <span class="text-light h5" id="cool">0</span>
                </li>
                <li class="list-inline-item text-center">
                    <a href="javascript:void(0)" class="emojis" data-name="heart">
                        <img src="img/emojis/heart.png" class="img-fluid rounded emojis-img" />
                    </a>
                    <span class="text-light h5" id="heart">0</span>
                </li>
                <li class="list-inline-item text-center">
                    <a href="javascript:void(0)" class="emojis" data-name="hooting">
                        <img src="img/emojis/hooting.png" class="img-fluid rounded emojis-img" />
                    </a>
                    <span class="text-light h5" id="hooting">0</span>
                </li>
                <li class="list-inline-item text-center">
                    <a href="javascript:void(0)" class="emojis" data-name="like">
                        <img src="img/emojis/like.png" class="img-fluid rounded emojis-img" />
                    </a>
                    <span class="text-light h5" id="like">0</span>
                </li>
            </ul>

        </div>
    </div>
</div>
<style>
    #feedback_success_msg {
        color: green;
        text-align: center;
    }

    #feedback_error_msg {
        color: red;
        text-align: center;
    }
</style>

<div class="modal" id="FeedbackModel" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Feedback</h5>
                <button type="button" class="close" onclick="closeFeedbackModal()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="feedback_success_msg"></div>
                <div id="feedback_error_msg"></div>
                <div id="FeedbackContent">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="FeedbackResultModel" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Feedback Result</h5>
                <button type="button" class="close" onclick="closeFeedbackResultModal()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="FeedbackResultContent">
                </div>
            </div>
        </div>
    </div>
</div>

<script>var event_id = '<?php echo EVENT_ID;?>'</script>
<script>var firebase_url = '<?php echo FIREBASE_URL;?>'</script>
<script>var firebase_url2= '<?php echo FIREBASE_URL2;?>'</script>
<script>var api_key = '<?php echo API_KEY; ?>'</script>
<script>var firbase_ajax_setting = <?=$firebase_ajax?></script>
<script type="text/javascript" src="assets/js/firebase/app.js?v=2"></script>
<script type="text/javascript" src="assets/js/firebase/pointer.js"></script>
<script type="text/javascript" src="js/imageMapResizer.min.js"></script>
<script type='text/javascript'>
    function openFeedbackResultModal() {
        $("#FeedbackResultContent").html("<div class='center'>Loading...</div>");
        $("#FeedbackResultContent").load("live_feedback_result.php");
        $("#FeedbackResultModel").modal('show');
    }

    function closeFeedbackResultModal() {
        $("#FeedbackResultContent").html("");
        $("#FeedbackResultModel").modal('hide');
    }

    function openFeedbackModal() {
        $("#feedback_success_msg").html("");
        $("#feedback_error_msg").html("");
        $("#FeedbackContent").html("<div class='center'>Loading...</div>");
        $("#FeedbackContent").load("feedback_modal.php");
        $("#FeedbackModel").modal('show');
    }

    function closeFeedbackModal() {
        $("#FeedbackContent").html("");
        $("#FeedbackModel").modal('hide');
    }

    $( document ).ready(function() {
        $('map').imageMapResize();

        $(document).on("click", ".messaging-btn", function (evt) {
            evt.preventDefault();
            $(".messaging-box").show();
        });
        $(document).on("click", ".close-qna", function (evt) {
            evt.preventDefault();
            $(".messaging-box").hide();
        });

        $(document).on('click', '.action', function (evt) {
            evt.preventDefault();
            var prefix = $(this).attr('title');
            swal.fire(prefix+' is closed','','success');
        });

        setInterval(function(){ update_logout_time(); }, <?=LOGOUT_TIME_INTERVAL?>);
        update_logout_time();

        $( "#ppt-swap-icon" ).click(function() {
            if($("#video_section").hasClass("col-md-4")){
                $("#ppt-swap-icon").removeClass("swap-icon-img");
                $("#ppt-swap-icon").addClass("swap-to-ppt");

                $("#video_section").removeClass("col-md-4").addClass("col-md-8 order-md-2");
                $("#video_section .question_container").hide();
            }else{
                $("#ppt-swap-icon").addClass("swap-icon-img");
                $("#ppt-swap-icon").removeClass("swap-to-ppt");

                $("#video_section").removeClass("col-md-8 order-md-2").addClass("col-md-4");
                $("#video_section .question_container").show();
            }
            if($("#ppt_section").hasClass("col-md-8")){
                $("#ppt_section").removeClass("col-md-8").addClass("col-md-4 order-md-1");
                $("#ppt_section .question_container").show();
            }else{
                $("#ppt_section").removeClass("col-md-4 order-md-1").addClass("col-md-8");
                $("#ppt_section .question_container").hide();
            }
        });

        if(firbase_ajax_setting == 2) {
            setInterval(function () {
                get_current_slide();
            }, <?=AJAX_CURRENT_SLIDE_INTERVAL?>);
        }

        setInterval(function(){ get_firebase_ajax_setting(); }, <?=FIREBASE_AJAX_SETTING_INTERVAL?>);

        /* Submit Message Form */
        $( ".query_form" ).on( "submit", function( event ) {
            event.preventDefault();
            var self = $(this);
            self.find("#success_msg").html('');
            self.find("#error_msg").html('');
            var error = 0;
            var question = self.find('#q_question').val();

            if(question ==''){
                self.find("#error_msg").html('Please enter question.');
                self.find("#q_question").focus();
                error = 1;
                return false;
            }

            if(error == 0){
                self.find("#success_msg").html('Submitting...');
                $.ajax({
                    type: "POST",
                    async: true,
                    url: "ajax.php",
                    data: $(this).serialize(),
                    cache: false,
                    success: function(result){
                        if(result == 'success'){
                            self.find("#success_msg").html('Successfully Submitted');
                            $(self)[0].reset();
                            setTimeout(function(){
                                self.find("#success_msg").html('');
                            }, 3000);
                        }else{
                            self.find("#success_msg").html('');
                            self.find("#error_msg").html(result);
                        }
                    },
                    error: function( result ) {
                        self.find("#success_msg").html('');
                        self.find('#error_msg').html('Something goes wrong.');
                    }
                });
            }
        });

        function update_logout_time(){
            var send_data = { 'uid': <?=$_SESSION['uid']?>, 'login_session_id': <?=$_SESSION['login_session_id']?> };
            $.ajax({
                type: "POST",
                async: true,
                data:send_data,
                url: "update_logout_time.php",
                cache: false,
                dataType:"json",
                success: function(result){
                    if(result.status == 0){
                        window.location = "index.php";
                    }
                },
                error: function( result ) {

                }
            });
        }

        function get_current_slide(){
            $.ajax({
                type: "POST",
                async: true,
                url: "get_current_slide.php",
                cache: false,
                dataType:"json",
                success: function(result){
                    $("#lbldata").html(result.announcement);
                    var delay_time_out = 0;
                    if(result.delay>0){
                        delay_time_out = result.delay;
                    }
                    setTimeout(function(){

                        var exist_src = $("#video_slide_img").attr("src");
                        if(exist_src != result.src) {
                            console.log("change");
                            $("#video_slide_img").attr("src", result.src);
                        }

                    }, delay_time_out);

                },
                error: function( result ) {

                }
            });
        }

        function get_firebase_ajax_setting(){
            $.ajax({
                type: "POST",
                async: true,
                url: "get_firebase_ajax_setting.php",
                success: function(result){
                    if(firbase_ajax_setting != result){
                        firbase_ajax_setting = result;
                        window.location = "presentation.php";
                    }
                },
                error: function( result ) {

                }
            });
        }


        /**
         * Emojis
         * @type {boolean}
         */
        var ajax_called_flag = false;
        var emojis_ajax_call = function() {
            if (ajax_called_flag) {
                return false;
            }
            ajax_called_flag = true;
            let sendData = {"action": "audi_emojis", "dataAction": "auditorium"};
            $.ajax({
                url: 'emojis-ajax.php',
                type: "POST",
                dataType: "JSON",
                data: sendData,
                success: function(response){
                    ajax_called_flag = false;
                    $("#like").text(response.like);
                    $("#heart").text(response.heart);
                    $("#clap").text(response.clap);
                    $("#cool").text(response.cool);
                    $("#hooting").text(response.hooting);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ajax_called_flag = false;
                }
            });
        }

        $(document).on("click", ".emojis", function (event) {
            event.preventDefault();
            var like_role = $(this).data("name");
            let sendData = {"action": "audi_emojis", "role": "set_data", "name": like_role, "dataAction": "auditorium"};
            $.ajax({
                url: 'emojis-ajax.php',
                type: 'POST',
                dataType: "JSON",
                data: sendData,
                success: function (response) {
                    $("#like").text(response.like);
                    $("#heart").text(response.heart);
                    $("#clap").text(response.clap);
                    $("#cool").text(response.cool);
                    $("#hooting").text(response.hooting);
                }
            });
        });
        emojis_ajax_call();
        setInterval(emojis_ajax_call(), 30000);

    });
</script>
<?php include 'google_analytics.php'; ?>
</body>
</html>