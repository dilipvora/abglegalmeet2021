<?php include_once "header1.php"; ?>
<style type="text/css">
    html, body {
        height: 100%;
    }

    body {
        background-image: url("<?=SITE_URL?>img/speaker-wall-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        background-position: center;
        background-color: #e2bc83;
    }

    .outer {
        display: table;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }

    .middle {
        display: table-cell;
        vertical-align: middle;
    }

    .inner {
        margin: auto;
        width: auto;
        /*whatever width you want*/
    }
    .modal-lg{
        width: 100%;
        max-width: 1080px !important;
    }
    .modal-content {
        background: linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
    }
    .close-modal {
        position: absolute;
        z-index: 99;
        right: 1%;
        top: 1%;
        color: crimson;
        font-size: 2rem;
        background: white !important;
        padding: 0 7px !important;
        border-radius: 5px;
    }
    .quotes-btn {
        position: absolute;
        top: 88%;
        right: 1%;
    }
</style>
<div id="warning-message">
    <img style="width: 100vw; margin-top: 20%;" src="<?= SITE_URL ?>img/rotatescreen.gif"/>
    <h4 style="text-align:center;">Please rotate your phone to landscape.</h4>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
            <img src="<?= SITE_URL ?>img/nominee-banner.png" class="img-fluid w-100" />
        </div>
    </div>
</div>

<div class="container-fluid h-75">
    <div class="row h-100">
        <div class="col-12 col-sm-11 col-md-11 col-lg-11 col-xl-11 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center">
                            <img src="<?= SITE_URL ?>img/speaker-wall.png" class="img-fluid w-75" usemap="#image_map" />
                            <div class="quotes-btn">
                                <input type="button" class="btn btn-primary theme_button" value="Back" onclick="window.location.href = '<?=SITE_URL?>lobby.php#lobby'">
                            </div>
                            <map name="image_map">
                                <area alt="Speaker 01" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp1" coords="9,46,231,338" shape="rect" />
                                <area alt="Speaker 02" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp2" coords="273,46,495,338" shape="rect" />
                                <area alt="Speaker 03" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp3" coords="534,47,756,339" shape="rect" />
                                <area alt="Speaker 04" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp4" coords="790,41,1013,333" shape="rect" />
                                <area alt="Speaker 05" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp5" coords="1051,44,1274,336" shape="rect" />
                                <area alt="Speaker 06" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp6" coords="114,384,336,676" shape="rect" />
                                <area alt="Speaker 07" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp7" coords="379,384,601,676" shape="rect" />
                                <area alt="Speaker 08" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp8" coords="648,383,870,675" shape="rect" />
                                <area alt="Speaker 09" title="Click to view Profile" href="javascript:void(0)" class="speaker-btn" data-img="sp9" coords="911,381,1133,673" shape="rect" />
                            </map>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" aria-hidden="true" tabindex="-1" id="speakerModal" role="dialog" aria-labelledby="speakerModalLabel" data-keyboard="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                   <!-- <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>-->
                    <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                    <div class="row">
                        <div class="col-md-12">
                            <img src="#!" class="img-fluid w-100" id="speaker-img" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= SITE_URL ?>js/imageMapResizer.min.js"></script>
<?php include_once "footer.php"; ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('map').imageMapResize();

        $(document).on("click", ".speaker-btn", function () {
            var speakerImage = $(this).data("img");
            $("#speakerModal").modal("show");
            $("#speaker-img").attr("src", "img/speakers/"+speakerImage+".jpg?v=1");
        });
    });
</script>
</body>
</html>
