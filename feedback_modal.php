<?php
    include 'config.php';
?>

<script type='text/javascript'>
    $( document ).ready(function() {
        $("#FeedbackForm").on( "submit", function( event ) {
            $.ajax({
                type: "POST",
                async: true,
                url: "ajax.php",
                data: $(this).serialize(),
                cache: false,
                success: function(result){
                    if(result == 'success'){
                        $("#FeedbackContent").html("");
                        $("#feedback_success_msg").html("Thanks for Feedback!");
                        closeFeedbackModal_Success();
                    }else{
                        $("#feedback_error_msg").html(result);
                        $("#FeedbackModel").scrollTop();
                    }
                },
                error: function( result ) {
                    $('#error_msg').html('Something goes wrong.');
                    $("#FeedbackModel").scrollTop();
                }
            });
            return false;
        });
    });

    function closeFeedbackModal_Success() {
        setTimeout(function() {closeFeedbackModal(); }, 3000);
    }
</script>
<div class="container">
    <div class="row"><?php
        $is_feedback_available = true;
        if(check_feedback_is_access()) {
            if (user_is_access_feedback_page()) {
                $is_feedback_available = false;
            }
        } else {
            $is_feedback_available = false;
            ?>
            <div class="col-md-12">
                <div id='feedback_success_msg' class="text-center">
                    <h6>No feedback available at this movement!</h6>
                </div>
            </div>
            <script type='text/javascript'>
                closeFeedbackModal_Success();
            </script>
            <?php
            die();
        }

        if($is_feedback_available) {
            $feedback_id = get_values("options","option_value","event_id = '".EVENT_ID."' AND option_name='feedback_id'");
            $feedback_data = get_feedback_data($feedback_id);
            if(!empty($feedback_data)) {
                ?>
                <div class="col-md-12">
                    <h5 style="margin-left: 10px"><?php echo $feedback_data->title; ?></h5>
                </div>
                <form id="FeedbackForm" name="frm_submit_feedback" class="form-horizontal col s12" method="POST">
                    <input type="hidden" name="feedback_id" value="<?php echo $feedback_data->id; ?>"><?php
                    $feedback_question_data = feedback_question_data($feedback_id);
                    if(!empty($feedback_question_data)) {
                        $count=1;
                        foreach ($feedback_question_data as $key => $feedback_question_row) { ?>
                            <div class="row"></div>
                            <input type="hidden" name="question_ids[<?=$key?>]" value="<?=$feedback_question_row->id?>">
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <h6><b><?=$count++?>. <?=$feedback_question_row->title;?></b></h6>
                                </div>
                                <?php
                                if($feedback_question_row->is_objective == 1) {
                                    ?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name='answer[<?=$key?>]' value="1" required="required"> &nbsp;<?php echo $feedback_question_row->option1; ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name='answer[<?=$key?>]' value="2" required="required"> &nbsp;<?php echo $feedback_question_row->option2; ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name='answer[<?=$key?>]' value="3" required="required"> &nbsp;<?php echo $feedback_question_row->option3; ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name='answer[<?=$key?>]' value="4" required="required"> &nbsp;<?php echo $feedback_question_row->option4; ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                } else { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="answer[<?=$key?>]" required="required"></textarea>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                }
                        } ?>
                        <button type="submit" class="btn theme_button submit_btn_add">Submit</button>&nbsp;&nbsp;
                        <button type="button" class="btn btn-danger" onclick="closeFeedbackModal()">Cancel</button>
                        <br/><br/>
                        <?php
                    } ?>
                </form><?php
            } else { ?>
                <div class="col-md-12">
                    <div id='feedback_success_msg' class="text-center">
                        <h6>No feedback available at this movement!</h6>
                    </div>
                </div>
                <script type='text/javascript'>
                    closeFeedbackModal_Success();
                </script><?php
            }
        } else { ?>
            <div class="col-md-12">
                <div id='feedback_success_msg' class="text-center">
                    <h6>You have already give a Feedback!</h6>
                </div>
            </div>
            <script type='text/javascript'>
                closeFeedbackModal_Success();
            </script><?php
        }
        ?>
    </div>
</div>
