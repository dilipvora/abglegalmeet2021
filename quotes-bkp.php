<?php include 'header1.php';

$uid = $_SESSION['uid'];
$event_id = EVENT_ID;
$now_time = date('Y-m-d H:i:s');
$workExperienceForm = true;

if (isset($_POST['submit_btn_add'])) {
    if (!empty($_POST['f1']) && !empty($_POST['f2']) && !empty($_POST['f3'])) {
        insert_details("extra_form", "`uid` = '{$uid}', `event_id` = '{$event_id}', `f1` = '{$_POST['f1']}', `f2` = '{$_POST['f2']}', `f3` = '{$_POST['f3']}', `role` = 'quotes'");
        $_SESSION['message'] = array('status' => 'success', 'msg' => 'Your quote has been successfully submitted');
    } else {
        $_SESSION['message'] = array('status' => 'danger', 'msg' => 'Please enter your quote.');
    }
    header("location:" . SITE_URL . "quotes.php");
    exit();
} ?>

<style type="text/css">
    html, body {
        height: 100%;
    }
    body {
        background-image: url("img/video-bg.jpg"), linear-gradient(to right, #c6a24e, #cba452, #d1a557, #d6a75b, #dba960);
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        background-position: center;
        background-color: #e2bc83;
    }
    .form-control {
        text-align: center;
        background: #901412;
        font-size: 1.15rem;
        letter-spacing: 1.25px;
        color: white;
        border: none;
        border-bottom: 3px solid #c5491f;
        border-radius: 1rem;
    }
    .form-control:focus {
        background: #680502;
        color: white;
    }
    .textarea-div {
        background: #a32421;
        border-radius: 1rem;
    }
    /*.label {
        text-align: center;
        font-weight: bold;
        font-size: 1.25rem;
        background: #a32421;
        color: white;
        margin-bottom: 0;
        padding: .75rem;
        border-top-right-radius: 1rem;
        border-top-left-radius: 1rem;
    }*/
    .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show > .btn-primary.dropdown-toggle,
    .theme_button {
        font-size: 1.25rem;
        line-height: 1.6;
        letter-spacing: 1.5px;
        background: #ea5725;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #680502;
        border-radius: 8px;
    }
    .theme_button:hover, .theme_button:focus, .theme_button:active {
        background: #6e090c;
        color: #ffffff;
        font-weight: bolder;
        border: none;
        border-bottom: 3px solid #680502;
    }
    .outer {
        display: table;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }
    .middle {
        display: table-cell;
        vertical-align: middle;
    }
    .inner {
        margin: auto;
        width: auto;
        /*whatever width you want*/
    }
    @media screen and (max-width: 767px) {
        body {
            background-image: url("img/video-mobile-bg.jpg");
        }
        .outer {
            position: unset;
        }
    }

    ::-webkit-scrollbar-thumb {
        background-color: #939194;
        border: 2px solid transparent;
        border-radius: 5px;
        background-clip: padding-box;
    }

    ::-webkit-scrollbar {
        width: .65rem;
    }
    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #ffffff !important;
        font-weight: 500;
    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: #ffffff !important;
        font-weight: 500;
    }
    :-ms-input-placeholder { /* IE 10+ */
        color: #ffffff !important;
        font-weight: 500;
    }
    :-moz-placeholder { /* Firefox 18+ */
        color: #ffffff !important;
        font-weight: 500;
    }
</style>

<div class="container-fluid h-100">
    <div class="row h-100">
        <div class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7 mx-auto">
            <?php if (file_exists('img/homeslide.png')) { ?>
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="text-center">
                            <img src="img/homeslide.png" class="img-fluid w-50"/>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 mx-auto">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <form method="post" autocomplete="on">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2">
                                    <div class="text-center">
                                        <img src="img/quotes-banner.png" class="img-fluid w-75" alt="Quotes Img" />
                                    </div>
                                </div>
                                <?php if (!empty($_SESSION['message'])) { ?>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="alert alert-<?=$_SESSION['message']['status']?>">
                                        <strong><?=$_SESSION['message']['msg']?></strong>
                                    </div>
                                </div>
                                <?php unset($_SESSION['message']); } ?>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group textarea-div">
                                        <!--<label class="d-block label" for="f1">Name</label>-->
                                        <input type="text" name="f1" id="f1" placeholder="Name" class="form-control" required />
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group textarea-div">
                                        <!--<label class="d-block label" for="f2">Email Id</label>-->
                                        <input type="text" name="f2" id="f2" placeholder="Email Id" class="form-control" required />
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="form-group textarea-div">
                                        <!--<label class="d-block label" for="f3">Type in your quote bellow</label>-->
                                        <textarea class="form-control" rows="3" placeholder="Type in your quote here" name="f3" id="f3" required></textarea>
                                    </div>
                                </div>

                                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">

                                            <input type="submit" name="submit_btn_add" class="btn-block btn btn-primary theme_button" value="Submit"/>

                                    </div>
                                </div>
                                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">

                                            <input type="button" class="btn btn-primary btn-block theme_button" value="Back" onclick="window.location.href = '<?=SITE_URL?>presentation.php'"/>

                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include_once "footer.php"; ?>
</body>
</html>